A pyhton ddisasm datalog rules wrapper
---------------------------


First of all it is **not** a ddisasm tool wrapper, it is just a wraper for reusing rule inside ddisasm. This little program will contain:

NOTE: please compile my forked version of ddisasm, every rule is exposed in there

- make all rule in ddisasm has output

- generate a datalog "interface" for ddisasm so new rule can take everything from a datalog folder as input

- a grpc server can work with different client(using python + grpc + asyncio)

- allow user upload datalog rule and run datalog rules

- query facts and generated output

- upload facts 

- every time a new rule imported reuse result of previous rule

- edit loaded datalog file

- query generated asm


**Problem**

- fix no argument `run` command

- need to handle option part for #ifdef like things

- handle inline rules

- generate GTIRB file

- allow add single datalog rule line

- add relation template decsribption 

**Datalog Plan**

-  extend current def-use analysis into global variable too

-  detect possible uninitialized global varible: this can be a unsound guese, if some global variable is uninitlized but used in a function

-  some other uninitialized case ....(still thinking)

-  detect uninitialized buffer by taint input (my plan is find input by search `getc` `scanf` function ...)


**Example**

- some of my hand written C++

- CGC bench from GrammaTech

- souffle itself



some running time:
|                  | ddisasm |       stack_var  |     object_ptr  |    static_var  |    uninitial_static  |
|------------------|---------|------------------|-----------------|----------------|----------------------|
|souffle (cache):  |  170s   |       11.881176  |     58.349096   |    05.008129   |    00.038896         |
|souffle (cache):  |  170s   |       11.881176  |     66.017223   |    66.998470   |    66.527745         |
|bison (cache):    |    7s   |       00.931909  |     01.409485   |    00.544789   |    00.021761         |
|bison             |    7s   |       00.934186  |     01.916311   |    02.121602   |    02.074632         |
|gzip (cache):     | 865ms   |       00.203971  |     00.168545   |    00.109176   |    00.020399         |
|gzip              | 865ms   |       00.195801  |     00.331583   |    00.351929   |    00.361698         |
|re2c (cache):     |    9s   |       01.456632  |     04.417014   |    00.704410   |    00.024695         |
|re2c              |    9s   |       01.493517  |     05.257008   |    05.449103   |    05.458423         |
|redis (cache):    |   11s   |       01.917906  |     02.543736   |    01.301989   |    00.024867         |
|redis             |   11s   |       01.918271  |     03.525127   |    03.711665   |    03.726320         |
|rsync (cache):    |    8s   |       00.766078  |     00.907768   |    00.481009   |    00.027987         |
|rsync (cache):    |    8s   |       00.782772  |     01.324923   |    01.432142   |    01.383521         |
