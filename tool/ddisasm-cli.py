# a CLI tool can run/query a datalog(souffle) rule interactively
#  this is blocking now but my server is in async.... so have to make it async

# Yihao Sun
# Syracuse 2020

import os
import re
import hashlib
import random
import string
import shutil
import subprocess
import asyncio
from optparse import OptionParser
import grpc
import datetime
import binascii

from prompt_toolkit import PromptSession
from prompt_toolkit.patch_stdout import patch_stdout
from prompt_toolkit.validation import Validator, ValidationError
from prompt_toolkit.completion import Completer, Completion, PathCompleter, WordCompleter, merge_completers
from prompt_toolkit.document import Document

from datalog_as_lib import DatalogLib
from DisasmDatalog import DisasmDatalogManager
from ddisasm_pb2_grpc import CommandServiceStub
from ddisasm_pb2 import PutHashesRequest, BinaryChunk, RunHashesRequest, ErrorResponse, ASMRequest
from ddisasm_pb2 import DatabaseRequest, RelationRequest, DisconnectRequest, UpdateHashRequest, FactsChunk
from ddisasm_pb2 import ASMChunk

import ghidra_bridge

BANNER = '''
                                _____ 3_____ _____    
                                ||  )  ||_// ||==     
                                ||_//  || \\\\ ||___    
                                
    This a datalog query tool, please enter 'help' or '?' to see detail, happy hacking with 
    ddisasm & souffle!
'''

PROMPT = "\U0001f95e > "

COMMANDS = {
    'help': 'show help info',
    'load': 'load [file_path ...] : load datalog files, you can directly use rule from ddisasm in your fil',
    'files': 'show loaded files',
    'run': 'run : run the loaded filed',
    'query': 'query [rule_name] : output the data of a rule, plz mind, your rule must be specified to ".output"',
    'datalog': 'datalog [a line of souffle datalog code] : send **one line** of datalog code',
    'rules': 'list all rules can be output',
    'facts': 'facts [file_path] : send a facts file to server',
    'factsdir': 'factsdir [dir_path] : send all .facts file under a dir to server',
    'edit': 'edit [filename] : edit a loaded file',
    'asm': 'check the decompiled asm code',
    'printGhidra': 'print some relation into ghidra console(this only work in ghidra mode)',
    'highlight': 'highlight all address in add_highlight relation in ghidra(only work in ghidra mode)',
    'comment': 'comment a address related relation on some address in ghidra(only work in ghidra mode)',
    'hex2dec': 'hex number to decimal',
    'dec2hex': 'deccimal to hex number',
    ':q': 'alias for query',
    ':': 'alias for datalog',
    ':l': 'alias for load',
    '?': 'alias for help',
    ':r': 'alias for run',
    ':e': 'alias for edit',
    'quit': 'quit program'
}

GHIDRA_MODE_COMMAND = ['printGhidra', 'highlight', 'comment']

TMP_DIR = '.dd'


def hexify_fact_line(fline):
    '''
    change all int in a fact line into a hex
    '''
    def is_num(s):
        ts = s.replace('-', '0')
        return ts.isdigit()
    fldata = fline.strip().split('\t')
    new_data = []
    for d in fldata:
        if is_num(d):
            new_data.append('{0:08x}'.format(int(d)))
        else:
            new_data.append(d)
    return '\t'.join(new_data) + '\n'


def is_valid_dl(line):
    '''
    check if a **LINE** is a valid datalog rule, this is very inprecies, not realy
    parsing is applied here, will move so something more precise in future
    '''
    line = line.strip()
    if line.startswith('.decl') and line.endswith(')'):
        return True
    elif line.startswith('.input') or line.startswith('.output'):
        return True
    elif line.startswith('.type'):
        return True
    elif line.endswith('.') and (line.find(':-') != -1):
        return True
    else:
        return False


def get_rule_name(rule_decl):
    rule_name = re.findall(r"\.decl(.+)\(", rule_decl)[0]
    return rule_name.strip()


def get_comp_name(comp_decl):
    name = re.findall(r"\.comp(.+) *\{", comp_decl)[0]
    return name.strip()


def split_command(command_full):
    '''
    split a command line into command and argument
    '''
    command = command_full.split(' ')[0].strip()
    arg_txt = command_full[len(command):].strip()
    return command, arg_txt


def seperate_args(raw_args):
    '''
    seperaate a raw args by <SPACE>
    '''
    rargs = raw_args.split(' ')
    args = []
    for ra in rargs:
        if ra.strip() != '':
            args.append(ra.strip())
    return args


class GhidraFacts:
    def __init__(self, current_address=None, current_highlight=None):
        self.current_address = current_address
        self.current_highlight = current_address

    def __str__(self):
        fact_str = '\n'
        if self.current_highlight is not None:
            fact_str = fact_str + \
                'current_address({}).'.format(self.current_address)
            fact_str = fact_str + '\n'
        if self.current_highlight is not None:
            fact_str = fact_str + \
                'current_highlight({}).'.format(self.current_highlight)
            fact_str = fact_str + '\n'
        return fact_str


class CommandValidator(Validator):
    def validate(self, document):
        text = document.text
        command, arg_txt = split_command(text)
        if command not in COMMANDS.keys():
            raise ValidationError(
                message='{} is not a valid command'.format(command))
        if (command == ':' or command == 'datalog') and is_valid_dl(arg_txt):
            raise ValidationError(
                message='{} is not a valid datalog line'.format(arg_txt))


class PageValidator(Validator):
    def validate(self, document):
        text = document.text
        page_command = ['next', 'prev', 'exit']
        if text.strip() not in page_command:
            raise ValidationError(
                message='{} is not a valid command'.format(text))


class CommandExecutor:
    def __init__(self, stub: CommandServiceStub, session_key, ghidra_flag=False):
        self.stub = stub
        self.session_key = session_key
        self.loaded_file = {}
        self.ghidra_flag = ghidra_flag

    async def run_command(self, command, args):
        # move some checking logic into validator
        if command == 'load' or command == ':l':
            file_paths = seperate_args(args)
            check_flag = True
            for fp in file_paths:
                if not os.path.exists(fp):
                    check_flag = False
                    print('file {} not exists'.format(fp))
            if check_flag:
                await self.__send_load(file_paths)
        if command == 'help' or command == '?':
            self.__print_help(args)
        if command == 'files':
            self.__print_loaded_files()
        if command == 'run' or command == ':r':
            names = seperate_args(args)
            for n in names:
                if n not in self.loaded_file.keys():
                    print('file {} not loaded'.format(n))
                    return True
            hashes = list(map(lambda n: self.loaded_file[n], names))
            await self.__send_run(hashes)
        if command == 'rules':
            await self.__get_all_rules()
        if command == 'query' or command == ':q':
            arg_list = seperate_args(args)
            if arg_list == []:
                print('plz input the name of rule!')
            else:
                name = arg_list[0]
                await self.__query(name)
        if command == 'edit' or command == ':e':
            if len(seperate_args(args)) == 0:
                print('plz enter the file you want to edit')
                return False
            file_path = seperate_args(args)[0]
            if file_path in self.loaded_file.keys():
                await self.__edit(self.loaded_file[file_path], file_path)
            else:
                print('file is not loaded!')
        if command == 'facts':
            file_paths = seperate_args(args)
            for fpath in file_paths:
                if os.path.exists(fpath):
                    await self.__send_facts(fpath)
                else:
                    print('facts file {} not exist'.format(fpath))
        if command == 'factsdir':
            if args != '':
                dir_path = seperate_args(args)[0]
                if os.path.exists(dir_path):
                    for fpath in os.listdir(dir_path):
                        if fpath.endswith('.facts'):
                            await self.__send_facts(fpath)
                else:
                    print('facts dir {} not exists'.format(dir_path))
            else:
                print('plz input a file dir')
        if command == 'hex2dec':
            num = seperate_args(args)[0]
            print(int(num, 0))
        if command == 'dec2hex':
            num = seperate_args(args)[0]
            print(hex(num))
        if command == 'asm':
            await self.__asm()
        if command == 'quit':
            await self.__quit()
            return False
        if command == 'highlight':
            if self.ghidra_flag:
                await self.__hightlight_ghidra()
            else:
                print('not in ghidra mode!')
        if command == 'comment':
            if self.ghidra_flag:
                await self.__comment_ghidra()
        return True

    async def __hightlight_ghidra(self):
        request = RelationRequest(
            session_key=self.session_key, database_id='add_highlight', tag=0)
        facts = []
        async for tuple_response in self.stub.GetTuples(request):
            facts = facts + list(map(hexify_fact_line, tuple_response.data))
        with ghidra_bridge.GhidraBridge(namespace=globals()):
            factory = currentProgram.getAddressFactory()
            addr_set = ghidra.program.model.address.AddressSet()
            for f in facts:
                addr_set.add(factory.getAddress(f.strip()))
            createHighlight(addr_set)

    async def __comment_ghidra(self):
        comment_request = RelationRequest(
            session_key=self.session_key, database_id='add_comment', tag=0)
        comment_facts = []
        async for tuple_response in self.stub.GetTuples(comment_request):
            comment_facts = comment_facts + list(tuple_response.data)
        for cf in comment_facts:
            cf_rule = cf.split('\t')[0].strip()
            cf_col = int(cf.split('\t')[1].strip())
            q_request = RelationRequest(
                session_key=self.session_key, database_id=cf_rule, tag=0)
            rule_facts = {}
            async for tuple_response in self.stub.GetTuples(q_request):
                for d in tuple_response.data:
                    hex_d = hexify_fact_line(d)
                    addr_str = hex_d.split('\t')[cf_col].strip()
                    rule_facts[addr_str] = hex_d
            with ghidra_bridge.GhidraBridge(namespace=globals(), interactive_mode=True):
                start()
                factory = currentProgram.getAddressFactory()
                for k, v in rule_facts.items():
                    setPreComment(factory.getAddress(
                        k), "[d3re] : {} >>>\n  {}".format(cf_rule, v))
                end(True)

    async def __send_load(self, files):
        file_datas = []
        file_hashes = []
        for fpath in files:
            with open(fpath, 'r') as f:
                fdata = f.read()
                file_datas.append(fdata)
                h = hashlib.sha256()
                h.update(fdata.encode('utf-8'))
                hsh = h.hexdigest()
                file_hashes.append(hsh)
        request = PutHashesRequest()
        request.session_key = self.session_key
        request.bodies.extend(file_datas)
        response = await self.stub.PutHashes(request)
        if not response.success:
            print(response.message)
        else:
            for i in range(len(file_datas)):
                self.loaded_file[files[i]] = file_hashes[i]

    async def __send_facts(self, path):
        filename = path.split('/')[-1]

        async def facts_chunk_generator():
            with open(path, 'r') as f:
                lines = f.read()
                for i in range(0, len(lines), 10):
                    chunk = FactsChunk()
                    chunk.session_key = self.session_key
                    chunk.name = filename
                    chunk.lines.extend(lines[i:i+10])
                    yield chunk
        await self.stub.PutFacts(facts_chunk_generator())

    async def __send_run(self, hashes):
        request = RunHashesRequest()
        request.session_key = self.session_key
        request.hashes.extend(hashes)
        response = await self.stub.RunHashes(request)
        if not response.success:
            print(response.error_msg)

    def __print_help(self, arg):
        if arg.strip() == '':
            for k, v in COMMANDS.items():
                print('{:<10} : {}'.format(k, v))
        else:
            real_arg = list(filter(lambda a: a.strip()
                                   != '', arg.split(' ')))[0]
            if real_arg in COMMANDS.keys():
                print('{:<10} {}'.format(real_arg, COMMANDS[real_arg]))
            else:
                print('command : {} not exists plz see `help` \n'.format(real_arg))

    def __print_loaded_files(self):
        for n in self.loaded_file.keys():
            print(n)

    async def __get_all_rules(self):
        request = DatabaseRequest(session_key=self.session_key, database_id='')
        response = await self.stub.GetRelations(request)
        if response.success:
            rel_descs = response.relations
            for i in range(0, len(rel_descs), 2):
                pos1 = rel_descs[i].name
                pos2 = rel_descs[i+1].name if i+1 < len(rel_descs) else ''
                print('{:>40} {:>40}'.format(pos1, pos2))
        else:
            print('query rule failed!')

    async def __query(self, rule_name):
        request = RelationRequest(
            session_key=self.session_key, database_id=rule_name, tag=0)
        facts = []
        async for tuple_response in self.stub.GetTuples(request):
            facts = facts + list(map(hexify_fact_line, tuple_response.data))
        if self.ghidra_flag:
            session = PromptSession()
            cur = 0
            with ghidra_bridge.GhidraBridge(namespace=globals()):
                println('in page mode')
                while cur < len(facts):
                    println('\n'+''.join(facts[cur:cur+50]))
                    println('next -- prev -- exit')
                    print(''.join(facts[cur:cur+50]))
                    pcmd = await session.prompt_async('>>> ', validator=PageValidator())
                    if pcmd.strip() == 'next':
                        cur = cur + 50
                    elif pcmd.strip() == 'prev' and (cur - 50 > 0):
                        cur = cur - 50
                    elif pcmd.strip() == 'exit':
                        println('exit page mode')
                        break
        else:
            print(''.join(facts), end='')

    async def __quit(self):
        request = DisconnectRequest(session_key=self.session_key)
        await self.stub.Disconnect(request)

    async def __edit(self, _old_hash, file_path):
        with open(file_path, 'r') as f:
            old_data = f.read()
        full_path = os.path.abspath(file_path)
        os.system('emacs {}'.format(full_path))
        with open(file_path, 'r') as f:
            new_data = f.read()
        if old_data != new_data:
            request = UpdateHashRequest(
                session_key=self.session_key, old_hash=_old_hash, new_body=new_data)
            await self.stub.UpdateHash(request)
            h = hashlib.sha256()
            h.update(new_data.encode('utf-8'))
            hsh = h.hexdigest()
            self.loaded_file[file_path] = hsh

    async def __asm(self):
        request = ASMRequest(session_key=self.session_key)
        buffer = []
        async for res_chunk in self.stub.GetASM(request):
            buffer.append(res_chunk)
        buffer.sort(key=lambda r: r.seq)
        fpath = '{}/d.asm'.format(TMP_DIR)
        with open(fpath, 'w+') as f:
            for r in buffer:
                f.write(''.join(r.lines))
        os.system('emacs {}'.format(fpath))


class CmdPathCompleter(Completer):
    def __init__(self):
        self.path_compl = PathCompleter()

    def get_completions(self, document, complete_event):
        if document.text.startswith('load ') or document.text.startswith('edit '):
            path_doc = Document(document.text[5:])
            yield from (
                Completion(complete.text, complete.start_position, display=complete.display) 
                for complete 
                in self.path_compl.get_completions(path_doc, complete_event))
        elif document.text.startswith('run '):
            path_doc = Document(document.text[4:])
            yield from (
                Completion(complete.text, complete.start_position, display=complete.display) 
                for complete 
                in self.path_compl.get_completions(path_doc, complete_event))
        

async def cli_run(bin_path, ghidra_flag=False):
    bin_name = bin_path.split('/')[-1]
    cmd_compl = WordCompleter(COMMANDS.keys(), True)
    path_compl = CmdPathCompleter()

    async def chunk_generator():
        with open(bin_path, 'rb') as f:
            fdata = f.read()
            step = 4096
            for i in range(0, len(fdata), step):
                yield BinaryChunk(name=bin_name, seq=i, content=fdata[i:i+step])

    session = PromptSession(completer=merge_completers([path_compl, cmd_compl]))
    # now every connect should all be started by send a binary
    # and in message return, session key will be in message body
    async with grpc.aio.insecure_channel('localhost:50051') as channel:
        stub = CommandServiceStub(channel)
        put_bin_res = await stub.PutBinary(chunk_generator())
        if not put_bin_res.success:
            print(put_bin_res.error_msg)
            exit(0)
        session_key = put_bin_res.error_msg
        exector = CommandExecutor(stub, session_key, ghidra_flag)
        while True:
            with patch_stdout():
                cmd_raw = await session.prompt_async(PROMPT, validator=CommandValidator())
                command, args = split_command(cmd_raw)
                before_time = datetime.datetime.now()
                res = await exector.run_command(command, args)
                after_time = datetime.datetime.now()
                # print("command finish in {} s".format(after_time - before_time))
                if res == False:
                    break


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-B", "--binary", dest="bin_path",
                      help="path of binary file")
    parser.add_option("-G", "--ghidra", dest="ghidra",
                      help="run in ghodra mode")
    (options, args) = parser.parse_args()
    if options.ghidra is not None:
        ghidra_flag = True
    else:
        ghidra_flag = False
    print(BANNER)
    if ghidra_flag:
        with ghidra_bridge.GhidraBridge(namespace=globals()) as b:
            println(BANNER)
    if os.path.exists(TMP_DIR):
        shutil.rmtree(TMP_DIR)
    os.mkdir(TMP_DIR)
    binary_name = options.bin_path.split('/')[-1]
    asyncio.get_event_loop().run_until_complete(
        cli_run(options.bin_path, ghidra_flag))
    print('(TvT)/ bye~~')
