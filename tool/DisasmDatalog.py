
import os
import re
import random
import string
import shutil
import subprocess
from datalog_as_lib import *

DDISASM_COMMAND = '../ddisasm/build/bin/ddisasm'
SOUFFLE_COMMAND = 'souffle'
ASM_NAME = 'd.asm'
DDISASM_INCLUDE = 'ddisasm_include.dl'
CACHE_INCLUDE_FILE = 'include.dl'
INCLUDE_DIR = 'include'
FACTS_DIR = 'facts'
OUT_DIR = 'outs'
DL_DIR = "dl"
CACHE_DIR = "cache"
EMPTY_FILE = '_empty.dl'


def random_str():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))


class DisasmDatalogManager:
    '''
    make sure run ddisasm and setup tmp dir after create manager for a empty tmp dir
    '''

    def __init__(self, BASE_DIR, include_path, bin_path, inited=False):
        self.session_id = random_str()
        self.bin_path = bin_path
        self.BASE_DIR = BASE_DIR
        self.cache_dir = 'cache/{}'.format(self.session_id)
        self.out_dir = 'outs/{}'.format(self.session_id)
        self.is_binary_loaded = False
        if not inited:
            self.prepare_tmp()
            self.prepare_session()
            # self.run_ddisasm(bin_path)
        else:
            self.prepare_session()
        shutil.copyfile(include_path, self.get_ddisasm_include_path())
        self.imported_rules = []
        self.include_files = []
        self.enable_profile = False
        self.mode = 0  # souffle compiler model 0 for interpretor, 1 for compile
        self.cores = -1  # -1 for using all core
        self.cachefile = self.session_id + '.dl'
        self.ddisasmLib = DatalogLib('ddisasm')
        self.ddisasmLib.add_file('{}/{}'.format(BASE_DIR, DDISASM_INCLUDE))
        self.create_cache()
        self.cached_relation = []
        self.profile = False
        self.activate = True

    def create_cache(self):
        cache_file_path = '{}/{}/{}'.format(self.BASE_DIR,
                                            self.cache_dir, self.cachefile)
        cache_include_path = '{}/{}/{}'.format(
            self.BASE_DIR, self.cache_dir, CACHE_INCLUDE_FILE)
        with open(cache_include_path, 'w+') as f:
            f.write('// include file for cache\n')
            # import all type init from ddisasm to here
            for i in self.ddisasmLib.inits:
                f.write(i)
            for i in self.ddisasmLib.type_decls:
                f.write(i)
            for c in self.ddisasmLib.comp_decls.keys():
                f.write('.comp {} {{ \n \n }}\n'.format(c))
            for i in self.include_files:
                f.write('#include "../{}/{}.dl"\n'.format(INCLUDE_DIR, i.name))
        if os.path.exists(cache_file_path):
            os.remove(cache_file_path)
        if os.path.exists(cache_file_path):
            os.remove(cache_include_path)
        with open(cache_file_path, 'w+') as f:
            f.write('#include "./{}"\n\n'.format(CACHE_INCLUDE_FILE))

    def run_souffle_file(self, fname):
        '''
        execute file loaded by name, file not exists return False
        '''
        file_full_path = os.path.abspath(
            '{}/{}.dl'.format(self.get_loaded_file_dir(), fname))
        # prepare running file
        run_file_path = '{}/{}/{}_run.dl'.format(self.BASE_DIR, self.cache_dir, fname)
        buf = ''
        orules = []
        # TODO: check if file already run before
        for include in self.include_files:
            if include.name == fname:
                orules = include.outside_rule_names()
        ddisasm_rules = list(filter(lambda n: self.ddisasmLib.is_rule_exists(n), orules))
        buf = buf + self.ddisasmLib.generate_inlcude(all_out=True, select_outs=ddisasm_rules, to_disk=False)
        for include in self.include_files:
            if include.name != fname:
                irules = list(filter(lambda n: include.is_rule_exists(n), orules))
                buf = buf + include.generate_inlcude(select_outs=irules, to_disk=False)
        buf = buf + '\n#include "{}" \n'.format(file_full_path)
        with open(run_file_path, 'w+') as f:
            f.write(buf)
        res = self.__run_souffle(fname, run_file_path)
        # post execution
        if not res:
            print('souffle compiling error')
            self.cleancache()
            return res
        for include in self.include_files:
            if include.name == fname:
                print('generating new include for {}'.format(fname))
                include.generate_inlcude()
        new_cache_file_lines = []
        # update cache
        cache_include_path = '{}/{}/{}'.format(self.BASE_DIR, self.cache_dir, CACHE_INCLUDE_FILE)
        with open(cache_include_path, 'r') as f:
            for line in f:
                # loaded file but haven't been rewrited
                if line.find('#include') != -1 and line.find('_include.dl') == -1 and line.find(fname) != -1:
                    new_line = '#include "../../include/{}_include.dl"\n'.format(fname)
                    new_cache_file_lines.append(new_line)
                else:
                    new_cache_file_lines.append(line)
        with open(cache_include_path, 'w+') as f:
            f.write(''.join(new_cache_file_lines))
        return res

    def __run_souffle(self, fname, fpath):
        f_full_path = os.path.abspath(fpath)
        fact_full_path = os.path.abspath(self.BASE_DIR+'/'+FACTS_DIR)
        out_full_path = os.path.abspath(self.BASE_DIR+'/'+self.out_dir)
        _exec = os.path.abspath(
            '{}/{}/{}'.format(self.BASE_DIR, self.cache_dir, fname.split('.')[0]))
        _cpp = os.path.abspath(
            '{}/{}/{}.cpp'.format(self.BASE_DIR, self.cache_dir, fname.split('.')[0]))
        profile_flag = '-p {}/p.log'.format(self.BASE_DIR) if self.profile else ''
        if os.path.exists(os.path.abspath(fpath)):
            if self.cores != -1:
                core_str = '-j{}'.format(str(self.cores))
            else:
                core_str = ''
            if self.mode == 1:
                print('in compiler mode....')
                # clean before running
                if os.path.exists(_exec):
                    os.remove(_exec)
                if os.path.exists(_cpp):
                    os.remove(_cpp)
                command = '{} {} -F {} -D {} {} -o {} {}'.format(
                    SOUFFLE_COMMAND, profile_flag,
                    fact_full_path, out_full_path, core_str, _exec, f_full_path)
                try:
                    res = subprocess.check_output(command, shell=True)
                    print(res)
                except:
                    return False
                subprocess.check_output(_exec, shell=True)
            else:
                command = '{} {} -F {} -D {} {} {}'.format(
                    SOUFFLE_COMMAND, profile_flag,
                    fact_full_path, out_full_path, core_str, f_full_path)
                try:
                    res = subprocess.check_output(command, shell=True)
                    print(res)
                except:
                    return False
            self.rename_out_to_facts()
            return True
        else:
            print("file not exists")
            return False

    def run_souffle_cache(self):
        '''
        execute cached file, if fail clean cache
        '''
        cache_file_path = os.path.abspath(
            self.BASE_DIR+'/'+self.cache_dir+'/'+self.cachefile)
        res = self.__run_souffle(self.cachefile, cache_file_path)
        # post execution
        if not res:
            print('souffle compiling error')
            self.cleancache()
            return res
        # if success, reload all include file, switch all output into input
        new_cache_file_lines = []
        cache_include_path = '{}/{}/{}'.format(self.BASE_DIR, self.cache_dir, CACHE_INCLUDE_FILE)
        with open(cache_include_path, 'r') as f:
            for line in f:
                # print(line)
                # loaded file but haven't been rewrited
                if line.find('#include') != -1 and line.find('_include.dl') == -1:
                    hname = re.findall(r'#include ".+/(.+)\.dl"', line)[0]
                    for include in self.include_files:
                        if include.name == hname:
                            print('generating new include for {}'.format(hname))
                            include.generate_inlcude()
                    new_line = '#include "../../include/{}_include.dl"\n'.format(hname)
                    new_cache_file_lines.append(new_line)
                else:
                    new_cache_file_lines.append(line)
        with open(cache_include_path, 'w+') as f:
            f.write(''.join(new_cache_file_lines))
        return res

    def cleancache(self):
        '''
        clean query cache
        '''
        self.create_cache()
        self.imported_rules = []
        self.include_files = []
        out_full_path = self.BASE_DIR+'/'+self.out_dir
        if os.path.exists(out_full_path):
            shutil.rmtree(out_full_path)
            os.makedirs(out_full_path)

    def load_one_file(self, fname, fcontent):
        new_full_path = "{}/{}/{}.dl".format(self.BASE_DIR, INCLUDE_DIR, fname)
        with open(new_full_path, 'w+') as f:
            f.write(fcontent)
        with open(new_full_path, 'r') as f:
            fdata = f.read()
        all_rule_name = re.findall(r'([a-zA-Z_]+)\(.+\)', fdata)
        loaded_file_names = map(lambda f: f.name, self.include_files)
        if fname not in loaded_file_names:
            inlcude_new_path = '{}/{}/{}_include.dl'.format(self.BASE_DIR, INCLUDE_DIR, fname)
            new_dl_lib = DatalogLib(fname, inlcude_new_path)
            new_dl_lib.add_file(new_full_path)
            self.include_files.append(new_dl_lib)
            # update include header for cache
            cache_inlcude = '{}/{}/{}'.format(self.BASE_DIR,
                                              self.cache_dir, CACHE_INCLUDE_FILE)
            for rname in all_rule_name:
                decl, comp = self.ddisasmLib.find_decl_by_name(rname)
                self.__import_ddisasm_cache(decl, comp, cache_inlcude)
            with open(cache_inlcude, 'a+') as f:
                f.write('#include "../../{}/{}.dl"\n'.format(INCLUDE_DIR, fname))

    def remove_one_file(self, fname):
        # this will reload all inlcude file except removed
        cache_inlcude = '{}/{}/{}'.format(self.BASE_DIR, self.cache_dir, CACHE_INCLUDE_FILE)
        os.remove(cache_inlcude)
        self.imported_rules = []
        self.create_cache()
        loaded_file_names = map(lambda f: f.name, self.include_files)
        if fname in loaded_file_names:
            new_inlcudes = []
            for i in self.include_files:
                if i.name != fname:
                    new_inlcudes.append(i.name)
            self.include_files = []
            for i in new_inlcudes:
                fpath = "{}/{}/{}.dl".format(self.BASE_DIR, INCLUDE_DIR, i)
                with open(fpath, 'r') as f:
                    fdata = f.read()
                self.load_one_file(i, fdata)
            return True
        else:
            print('file not loaded')
            return False  

    def load_dl_files(self, name_file_dict):
        for fname, fcontent in name_file_dict.items():
            self.load_one_file(fname, fcontent)
        self.run_souffle_cache()

    def load_facts(self, fact_path):
        if os.path.exists(fact_path):
            for e in os.listdir(fact_path):
                if e.endswith('.facts'):
                    shutil.copyfile(fact_path+'/'+e, self.BASE_DIR +
                                    '/' + FACTS_DIR + '/' + e)

    def __import_ddisasm_cache(self, rule, comp, file_path):
        '''
        import a rule from ddisasm into a file 
        '''
        # file_path = '{}/{}/{}'.format(self.BASE_DIR,
        #                                   self.cache_dir, CACHE_INCLUDE_FILE)
        if rule is None:
            return
        rule_name = get_rule_name(rule)
        if rule_name in self.imported_rules:
            return
        self.imported_rules.append(rule_name)
        if comp is not None:
            rule_name = get_rule_name(rule)
            new_file_lines = []
            with open(file_path, 'r+') as f:
                for line in f:
                    new_file_lines.append(line)
                    if (line.find('.comp') != -1) and (get_comp_name(line) == comp):
                        new_file_lines.append(rule)
                        new_file_lines.append('.input {}\n'.format(rule_name))
            with open(file_path, 'w+') as f:
                f.seek(0)
                f.write(''.join(new_file_lines))
        else:
            with open(file_path, 'a+') as f:
                f.write(rule)
                f.write('.input {}\n'.format(rule_name))

    def add_dl_line(self, rule_line):
        '''
        add a line to current datalog cache and then compile
        '''
        if rule_line.startswith('.output'):
            # there is already a output?
            rule_name = rule_line[7:].strip().split('.')[-1]
            # check if rule need to be import from ddisasm
            # decl, comp = self.ddisasmLib.find_decl_by_name(rule_name)
            # self.import_ddisasm_cache(decl, comp)
            outs_files = os.listdir(self.BASE_DIR+'/'+self.out_dir)
            facts_files = os.listdir(self.BASE_DIR+'/'+FACTS_DIR)
            if (not ((rule_name+'.csv') in outs_files)) and (not ((rule_name+'.facts') in facts_files)):
                with open(self.BASE_DIR+'/'+self.cache_dir+'/'+self.cachefile, 'r+') as f:
                    for r in self.cached_relation:
                        if rule_name == r.name:
                            f.write('\n'+rule_line+'\n')
                            r.is_out = True
                self.run_souffle_cache()
        else:
            if rule_line.startswith('.decl'):
                self.cached_relation.append(parse_rule_decl(rule_line))
            with open(self.BASE_DIR+'/'+self.cache_dir+'/'+self.cachefile, 'a+') as f:
                f.write(rule_line+'\n')
        return True

    def query_relation(self, rel_name):
        '''
        query the output of a relation
        '''
        # check output dir
        data = []
        out_dir = self.get_output_dir()
        if os.path.exists('{}/{}.csv'.format(out_dir, rel_name)):
            with open('{}/{}.csv'.format(out_dir, rel_name), 'r') as f:
                for line in f:
                    data.append(line)
                return data
        fact_dir = self.get_facts_dir()
        if os.path.exists('{}/{}.facts'.format(fact_dir, rel_name)):
            with open('{}/{}.facts'.format(fact_dir, rel_name), 'r') as f:
                for line in f:
                    data.append(line)
                return data
        return None

    def get_avaliable_relations(self):
        '''
        return all relations
        '''
        rels_ddisasm = self.ddisasmLib.relations
        rels_include = []
        for i in self.include_files:
            rels_include = rels_include + i.relations
        all_rules = rels_include + rels_ddisasm
        avaliable_rules = list(
            filter(lambda r: r.is_out or r.is_input, all_rules)) + self.cached_relation
        return avaliable_rules

    def get_loaded_file_dir(self):
        return "{}/{}".format(self.BASE_DIR, INCLUDE_DIR)

    def get_cache_file_path(self):
        return '{}/{}/{}'.format(self.BASE_DIR, self.cache_dir, self.cachefile)

    def get_facts_dir(self):
        return self.BASE_DIR + '/' + FACTS_DIR

    def get_output_dir(self):
        return self.BASE_DIR + '/' + self.out_dir

    def get_asm_path(self):
        return self.BASE_DIR + '/' + ASM_NAME

    def get_ddisasm_include_path(self):
        return self.BASE_DIR + '/'+DDISASM_INCLUDE

    def rename_out_to_facts(self):
        # copy all outs into facs
        outs_full_path = self.get_output_dir()
        fact_full_path = self.get_facts_dir()
        for o in os.listdir(outs_full_path):
            shutil.copyfile('{}/{}'.format(outs_full_path, o), '{}/{}'.format(fact_full_path, o))
        # rename in facts dir
        entries = os.listdir(fact_full_path)
        facts = filter(lambda e: e.endswith('.facts'), entries)
        facts = map(lambda e: e[:-6], facts)
        outs = filter(lambda e: e.endswith('.csv'), entries)
        outs = map(lambda e: e[:-4], outs)
        for e in outs:
            # remove input if it is also output
            if e in facts:
                os.remove(fact_full_path+"/"+e+'.facts')
            os.rename(fact_full_path+"/"+e+'.csv', fact_full_path+"/"+e+".facts")

    def run_ddisasm(self, bin_path):
        facts_dir = os.path.abspath(self.BASE_DIR + '/' + FACTS_DIR)
        asm_path = os.path.abspath(self.BASE_DIR + '/' + ASM_NAME)
        os.system("{} --debug-dir {} --asm {} {}".format(os.path.abspath(DDISASM_COMMAND),
                                                         facts_dir, asm_path, os.path.abspath(bin_path)))
        self.rename_out_to_facts()
        res = os.popen('cat /etc/services').read()
        if res.find('Error:') != -1:
            return False
        self.is_binary_loaded = True
        return True

    def prepare_tmp(self):
        if not os.path.exists(self.BASE_DIR):
            # shutil.rmtree(self.BASE_DIR)
            os.makedirs(self.BASE_DIR)
        os.makedirs(self.BASE_DIR + '/' + FACTS_DIR)
        os.makedirs(self.BASE_DIR + '/' + INCLUDE_DIR)
        os.makedirs(self.BASE_DIR + '/' + OUT_DIR)
        os.makedirs(self.BASE_DIR + '/' + DL_DIR)
        os.makedirs(self.BASE_DIR + '/' + CACHE_DIR)

    def close_session(self):
        shutil.rmtree(self.BASE_DIR + '/' + self.cache_dir)
        shutil.rmtree(self.BASE_DIR + '/' + self.out_dir)

    def prepare_session(self):
        os.makedirs(self.BASE_DIR + '/' + self.cache_dir)
        os.makedirs(self.BASE_DIR + '/' + self.out_dir)

    def set_core(self, cores):
        self.cores = cores

    def set_mode(self, mode):
        self.mode = mode

    def finish(self):
        '''
        clean up everything
        '''
        self.activate = False
        shutil.rmtree(self.BASE_DIR + '/' + self.cache_dir)
        shutil.rmtree(self.BASE_DIR + '/' + self.out_dir)
