# rename all .facts into csv input file

from optparse import OptionParser
import os

def rename_out_to_facts(path):
    entries = os.listdir(path)
    facts = filter(lambda e: e.endswith('.facts'), entries)
    facts = map(lambda e: e[:-6], facts)
    outs = filter(lambda e: e.endswith('.csv'), entries)
    outs = map(lambda e: e[:-4], outs)
    for e in outs:
        # remove input if it is also output
        if e in facts:
            os.remove(path+"/"+e+'facts')
        os.rename(path+"/"+e+'.csv', path+"/"+e+".facts")


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-D", "--dir", help="rename all csv file under dir ... into .facts", dest="path")
    (options, args) = parser.parse_args()
    rename_out_to_facts(options.path)
