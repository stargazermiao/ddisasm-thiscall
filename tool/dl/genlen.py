
crypto_counts = {}

with open('non_sparse_crypto_const.facts', 'r') as f:
    for l in f:
        l = l.split('\t')
        if (l[1], l[2]) in crypto_counts.keys():
            crypto_counts[(l[1], l[2])] = crypto_counts[(l[1], l[2])] + 1
        else:
            crypto_counts[(l[1], l[2])] = 1

with open('non_sparse_crypto_const_len.facts', 'w+') as f:
    for p in crypto_counts.keys():
        f.write(f'{p[0]}\t{p[1].strip()}\t{crypto_counts[p]}\n')

