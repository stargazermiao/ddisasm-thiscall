# a grpc server for query ddisasm rules

import os
from datetime import datetime
import asyncio
import functools
import optparse
from typing import Dict
import hashlib
import shutil
from concurrent.futures import ThreadPoolExecutor
import grpc
import logging

from ddisasm_pb2_grpc import CommandService, add_CommandServiceServicer_to_server
from ddisasm_pb2 import Pong, ErrorResponse, RelationDescription, RelationDescriptionsResponse, Tuples, ASMChunk

from DisasmDatalog import DisasmDatalogManager

# Statuses
STATUS_PENDING = 0
STATUS_FAILED = 1
STATUS_RESOLVED = 2
STATUS_NOSUCHPROMISE = -1

TMP_DIR = '.tmp'

class DDisasmService(CommandService):

    def __init__(self, dddisasm_include_path):
        self.home_path = '.tmp'
        self.dddisasm_include_path = dddisasm_include_path
        super(DDisasmService, self).__init__()
        # dict of session id and it's manager
        self.managers: Dict[str, DisasmDatalogManager] = {}
        self.loaded_bins = []
        self.running = []

    async def Ping(self, request, context):
        '''
        test connection
        '''
        return Pong(data=True)

    async def PutBinary(self, request, context):
        '''
        load a binary, if it is already loaded, using cache
        '''
        buffer = []
        async for req_chunk in request:
            bname = req_chunk.name
            buffer.append(req_chunk)
        buffer.sort(key=lambda c: c.seq)
        bname = buffer[0].name
        bin_dir = '.tmp'+'/' + bname
        bin_path = bin_dir + '/' + bname
        # check if is already loaded
        # if bname in self.loaded_bins:
        #     return ErrorResponse(success=True, error_msg='binary is loaded before')
        # using prev cache
        if not os.path.exists(bin_dir):
            logging.info('put hash {}'.format(bname))
            self.loaded_bins.append(bname)
            os.makedirs(bin_dir)
            with open(bin_path, 'wb+') as f:
                for c in buffer:
                    f.write(c.content)
            datalogManager = DisasmDatalogManager(
                bin_dir, self.dddisasm_include_path, bin_path)
            loop = asyncio.get_running_loop()
            with ThreadPoolExecutor(max_workers=2) as pool:
                res = await loop.run_in_executor(
                    pool, functools.partial(datalogManager.run_ddisasm, bin_path))
                if not res:
                    ErrorResponse(
                        success=False, error_msg='fail to load binary {}'.format(bname))
        else:
            datalogManager = DisasmDatalogManager(
                bin_dir, self.dddisasm_include_path, bin_path, True)
        self.managers[datalogManager.session_id] = datalogManager
        return ErrorResponse(success=True, error_msg=datalogManager.session_id)

    async def GetRelations(self, request, context):
        '''
        response all output input relation
        '''
        manager: DisasmDatalogManager = self.managers[request.session_key]
        logging.info('{} querying avaliable relations....'.format(request.session_key))
        avaliable_rules = manager.get_avaliable_relations()
        response = RelationDescriptionsResponse()
        response.success = True
        for r in avaliable_rules:
            d = RelationDescription(name=r.name, arity=r.arity, tag=0)
            response.relations.extend([d])
        return response

    async def PutHashes(self, request, context):
        '''
        add a datalog file
        '''
        manager: DisasmDatalogManager = self.managers[request.session_key]
        bodies = request.bodies
        for b in bodies:
            h = hashlib.sha256()
            h.update(b.encode('utf-8'))
            hsh = h.hexdigest()
            fname = hsh
            manager.load_one_file(fname, b)
        return ErrorResponse(success=True)

    async def RunHashes(self, request, context):
        '''
        run a loaded datalog file
        '''
        loop = asyncio.get_running_loop()
        response = ErrorResponse()
        response.success = True
        manager: DisasmDatalogManager = self.managers[request.session_key]
        with ThreadPoolExecutor(max_workers=6) as pool:
            failed = []
            count = 0
            for h in request.hashes:
                count = count + 1
                if h in self.running:
                    response.success = False
                    response.error_msg = 'datalog query is running'
                    return
                self.running.append(h)
                time_before_run = datetime.now()
                res = await loop.run_in_executor(pool, functools.partial(manager.run_souffle_file, h))
                time_after_run = datetime.now()
                logging.info('datalog running for {} '.format(time_after_run - time_before_run))
                if res == False:
                    failed.append(h)
            if failed != []:
                response.success = False
                response.error_msg = '{} run failed!'.format(','.join(failed))
            if count == 0:
                # empty hash in request, run cache
                logging.info('running cache souffle file')
                time_before_run = datetime.now()
                res = await loop.run_in_executor(pool, manager.run_souffle_cache)
                time_after_run = datetime.now()
                logging.info('datalog running for {} '.format(time_after_run - time_before_run))
                if  res == False:
                    response.success = False
                    response.error_msg = 'running cached file failed!'
            return response

    def ExchangeHashes(self, request, context):
        '''
        not implement
        '''

    def QueryPromise(self, request, context):
        '''
        not implement
        '''

    async def GetTuples(self, request, context):
        '''
        query the output of some relation
        '''
        manager: DisasmDatalogManager = self.managers[request.session_key]
        name = request.database_id
        res = manager.query_relation(name)
        if res is not None:
            for i in range(0, len(res), 10):
                t = Tuples()
                t.status = STATUS_RESOLVED
                t.num_tuples = 10
                t.data.extend(res[i:i+10])
                yield t

    async def GetASM(self, request, context):
        '''
        return decompiled asm file 
        '''
        if request.session_key not in self.managers.keys():
            yield ASMChunk(seq=0)
        else:
            manager: DisasmDatalogManager = self.managers[request.session_key]
            dfile_path = manager.get_asm_path()
            with open(dfile_path, 'r') as f:
                count = 1
                step = 100
                data = list(f)
                for i in range(0, len(data), step):
                    print()
                    chunk = ASMChunk(seq=count, lines=data[i:i+step])
                    yield chunk
                    count = count + 1
                    

    async def Disconnect(self, request, context):
        '''
        disconnect from a client, delete all session related thing 
        '''
        manager: DisasmDatalogManager = self.managers[request.session_key]
        manager.finish()
        del self.managers[request.session_key]
        return ErrorResponse(success=True)

    async def UpdateHash(self, request, context):
        '''
        update a dl file on server
        '''
        manager: DisasmDatalogManager = self.managers[request.session_key]
        manager.remove_one_file(request.old_hash)
        h = hashlib.sha256()
        h.update(request.new_body.encode('utf-8'))
        hsh = h.hexdigest()
        fname = hsh
        manager.load_one_file(fname, request.new_body)
        response = ErrorResponse(success=True)
        return response

    async def PutFacts(self, request, content):
        '''
        put a facts file into server, if exist overwrite
        '''
        buffer = []
        async for req_chunk in request:
            session_key = req_chunk.session_key
            fname = req_chunk.name
            buffer.append(req_chunk.lines)
        manager: DisasmDatalogManager = self.managers[session_key]
        new_fact_path = '{}/{}'.format(manager.get_facts_dir(), fname)
        # some file can be large, running in thread
        def _write(path, buffer):
            with open(path, 'w+') as f:
                for l in buffer:
                    f.write(''.join(l))
        loop = asyncio.get_running_loop()
        with ThreadPoolExecutor(max_workers=2) as pool:
            res = await loop.run_in_executor(
                pool, functools.partial(_write, new_fact_path, buffer)
            )
        response = ErrorResponse(success=True, error_msg='')
        return response


async def serve(include_path):
    server = grpc.aio.server()
    add_CommandServiceServicer_to_server(DDisasmService(include_path), server)
    listen_addr = '[::]:50051'
    server.add_insecure_port(listen_addr)
    logging.info("Starting server on %s", listen_addr)
    await server.start()
    await server.wait_for_termination()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = optparse.OptionParser()
    parser.add_option(
        "-I", "--include", help="ddisasm rule declaration .dl file, can be generated by my datlog libtool", dest="include_path")
    (options, args) = parser.parse_args()
    # prepare tmp
    if os.path.exists(TMP_DIR):
        shutil.rmtree(TMP_DIR)
    else:
        os.makedirs(TMP_DIR)
    if options.include_path is not None:
        asyncio.run(serve(options.include_path))
    else:
        print('plz specify ddisasm include file')
