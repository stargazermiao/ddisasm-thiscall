# rename all .facts into csv input file

from optparse import OptionParser
import os

def rename_out_to_facts(path):
    entries = os.listdir(path)
    for e in entries:
        if e.endswith(".csv"):
            os.rename(path+"/"+e, path+"/"+e[:-4]+".facts")

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-D", "--dir", help="rename all csv file under dir ... into .facts", dest="path")
    (options, args) = parser.parse_args()
    rename_out_to_facts(options.path)
