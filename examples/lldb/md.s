#===================================
.intel_syntax noprefix
#===================================

nop
nop
nop
nop
nop
nop
nop
nop

#===================================
.text
.align 16
#===================================

.L_3030:

            lea RDI,QWORD PTR [RIP+.L_12460]
            lea RAX,QWORD PTR [RIP+.L_12460]
            cmp RAX,RDI
            je .L_3058

            mov RAX,QWORD PTR [RIP+73112]
            test RAX,RAX
            je .L_3058

            jmp RAX
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3058:

            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3060:

            lea RDI,QWORD PTR [RIP+.L_12460]
            lea RSI,QWORD PTR [RIP+.L_12460]
            sub RSI,RDI
            mov RAX,RSI
            shr RSI,63
            sar RAX,3
            add RSI,RAX
            sar RSI,1
            je .L_3098

            mov RAX,QWORD PTR [RIP+73608]
            test RAX,RAX
            je .L_3098

            jmp RAX
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_3098:

            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x66
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_30f0:

            nop
            nop
            nop
            nop
            jmp .L_3060

            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl __mkd_io_strget
.type __mkd_io_strget, @function
#-----------------------------------
__mkd_io_strget:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov EAX,DWORD PTR [RDI+8]
            test EAX,EAX
            je .L_3120

            sub EAX,1
            mov DWORD PTR [RDI+8],EAX
            mov RAX,QWORD PTR [RDI]
            lea RDX,QWORD PTR [RAX+1]
            mov QWORD PTR [RDI],RDX
            movsx EAX,BYTE PTR [RAX]
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_3120:

            mov EAX,4294967295
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl __mkd_new_Document
.type __mkd_new_Document, @function
#-----------------------------------
__mkd_new_Document:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            mov ESI,1
            mov EDI,136
            call QWORD PTR [RIP+calloc@GOTPCREL]

            mov R12,RAX
            test RAX,RAX
            je .L_316c

            mov ESI,1
            mov EDI,96
            call QWORD PTR [RIP+calloc@GOTPCREL]

            mov QWORD PTR [R12+80],RAX
            test RAX,RAX
            je .L_3172

            mov DWORD PTR [R12],425723697
.L_316c:

            mov RAX,R12
            pop R12
.cfi_remember_state 
.cfi_def_cfa_offset 8
            ret 
.L_3172:

.cfi_restore_state 
            mov RDI,R12
            xor R12D,R12D
            call QWORD PTR [RIP+free@GOTPCREL]

            jmp .L_316c
.cfi_endproc 
#-----------------------------------
.align 16
.globl __mkd_enqueue
.type __mkd_enqueue, @function
#-----------------------------------
__mkd_enqueue:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            mov R12,RDI
            mov EDI,40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov RBX,RSI
            mov ESI,1
            sub RSP,24
.cfi_def_cfa_offset 80
            call QWORD PTR [RIP+calloc@GOTPCREL]

            cmp QWORD PTR [R12+32],0
            mov R13D,DWORD PTR [RBX+8]
            mov R15,QWORD PTR [RBX]
            mov RBP,RAX
            je .L_3328

            mov RAX,QWORD PTR [R12+40]
            mov QWORD PTR [RAX+16],RBP
            mov QWORD PTR [R12+40],RBP
.L_31c8:

            mov EAX,DWORD PTR [RBP+12]
            mov RDI,QWORD PTR [RBP]
            test R13D,R13D
            je .L_3300

            add R13,R15
            xor EBX,EBX
            xor R14D,R14D
            jmp .L_3243
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_31e8:

            cmp DL,31
            jbe .L_323e

            cmp DL,124
            jne .L_31f6

            or DWORD PTR [RBP+28],1
.L_31f6:

            cmp EAX,EBX
            jg .L_3226

            add EAX,100
            mov BYTE PTR [RSP+15],DL
            mov DWORD PTR [RBP+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_32e0

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBX,DWORD PTR [RBP+8]
            movzx EDX,BYTE PTR [RSP+15]
            mov RDI,RAX
.L_3222:

            mov QWORD PTR [RBP],RDI
.L_3226:

            lea EAX,DWORD PTR [RBX+1]
            add R14D,1
            mov DWORD PTR [RBP+8],EAX
            mov BYTE PTR [RDI+RBX*1],DL
            movsxd RBX,DWORD PTR [RBP+8]
            mov EAX,DWORD PTR [RBP+12]
            mov RDI,QWORD PTR [RBP]
.L_323e:

            cmp R15,R13
            je .L_32b0
.L_3243:

            movzx EDX,BYTE PTR [R15]
            add R15,1
            cmp DL,9
            jne .L_31e8

            jmp .L_328f
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_3258:

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBX,DWORD PTR [RBP+8]
            mov RDI,RAX
.L_3265:

            mov QWORD PTR [RBP],RDI
.L_3269:

            lea EAX,DWORD PTR [RBX+1]
            add R14D,1
            mov DWORD PTR [RBP+8],EAX
            mov EAX,R14D
            mov BYTE PTR [RDI+RBX*1],32
            cdq 
            movsxd RBX,DWORD PTR [RBP+8]
            mov RDI,QWORD PTR [RBP]
            idiv DWORD PTR [R12+68]
            mov EAX,DWORD PTR [RBP+12]
            test EDX,EDX
            je .L_323e
.L_328f:

            cmp EBX,EAX
            jl .L_3269

            add EAX,100
            mov DWORD PTR [RBP+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            jne .L_3258

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_3265
          .byte 0x90
.L_32b0:

            cmp EBX,EAX
            jge .L_3300
.L_32b4:

            lea EAX,DWORD PTR [RBX+1]
            mov DWORD PTR [RBP+8],EAX
            mov BYTE PTR [RDI+RBX*1],0
            mov RDI,RBP
            sub DWORD PTR [RBP+8],1
            call QWORD PTR [RIP+mkd_firstnonblank@GOTPCREL]

            mov DWORD PTR [RBP+24],EAX
            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_32e0:

.cfi_restore_state 
            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            movzx EDX,BYTE PTR [RSP+15]
            mov RDI,RAX
            jmp .L_3222
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3300:

            add EAX,100
            mov DWORD PTR [RBP+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_3340

            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov RDI,RAX
.L_3317:

            mov QWORD PTR [RBP],RDI
            movsxd RBX,DWORD PTR [RBP+8]
            jmp .L_32b4
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3328:

            mov QWORD PTR [R12+40],RAX
            mov QWORD PTR [R12+32],RAX
            jmp .L_31c8
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3340:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_3317
.cfi_endproc 

            nop
            nop
#-----------------------------------
.align 16
.globl __mkd_trim_line
.type __mkd_trim_line, @function
#-----------------------------------
__mkd_trim_line:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            mov RBX,RDI
            sub RSP,8
.cfi_def_cfa_offset 32
            mov EDX,DWORD PTR [RDI+8]
            cmp EDX,ESI
            jg .L_3380

            mov RAX,QWORD PTR [RDI]
            mov DWORD PTR [RDI+24],0
            mov DWORD PTR [RDI+8],0
            mov BYTE PTR [RAX],0
.L_3374:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_3380:

.cfi_restore_state 
            mov EBP,ESI
            test ESI,ESI
            jle .L_3374

            mov RDI,QWORD PTR [RDI]
            sub EDX,ESI
            movsxd RSI,ESI
            add EDX,1
            add RSI,RDI
            movsxd RDX,EDX
            call QWORD PTR [RIP+memmove@GOTPCREL]

            sub DWORD PTR [RBX+8],EBP
            mov RDI,RBX
            call QWORD PTR [RIP+mkd_firstnonblank@GOTPCREL]

            mov DWORD PTR [RBX+24],EAX
            add RSP,8
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl populate
.type populate, @function
#-----------------------------------
populate:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RSI
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov RBX,RDI
            sub RSP,56
.cfi_def_cfa_offset 112
            mov DWORD PTR [RSP+8],EDX
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+40],RAX
            xor EAX,EAX
            call QWORD PTR [RIP+__mkd_new_Document@GOTPCREL]

            mov R13,RAX
            test RAX,RAX
            je .L_34ce

            mov DWORD PTR [RAX+68],4
            xor R12D,R12D
            lea R14,QWORD PTR [RSP+16]
            mov QWORD PTR [RSP+16],0
            mov QWORD PTR [RSP+24],0
            nop
            nop
            nop
            nop
            nop
.L_3420:

            mov RDI,RBP
            call RBX

            mov R15D,EAX
            cmp EAX,-1
            je .L_34a4
.L_342d:

            cmp R15D,10
            je .L_34f8

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R8,RAX
            movsxd RAX,R15D
            mov RCX,QWORD PTR [R8]
            test WORD PTR [RCX+RAX*2],24576
            jne .L_3454

            test R15B,128
            je .L_3420
.L_3454:

            movsxd RCX,DWORD PTR [RSP+24]
            mov EAX,DWORD PTR [RSP+28]
            mov RDI,QWORD PTR [RSP+16]
            cmp ECX,EAX
            jl .L_348c

            add EAX,100
            movsxd RSI,EAX
            test RDI,RDI
            je .L_3550

            mov DWORD PTR [RSP+28],EAX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RCX,DWORD PTR [RSP+24]
            mov RDI,RAX
.L_3487:

            mov QWORD PTR [RSP+16],RDI
.L_348c:

            lea EAX,DWORD PTR [RCX+1]
            mov DWORD PTR [RSP+24],EAX
            mov BYTE PTR [RDI+RCX*1],R15B
            mov RDI,RBP
            call RBX

            mov R15D,EAX
            cmp EAX,-1
            jne .L_342d
.L_34a4:

            mov EDX,DWORD PTR [RSP+24]
            test EDX,EDX
            jne .L_35f8
.L_34b0:

            mov EAX,DWORD PTR [RSP+28]
            test EAX,EAX
            jne .L_35d8

            mov DWORD PTR [RSP+24],0
.L_34c4:

            cmp R12D,3
            je .L_3570
.L_34ce:

            mov RAX,QWORD PTR [RSP+40]
            sub RAX,QWORD PTR FS:[40]
            jne .L_360b

            add RSP,56
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov RAX,R13
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_34f8:

.cfi_restore_state 
            cmp R12D,-1
            je .L_3521

            cmp R12D,2
            jg .L_3521

            mov ECX,DWORD PTR [RSP+24]
            test ECX,ECX
            je .L_3540

            mov RAX,QWORD PTR [RSP+16]
            add R12D,1
            cmp BYTE PTR [RAX],37
            mov EAX,4294967295
            cmovne R12D,EAX
.L_3521:

            mov RSI,R14
            mov RDI,R13
            call QWORD PTR [RIP+__mkd_enqueue@GOTPCREL]

            mov DWORD PTR [RSP+24],0
            jmp .L_3420
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_3540:

            mov R12D,4294967295
            jmp .L_3521

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_3550:

            mov DWORD PTR [RSP+12],ECX
            mov RDI,RSI
            mov DWORD PTR [RSP+28],EAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            movsxd RCX,DWORD PTR [RSP+12]
            mov RDI,RAX
            jmp .L_3487

            nop
            nop
.L_3570:

            test DWORD PTR [RSP+8],65552
            jne .L_34ce

            mov RBX,QWORD PTR [R13+32]
            mov ESI,1
            mov RDI,RBX
            mov QWORD PTR [R13+8],RBX
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov RDI,QWORD PTR [RBX+16]
            mov ESI,1
            mov QWORD PTR [R13+16],RDI
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov RAX,QWORD PTR [RBX+16]
            mov ESI,1
            mov RDI,QWORD PTR [RAX+16]
            mov QWORD PTR [R13+24],RDI
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov RAX,QWORD PTR [RBX+16]
            mov RAX,QWORD PTR [RAX+16]
            mov RAX,QWORD PTR [RAX+16]
            mov QWORD PTR [R13+32],RAX
            jmp .L_34ce
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_35d8:

            mov RDI,QWORD PTR [RSP+16]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov QWORD PTR [RSP+24],0
            jmp .L_34c4
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_35f8:

            lea RSI,QWORD PTR [RSP+16]
            mov RDI,R13
            call QWORD PTR [RIP+__mkd_enqueue@GOTPCREL]

            jmp .L_34b0
.L_360b:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_in
.type mkd_in, @function
#-----------------------------------
mkd_in:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov EDX,ESI
            mov RSI,RDI
            mov RDI,QWORD PTR [RIP+fgetc@GOTPCREL]
            and EDX,196608
            jmp QWORD PTR [RIP+populate@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_string
.type mkd_string, @function
#-----------------------------------
mkd_string:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            sub RSP,40
.cfi_def_cfa_offset 48
            and EDX,196608
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+24],RAX
            xor EAX,EAX
            mov QWORD PTR [RSP],RDI
            mov RDI,QWORD PTR [RIP+__mkd_io_strget@GOTPCREL]
            mov DWORD PTR [RSP+8],ESI
            mov RSI,RSP
            call QWORD PTR [RIP+populate@GOTPCREL]

            mov RCX,QWORD PTR [RSP+24]
            sub RCX,QWORD PTR FS:[40]
            jne .L_3687

            add RSP,40
.cfi_remember_state 
.cfi_def_cfa_offset 8
            ret 
.L_3687:

.cfi_restore_state 
            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_generatehtml
.type mkd_generatehtml, @function
#-----------------------------------
mkd_generatehtml:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSI
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            mov RBX,RDI
            sub RSP,24
.cfi_def_cfa_offset 48
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+8],RAX
            xor EAX,EAX
            mov RSI,RSP
            call QWORD PTR [RIP+mkd_document@GOTPCREL]

            movsxd RSI,EAX
            cmp ESI,-1
            je .L_3720

            mov RAX,QWORD PTR [RBX+80]
            mov RDI,QWORD PTR [RSP]
            test BYTE PTR [RAX+80],128
            jne .L_3710

            mov RCX,RBP
            mov EDX,1
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            cmp RAX,1
            jne .L_3720
.L_36df:

            mov RSI,RBP
            mov EDI,10
            call QWORD PTR [RIP+putc@GOTPCREL]

            cmp EAX,-1
            sete AL
            movzx EAX,AL
            neg EAX
.L_36f8:

            mov RCX,QWORD PTR [RSP+8]
            sub RCX,QWORD PTR FS:[40]
            jne .L_3727

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
          .byte 0x90
.L_3710:

.cfi_restore_state 
            mov RDX,RBP
            call QWORD PTR [RIP+mkd_generatexml@GOTPCREL]

            cmp EAX,-1
            jne .L_36df

            nop
            nop
.L_3720:

            mov EAX,4294967295
            jmp .L_36f8
.L_3727:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
#-----------------------------------
.align 16
.globl markdown
.type markdown, @function
#-----------------------------------
markdown:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            mov R12,RSI
            mov ESI,EDX
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            mov RBP,RDI
            sub RSP,8
.cfi_def_cfa_offset 32
            call QWORD PTR [RIP+mkd_compile@GOTPCREL]

            test EAX,EAX
            je .L_3770

            mov RSI,R12
            mov RDI,RBP
            call QWORD PTR [RIP+mkd_generatehtml@GOTPCREL]

            mov RDI,RBP
            call QWORD PTR [RIP+mkd_cleanup@GOTPCREL]

            xor EAX,EAX
.L_3760:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3770:

.cfi_restore_state 
            mov EAX,4294967295
            jmp .L_3760
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_line
.type mkd_line, @function
#-----------------------------------
mkd_line:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R14
.cfi_def_cfa_offset 16
.cfi_offset 14, -16
            mov R14D,ESI
            xor ESI,ESI
            push R13
.cfi_def_cfa_offset 24
.cfi_offset 13, -24
            mov R13,RDX
            push R12
.cfi_def_cfa_offset 32
.cfi_offset 12, -32
            mov R12,RDI
            push RBP
.cfi_def_cfa_offset 40
.cfi_offset 6, -40
            push RBX
.cfi_def_cfa_offset 48
.cfi_offset 3, -48
            mov EBX,ECX
            sub RSP,112
.cfi_def_cfa_offset 160
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+104],RAX
            xor EAX,EAX
            mov RBP,RSP
            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_initmmiot@GOTPCREL]

            mov RDI,R12
            xor R8D,R8D
            mov RCX,RBP
            xor EDX,EDX
            mov ESI,R14D
            mov DWORD PTR [RSP+80],EBX
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_emblock@GOTPCREL]

            mov R12D,DWORD PTR [RSP+8]
            test R12D,R12D
            je .L_3880

            mov EAX,DWORD PTR [RSP+12]
            mov RDI,QWORD PTR [RSP]
            movsxd RBX,R12D
            cmp EAX,R12D
            jle .L_3840
.L_37f4:

            lea EAX,DWORD PTR [RBX+1]
            mov DWORD PTR [RSP+8],EAX
            mov BYTE PTR [RDI+RBX*1],0
            mov RDI,QWORD PTR [RSP]
            call QWORD PTR [RIP+strdup@GOTPCREL]

            mov QWORD PTR [R13],RAX
.L_380d:

            xor ESI,ESI
            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_freemmiot@GOTPCREL]

            mov RAX,QWORD PTR [RSP+104]
            sub RAX,QWORD PTR FS:[40]
            jne .L_3893

            add RSP,112
.cfi_remember_state 
.cfi_def_cfa_offset 48
            mov EAX,R12D
            pop RBX
.cfi_def_cfa_offset 40
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3840:

.cfi_restore_state 
            add EAX,100
            mov DWORD PTR [RSP+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_3868

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBX,DWORD PTR [RSP+8]
            mov RDI,RAX
.L_385d:

            mov QWORD PTR [RSP],RDI
            jmp .L_37f4
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_3868:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_385d
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3880:

            mov QWORD PTR [R13],0
            mov R12D,4294967295
            jmp .L_380d
.L_3893:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_string_to_anchor
.type mkd_string_to_anchor, @function
#-----------------------------------
mkd_string_to_anchor:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14D,R8D
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,R9
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RCX
            mov ECX,536870912
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov RBX,RDX
            sub RSP,88
.cfi_def_cfa_offset 144
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+72],RAX
            xor EAX,EAX
            lea RDX,QWORD PTR [RSP+64]
            call QWORD PTR [RIP+mkd_line@GOTPCREL]

            mov R8,QWORD PTR [RSP+64]
            test R8,R8
            je .L_395d

            mov RDX,QWORD PTR [R13+88]
            mov R15D,EAX
            mov RAX,QWORD PTR [RDX+24]
            test RAX,RAX
            je .L_3980

            mov RDI,R8
            mov RDX,QWORD PTR [RDX]
            mov ESI,R15D
            call RAX

            mov RDI,QWORD PTR [RSP+64]
            mov R12,RAX
            call QWORD PTR [RIP+free@GOTPCREL]

            test R12,R12
            je .L_395d
.L_3916:

            movzx EAX,BYTE PTR [R12]
            lea R14,QWORD PTR [R12+1]
            test AL,AL
            je .L_393d

            nop
            nop
            nop
            nop
.L_3928:

            add R14,1
            movsx EDI,AL
            mov RSI,RBP
            call RBX

            movzx EAX,BYTE PTR [R14-1]
            test AL,AL
            jne .L_3928
.L_393d:

            mov RAX,QWORD PTR [R13+88]
            cmp QWORD PTR [RAX+24],0
            je .L_3a00

            mov RDX,QWORD PTR [RAX+32]
            test RDX,RDX
            je .L_395d

            mov RSI,QWORD PTR [RAX]
            mov RDI,R12
            call RDX
.L_395d:

            mov RAX,QWORD PTR [RSP+72]
            sub RAX,QWORD PTR FS:[40]
            jne .L_3bd2

            add RSP,88
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
.L_3980:

.cfi_restore_state 
            mov EAX,DWORD PTR [R13+80]
            mov QWORD PTR [RSP+8],R8
            and EAX,268435456
            mov DWORD PTR [RSP+24],EAX
            test R14D,R14D
            je .L_3a10

            lea EDI,DWORD PTR [R15*4+2]
            movsxd RDI,EDI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov R8,QWORD PTR [RSP+8]
            test RAX,RAX
            mov R12,RAX
            je .L_3bd8

            mov ECX,DWORD PTR [RSP+24]
            test ECX,ECX
            jne .L_3bc8

            mov QWORD PTR [RSP+8],R8
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R8,QWORD PTR [RSP+8]
            mov RAX,QWORD PTR [RAX]
            movsx RDX,BYTE PTR [R8]
            test BYTE PTR [RAX+RDX*2+1],4
            jne .L_3bc8

            mov BYTE PTR [R12],76
            lea RDX,QWORD PTR [R12+1]
            mov ESI,1
            jmp .L_3a33
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3a00:

            mov RDI,R12
            call QWORD PTR [RIP+free@GOTPCREL]

            jmp .L_395d
          .byte 0x66
          .byte 0x90
.L_3a10:

            lea EDI,DWORD PTR [R15+2]
            movsxd RDI,EDI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov R8,QWORD PTR [RSP+8]
            test RAX,RAX
            mov RDX,RAX
            je .L_3bd8

            mov R12,RAX
            xor ESI,ESI
.L_3a33:

            test R15D,R15D
            jle .L_3b87

            cmp DWORD PTR [RSP+24],1
            mov QWORD PTR [RSP+48],RBP
            mov RBP,R12
            mov R12D,ESI
            sbb EAX,EAX
            mov QWORD PTR [RSP+56],R13
            mov R13D,R14D
            and EAX,8
            mov QWORD PTR [RSP+32],R8
            add EAX,37
            mov QWORD PTR [RSP+40],RBX
            mov BYTE PTR [RSP+31],AL
            lea EAX,DWORD PTR [R15-1]
            mov R15,R8
            lea RAX,QWORD PTR [R8+RAX*1+1]
            mov QWORD PTR [RSP+8],RAX
            jmp .L_3b3c
.L_3a80:

            mov QWORD PTR [RSP+16],RCX
            mov BYTE PTR [RSP+30],DIL
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov EDX,DWORD PTR [RSP+24]
            mov RCX,QWORD PTR [RSP+16]
            mov R11,RAX
            movzx EAX,BYTE PTR [RSP+30]
            mov R11,QWORD PTR [R11]
            test EDX,EDX
            mov RDI,RAX
            movzx EAX,WORD PTR [R11+RAX*2]
            jne .L_3ba0

            test AL,8
            jne .L_3b5c

            lea EAX,DWORD PTR [RDI-46]
            cmp AL,49
            ja .L_3bab

            movabs RSI,562949953425409
            bt RSI,RAX
            jb .L_3b5c
.L_3adb:

            movzx EAX,BYTE PTR [RSP+31]
            lea RSI,QWORD PTR [RIP+.L_e000]
            mov BYTE PTR [RCX],AL
            mov EAX,EDI
            and EDI,15
            lea ECX,DWORD PTR [R12+3]
            shr AL,4
            movzx EDX,BYTE PTR [RSI+RDI*1]
            and EAX,15
            movzx EAX,BYTE PTR [RSI+RAX*1]
            mov BYTE PTR [RBX],AL
            lea EAX,DWORD PTR [R12+2]
            cdqe 
            mov BYTE PTR [RBP+RAX*1],DL
            mov EAX,DWORD PTR [RSP+24]
            movsxd RDX,ECX
            lea RBX,QWORD PTR [RBP+RDX*1]
            test EAX,EAX
            jne .L_3bc0

            add R12D,4
            mov BYTE PTR [RBX],45
            movsxd RDX,R12D
            lea RBX,QWORD PTR [RBP+RDX*1]
.L_3b31:

            add R15,1
            cmp R15,QWORD PTR [RSP+8]
            je .L_3b6d
.L_3b3c:

            lea R14D,DWORD PTR [R12+1]
            movsxd RCX,R12D
            movzx EDI,BYTE PTR [R15]
            movsxd RDX,R14D
            add RCX,RBP
            lea RBX,QWORD PTR [RBP+RDX*1]
            test R13D,R13D
            jne .L_3a80
.L_3b5c:

            mov BYTE PTR [RCX],DIL
            mov R12D,R14D
            add R15,1
            cmp R15,QWORD PTR [RSP+8]
            jne .L_3b3c
.L_3b6d:

            mov R12,RBP
            mov RDX,RBX
            mov R8,QWORD PTR [RSP+32]
            mov RBX,QWORD PTR [RSP+40]
            mov RBP,QWORD PTR [RSP+48]
            mov R13,QWORD PTR [RSP+56]
.L_3b87:

            mov BYTE PTR [RDX],0
            mov RDI,R8
            call QWORD PTR [RIP+free@GOTPCREL]

            jmp .L_3916
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3ba0:

            test AH,32
            jne .L_3bab

            cmp DIL,37
            jne .L_3b5c
.L_3bab:

            cmp DIL,32
            jne .L_3adb

            mov BYTE PTR [RCX],45
            mov R12D,R14D
            jmp .L_3b31
.L_3bc0:

            mov R12D,ECX
            jmp .L_3b31
.L_3bc8:

            mov RDX,R12
            xor ESI,ESI
            jmp .L_3a33
.L_3bd2:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.L_3bd8:

            mov RDI,R8
            call QWORD PTR [RIP+free@GOTPCREL]

            jmp .L_395d
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_generateline
.type mkd_generateline, @function
#-----------------------------------
mkd_generateline:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R14
.cfi_def_cfa_offset 16
.cfi_offset 14, -16
            mov R14D,ESI
            xor ESI,ESI
            push R13
.cfi_def_cfa_offset 24
.cfi_offset 13, -24
            mov R13,RDI
            push R12
.cfi_def_cfa_offset 32
.cfi_offset 12, -32
            mov R12,RDX
            push RBP
.cfi_def_cfa_offset 40
.cfi_offset 6, -40
            push RBX
.cfi_def_cfa_offset 48
.cfi_offset 3, -48
            mov EBX,ECX
            sub RSP,112
.cfi_def_cfa_offset 160
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+104],RAX
            xor EAX,EAX
            mov RBP,RSP
            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_initmmiot@GOTPCREL]

            xor R8D,R8D
            xor EDX,EDX
            mov DWORD PTR [RSP+80],EBX
            mov RCX,RBP
            mov ESI,R14D
            mov RDI,R13
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_emblock@GOTPCREL]

            and EBX,128
            je .L_3c98

            mov ESI,DWORD PTR [RSP+8]
            mov RDI,QWORD PTR [RSP]
            mov RDX,R12
            xor EBX,EBX
            call QWORD PTR [RIP+mkd_generatexml@GOTPCREL]

            cmp EAX,-1
            setne BL
.L_3c67:

            xor ESI,ESI
            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_freemmiot@GOTPCREL]

            lea EAX,DWORD PTR [RBX-1]
            mov RCX,QWORD PTR [RSP+104]
            sub RCX,QWORD PTR FS:[40]
            jne .L_3cbe

            add RSP,112
.cfi_remember_state 
.cfi_def_cfa_offset 48
            pop RBX
.cfi_def_cfa_offset 40
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_3c98:

.cfi_restore_state 
            mov EDX,1
            movsxd RSI,DWORD PTR [RSP+8]
            mov RDI,QWORD PTR [RSP]
            xor EBX,EBX
            mov RCX,R12
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            movsxd RDX,DWORD PTR [RSP+8]
            cmp RDX,RAX
            sete BL
            jmp .L_3c67
.L_3cbe:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_e_url
.type mkd_e_url, @function
#-----------------------------------
mkd_e_url:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_3ce6

            cmp QWORD PTR [RDI+96],RSI
            je .L_3ce2

            mov DWORD PTR [RDI+60],1
.L_3ce2:

            mov QWORD PTR [RDI+96],RSI
.L_3ce6:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_e_flags
.type mkd_e_flags, @function
#-----------------------------------
mkd_e_flags:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_3d06

            cmp QWORD PTR [RDI+104],RSI
            je .L_3d02

            mov DWORD PTR [RDI+60],1
.L_3d02:

            mov QWORD PTR [RDI+104],RSI
.L_3d06:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_e_anchor
.type mkd_e_anchor, @function
#-----------------------------------
mkd_e_anchor:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_3d26

            cmp QWORD PTR [RDI+112],RSI
            je .L_3d22

            mov DWORD PTR [RDI+60],1
.L_3d22:

            mov QWORD PTR [RDI+112],RSI
.L_3d26:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_e_free
.type mkd_e_free, @function
#-----------------------------------
mkd_e_free:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_3d46

            cmp QWORD PTR [RDI+120],RSI
            je .L_3d42

            mov DWORD PTR [RDI+60],1
.L_3d42:

            mov QWORD PTR [RDI+120],RSI
.L_3d46:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_e_data
.type mkd_e_data, @function
#-----------------------------------
mkd_e_data:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_3d66

            cmp QWORD PTR [RDI+88],RSI
            je .L_3d62

            mov DWORD PTR [RDI+60],1
.L_3d62:

            mov QWORD PTR [RDI+88],RSI
.L_3d66:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_e_code_format
.type mkd_e_code_format, @function
#-----------------------------------
mkd_e_code_format:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_3d8c

            cmp QWORD PTR [RDI+128],RSI
            je .L_3d8c

            mov DWORD PTR [RDI+60],1
            mov QWORD PTR [RDI+128],RSI
.L_3d8c:

            ret 
.cfi_endproc 

            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_ref_prefix
.type mkd_ref_prefix, @function
#-----------------------------------
mkd_ref_prefix:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_3da6

            cmp QWORD PTR [RDI+72],RSI
            je .L_3da2

            mov DWORD PTR [RDI+60],1
.L_3da2:

            mov QWORD PTR [RDI+72],RSI
.L_3da6:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_3db0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov ECX,EDX
            shr ECX,23
            xor ECX,1
            and ECX,1
            jmp .L_3df6
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_3dc0:

            test CL,CL
            je .L_3e00

            mov R8,QWORD PTR [RDI+16]
            test R8,R8
            je .L_3e03

            movsxd RAX,DWORD PTR [RDI+8]
            cmp EAX,2
            jle .L_3e00

            mov EDX,DWORD PTR [RDI+24]
            test EDX,EDX
            jne .L_3e00

            mov RDX,QWORD PTR [RDI]
            cmp BYTE PTR [RDX],61
            jne .L_3e00

            cmp BYTE PTR [RDX+RAX*1-1],61
            jne .L_3e00

            cmp DWORD PTR [R8+24],3
            jg .L_3e10

            mov RDI,R8
.L_3df6:

            test RDI,RDI
            jne .L_3dc0

            nop
            nop
            nop
            nop
            nop
.L_3e00:

            xor R8D,R8D
.L_3e03:

            mov RAX,R8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_3e10:

            mov R8,RDI
            mov DWORD PTR [RSI],4
            mov RAX,R8
            ret 
.cfi_endproc 

            nop
            nop
            nop
#-----------------------------------
.align 16
.globl __mkd_footsort
.type __mkd_footsort, @function
#-----------------------------------
__mkd_footsort:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R13
.cfi_def_cfa_offset 16
.cfi_offset 13, -16
            push R12
.cfi_def_cfa_offset 24
.cfi_offset 12, -24
            push RBP
.cfi_def_cfa_offset 32
.cfi_offset 6, -32
            push RBX
.cfi_def_cfa_offset 40
.cfi_offset 3, -40
            sub RSP,8
.cfi_def_cfa_offset 48
            mov EBP,DWORD PTR [RDI+8]
            mov EDX,DWORD PTR [RSI+8]
            mov EAX,EBP
            sub EAX,EDX
            cmp EBP,EDX
            jne .L_3ea7

            test EBP,EBP
            jle .L_3ea5

            mov R13,RDI
            mov R12,RSI
            call QWORD PTR [RIP+__ctype_tolower_loc@GOTPCREL]

            mov R13,QWORD PTR [R13]
            mov R12,QWORD PTR [R12]
            mov RBX,QWORD PTR [RAX]
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movsxd RDI,EBP
            mov R9,QWORD PTR [RAX]
            xor EAX,EAX
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_3e68:

            movsx RDX,BYTE PTR [R13+RAX*1]
            movsx RCX,BYTE PTR [R12+RAX*1]
            mov EDX,DWORD PTR [RBX+RDX*4]
            mov ECX,DWORD PTR [RBX+RCX*4]
            movsx RSI,DL
            mov R8D,EDX
            movsx R10D,CL
            test BYTE PTR [R9+RSI*2+1],32
            je .L_3e98

            movsx RSI,CL
            test BYTE PTR [R9+RSI*2+1],32
            jne .L_3e9c
.L_3e98:

            cmp DL,CL
            jne .L_3eb8
.L_3e9c:

            add RAX,1
            cmp RDI,RAX
            jne .L_3e68
.L_3ea5:

            xor EAX,EAX
.L_3ea7:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 40
            pop RBX
.cfi_def_cfa_offset 32
            pop RBP
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_3eb8:

.cfi_restore_state 
            add RSP,8
.cfi_def_cfa_offset 40
            movsx EAX,R8B
            pop RBX
.cfi_def_cfa_offset 32
            sub EAX,R10D
            pop RBP
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
.L_3ed0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov RBX,RDI
            sub RSP,24
.cfi_def_cfa_offset 80
            mov EBP,DWORD PTR [RDI+24]
            or DWORD PTR [RDI+28],2
            mov QWORD PTR [RDI+32],0
            cmp EBP,3
            jg .L_4080

            mov R12D,DWORD PTR [RDI+8]
            cmp EBP,R12D
            jge .L_3f31

            mov DWORD PTR [RSP+8],ESI
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RDX,QWORD PTR [RBX]
            mov ESI,DWORD PTR [RSP+8]
            mov R8,QWORD PTR [RAX]
            movsxd RAX,R12D
.L_3f19:

            movsx RDI,BYTE PTR [RDX+RAX*1-1]
            mov ECX,EAX
            test BYTE PTR [R8+RDI*2+1],32
            je .L_3f40

            sub RAX,1
            cmp EBP,EAX
            jl .L_3f19
.L_3f31:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
.L_3f40:

.cfi_restore_state 
            cmp EBP,ECX
            jge .L_3f31

            movsxd RAX,EBP
            not EBP
            and ESI,33554432
            xor EDI,EDI
            add RDX,RAX
            lea EAX,DWORD PTR [RBP+RCX*1]
            mov DWORD PTR [RSP+12],ESI
            xor R10D,R10D
            mov DWORD PTR [RSP+8],0
            xor R15D,R15D
            xor R9D,R9D
            xor EBP,EBP
            lea R8,QWORD PTR [RDX+RAX*1+1]
            xor R13D,R13D
            xor R11D,R11D
            xor R14D,R14D
            mov R12D,1
            jmp .L_3fa1
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_3f88:

            mov DWORD PTR [RBX+36],ECX
            test EDI,EDI
            je .L_3f31

            mov R10D,EDI
            nop
            nop
            nop
            nop
            nop
            nop
.L_3f98:

            add RDX,1
            cmp R8,RDX
            je .L_3fd8
.L_3fa1:

            movzx EAX,BYTE PTR [RDX]
            cmp AL,32
            je .L_4070

            mov ECX,DWORD PTR [RBX+36]
            lea ESI,DWORD PTR [RCX+1]
            mov DWORD PTR [RBX+36],ESI
            cmp AL,61
            je .L_4060

            jg .L_4020

            cmp AL,45
            je .L_4040

            jg .L_3f88

            cmp AL,42
            jne .L_3f88

            add RDX,1
            mov R9D,1
            cmp R8,RDX
            jne .L_3fa1
.L_3fd8:

            lea EAX,DWORD PTR [R14+RBP*1]
            add EAX,R13D
            add EAX,R9D
            add EAX,R15D
            add EAX,DWORD PTR [RSP+8]
            cmp EAX,1
            jg .L_3f31

            test R11D,R11D
            jne .L_4096

            or R13D,R9D
            jne .L_40a2

            test R14D,R14D
            je .L_40b8

            mov DWORD PTR [RBX+32],3
            jmp .L_3f31
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4020:

            cmp AL,95
            jne .L_40f1

            test EDI,EDI
            jne .L_4050

            mov R13D,1
            jmp .L_3f98
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4040:

            test EDI,EDI
            jne .L_4050

            mov R14D,1
            jmp .L_3f98
          .byte 0x90
.L_4050:

            mov DWORD PTR [RBX+36],ECX
            mov R10D,EDI
            jmp .L_3f98
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_4060:

            mov EBP,1
            jmp .L_3f98
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_4070:

            test EDI,EDI
            cmove R11D,R12D
            cmovne R10D,EDI
            jmp .L_3f98
          .byte 0x90
.L_4080:

            mov DWORD PTR [RDI+32],1
            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
.L_4096:

.cfi_restore_state 
            or R9D,R14D
            or R9D,R13D
            je .L_3f31
.L_40a2:

            mov DWORD PTR [RBX+32],2
            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
.L_40b8:

.cfi_restore_state 
            test EBP,EBP
            jne .L_40cd

            test R15D,R15D
            je .L_40d9

            mov DWORD PTR [RBX+32],4
            jmp .L_3f31
.L_40cd:

            mov DWORD PTR [RBX+32],6
            jmp .L_3f31
.L_40d9:

            mov EAX,DWORD PTR [RSP+8]
            test EAX,EAX
            je .L_3f31

            mov DWORD PTR [RBX+32],5
            jmp .L_3f31
.L_40f1:

            cmp DWORD PTR [RSP+12],0
            je .L_3f88

            cmp AL,96
            jne .L_4120

            test R10D,R10D
            jne .L_3f31

            mov DWORD PTR [RSP+8],1
            mov EDI,1
            jmp .L_3f98
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_4120:

            cmp AL,126
            jne .L_3f88

            test R10D,R10D
            jne .L_3f31

            mov EDI,1
            mov R15D,1
            jmp .L_3f98
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_4150:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov EAX,DWORD PTR [RDI+24]
            xor R8D,R8D
            cmp EAX,3
            jg .L_4180

            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            movsxd RBX,EAX
            sub RSP,8
.cfi_def_cfa_offset 32
            mov RBP,QWORD PTR [RDI]
            cmp BYTE PTR [RBP+RBX*1],58
            je .L_4188

            add RSP,8
.cfi_def_cfa_offset 24
            mov EAX,R8D
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4180:

.cfi_restore 3
.cfi_restore 6
            mov EAX,R8D
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_4188:

.cfi_def_cfa_offset 32
.cfi_offset 3, -24
.cfi_offset 6, -16
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movsx RDX,BYTE PTR [RBP+RBX*1+1]
            mov RAX,QWORD PTR [RAX]
            movzx R8D,WORD PTR [RAX+RDX*2]
            add RSP,8
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            shr R8W,13
            and R8D,1
            mov EAX,R8D
            ret 
.cfi_endproc 

            nop
.L_41b0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_4290

            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,8
.cfi_def_cfa_offset 64
            movsxd RBP,DWORD PTR [RDI+8]
            mov R12,QWORD PTR [RDI]
            cmp EBP,2
            jle .L_4250

            cmp BYTE PTR [R12],60
            jne .L_4250

            movsx RBX,BYTE PTR [R12+1]
            cmp BL,33
            je .L_4268
.L_41e9:

            lea R14,QWORD PTR [R12+1]
            mov R15D,1
            jmp .L_4205
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4200:

            movsx RBX,BYTE PTR [R12+R15*1]
.L_4205:

            mov R13D,R15D
            cmp BL,62
            je .L_4248

            cmp BL,47
            je .L_4248

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RAX,QWORD PTR [RAX]
            test BYTE PTR [RAX+RBX*2+1],32
            jne .L_4248

            add R15,1
            cmp RBP,R15
            jne .L_4200
.L_422b:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov ESI,R13D
            mov RDI,R14
            pop RBX
.cfi_restore 3
.cfi_def_cfa_offset 48
            pop RBP
.cfi_restore 6
.cfi_def_cfa_offset 40
            pop R12
.cfi_restore 12
.cfi_def_cfa_offset 32
            pop R13
.cfi_restore 13
.cfi_def_cfa_offset 24
            pop R14
.cfi_restore 14
.cfi_def_cfa_offset 16
            pop R15
.cfi_restore 15
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+mkd_search_tags@GOTPCREL]
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_4248:

.cfi_restore_state 
            sub R13D,1
            jmp .L_422b
          .byte 0x66
          .byte 0x90
.L_4250:

            xor EAX,EAX
.L_4252:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4268:

.cfi_restore_state 
            cmp BYTE PTR [R12+2],45
            jne .L_41e9

            cmp BYTE PTR [R12+3],45
            jne .L_41e9

            lea RAX,QWORD PTR [RIP+.L_12010]
            jmp .L_4252
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4290:

.cfi_def_cfa_offset 8
.cfi_restore 3
.cfi_restore 6
.cfi_restore 12
.cfi_restore 13
.cfi_restore 14
.cfi_restore 15
            xor EAX,EAX
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_42a0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test EDX,16777216
            je .L_43a8

            push R14
.cfi_def_cfa_offset 16
.cfi_offset 14, -16
            push R13
.cfi_def_cfa_offset 24
.cfi_offset 13, -24
            mov R13,RSI
            push R12
.cfi_def_cfa_offset 32
.cfi_offset 12, -32
            push RBP
.cfi_def_cfa_offset 40
.cfi_offset 6, -40
            mov EBP,EDX
            push RBX
.cfi_def_cfa_offset 48
.cfi_offset 3, -48
            mov RBX,RDI
            test RDI,RDI
            je .L_4398

            cmp QWORD PTR [RBX+16],0
            je .L_4398
.L_42d0:

            mov EAX,DWORD PTR [RBX+8]
            test EAX,EAX
            je .L_4398

            mov RDX,QWORD PTR [RBX]
            cmp BYTE PTR [RDX],61
            je .L_4398

            movsxd RCX,EAX
            cmp BYTE PTR [RDX+RCX*1-1],61
            je .L_4398

            mov EDX,DWORD PTR [RBX+24]
            cmp EAX,EDX
            jle .L_4398

            cmp EDX,3
            jg .L_4398

            test BYTE PTR [RBX+28],2
            je .L_43c8
.L_4313:

            cmp DWORD PTR [RBX+36],2
            jg .L_43b0
.L_431d:

            mov EAX,DWORD PTR [RBX+24]
            test EAX,EAX
            jne .L_4332

            cmp DWORD PTR [RBX+8],1
            jle .L_4332

            mov RAX,QWORD PTR [RBX]
            cmp BYTE PTR [RAX],35
            je .L_4398
.L_4332:

            mov R12,QWORD PTR [RBX+16]
            test R12,R12
            je .L_4398

            test BYTE PTR [R12+28],2
            je .L_43ed
.L_4347:

            mov EAX,DWORD PTR [R12+32]
            cmp EAX,3
            je .L_4398

            cmp EAX,6
            je .L_4398

            mov R14,QWORD PTR [RBX+16]
            test R14,R14
            je .L_4398

            mov RDI,R14
            jmp .L_4371
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_4368:

            mov RDI,QWORD PTR [RDI+16]
            test RDI,RDI
            je .L_4384
.L_4371:

            mov R12D,DWORD PTR [RDI+24]
            cmp R12D,DWORD PTR [RDI+8]
            je .L_4368

            call .L_4150

            test EAX,EAX
            jne .L_43e0
.L_4384:

            mov RBX,R14
            cmp QWORD PTR [RBX+16],0
            jne .L_42d0

            nop
            nop
            nop
            nop
            nop
            nop
.L_4398:

            xor EAX,EAX
.L_439a:

            pop RBX
.cfi_def_cfa_offset 40
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_43a8:

.cfi_restore 3
.cfi_restore 6
.cfi_restore 12
.cfi_restore 13
.cfi_restore 14
            xor EAX,EAX
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_43b0:

.cfi_def_cfa_offset 48
.cfi_offset 3, -48
.cfi_offset 6, -40
.cfi_offset 12, -32
.cfi_offset 13, -24
.cfi_offset 14, -16
            mov EAX,DWORD PTR [RBX+32]
            lea EDX,DWORD PTR [RAX-2]
            cmp EDX,1
            jbe .L_4398

            cmp EAX,6
            je .L_4398

            jmp .L_431d
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_43c8:

            mov ESI,EBP
            mov RDI,RBX
            call .L_3ed0

            jmp .L_4313
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_43e0:

            add R12D,2
            mov RAX,RBX
            mov DWORD PTR [R13],R12D
            jmp .L_439a
.L_43ed:

            mov ESI,EBP
            mov RDI,R12
            call .L_3ed0

            jmp .L_4347
.cfi_endproc 

            nop
            nop
            nop
            nop
.L_4400:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            cmp EDX,ESI
            jge .L_4540

            push R13
.cfi_def_cfa_offset 16
.cfi_offset 13, -16
            mov R13,RDI
            push R12
.cfi_def_cfa_offset 24
.cfi_offset 12, -24
            mov R12D,ESI
            push RBP
.cfi_def_cfa_offset 32
.cfi_offset 6, -32
            mov EBP,EDX
            push RBX
.cfi_def_cfa_offset 40
.cfi_offset 3, -40
            movsxd RBX,EBP
            add RBX,R13
            sub RSP,8
.cfi_def_cfa_offset 48
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RDX,QWORD PTR [RAX]
            jmp .L_443d
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_4430:

            add RBX,1
            cmp R12D,EBP
            je .L_4520
.L_443d:

            movsx RAX,BYTE PTR [RBX]
            mov ECX,EBP
            add EBP,1
            test BYTE PTR [RDX+RAX*2+1],32
            jne .L_4430
.L_444d:

            sub R12D,EBP
            xor EAX,EAX
            test R12D,R12D
            jle .L_445c

            cmp BYTE PTR [RBX],37
            je .L_4470
.L_445c:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 40
            pop RBX
.cfi_def_cfa_offset 32
            pop RBP
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4470:

.cfi_restore_state 
            movsxd RDX,R12D
            cmp BYTE PTR [RBX+RDX*1],37
            jne .L_445c

            lea R13,QWORD PTR [RBX+1]
            mov EDX,3
            lea RSI,QWORD PTR [RIP+.L_e011]
            mov RDI,R13
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            test EAX,EAX
            je .L_4543

            mov EDX,6
            lea RSI,QWORD PTR [RIP+.L_e015]
            mov RDI,R13
            xor EBP,EBP
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            test EAX,EAX
            je .L_4551
.L_44b9:

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movsx RDX,BYTE PTR [R13]
            mov RSI,QWORD PTR [RAX]
            test BYTE PTR [RSI+RDX*2+1],4
            jne .L_44e4

            cmp DL,95
            setne CL
            cmp DL,45
            setne DL
            xor EAX,EAX
            test CL,DL
            jne .L_445c
.L_44e4:

            lea EAX,DWORD PTR [RBP+1]
            cdqe 
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_44f0:

            cmp R12D,EAX
            jle .L_4530

            movsx RCX,BYTE PTR [RBX+RAX*1]
            add RAX,1
            mov RDX,RCX
            test WORD PTR [RSI+RCX*2],3072
            jne .L_44f0

            cmp CL,45
            setne CL
            cmp DL,95
            setne DL
            test CL,DL
            je .L_44f0

            xor EAX,EAX
            jmp .L_445c
.L_4520:

            movsxd RBX,R12D
            lea EBP,DWORD PTR [RCX+2]
            add RBX,R13
            jmp .L_444d
          .byte 0x66
          .byte 0x90
.L_4530:

            add RSP,8
.cfi_def_cfa_offset 40
            mov EAX,1
            pop RBX
.cfi_def_cfa_offset 32
            pop RBP
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
.L_4540:

.cfi_restore 3
.cfi_restore 6
.cfi_restore 12
.cfi_restore 13
            xor EAX,EAX
            ret 
.L_4543:

.cfi_def_cfa_offset 48
.cfi_offset 3, -40
.cfi_offset 6, -32
.cfi_offset 12, -24
.cfi_offset 13, -16
            lea R13,QWORD PTR [RBX+4]
            mov EBP,3
            jmp .L_44b9
.L_4551:

            lea R13,QWORD PTR [RBX+7]
            mov EBP,6
            jmp .L_44b9
.cfi_endproc 

            nop
.L_4560:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,40
.cfi_def_cfa_offset 96
            mov RBP,QWORD PTR [RSI]
            mov QWORD PTR [RSP+16],RDI
            mov RBX,QWORD PTR [RBP+16]
            mov QWORD PTR [RSP+24],RSI
            test RBX,RBX
            je .L_4600

            mov R13D,EDX
            mov R12D,EDX
            and R13D,33554432
            je .L_45b0

            mov R14D,DWORD PTR [RBP+36]
            test BYTE PTR [RBX+28],2
            je .L_46f0

            mov EAX,DWORD PTR [RBX+32]
            sub EAX,4
            cmp EAX,1
            jbe .L_45f8
.L_45ac:

            mov RBX,QWORD PTR [RBP+16]
.L_45b0:

            mov R15,RBP
            jmp .L_45e6
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_45b8:

            mov R14D,DWORD PTR [RBP+32]
            mov R8D,DWORD PTR [RBP+36]
            mov RAX,RBX
            test BYTE PTR [RBX+28],2
            je .L_46c8
.L_45cd:

            mov EDX,DWORD PTR [RBX+32]
            test R14D,R14D
            je .L_4618

            cmp R14D,EDX
            je .L_4620
.L_45da:

            test RAX,RAX
            je .L_4600
.L_45df:

            mov RBX,QWORD PTR [RAX+16]
            mov R15,RAX
.L_45e6:

            test RBX,RBX
            je .L_4600

            test R13D,R13D
            jne .L_45b8

            mov RAX,RBX
            jmp .L_45df
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_45f8:

            cmp R14D,DWORD PTR [RBX+36]
            jg .L_45ac

            nop
            nop
.L_4600:

            xor R12D,R12D
.L_4603:

            add RSP,40
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov RAX,R12
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_4618:

.cfi_restore_state 
            sub EDX,4
            cmp EDX,1
            ja .L_45da
.L_4620:

            cmp R8D,DWORD PTR [RBX+36]
            jg .L_45da

            mov RCX,QWORD PTR [RSP+24]
            mov RAX,QWORD PTR [RAX+16]
            mov ESI,1
            mov EDI,56
            mov QWORD PTR [RCX],RAX
            mov RBX,QWORD PTR [RBP+16]
            call QWORD PTR [RIP+calloc@GOTPCREL]

            mov RCX,QWORD PTR [RSP+16]
            mov QWORD PTR [RAX+16],RBX
            mov R12,RAX
            cmp QWORD PTR [RCX],0
            mov DWORD PTR [RAX+40],1
            je .L_4710

            mov RAX,QWORD PTR [RCX+8]
            mov QWORD PTR [RAX],R12
            mov QWORD PTR [RCX+8],R12
.L_466e:

            movsxd RDI,DWORD PTR [RBP+36]
            mov EAX,DWORD PTR [RBP+8]
            sub EAX,EDI
            test EAX,EAX
            jle .L_4728

            add RDI,QWORD PTR [RBP]
            cmp BYTE PTR [RDI],32
            jne .L_4699

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_4690:

            add RDI,1
            cmp BYTE PTR [RDI],32
            je .L_4690
.L_4699:

            call QWORD PTR [RIP+strdup@GOTPCREL]

            mov QWORD PTR [R12+32],RAX
.L_46a4:

            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]

            mov RDI,QWORD PTR [R15+16]
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]

            mov QWORD PTR [R15+16],0
            jmp .L_4603
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_46c8:

            mov ESI,R12D
            mov RDI,RBX
            mov DWORD PTR [RSP+12],R8D
            call .L_3ed0

            mov RAX,QWORD PTR [R15+16]
            mov R8D,DWORD PTR [RSP+12]
            jmp .L_45cd
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_46f0:

            mov ESI,EDX
            mov RDI,RBX
            call .L_3ed0

            mov EAX,DWORD PTR [RBX+32]
            sub EAX,4
            cmp EAX,1
            ja .L_45ac

            jmp .L_45f8
          .byte 0x66
          .byte 0x90
.L_4710:

            mov RAX,QWORD PTR [RSP+16]
            mov QWORD PTR [RAX+8],R12
            mov QWORD PTR [RAX],R12
            jmp .L_466e
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4728:

            mov QWORD PTR [R12+32],0
            jmp .L_46a4
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_4740:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,RSI
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            mov R12,RDI
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov EBX,EDX
            sub RSP,72
.cfi_def_cfa_offset 128
            mov QWORD PTR [RSP+8],RCX
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+56],RAX
            xor EAX,EAX
            test RDI,RDI
            je .L_4800

            mov EAX,DWORD PTR [RDI+24]
            cmp DWORD PTR [RDI+8],EAX
            jg .L_47a8
.L_477c:

            xor EAX,EAX
.L_477e:

            mov RBX,QWORD PTR [RSP+56]
            sub RBX,QWORD PTR FS:[40]
            jne .L_4abf

            add RSP,72
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_47a8:

.cfi_restore_state 
            test BYTE PTR [RDI+28],2
            je .L_4990
.L_47b2:

            cmp DWORD PTR [R12+36],2
            jg .L_4970
.L_47be:

            mov EAX,DWORD PTR [R12+24]
            test EAX,EAX
            jne .L_47d8

            cmp DWORD PTR [R12+8],1
            jle .L_47d8

            mov RAX,QWORD PTR [R12]
            cmp BYTE PTR [RAX],35
            je .L_477c
.L_47d8:

            mov RBP,QWORD PTR [R12+16]
            test RBP,RBP
            je .L_4800

            test BYTE PTR [RBP+28],2
            je .L_4ab0
.L_47ec:

            mov EAX,DWORD PTR [RBP+32]
            cmp EAX,3
            je .L_477c

            cmp EAX,6
            je .L_477c

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_4800:

            test EBX,1048592
            je .L_4948
.L_480c:

            movsxd R15,DWORD PTR [R12+24]
            mov R14,QWORD PTR [R12]
            lea RDI,QWORD PTR [RIP+.L_e01c]
            lea RAX,QWORD PTR [R14+R15*1]
            mov RBP,R15
            movsx ESI,BYTE PTR [RAX]
            mov QWORD PTR [RSP+32],RAX
            mov BYTE PTR [RSP+16],SIL
            call QWORD PTR [RIP+strchr@GOTPCREL]

            mov EDX,DWORD PTR [R12+8]
            test RAX,RAX
            je .L_4865

            mov DWORD PTR [RSP+24],EDX
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov EDX,DWORD PTR [RSP+24]
            mov RSI,QWORD PTR [RAX]
            lea RAX,QWORD PTR [R14+R15*1+1]
            movsx RCX,BYTE PTR [RAX]
            test BYTE PTR [RSI+RCX*2+1],32
            jne .L_4a60
.L_4865:

            cmp EBP,EDX
            jge .L_477c

            mov DWORD PTR [RSP+24],EDX
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            lea RDI,QWORD PTR [R14+R15*1+1]
            mov EDX,DWORD PTR [RSP+24]
            mov R15D,EBP
            mov RSI,QWORD PTR [RAX]
            mov RCX,RAX
            movsx RAX,BYTE PTR [RSP+16]
            jmp .L_48a9
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4898:

            add R15D,1
            cmp R15D,EDX
            je .L_48c0

            movsx RAX,BYTE PTR [RDI]
            add RDI,1
.L_48a9:

            test BYTE PTR [RSI+RAX*2+1],32
            je .L_4898

            cmp R15D,EBP
            jle .L_477c

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_48c0:

            movsxd R9,R15D
            cmp BYTE PTR [R14+R9*1-1],46
            lea R10,QWORD PTR [R9-1]
            jne .L_477c

            and EBX,524304
            jne .L_49c0

            add EBP,2
            cmp EBP,R15D
            jne .L_49c0

            movsx RAX,BYTE PTR [RSP+16]
            test BYTE PTR [RSI+RAX*2+1],4
            je .L_49c0

            cmp R15D,EDX
            jge .L_491e

            add R9,R14
            jmp .L_4913
.L_4906:

            add R15D,1
            add R9,1
            cmp R15D,EDX
            je .L_491e
.L_4913:

            movsx RAX,BYTE PTR [R9]
            test BYTE PTR [RSI+RAX*2+1],32
            jne .L_4906
.L_491e:

            mov EAX,4
            cmp R15D,4
            cmovg R15D,EAX
            mov RAX,QWORD PTR [RSP+8]
            mov DWORD PTR [R13],R15D
            mov DWORD PTR [RAX],9
            mov EAX,9
            jmp .L_477e
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_4948:

            mov RAX,QWORD PTR [RSP+8]
            mov EDX,EBX
            mov RSI,R13
            mov RDI,R12
            mov DWORD PTR [RAX],1
            call .L_3db0

            test RAX,RAX
            je .L_49a0
.L_4965:

            mov EAX,6
            jmp .L_477e
          .byte 0x90
.L_4970:

            mov EAX,DWORD PTR [R12+32]
            lea EDX,DWORD PTR [RAX-2]
            cmp EDX,1
            jbe .L_477c

            cmp EAX,6
            je .L_477c

            jmp .L_47be
          .byte 0x90
.L_4990:

            mov ESI,EDX
            call .L_3ed0

            jmp .L_47b2
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_49a0:

            mov RAX,QWORD PTR [RSP+8]
            mov EDX,EBX
            mov RDI,R12
            mov DWORD PTR [RAX],2
            call .L_42a0

            test RAX,RAX
            je .L_480c

            jmp .L_4965
.L_49c0:

            mov QWORD PTR [RSP+40],RCX
            lea RSI,QWORD PTR [RSP+48]
            mov EDX,10
            mov RDI,QWORD PTR [RSP+32]
            mov QWORD PTR [RSP+24],R10
            mov QWORD PTR [RSP+16],R9
            call QWORD PTR [RIP+strtoul@GOTPCREL]

            mov RDX,QWORD PTR [R12]
            movsxd RAX,DWORD PTR [R12+24]
            mov RSI,QWORD PTR [RSP+48]
            mov R9,QWORD PTR [RSP+16]
            add RAX,RDX
            mov R10,QWORD PTR [RSP+24]
            mov RCX,QWORD PTR [RSP+40]
            cmp RSI,RAX
            jbe .L_477c

            add R10,RDX
            cmp RSI,R10
            jne .L_477c

            mov ESI,DWORD PTR [R12+8]
            cmp ESI,R15D
            jle .L_4a43

            mov RCX,QWORD PTR [RCX]
            add RDX,R9
            jmp .L_4a38
.L_4a2b:

            add R15D,1
            add RDX,1
            cmp ESI,R15D
            je .L_4a43
.L_4a38:

            movsx RAX,BYTE PTR [RDX]
            test BYTE PTR [RCX+RAX*2+1],32
            jne .L_4a2b
.L_4a43:

            mov RAX,QWORD PTR [RSP+8]
            mov DWORD PTR [R13],R15D
            mov DWORD PTR [RAX],8
            mov EAX,9
            jmp .L_477e
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_4a60:

            add EBP,1
            cmp EBP,EDX
            jl .L_4a7b

            jmp .L_4a86
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4a70:

            add EBP,1
            add RAX,1
            cmp EBP,EDX
            je .L_4a86
.L_4a7b:

            movsx RCX,BYTE PTR [RAX]
            test BYTE PTR [RSI+RCX*2+1],32
            jne .L_4a70
.L_4a86:

            cmp EBP,4
            mov EAX,4
            cmovg EBP,EAX
            mov RAX,QWORD PTR [RSP+8]
            mov DWORD PTR [R13],EBP
            mov DWORD PTR [RAX],7
            mov EAX,EBX
            sar EAX,31
            and EAX,4294967294
            add EAX,9
            jmp .L_477e
.L_4ab0:

            mov ESI,EBX
            mov RDI,RBP
            call .L_3ed0

            jmp .L_47ec
.L_4abf:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_4ad0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,40
.cfi_def_cfa_offset 96
            mov QWORD PTR [RSP+8],RCX
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+24],RAX
            xor EAX,EAX
            test RDI,RDI
            je .L_4c7b

            mov R13D,EDX
            mov EDX,DWORD PTR [RDI+28]
            mov R15,RDI
            mov EBP,ESI
            mov R14D,ESI
            mov R12D,4
.L_4b10:

            and EDX,4294967293
            mov ESI,R14D
            mov RDI,R15
            mov DWORD PTR [R15+28],EDX
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov R8,QWORD PTR [R15+16]
            cmp EBP,4
            cmovg EBP,R12D
            test R8,R8
            je .L_4c70

            mov RBX,R8
            jmp .L_4b4d
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_4b40:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_4c70
.L_4b4d:

            mov EAX,DWORD PTR [RBX+24]
            cmp EAX,DWORD PTR [RBX+8]
            je .L_4b40

            cmp R8,RBX
            je .L_4b6e

            cmp EBP,EAX
            jg .L_4bf2

            test R14D,R14D
            mov EBP,2
            cmovne EBP,R14D
.L_4b6e:

            mov EDX,DWORD PTR [RBX+28]
            cmp EAX,EBP
            jl .L_4b88
.L_4b75:

            cmp EBP,EAX
            mov R15,RBX
            cmovle EAX,EBP
            mov R14D,EAX
            jmp .L_4b10
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_4b88:

            and EDX,2
            je .L_4c48
.L_4b91:

            cmp DWORD PTR [RBX+36],2
            jg .L_4c30
.L_4b9b:

            lea RSI,QWORD PTR [RSP+20]
            mov EDX,R13D
            mov RDI,RBX
            mov RCX,RSI
            call .L_4740

            test EAX,EAX
            jne .L_4bd0

            cmp QWORD PTR [RSP+8],0
            je .L_4c24

            mov RAX,QWORD PTR [RSP+8]
            mov RDI,RBX
            call RAX

            test EAX,EAX
            je .L_4c24

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_4bd0:

            mov R14,QWORD PTR [RBX+16]
            test R14,R14
            je .L_4bee

            test BYTE PTR [R14+28],2
            je .L_4c58
.L_4be0:

            mov EAX,DWORD PTR [R14+32]
            cmp EAX,3
            je .L_4c1c

            cmp EAX,6
            je .L_4c1c
.L_4bee:

            mov R8,QWORD PTR [R15+16]
.L_4bf2:

            mov QWORD PTR [R15+16],0
.L_4bfa:

            mov RAX,QWORD PTR [RSP+24]
            sub RAX,QWORD PTR FS:[40]
            jne .L_4c83

            add RSP,40
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov RAX,R8
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
.L_4c1c:

.cfi_restore_state 
            mov DWORD PTR [RSP+20],1
.L_4c24:

            mov EAX,DWORD PTR [RBX+24]
            mov EDX,DWORD PTR [RBX+28]
            jmp .L_4b75
          .byte 0x90
.L_4c30:

            mov EAX,DWORD PTR [RBX+32]
            lea EDX,DWORD PTR [RAX-2]
            cmp EDX,1
            jbe .L_4bd0

            cmp EAX,6
            je .L_4bd0

            jmp .L_4b9b
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_4c48:

            mov ESI,R13D
            mov RDI,RBX
            call .L_3ed0

            jmp .L_4b91
.L_4c58:

            mov ESI,R13D
            mov RDI,R14
            call .L_3ed0

            jmp .L_4be0
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4c70:

            xor ESI,ESI
            mov RDI,R15
            call QWORD PTR [RIP+___mkd_freeLineRange@GOTPCREL]
.L_4c7b:

            xor R8D,R8D
            jmp .L_4bfa
.L_4c83:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_4c90:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            lea RAX,QWORD PTR [RIP+.L_12010]
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14,RDX
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov RBX,RDI
            sub RSP,24
.cfi_def_cfa_offset 80
            mov DWORD PTR [RDX],0
            cmp RSI,RAX
            je .L_4fb4

            mov EAX,DWORD PTR [RSI+12]
            mov R15,RSI
            xor EBP,EBP
            xor R13D,R13D
            test EAX,EAX
            jne .L_4e3a
.L_4ccd:

            test RBX,RBX
            jne .L_4ce3

            jmp .L_4d00
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_4cd8:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_4d00

            xor EBP,EBP
.L_4ce3:

            mov EAX,DWORD PTR [RBX+8]
            cmp EAX,EBP
            jle .L_4cd8

            mov RCX,QWORD PTR [RBX]
            lea EDX,DWORD PTR [RBP+1]
            movsxd RBP,EBP
            cmp BYTE PTR [RCX+RBP*1],60
            je .L_4d32

            mov EBP,EDX
            test RBX,RBX
            jne .L_4ce3
.L_4d00:

            mov DWORD PTR [R14],1
            xor EAX,EAX
.L_4d09:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4d20:

.cfi_restore_state 
            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_4e55

            mov EAX,DWORD PTR [RBX+8]
            xor EDX,EDX
.L_4d32:

            cmp EDX,EAX
            jge .L_4d20

            mov RDI,QWORD PTR [RBX]
            lea ECX,DWORD PTR [RDX+1]
            movsxd RDX,EDX
            movzx ESI,BYTE PTR [RDI+RDX*1]
            mov DWORD PTR [RSP+8],ESI
            cmp ESI,33
            je .L_4d66

            jmp .L_5021
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_4d58:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_4d00

            mov EAX,DWORD PTR [RBX+8]
            xor ECX,ECX
.L_4d66:

            cmp ECX,EAX
            jge .L_4d58

            mov RDX,QWORD PTR [RBX]
            lea EBP,DWORD PTR [RCX+1]
            movsxd RCX,ECX
            cmp BYTE PTR [RDX+RCX*1],45
            jne .L_4ccd

            jmp .L_4f96
.L_4d82:

            add R12D,1
            je .L_4d00

            xor EAX,EAX
            test RBX,RBX
            je .L_4d09

            mov R12D,EBP
.L_4d9a:

            mov R13D,DWORD PTR [RBX+8]
            cmp R12D,R13D
            jge .L_4e3a

            mov EDI,1
            mov ESI,40
            call QWORD PTR [RIP+calloc@GOTPCREL]

            mov RBP,RAX
            mov RAX,QWORD PTR [RBX+16]
            mov RDI,QWORD PTR [RBP]
            mov QWORD PTR [RBP+16],RAX
            mov EAX,R13D
            movsxd R13,R12D
            add R13,QWORD PTR [RBX]
            sub EAX,R12D
            mov QWORD PTR [RBX+16],RBP
            movsxd R14,EAX
            add EAX,DWORD PTR [RBP+12]
            mov DWORD PTR [RBP+12],EAX
            test RDI,RDI
            je .L_5122

            movsxd RSI,EAX
            call QWORD PTR [RIP+realloc@GOTPCREL]
.L_4df1:

            mov EDX,DWORD PTR [RBX+8]
            movsxd RCX,DWORD PTR [RBP+8]
            mov QWORD PTR [RBP],RAX
            mov RSI,R13
            sub EDX,R12D
            lea RDI,QWORD PTR [RAX+RCX*1]
            add EDX,ECX
            mov DWORD PTR [RBP+8],EDX
            mov RDX,R14
            call QWORD PTR [RIP+memcpy@GOTPCREL]

            mov EAX,DWORD PTR [RBP+12]
            cmp DWORD PTR [RBP+8],EAX
            jge .L_5101

            mov RAX,QWORD PTR [RBP]
.L_4e24:

            movsxd RDX,DWORD PTR [RBP+8]
            lea ECX,DWORD PTR [RDX+1]
            mov DWORD PTR [RBP+8],ECX
            mov BYTE PTR [RAX+RDX*1],0
            sub DWORD PTR [RBP+8],1
            mov DWORD PTR [RBX+8],R12D
.L_4e3a:

            mov RAX,QWORD PTR [RBX+16]
            mov QWORD PTR [RBX+16],0
            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
.L_4e55:

.cfi_restore_state 
            mov DWORD PTR [RSP+8],4294967295
            xor EBP,EBP
            mov R12,-1
.L_4e66:

            mov R8D,DWORD PTR [R15+8]
            test R8D,R8D
            jle .L_506b

            mov RDI,QWORD PTR [R15]
            mov DWORD PTR [RSP+12],R8D
            mov QWORD PTR [RSP],RDI
            call QWORD PTR [RIP+__ctype_toupper_loc@GOTPCREL]

            mov R8D,DWORD PTR [RSP+12]
            mov RDI,QWORD PTR [RSP]
            mov RCX,QWORD PTR [RAX]
            mov EAX,1
            lea R9D,DWORD PTR [R8+1]
            nop
            nop
            nop
            nop
            nop
            nop
.L_4ea0:

            movsx EDX,BYTE PTR [RDI+RAX*1-1]
            cmp EDX,DWORD PTR [RCX+R12*4]
            jne .L_4ccd

            mov EDX,EAX
            test RBX,RBX
            jne .L_4ecf

            jmp .L_4f78
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_4ec0:

            mov RBX,QWORD PTR [RBX+16]
            xor EBP,EBP
            test RBX,RBX
            je .L_4f78
.L_4ecf:

            cmp EBP,DWORD PTR [RBX+8]
            jge .L_4ec0

            mov R11,QWORD PTR [RBX]
            movsxd R10,EBP
            add EBP,1
            movzx R12D,BYTE PTR [R11+R10*1]
.L_4ee2:

            add RAX,1
            cmp R9,RAX
            jne .L_4ea0
.L_4eeb:

            cmp EDX,R8D
            jne .L_4ccd

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R8,RAX
            movsxd RAX,R12D
            mov RDX,QWORD PTR [R8]
            test BYTE PTR [RDX+RAX*2],8
            jne .L_4ccd

            xor EAX,EAX
            cmp DWORD PTR [RSP+8],47
            setne AL
            lea R13D,DWORD PTR [R13+RAX*2-1]
            test R13D,R13D
            jne .L_4ccd

            cmp R12D,62
            je .L_4d82

            cmp R12D,-1
            je .L_4d00
.L_4f39:

            test RBX,RBX
            jne .L_4f57

            jmp .L_4d00
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_4f48:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_4d00

            xor EBP,EBP
.L_4f57:

            cmp DWORD PTR [RBX+8],EBP
            jle .L_4f48

            mov RAX,QWORD PTR [RBX]
            lea R12D,DWORD PTR [RBP+1]
            movsxd RBP,EBP
            cmp BYTE PTR [RAX+RBP*1],62
            je .L_4d9a

            mov EBP,R12D
            jmp .L_4f39
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_4f78:

            mov R12,-1
            jmp .L_4ee2
.L_4f84:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_4d00

            mov EAX,DWORD PTR [RBX+8]
            xor EBP,EBP
.L_4f96:

            cmp EBP,EAX
            jge .L_4f84

            mov RDX,QWORD PTR [RBX]
            lea EAX,DWORD PTR [RBP+1]
            movsxd RBP,EBP
            cmp BYTE PTR [RDX+RBP*1],45
            je .L_50c7
.L_4fad:

            mov EBP,EAX
            jmp .L_4ccd
.L_4fb4:

            lea R12,QWORD PTR [RIP+.L_e020]
            test RDI,RDI
            je .L_4d00
.L_4fc4:

            mov R13,QWORD PTR [RBX]
            mov RSI,R12
            mov RDI,R13
            call QWORD PTR [RIP+strstr@GOTPCREL]

            test RAX,RAX
            je .L_5013

            sub RAX,R13
            mov R15D,DWORD PTR [RBX+8]
            lea EBP,DWORD PTR [RAX+3]
            cmp EBP,R15D
            jge .L_4e3a

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RCX,QWORD PTR [RAX]
            movsxd RAX,EBP
            jmp .L_5006
.L_4ff9:

            add RAX,1
            cmp R15D,EAX
            jle .L_4e3a
.L_5006:

            movsx RDX,BYTE PTR [R13+RAX*1]
            test BYTE PTR [RCX+RDX*2+1],32
            jne .L_4ff9
.L_5013:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            jne .L_4fc4

            jmp .L_4d00
.L_5021:

            cmp DWORD PTR [RSP+8],47
            je .L_5046

            movsxd R12,DWORD PTR [RSP+8]
            mov EBP,ECX
            jmp .L_4e66
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_5038:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_505d

            mov EAX,DWORD PTR [RBX+8]
            xor ECX,ECX
.L_5046:

            cmp ECX,EAX
            jge .L_5038

            mov RAX,QWORD PTR [RBX]
            lea EBP,DWORD PTR [RCX+1]
            movsxd RCX,ECX
            movzx R12D,BYTE PTR [RAX+RCX*1]
            jmp .L_4e66
.L_505d:

            xor EBP,EBP
            mov R12,-1
            jmp .L_4e66
.L_506b:

            xor EDX,EDX
            jmp .L_4eeb
.L_5072:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_4d00

            mov EAX,DWORD PTR [RBX+8]
            xor EDX,EDX
.L_5084:

            cmp EDX,EAX
            jge .L_5072

            mov RAX,QWORD PTR [RBX]
            lea EBP,DWORD PTR [RDX+1]
            movsxd RDX,EDX
            cmp BYTE PTR [RAX+RDX*1],45
            jne .L_50c9

            jmp .L_50af
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_50a0:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_4d00

            xor EBP,EBP
.L_50af:

            cmp DWORD PTR [RBX+8],EBP
            jle .L_50a0

            mov RDX,QWORD PTR [RBX]
            lea EAX,DWORD PTR [RBP+1]
            movsxd RBP,EBP
            cmp BYTE PTR [RDX+RBP*1],62
            je .L_4fad
.L_50c7:

            mov EBP,EAX
.L_50c9:

            test RBX,RBX
            jne .L_50e7

            jmp .L_4d00
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_50d8:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_4d00

            xor EBP,EBP
.L_50e7:

            mov EAX,DWORD PTR [RBX+8]
            cmp EAX,EBP
            jle .L_50d8

            mov RCX,QWORD PTR [RBX]
            lea EDX,DWORD PTR [RBP+1]
            movsxd RBP,EBP
            cmp BYTE PTR [RCX+RBP*1],45
            je .L_5084

            mov EBP,EDX
            jmp .L_50c9
.L_5101:

            mov RDI,QWORD PTR [RBP]
            add EAX,100
            mov DWORD PTR [RBP+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_5130

            call QWORD PTR [RIP+realloc@GOTPCREL]
.L_5119:

            mov QWORD PTR [RBP],RAX
            jmp .L_4e24
.L_5122:

            movsxd RDI,EAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            jmp .L_4df1
.L_5130:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            jmp .L_5119
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_firstnonblank
.type mkd_firstnonblank, @function
#-----------------------------------
mkd_firstnonblank:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            sub RSP,8
.cfi_def_cfa_offset 32
            mov EBP,DWORD PTR [RDI+8]
            mov RBX,QWORD PTR [RDI]
            test EBP,EBP
            jle .L_518a

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            xor R8D,R8D
            mov RCX,QWORD PTR [RAX]
            mov RAX,RBX
            jmp .L_5175
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_5168:

            add R8D,1
            add RAX,1
            cmp EBP,R8D
            je .L_5180
.L_5175:

            movsx RDX,BYTE PTR [RAX]
            test BYTE PTR [RCX+RDX*2+1],32
            jne .L_5168
.L_5180:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 24
            mov EAX,R8D
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
.L_518a:

.cfi_restore_state 
            xor R8D,R8D
            jmp .L_5180
.cfi_endproc 

            nop
#-----------------------------------
.align 16
.globl ___mkd_tidy
.type ___mkd_tidy, @function
#-----------------------------------
___mkd_tidy:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            sub RSP,8
.cfi_def_cfa_offset 32
            mov EBX,DWORD PTR [RDI+8]
            test EBX,EBX
            je .L_51e0

            mov RBP,RDI
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            lea EDX,DWORD PTR [RBX-1]
            movsxd RCX,EBX
            mov RSI,QWORD PTR [RBP]
            mov RDI,QWORD PTR [RAX]
            sub RCX,2
            movsxd RAX,EDX
            mov EDX,EDX
            sub RCX,RDX
            jmp .L_51d4
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_51c8:

            mov DWORD PTR [RBP+8],EAX
            sub RAX,1
            cmp RCX,RAX
            je .L_51e0
.L_51d4:

            movsx RDX,BYTE PTR [RSI+RAX*1]
            test BYTE PTR [RDI+RDX*2+1],32
            jne .L_51c8
.L_51e0:

            add RSP,8
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_51f0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,RDI
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,168
.cfi_def_cfa_offset 224
            mov QWORD PTR [RSP+72],RDI
            mov DWORD PTR [RSP+12],ESI
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+152],RAX
            xor EAX,EAX
            mov DWORD PTR [RSP+4],0
            mov QWORD PTR [RSP+96],0
            mov QWORD PTR [RSP+104],0
            test RDI,RDI
            je .L_53bd

            mov R14,RDX
.L_5246:

            mov EAX,DWORD PTR [RDI+24]
            cmp DWORD PTR [RDI+8],EAX
            jle .L_55e4

            movzx EAX,BYTE PTR [RSP+12]
            mov QWORD PTR [RSP+72],RDI
            mov RBP,RDI
            mov DWORD PTR [RSP+8],0
            and EAX,1
            mov BYTE PTR [RSP+19],AL
            lea RAX,QWORD PTR [RSP+112]
            mov QWORD PTR [RSP+32],RAX
.L_5278:

            cmp DWORD PTR [RBP+24],3
            mov R12D,DWORD PTR [R14+80]
            jle .L_53f0

            mov ESI,1
            mov EDI,56
            call QWORD PTR [RIP+calloc@GOTPCREL]

            cmp QWORD PTR [RSP+96],0
            mov QWORD PTR [RAX+16],RBP
            mov R15,RAX
            mov DWORD PTR [RAX+40],1
            je .L_5616

            mov RAX,QWORD PTR [RSP+104]
            mov QWORD PTR [RAX],R15
            mov QWORD PTR [RSP+104],R15
.L_52bd:

            and R12D,8192
            jne .L_5604
.L_52ca:

            test RBP,RBP
            je .L_5316

            nop
.L_52d0:

            mov ESI,4
            mov RDI,RBP
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov RBX,QWORD PTR [RBP+16]
            test RBX,RBX
            jne .L_52fd

            jmp .L_55c8
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_52f0:

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            je .L_55c8
.L_52fd:

            mov EAX,DWORD PTR [RBX+24]
            cmp EAX,DWORD PTR [RBX+8]
            je .L_52f0

            cmp EAX,3
            jle .L_55c8

            mov RBP,RBX
            test RBP,RBP
            jne .L_52d0
.L_5316:

            mov QWORD PTR [RSP+72],RBP
.L_531b:

            mov EAX,DWORD PTR [RSP+4]
            or EAX,DWORD PTR [RSP+12]
            je .L_5335

            mov EDI,DWORD PTR [R15+44]
            test EDI,EDI
            jne .L_5335

            mov DWORD PTR [R15+44],1
.L_5335:

            add DWORD PTR [RSP+8],1
            mov EAX,DWORD PTR [RSP+8]
            cmp EAX,1
            setg BL
            or BL,BYTE PTR [RSP+19]
            test RBP,RBP
            je .L_5c69

            mov RDI,RBP
            mov DWORD PTR [RSP+4],0
            mov EAX,DWORD PTR [RDI+24]
            cmp DWORD PTR [RDI+8],EAX
            jg .L_5387
.L_5364:

            mov R12,QWORD PTR [RDI+16]
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]

            add DWORD PTR [RSP+4],1
            test R12,R12
            je .L_5c93

            mov RDI,R12
            mov EAX,DWORD PTR [RDI+24]
            cmp DWORD PTR [RDI+8],EAX
            jle .L_5364
.L_5387:

            mov ECX,DWORD PTR [RSP+4]
            mov QWORD PTR [RSP+72],RDI
            mov RBP,RDI
            test ECX,ECX
            je .L_5278

            mov EAX,DWORD PTR [R15+44]
            test EAX,EAX
            jne .L_5278
.L_53a7:

            mov DWORD PTR [R15+44],1
            test RBP,RBP
            jne .L_5278
.L_53b8:

            mov R13,QWORD PTR [RSP+96]
.L_53bd:

            mov RAX,QWORD PTR [RSP+152]
            sub RAX,QWORD PTR FS:[40]
            jne .L_618b

            add RSP,168
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov RAX,R13
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_53f0:

.cfi_restore_state 
            test R12D,33554432
            je .L_5412

            test BYTE PTR [RBP+28],2
            je .L_58e5
.L_5403:

            mov EAX,DWORD PTR [RBP+32]
            sub EAX,4
            cmp EAX,1
            jbe .L_5850
.L_5412:

            mov RBX,QWORD PTR [RSP+72]
            mov RBP,RBX
            test BYTE PTR [RBX+28],2
            je .L_58f9
.L_5424:

            cmp DWORD PTR [RBX+36],2
            jg .L_5882
.L_542e:

            mov EDX,DWORD PTR [R14+80]
            lea RCX,QWORD PTR [RSP+80]
            lea RSI,QWORD PTR [RSP+84]
            mov RDI,RBP
            call .L_4740

            mov R12,QWORD PTR [RSP+72]
            mov DWORD PTR [RSP+20],EAX
            test EAX,EAX
            je .L_5625

            mov RBX,QWORD PTR [RSP+96]
            cmp EAX,6
            je .L_5917

            mov EBP,DWORD PTR [RSP+80]
            mov ESI,1
            mov EDI,56
            call QWORD PTR [RIP+calloc@GOTPCREL]

            mov QWORD PTR [RAX+16],R12
            mov R15,RAX
            mov DWORD PTR [RAX+40],EBP
            test RBX,RBX
            je .L_5d03

            mov RAX,QWORD PTR [RSP+104]
            mov QWORD PTR [RAX],R15
            mov QWORD PTR [RSP+104],R15
.L_5497:

            mov EBP,DWORD PTR [RSP+84]
            mov DWORD PTR [RSP+88],EBP
            test R12,R12
            je .L_617b

            lea RAX,QWORD PTR [RSP+92]
            mov QWORD PTR [RSP+48],R15
            xor R13D,R13D
            xor EBX,EBX
            mov QWORD PTR [RSP+24],0
            mov QWORD PTR [RSP+40],RAX
            mov EAX,EBP
            mov RBP,R12
            mov R12D,EAX
            nop
            nop
            nop
.L_54d0:

            mov ESI,1
            mov EDI,56
            call QWORD PTR [RIP+calloc@GOTPCREL]

            cmp QWORD PTR [RSP+24],0
            mov QWORD PTR [RAX+16],RBP
            mov R15,RAX
            mov DWORD PTR [RAX+40],10
            je .L_5c5f

            mov QWORD PTR [R13],RAX
.L_54fe:

            mov EDX,DWORD PTR [R14+80]
            xor ECX,ECX
            mov ESI,R12D
            mov RDI,RBP
            call .L_4ad0

            mov RDI,QWORD PTR [R15+16]
            mov RDX,R14
            xor ESI,ESI
            mov R13,RAX
            call .L_51f0

            mov QWORD PTR [R15+16],0
            mov QWORD PTR [R15+8],RAX
            test RAX,RAX
            je .L_553c

            test EBX,EBX
            je .L_553c

            mov DWORD PTR [RAX+44],1
.L_553c:

            test R13,R13
            je .L_5c4b

            mov RBP,R13
            jmp .L_555d
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_5550:

            mov RBP,QWORD PTR [RBP+16]
            test RBP,RBP
            je .L_5c4b
.L_555d:

            mov EAX,DWORD PTR [RBP+8]
            cmp DWORD PTR [RBP+24],EAX
            je .L_5550

            mov EDX,DWORD PTR [R14+80]
            mov RCX,QWORD PTR [RSP+40]
            lea RSI,QWORD PTR [RSP+88]
            mov RDI,RBP
            call .L_4740

            cmp DWORD PTR [RSP+20],EAX
            jne .L_5c4b

            xor EBX,EBX
            cmp R13,RBP
            je .L_55b7

            mov QWORD PTR [RSP+128],R13
            mov RDI,QWORD PTR [RSP+32]
            mov RSI,RBP
            call QWORD PTR [RIP+___mkd_freeLineRange@GOTPCREL]

            mov RAX,QWORD PTR [R15+8]
            test RAX,RAX
            je .L_55b2

            mov DWORD PTR [RAX+44],1
.L_55b2:

            mov EBX,1
.L_55b7:

            mov R12D,DWORD PTR [RSP+88]
            mov R13,R15
            jmp .L_54d0
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_55c8:

            mov RDI,RBP
            mov RSI,RBX
            call QWORD PTR [RIP+___mkd_freeLineRange@GOTPCREL]

            mov QWORD PTR [RBP+16],0
            mov RBP,RBX
            jmp .L_5316
.L_55e4:

            mov RBX,QWORD PTR [RDI+16]
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]

            add DWORD PTR [RSP+4],1
            test RBX,RBX
            je .L_53b8

            mov RDI,RBX
            jmp .L_5246
.L_5604:

            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_tidy@GOTPCREL]

            mov RBP,QWORD PTR [R15+16]
            jmp .L_52ca
.L_5616:

            mov QWORD PTR [RSP+104],RAX
            mov QWORD PTR [RSP+96],RAX
            jmp .L_52bd
.L_5625:

            movsxd RBP,DWORD PTR [R12+24]
            mov EBX,DWORD PTR [R14+80]
            cmp EBP,3
            jg .L_5659

            mov RAX,QWORD PTR [R12]
            movsxd RDX,EBP
            cmp BYTE PTR [RAX+RDX*1],62
            je .L_5e53

            test EBP,EBP
            jne .L_5659

            cmp DWORD PTR [R12+8],1
            jle .L_5659

            cmp BYTE PTR [RAX],35
            je .L_5b0d
.L_5659:

            mov RBP,QWORD PTR [R12+16]
            test RBP,RBP
            je .L_5686

            test BYTE PTR [RBP+28],2
            je .L_5d12
.L_566d:

            mov EAX,DWORD PTR [RBP+32]
            cmp EAX,3
            je .L_5b08

            cmp EAX,6
            je .L_5b08

            mov EBX,DWORD PTR [R14+80]
.L_5686:

            mov DWORD PTR [RSP+92],1
            mov ESI,1
            mov EDI,56
            call QWORD PTR [RIP+calloc@GOTPCREL]

            cmp QWORD PTR [RSP+96],0
            mov QWORD PTR [RAX+16],R12
            mov R15,RAX
            mov DWORD PTR [RAX+40],3
            je .L_600a

            mov RAX,QWORD PTR [RSP+104]
            mov QWORD PTR [RAX],R15
            mov QWORD PTR [RSP+104],R15
.L_56c5:

            test BL,8
            je .L_5df8
.L_56ce:

            test R12,R12
            je .L_6209

            mov R13D,EBX
.L_56da:

            mov RBP,QWORD PTR [R12+16]
            test RBP,RBP
            je .L_56f0

            mov EAX,DWORD PTR [RBP+24]
            cmp DWORD PTR [RBP+8],EAX
            jg .L_5d55
.L_56f0:

            mov RCX,QWORD PTR [R15+16]
            mov RBX,RCX
            test RCX,RCX
            je .L_60ba

            movsxd RDX,DWORD PTR [R12+8]
            xor EAX,EAX
            cmp EDX,2
            jle .L_5770

            mov RDI,QWORD PTR [RCX]
            cmp BYTE PTR [RDI],45
            jne .L_61c0

            cmp BYTE PTR [RDI+1],62
            jne .L_61c0

            mov RAX,QWORD PTR [R12]
            lea RAX,QWORD PTR [RAX+RDX*1-2]
            cmp BYTE PTR [RAX],60
            jne .L_61c0

            movzx EDX,BYTE PTR [RAX+1]
            sub EDX,45
            jne .L_61c0

            mov EAX,DWORD PTR [RCX+8]
            cmp EAX,1
            jle .L_5762

            sub EAX,1
            lea RSI,QWORD PTR [RDI+2]
            movsxd RDX,EAX
            call QWORD PTR [RIP+memmove@GOTPCREL]

            mov RCX,QWORD PTR [R15+16]
            mov EDX,2
.L_5762:

            sub DWORD PTR [RBX+8],EDX
            mov EAX,2
            sub DWORD PTR [R12+8],2
.L_5770:

            mov DWORD PTR [R15+44],EAX
            mov QWORD PTR [RSP+72],RBP
            mov QWORD PTR [R12+16],0
            test DWORD PTR [R14+80],1040
            jne .L_531b

            test RCX,RCX
            je .L_531b

            mov RSI,QWORD PTR [RCX+16]
            test RSI,RSI
            je .L_531b

            cmp QWORD PTR [RSI+16],0
            je .L_531b

            mov RAX,RCX
.L_57b4:

            test BYTE PTR [RAX+28],1
            je .L_531b

            mov RAX,QWORD PTR [RAX+16]
            test RAX,RAX
            jne .L_57b4

            movsxd RAX,DWORD PTR [RCX+24]
            mov RCX,QWORD PTR [RCX]
            cmp BYTE PTR [RCX+RAX*1],124
            mov RDX,RAX
            je .L_6219
.L_57db:

            mov R13D,DWORD PTR [RSI+24]
            mov R12D,DWORD PTR [RSI+8]
            cmp R13D,R12D
            jge .L_583d

            mov RBX,QWORD PTR [RSI]
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            lea EDX,DWORD PTR [R12-1]
            mov RSI,QWORD PTR [RAX]
            movsxd RAX,R13D
            sub EDX,R13D
            add RAX,RBX
            lea RCX,QWORD PTR [RAX+RDX*1+1]
.L_5807:

            movsx RDI,BYTE PTR [RAX]
            mov RDX,RDI
            test BYTE PTR [RSI+RDI*2+1],32
            jne .L_5834

            cmp DL,45
            movsx EDI,DIL
            setne R8B
            cmp DL,58
            setne DL
            test R8B,DL
            je .L_5834

            cmp EDI,124
            jne .L_531b
.L_5834:

            add RAX,1
            cmp RCX,RAX
            jne .L_5807
.L_583d:

            mov DWORD PTR [R15+40],13
            jmp .L_531b
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_5850:

            cmp DWORD PTR [RBP+36],2
            jle .L_5412

            lea RSI,QWORD PTR [RSP+72]
            lea RDI,QWORD PTR [RSP+96]
            mov EDX,R12D
            call .L_4560

            mov R15,RAX
            test RAX,RAX
            je .L_590e
.L_5878:

            mov RBP,QWORD PTR [RSP+72]
            jmp .L_531b
.L_5882:

            mov EAX,DWORD PTR [RBX+32]
            lea EDX,DWORD PTR [RAX-2]
            cmp EDX,1
            jbe .L_5896

            cmp EAX,6
            jne .L_542e
.L_5896:

            mov ESI,1
            mov EDI,56
            call QWORD PTR [RIP+calloc@GOTPCREL]

            cmp QWORD PTR [RSP+96],0
            mov DWORD PTR [RAX+40],12
            mov R15,RAX
            je .L_5cbf

            mov RAX,QWORD PTR [RSP+104]
            mov QWORD PTR [RAX],R15
            mov QWORD PTR [RSP+104],R15
.L_58c9:

            mov RAX,QWORD PTR [RBP+16]
            mov RDI,RBP
            mov QWORD PTR [RSP+72],RAX
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]

            mov RBP,QWORD PTR [RSP+72]
            jmp .L_531b
.L_58e5:

            mov ESI,R12D
            mov RDI,RBP
            call .L_3ed0

            mov R12D,DWORD PTR [R14+80]
            jmp .L_5403
.L_58f9:

            mov ESI,R12D
            mov RDI,RBX
            call .L_3ed0

            mov RBP,QWORD PTR [RSP+72]
            jmp .L_5424
.L_590e:

            mov R12D,DWORD PTR [R14+80]
            jmp .L_5412
.L_5917:

            mov ESI,1
            mov EDI,56
            call QWORD PTR [RIP+calloc@GOTPCREL]

            mov QWORD PTR [RAX+16],R12
            mov R15,RAX
            mov DWORD PTR [RAX+40],6
            test RBX,RBX
            je .L_6060

            mov RAX,QWORD PTR [RSP+104]
            mov QWORD PTR [RAX],R15
            mov QWORD PTR [RSP+104],R15
.L_594b:

            mov EAX,DWORD PTR [RSP+84]
            mov DWORD PTR [RSP+48],EAX
            test R12,R12
            je .L_617b

            lea RAX,QWORD PTR [RSP+92]
            xor R13D,R13D
            mov QWORD PTR [RSP+56],R15
            mov QWORD PTR [RSP+24],0
            mov R15,R13
            mov QWORD PTR [RSP+64],RAX
.L_597a:

            mov R10D,DWORD PTR [R14+80]
            mov RSI,QWORD PTR [RSP+64]
            mov RDI,R12
            mov EDX,R10D
            call .L_3db0

            mov DWORD PTR [RSP+40],1
            mov RBX,RAX
            test RAX,RAX
            je .L_5cda
.L_59a2:

            mov RBP,QWORD PTR [RBX+16]
            test RBP,RBP
            je .L_5c30

            mov R13,RBP
            jmp .L_59c5
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_59b8:

            mov R13,QWORD PTR [R13+16]
            test R13,R13
            je .L_5cce
.L_59c5:

            mov EAX,DWORD PTR [R13+8]
            cmp DWORD PTR [R13+24],EAX
            je .L_59b8

            xor EAX,EAX
            cmp RBP,R13
            setne AL
            mov DWORD PTR [RSP+20],EAX
            je .L_59e9

            mov RSI,R13
            mov RDI,RBX
            call QWORD PTR [RIP+___mkd_freeLineRange@GOTPCREL]
.L_59e9:

            cmp DWORD PTR [RSP+40],1
            mov QWORD PTR [RBX+16],0
            je .L_6028
.L_59fc:

            lea RAX,QWORD PTR [RIP+.L_4150]
            cmp DWORD PTR [RSP+40],2
            mov EBX,0
            cmove RBX,RAX
            mov RAX,R15
            mov R15,RBX
            mov RBX,RAX
            nop
            nop
            nop
            nop
            nop
            nop
.L_5a20:

            mov ESI,1
            mov EDI,56
            mov RBP,RBX
            call QWORD PTR [RIP+calloc@GOTPCREL]

            cmp QWORD PTR [RSP+24],0
            mov QWORD PTR [RAX+16],R13
            mov RBX,RAX
            mov DWORD PTR [RAX+40],10
            je .L_5c55

            mov QWORD PTR [RBP],RAX
.L_5a51:

            mov EDX,DWORD PTR [R14+80]
            mov ESI,DWORD PTR [RSP+48]
            mov RCX,R15
            mov RDI,R13
            call .L_4ad0

            mov RDI,QWORD PTR [RBX+16]
            mov RDX,R14
            xor ESI,ESI
            mov RBP,RAX
            call .L_51f0

            mov QWORD PTR [RBX+16],R12
            mov QWORD PTR [RBX+8],RAX
            test RAX,RAX
            je .L_5a93

            mov R11D,DWORD PTR [RSP+20]
            test R11D,R11D
            je .L_5a93

            mov DWORD PTR [RAX+44],1
.L_5a93:

            test RBP,RBP
            je .L_5c30

            mov R13,RBP
            jmp .L_5ab5
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_5aa8:

            mov R13,QWORD PTR [R13+16]
            test R13,R13
            je .L_5c30
.L_5ab5:

            mov EAX,DWORD PTR [R13+8]
            cmp DWORD PTR [R13+24],EAX
            je .L_5aa8

            xor EAX,EAX
            cmp RBP,R13
            setne AL
            mov DWORD PTR [RSP+20],EAX
            je .L_5ae3

            mov QWORD PTR [RSP+128],RBP
            mov RDI,QWORD PTR [RSP+32]
            mov RSI,R13
            call QWORD PTR [RIP+___mkd_freeLineRange@GOTPCREL]
.L_5ae3:

            cmp DWORD PTR [RSP+40],2
            jne .L_5afd

            mov RDI,R13
            xor R12D,R12D
            call .L_4150

            test EAX,EAX
            jne .L_5a20
.L_5afd:

            mov R15,RBX
            mov R12,R13
            jmp .L_597a
.L_5b08:

            mov EBP,1
.L_5b0d:

            mov ESI,1
            mov EDI,56
            call QWORD PTR [RIP+calloc@GOTPCREL]

            cmp QWORD PTR [RSP+96],0
            mov QWORD PTR [RAX+16],R12
            mov R15,RAX
            mov DWORD PTR [RAX+40],11
            je .L_6019

            mov RAX,QWORD PTR [RSP+104]
            mov QWORD PTR [RAX],R15
            mov QWORD PTR [RSP+104],R15
.L_5b44:

            cmp EBP,1
            je .L_5d26

            mov R13,QWORD PTR [R12]
            mov EBX,DWORD PTR [R12+8]
            movzx ECX,BYTE PTR [R13]
            lea EDX,DWORD PTR [RBX-1]
            lea RAX,QWORD PTR [R13+1]
.L_5b62:

            cmp EBP,EDX
            jge .L_5b77

            cmp EBP,6
            je .L_5b77

            add RAX,1
            add EBP,1
            cmp BYTE PTR [RAX-1],CL
            je .L_5b62
.L_5b77:

            mov DWORD PTR [R15+48],EBP
            cmp EBX,EBP
            jle .L_6134

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RCX,QWORD PTR [RAX]
            movsxd RAX,EBP
            add RAX,R13
            jmp .L_5ba3
.L_5b94:

            add EBP,1
            add RAX,1
            cmp EBX,EBP
            je .L_6159
.L_5ba3:

            movsx RDX,BYTE PTR [RAX]
            mov RSI,RAX
            test BYTE PTR [RCX+RDX*2+1],32
            jne .L_5b94

            test EBP,EBP
            jg .L_615f

            and DWORD PTR [R12+28],4294967293
            mov DWORD PTR [R12+8],EBX
            cmp EBX,1
            je .L_5bec
.L_5bc9:

            movsxd RAX,EBX
            jmp .L_5bda
.L_5bce:

            lea EBX,DWORD PTR [RAX-1]
            sub RAX,1
            cmp EAX,1
            jle .L_5bec
.L_5bda:

            cmp BYTE PTR [R13+RAX*1-1],35
            mov EBX,EAX
            je .L_5bce
.L_5be4:

            test EBX,EBX
            je .L_5c13

            mov R13,QWORD PTR [R12]
.L_5bec:

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RCX,QWORD PTR [RAX]
            movsxd RAX,EBX
            jmp .L_5c04
.L_5bfa:

            sub RAX,1
            mov EBX,EAX
            test EAX,EAX
            je .L_5c13
.L_5c04:

            movsx RDX,BYTE PTR [R13+RAX*1-1]
            mov EBX,EAX
            test BYTE PTR [RCX+RDX*2+1],32
            jne .L_5bfa
.L_5c13:

            mov RBP,QWORD PTR [R12+16]
            mov DWORD PTR [R12+8],EBX
            mov QWORD PTR [R12+16],0
            jmp .L_5316
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_5c30:

            mov R15,QWORD PTR [RSP+56]
.L_5c35:

            mov RAX,QWORD PTR [RSP+24]
            mov QWORD PTR [R15+16],0
            mov QWORD PTR [R15+8],RAX
            jmp .L_5316
.L_5c4b:

            mov R15,QWORD PTR [RSP+48]
            mov RBP,R13
            jmp .L_5c35
.L_5c55:

            mov QWORD PTR [RSP+24],RAX
            jmp .L_5a51
.L_5c5f:

            mov QWORD PTR [RSP+24],RAX
            jmp .L_54fe
.L_5c69:

            mov QWORD PTR [RSP+72],0
            test BL,BL
            je .L_53b8

            mov ESI,DWORD PTR [R15+44]
            test ESI,ESI
            jne .L_53b8

            mov DWORD PTR [R15+44],1
            jmp .L_53b8
.L_5c93:

            mov QWORD PTR [RSP+72],0
            test BL,BL
            je .L_53b8

            mov EDX,DWORD PTR [R15+44]
            test EDX,EDX
            jne .L_53b8

            mov DWORD PTR [RSP+4],1
            xor EBP,EBP
            jmp .L_53a7
.L_5cbf:

            mov QWORD PTR [RSP+104],RAX
            mov QWORD PTR [RSP+96],RAX
            jmp .L_58c9
.L_5cce:

            mov R15,QWORD PTR [RSP+56]
            xor EBP,EBP
            jmp .L_5c35
.L_5cda:

            mov RSI,QWORD PTR [RSP+64]
            mov EDX,R10D
            mov RDI,R12
            call .L_42a0

            mov RBX,RAX
            test RAX,RAX
            je .L_6198

            mov DWORD PTR [RSP+40],2
            jmp .L_59a2
.L_5d03:

            mov QWORD PTR [RSP+104],RAX
            mov QWORD PTR [RSP+96],RAX
            jmp .L_5497
.L_5d12:

            mov ESI,EBX
            mov RDI,RBP
            call .L_3ed0

            mov R12,QWORD PTR [RSP+72]
            jmp .L_566d
.L_5d26:

            mov RDI,QWORD PTR [R12+16]
            mov RAX,QWORD PTR [RDI]
            cmp BYTE PTR [RAX],61
            mov EAX,2
            cmovne EBP,EAX
            mov DWORD PTR [R15+48],EBP
            mov RBP,QWORD PTR [RDI+16]
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]

            mov QWORD PTR [R12+16],0
            jmp .L_5316
.L_5d55:

            test BYTE PTR [RBP+28],2
            je .L_608c
.L_5d5f:

            cmp DWORD PTR [RBP+36],2
            jg .L_606f
.L_5d69:

            movsxd RAX,DWORD PTR [RBP+24]
            test EAX,EAX
            jne .L_60d5

            cmp DWORD PTR [RBP+8],1
            jle .L_5d88

            mov RCX,QWORD PTR [RBP]
            cmp BYTE PTR [RCX],35
            je .L_56f0
.L_5d88:

            mov RBX,QWORD PTR [RBP+16]
            test RBX,RBX
            je .L_5db9
.L_5d91:

            test BYTE PTR [RBX+28],2
            je .L_61f9
.L_5d9b:

            mov EAX,DWORD PTR [RBX+32]
            cmp EAX,3
            je .L_56f0

            cmp EAX,6
            je .L_56f0

            movsxd RAX,DWORD PTR [RBP+24]
.L_5db4:

            cmp EAX,3
            jg .L_5dc7
.L_5db9:

            mov RCX,QWORD PTR [RBP]
            cmp BYTE PTR [RCX+RAX*1],62
            je .L_56f0
.L_5dc7:

            mov R8D,DWORD PTR [RSP+12]
            test R8D,R8D
            je .L_5dd9
.L_5dd1:

            mov R12,RBP
            jmp .L_56da
.L_5dd9:

            mov RCX,QWORD PTR [RSP+32]
            mov EDX,R13D
            mov RDI,RBP
            mov RSI,RCX
            call .L_4740

            test EAX,EAX
            je .L_5dd1

            jmp .L_56f0

            nop
            nop
            nop
.L_5df8:

            mov RDI,R12
            call .L_41b0

            mov RSI,RAX
            test RAX,RAX
            je .L_5e38

            lea RAX,QWORD PTR [RSP+92]
            mov RDI,QWORD PTR [R15+16]
            mov RDX,RAX
            call .L_4c90

            mov R10D,DWORD PTR [RSP+92]
            mov QWORD PTR [RSP+72],RAX
            mov RBP,RAX
            test R10D,R10D
            jne .L_5e46

            mov DWORD PTR [R15+40],4
            jmp .L_531b
.L_5e38:

            mov R9D,DWORD PTR [RSP+92]
            test R9D,R9D
            je .L_5878
.L_5e46:

            mov EBX,DWORD PTR [R14+80]
            mov R12,QWORD PTR [R15+16]
            jmp .L_56ce
.L_5e53:

            mov ESI,1
            mov EDI,56
            call QWORD PTR [RIP+calloc@GOTPCREL]

            cmp QWORD PTR [RSP+96],0
            mov QWORD PTR [RAX+16],R12
            mov R15,RAX
            mov DWORD PTR [RAX+40],2
            je .L_60e7

            mov RAX,QWORD PTR [RSP+104]
            mov QWORD PTR [RAX],R15
            mov QWORD PTR [RSP+104],R15
.L_5e8a:

            and EBX,262160
.L_5e90:

            cmp EBP,3
            jg .L_5ea3

            mov RAX,QWORD PTR [R12]
            cmp BYTE PTR [RAX+RBP*1],62
            je .L_60f6
.L_5ea3:

            mov RAX,QWORD PTR [R12+16]
            test RAX,RAX
            je .L_61e0

            mov R13,RAX
            jmp .L_5ebf
.L_5eb6:

            mov R13,QWORD PTR [R13+16]
            test R13,R13
            je .L_5eea
.L_5ebf:

            movsxd RBP,DWORD PTR [R13+24]
            mov ESI,DWORD PTR [R13+8]
            cmp EBP,ESI
            je .L_5eb6

            cmp RAX,R13
            je .L_6002

            cmp EBP,3
            jg .L_5eea

            mov RDI,QWORD PTR [R13]
            movsxd RAX,EBP
            cmp BYTE PTR [RDI+RAX*1],62
            je .L_609c
.L_5eea:

            mov RDI,R12
            mov RSI,R13
            call QWORD PTR [RIP+___mkd_freeLineRange@GOTPCREL]

            mov R12,QWORD PTR [R15+16]
            test EBX,EBX
            jne .L_5fd7
.L_5f02:

            mov RBP,QWORD PTR [R12]
            mov EBX,DWORD PTR [R12+8]
            xor EDX,EDX
            mov ESI,EBX
            mov RDI,RBP
            call .L_4400

            test EAX,EAX
            je .L_5fd7

            mov RAX,QWORD PTR [R12+16]
            lea RDI,QWORD PTR [RBP+1]
            mov EDX,3
            lea RSI,QWORD PTR [RIP+.L_e011]
            mov QWORD PTR [RSP+24],RDI
            mov QWORD PTR [R15+16],RAX
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            test EAX,EAX
            je .L_61c7

            mov EDX,6
            mov RDI,QWORD PTR [RSP+24]
            lea RSI,QWORD PTR [RIP+.L_e015]
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            mov ECX,6
            lea RDX,QWORD PTR [RIP+.L_e027]
            mov R8D,EAX
            mov EAX,5
            test R8D,R8D
            cmovne ECX,DWORD PTR [RSP+20]
            mov DWORD PTR [RSP+20],ECX
.L_5f82:

            movsxd RCX,EBX
            mov QWORD PTR [RSP+24],RDX
            lea RDI,QWORD PTR [RAX+RCX*1+4]
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov QWORD PTR [R15+24],RAX
            mov RDI,RAX
            test RAX,RAX
            je .L_5fca

            mov ECX,DWORD PTR [RSP+20]
            mov RDX,QWORD PTR [RSP+24]
            lea EAX,DWORD PTR [RCX+1]
            lea ESI,DWORD PTR [RCX+2]
            mov ECX,EBX
            cdqe 
            sub ECX,ESI
            lea RSI,QWORD PTR [RIP+.L_e02d]
            lea R8,QWORD PTR [RBP+RAX*1]
            xor EAX,EAX
            call QWORD PTR [RIP+sprintf@GOTPCREL]
.L_5fca:

            mov RDI,R12
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]

            mov R12,QWORD PTR [R15+16]
.L_5fd7:

            mov RDX,R14
            mov ESI,1
            mov RDI,R12
            mov QWORD PTR [RSP+72],R13
            call .L_51f0

            mov QWORD PTR [R15+16],0
            mov RBP,QWORD PTR [RSP+72]
            mov QWORD PTR [R15+8],RAX
            jmp .L_531b
.L_6002:

            mov R12,RAX
            jmp .L_5e90
.L_600a:

            mov QWORD PTR [RSP+104],RAX
            mov QWORD PTR [RSP+96],RAX
            jmp .L_56c5
.L_6019:

            mov QWORD PTR [RSP+104],RAX
            mov QWORD PTR [RSP+96],RAX
            jmp .L_5b44
.L_6028:

            mov RBX,R12
.L_602b:

            mov EAX,DWORD PTR [RBX+8]
            test EAX,EAX
            jle .L_6048

            mov RDI,QWORD PTR [RBX]
            movsxd RDX,EAX
            lea RSI,QWORD PTR [RDI+1]
            call QWORD PTR [RIP+memmove@GOTPCREL]

            mov EAX,DWORD PTR [RBX+8]
            sub EAX,1
.L_6048:

            sub EAX,1
            and DWORD PTR [RBX+28],4294967293
            mov DWORD PTR [RBX+8],EAX
            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            jne .L_602b

            jmp .L_59fc
.L_6060:

            mov QWORD PTR [RSP+104],RAX
            mov QWORD PTR [RSP+96],RAX
            jmp .L_594b
.L_606f:

            mov EAX,DWORD PTR [RBP+32]
            lea ECX,DWORD PTR [RAX-2]
            cmp ECX,1
            jbe .L_56f0

            cmp EAX,6
            je .L_56f0

            jmp .L_5d69
.L_608c:

            mov ESI,R13D
            mov RDI,RBP
            call .L_3ed0

            jmp .L_5d5f
.L_609c:

            test EBX,EBX
            jne .L_60b2

            mov EDX,1
            call .L_4400

            test EAX,EAX
            jne .L_61a5
.L_60b2:

            mov R12,R13
            jmp .L_5e90
.L_60ba:

            mov DWORD PTR [R15+44],0
            mov QWORD PTR [RSP+72],RBP
            mov QWORD PTR [R12+16],0
            jmp .L_531b
.L_60d5:

            mov RBX,QWORD PTR [RBP+16]
            test RBX,RBX
            jne .L_5d91

            jmp .L_5db4
.L_60e7:

            mov QWORD PTR [RSP+104],RAX
            mov QWORD PTR [RSP+96],RAX
            jmp .L_5e8a
.L_60f6:

            cmp BYTE PTR [RAX],62
            je .L_61e8

            mov EDX,1
.L_6104:

            mov ECX,EDX
            add RDX,1
            cmp BYTE PTR [RAX+RDX*1-1],62
            jne .L_6104

            lea ESI,DWORD PTR [RCX+1]
            movsxd RDX,ESI
.L_6117:

            cmp BYTE PTR [RAX+RDX*1],32
            jne .L_6120

            lea ESI,DWORD PTR [RCX+2]
.L_6120:

            mov RDI,R12
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            and DWORD PTR [R12+28],4294967293
            jmp .L_5ea3
.L_6134:

            test EBP,EBP
            jle .L_613c

            cmp EBX,EBP
            jge .L_6159
.L_613c:

            and DWORD PTR [R12+28],4294967293
            mov DWORD PTR [R12+8],EBX
            cmp EBX,1
            jle .L_5be4

            mov R13,QWORD PTR [R12]
            jmp .L_5bc9
.L_6159:

            movsxd RSI,EBP
            add RSI,R13
.L_615f:

            mov EDX,EBX
            mov RDI,R13
            sub EDX,EBP
            add EDX,1
            movsxd RDX,EDX
            call QWORD PTR [RIP+memmove@GOTPCREL]

            mov EBX,DWORD PTR [R12+8]
            sub EBX,EBP
            jmp .L_613c
.L_617b:

            mov QWORD PTR [RSP+24],0
            xor EBP,EBP
            jmp .L_5c35
.L_618b:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_6198:

            mov R15,QWORD PTR [RSP+56]
            mov RBP,R13
            jmp .L_5c35
.L_61a5:

            mov RDI,R12
            mov RSI,R13
            call QWORD PTR [RIP+___mkd_freeLineRange@GOTPCREL]

            mov R12,QWORD PTR [R15+16]
            jmp .L_5f02
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_61c0:

            xor EAX,EAX
            jmp .L_5770
.L_61c7:

            mov DWORD PTR [RSP+20],3
            mov EAX,2
            lea RDX,QWORD PTR [RIP+.L_e024]
            jmp .L_5f82
.L_61e0:

            xor R13D,R13D
            jmp .L_5eea
.L_61e8:

            xor ECX,ECX
            mov EDX,1
            mov ESI,1
            jmp .L_6117
.L_61f9:

            mov ESI,R13D
            mov RDI,RBX
            call .L_3ed0

            jmp .L_5d9b
.L_6209:

            mov QWORD PTR [RSP+72],0
            xor EBP,EBP
            jmp .L_531b
.L_6219:

            mov RAX,RSI
.L_621c:

            cmp DWORD PTR [RAX+24],EDX
            mov ECX,EDX
            cmovle ECX,DWORD PTR [RAX+24]
            mov RDI,QWORD PTR [RAX]
            movsxd RCX,ECX
            cmp BYTE PTR [RDI+RCX*1],124
            jne .L_531b

            mov RAX,QWORD PTR [RAX+16]
            test RAX,RAX
            jne .L_621c

            jmp .L_57db
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_compile
.type mkd_compile, @function
#-----------------------------------
mkd_compile:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,120
.cfi_def_cfa_offset 176
            mov QWORD PTR [RSP+32],RDI
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+104],RAX
            xor EAX,EAX
            test RDI,RDI
            je .L_6ee8

            mov ECX,DWORD PTR [RDI+56]
            mov R8,QWORD PTR [RDI+80]
            mov EBP,ESI
            test ECX,ECX
            je .L_62cc

            cmp DWORD PTR [R8+80],ESI
            je .L_6ec0
.L_6293:

            mov RBX,QWORD PTR [RSP+32]
            mov RDI,QWORD PTR [RBX+48]
            mov QWORD PTR [RBX+56],0
            test RDI,RDI
            je .L_62b3

            call QWORD PTR [RIP+___mkd_freeParagraph@GOTPCREL]

            mov R8,QWORD PTR [RBX+80]
.L_62b3:

            cmp QWORD PTR [R8+72],0
            je .L_62cc

            mov RDI,R8
            call QWORD PTR [RIP+___mkd_freefootnotes@GOTPCREL]

            mov RAX,QWORD PTR [RSP+32]
            mov R8,QWORD PTR [RAX+80]
.L_62cc:

            mov R15,QWORD PTR [RSP+32]
            lea RDI,QWORD PTR [R8+8]
            xor EAX,EAX
            and RDI,-8
            mov DWORD PTR [R15+56],1
            mov QWORD PTR [R8],0
            mov QWORD PTR [R8+88],0
            sub R8,RDI
            lea ECX,DWORD PTR [R8+96]
            shr ECX,3

            rep stosq QWORD PTR [RDI]

            mov EDI,24
            mov RBX,QWORD PTR [R15+80]
            mov RAX,QWORD PTR [R15+72]
            mov QWORD PTR [RBX+64],RAX
            lea RAX,QWORD PTR [R15+88]
            mov QWORD PTR [RBX+88],RAX
            mov DWORD PTR [RBX+80],EBP
            mov QWORD PTR [RBX+16],0
            mov QWORD PTR [RBX+24],0
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov QWORD PTR [RBX+72],RAX
            mov DWORD PTR [RAX],0
            mov QWORD PTR [RAX+8],0
            mov QWORD PTR [RAX+16],0
            xor EAX,EAX
            call QWORD PTR [RIP+mkd_initialize@GOTPCREL]

            mov R12,QWORD PTR [R15+32]
            mov R13,QWORD PTR [R15+80]
            mov QWORD PTR [RSP+80],0
            mov QWORD PTR [RSP+88],0
            mov QWORD PTR [RSP+72],R12
            test R12,R12
            je .L_64b5

            mov QWORD PTR [RSP+24],0
            mov EBX,1
            mov QWORD PTR [RSP+8],0
            nop
            nop
            nop
            nop
            nop
            nop
.L_6398:

            test BYTE PTR [R13+80],8
            je .L_6510
.L_63a3:

            mov R15,QWORD PTR [RSP+72]
            mov EBP,DWORD PTR [R15+24]
            cmp EBP,3
            jg .L_63f8

            mov R14,QWORD PTR [R15]
            movsxd RAX,EBP
            cmp BYTE PTR [R14+RAX*1],91
            jne .L_63f8

            mov ESI,DWORD PTR [R15+8]
            add EBP,1
            cmp ESI,EBP
            jle .L_63f8

            movsxd RCX,EBP
            mov RAX,RCX
            jmp .L_63e9
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_63d8:

            cmp DL,93
            je .L_6770

            add RAX,1
            cmp ESI,EAX
            jle .L_63f8
.L_63e9:

            movzx EDX,BYTE PTR [R14+RAX*1]
            cmp DL,91
            jne .L_63d8

            nop
            nop
            nop
            nop
            nop
.L_63f8:

            test EBX,EBX
            jne .L_6650
.L_6400:

            cmp QWORD PTR [RSP+8],0
            je .L_6690
.L_640c:

            mov RAX,QWORD PTR [RSP+24]
            mov QWORD PTR [RSP+24],R15
            mov QWORD PTR [RAX+16],R15
.L_641a:

            mov EBX,1
            test R15,R15
            je .L_6431

            xor EBX,EBX
            mov EAX,DWORD PTR [R15+24]
            cmp DWORD PTR [R15+8],EAX
            setle BL
.L_6431:

            mov R12,QWORD PTR [R15+16]
            mov QWORD PTR [RSP+72],R12
.L_643a:

            test R12,R12
            jne .L_6398
.L_6443:

            cmp QWORD PTR [RSP+8],0
            mov R12,QWORD PTR [RSP+80]
            je .L_6740
.L_6454:

            mov RAX,QWORD PTR [RSP+24]
            mov ESI,1
            mov EDI,56
            mov QWORD PTR [RAX+16],0
            call QWORD PTR [RIP+calloc@GOTPCREL]

            mov DWORD PTR [RAX+40],14
            mov RBX,RAX
            test R12,R12
            je .L_6f20

            mov RAX,QWORD PTR [RSP+88]
            mov QWORD PTR [RAX],RBX
            mov QWORD PTR [RSP+88],RBX
.L_6491:

            mov RDI,QWORD PTR [RSP+8]
            mov RDX,R13
            mov ESI,1
            call .L_51f0

            mov R12,QWORD PTR [RSP+80]
            mov QWORD PTR [RBX+8],RAX
            mov RAX,QWORD PTR [RSP+32]
            mov R13,QWORD PTR [RAX+80]
.L_64b5:

            mov RAX,QWORD PTR [R13+72]
            mov RBX,QWORD PTR [RSP+32]
            mov EDX,80
            mov RCX,QWORD PTR [RIP+__mkd_footsort@GOTPCREL]
            movsxd RSI,DWORD PTR [RAX+16]
            mov RDI,QWORD PTR [RAX+8]
            mov QWORD PTR [RBX+48],R12
            call QWORD PTR [RIP+qsort@GOTPCREL]

            pxor XMM0,XMM0
            mov EAX,1
            movups XMMWORD PTR [RBX+32],XMM0
.L_64e9:

            mov RCX,QWORD PTR [RSP+104]
            sub RCX,QWORD PTR FS:[40]
            jne .L_6fa9

            add RSP,120
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_6510:

.cfi_restore_state 
            mov RDI,R12
            call .L_41b0

            mov R12,RAX
            test RAX,RAX
            je .L_63a3

            cmp QWORD PTR [RSP+8],0
            mov R15,QWORD PTR [RSP+80]
            je .L_6592

            mov RAX,QWORD PTR [RSP+24]
            mov ESI,1
            mov EDI,56
            mov QWORD PTR [RAX+16],0
            call QWORD PTR [RIP+calloc@GOTPCREL]

            mov DWORD PTR [RAX+40],14
            mov RBX,RAX
            test R15,R15
            je .L_6e90

            mov RAX,QWORD PTR [RSP+88]
            mov QWORD PTR [RAX],RBX
            mov QWORD PTR [RSP+88],RBX
.L_656e:

            mov RDI,QWORD PTR [RSP+8]
            mov RDX,R13
            mov ESI,1
            call .L_51f0

            mov R15,QWORD PTR [RSP+80]
            mov QWORD PTR [RSP+24],0
            mov QWORD PTR [RBX+8],RAX
.L_6592:

            mov EBX,4
            test BYTE PTR [R13+82],64
            jne .L_65ba

            mov RSI,QWORD PTR [R12]
            mov ECX,6
            lea RDI,QWORD PTR [RIP+.L_eac8]

            repe cmpsb BYTE PTR [RSI],BYTE PTR [RDI]

            seta AL
            sbb AL,0
            cmp AL,1
            adc EBX,0
.L_65ba:

            mov R14,QWORD PTR [RSP+72]
            mov ESI,1
            mov EDI,56
            call QWORD PTR [RIP+calloc@GOTPCREL]

            mov QWORD PTR [RAX+16],R14
            mov RBP,RAX
            mov DWORD PTR [RAX+40],EBX
            test R15,R15
            je .L_6e10

            mov RAX,QWORD PTR [RSP+88]
            mov QWORD PTR [RAX],RBP
            mov QWORD PTR [RSP+88],RBP
.L_65ef:

            mov RSI,R12
            lea RDX,QWORD PTR [RSP+68]
            mov RDI,R14
            mov EBX,1
            call .L_4c90

            mov QWORD PTR [RSP+8],0
            mov QWORD PTR [RSP+72],RAX
            mov R12,RAX
            mov EAX,DWORD PTR [RSP+68]
            test EAX,EAX
            je .L_643a

            mov DWORD PTR [RBP+40],14
            mov RDI,QWORD PTR [RBP+16]
            mov RDX,R13
            mov ESI,1
            call .L_51f0

            mov QWORD PTR [RBP+16],0
            mov R12,QWORD PTR [RSP+72]
            mov QWORD PTR [RBP+8],RAX
            jmp .L_643a
          .byte 0x90
.L_6650:

            mov ESI,DWORD PTR [R13+80]
            test ESI,33554432
            je .L_6400

            test BYTE PTR [R15+28],2
            je .L_6750

            mov EAX,DWORD PTR [R15+32]
            sub EAX,4
            cmp EAX,1
            jbe .L_66a0
.L_6677:

            cmp QWORD PTR [RSP+8],0
            mov R15,QWORD PTR [RSP+72]
            jne .L_640c

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_6690:

            mov QWORD PTR [RSP+24],R15
            mov QWORD PTR [RSP+8],R15
            jmp .L_641a

            nop
.L_66a0:

            cmp DWORD PTR [R15+36],2
            jle .L_6677

            cmp QWORD PTR [RSP+8],0
            je .L_670e

            mov RAX,QWORD PTR [RSP+24]
            mov ESI,1
            mov EDI,56
            mov QWORD PTR [RAX+16],0
            call QWORD PTR [RIP+calloc@GOTPCREL]

            cmp QWORD PTR [RSP+80],0
            mov DWORD PTR [RAX+40],14
            mov RBP,RAX
            je .L_6ed5

            mov RAX,QWORD PTR [RSP+88]
            mov QWORD PTR [RAX],RBP
            mov QWORD PTR [RSP+88],RBP
.L_66ef:

            mov RDI,QWORD PTR [RSP+8]
            mov RDX,R13
            mov ESI,1
            call .L_51f0

            mov QWORD PTR [RSP+24],0
            mov QWORD PTR [RBP+8],RAX
.L_670e:

            mov EDX,DWORD PTR [R13+80]
            lea RSI,QWORD PTR [RSP+72]
            lea RDI,QWORD PTR [RSP+80]
            call .L_4560

            test RAX,RAX
            je .L_6f2f

            mov QWORD PTR [RSP+8],0
            mov R12,QWORD PTR [RSP+72]
            jmp .L_643a
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_6740:

            mov RAX,QWORD PTR [RSP+32]
            mov R13,QWORD PTR [RAX+80]
            jmp .L_64b5
          .byte 0x66
          .byte 0x90
.L_6750:

            mov RDI,R15
            call .L_3ed0

            mov EAX,DWORD PTR [R15+32]
            sub EAX,4
            cmp EAX,1
            ja .L_6677

            jmp .L_66a0
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_6770:

            cmp BYTE PTR [R14+RAX*1+1],58
            jne .L_63f8

            mov RDX,QWORD PTR [R13+72]
            mov R12,QWORD PTR [R15+16]
            mov EBX,DWORD PTR [RDX+16]
            mov EAX,DWORD PTR [RDX+20]
            mov RDI,QWORD PTR [RDX+8]
            cmp EBX,EAX
            jl .L_67cd

            add EAX,100
            movsxd RSI,EAX
            lea RSI,QWORD PTR [RSI+RSI*4]
            shl RSI,4
            test RDI,RDI
            je .L_6e60

            mov DWORD PTR [RDX+20],EAX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov RDX,QWORD PTR [R13+72]
            mov R14,QWORD PTR [R15]
            mov RDI,RAX
            mov EAX,DWORD PTR [R15+24]
            mov EBX,DWORD PTR [RDX+16]
            lea EBP,DWORD PTR [RAX+1]
            movsxd RCX,EBP
.L_67c9:

            mov QWORD PTR [RDX+8],RDI
.L_67cd:

            lea EAX,DWORD PTR [RBX+1]
            mov DWORD PTR [RDX+16],EAX
            movsxd RDX,EBX
            lea RBX,QWORD PTR [RDX+RDX*4]
            lea RDX,QWORD PTR [R14+RCX*1]
            shl RBX,4
            add RBX,RDI
            mov QWORD PTR [RBX],0
            mov QWORD PTR [RBX+8],0
            mov QWORD PTR [RBX+16],0
            mov QWORD PTR [RBX+24],0
            mov QWORD PTR [RBX+32],0
            mov QWORD PTR [RBX+40],0
            mov QWORD PTR [RBX+48],0
            mov QWORD PTR [RBX+56],0
            mov DWORD PTR [RBX+72],0
            cmp BYTE PTR [RDX],93
            je .L_6e40

            add RCX,1
            mov QWORD PTR [RSP+16],R12
            xor EDI,EDI
            mov R12,R15
            mov QWORD PTR [RSP+40],R13
            xor EAX,EAX
            mov R13D,EBP
            xor R14D,R14D
            mov RBP,RBX
            mov R15,RCX
            mov RBX,RDX
            jmp .L_689e
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_6860:

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd R14,DWORD PTR [RBP+8]
            mov RDI,RAX
.L_686d:

            mov QWORD PTR [RBP],RDI
.L_6871:

            lea EAX,DWORD PTR [R14+1]
            add R13D,1
            mov DWORD PTR [RBP+8],EAX
            movzx EAX,BYTE PTR [RBX]
            mov BYTE PTR [RDI+R14*1],AL
            mov RBX,QWORD PTR [R12]
            add RBX,R15
            add R15,1
            cmp BYTE PTR [RBX],93
            je .L_68c0

            movsxd R14,DWORD PTR [RBP+8]
            mov EAX,DWORD PTR [RBP+12]
            mov RDI,QWORD PTR [RBP]
.L_689e:

            cmp R14D,EAX
            jl .L_6871

            add EAX,100
            mov DWORD PTR [RBP+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            jne .L_6860

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_686d
          .byte 0x90
.L_68c0:

            mov RBX,RBP
            mov R15,R12
            mov EBP,R13D
            mov R12,QWORD PTR [RSP+16]
            movsxd RAX,DWORD PTR [RBX+8]
            mov EDX,DWORD PTR [RBX+12]
            mov R13,QWORD PTR [RSP+40]
            mov RDI,QWORD PTR [RBX]
            cmp EAX,EDX
            jge .L_6de8
.L_68e5:

            lea EDX,DWORD PTR [RAX+1]
            add EBP,2
            mov DWORD PTR [RBX+8],EDX
            mov BYTE PTR [RDI+RAX*1],0
            mov R9,QWORD PTR [R15]
            sub DWORD PTR [RBX+8],1
            mov ESI,DWORD PTR [R15+8]
            cmp EBP,ESI
            jge .L_6a30

            mov QWORD PTR [RSP+40],R9
            mov DWORD PTR [RSP+16],ESI
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R9,QWORD PTR [RSP+40]
            mov ESI,DWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RAX]
            movsxd RAX,EBP
            add RAX,R9
            jmp .L_693f
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_6930:

            add EBP,1
            add RAX,1
            cmp ESI,EBP
            je .L_6a30
.L_693f:

            movsx RDX,BYTE PTR [RAX]
            test BYTE PTR [RCX+RDX*2+1],32
            jne .L_6930

            test BYTE PTR [R13+82],32
            jne .L_6a3b
.L_6955:

            mov ECX,DWORD PTR [RBX+28]
            movsxd R14,DWORD PTR [RBX+24]
            mov RDI,QWORD PTR [RBX+16]
            cmp ESI,EBP
            jle .L_6f44

            mov DWORD PTR [RSP+52],ECX
            mov QWORD PTR [RSP+40],R9
            mov QWORD PTR [RSP+16],RDI
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movsxd RDX,EBP
            mov QWORD PTR [RSP+56],R12
            mov R9,QWORD PTR [RSP+40]
            mov R12,R15
            mov QWORD PTR [RSP+40],R13
            mov R15,RDX
            mov RDI,QWORD PTR [RSP+16]
            mov ECX,DWORD PTR [RSP+52]
            mov EDX,EBP
            mov R13,RAX
            jmp .L_69ea
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_69a8:

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd R14,DWORD PTR [RBX+24]
            mov EDX,DWORD PTR [RSP+16]
            mov RDI,RAX
.L_69b9:

            mov QWORD PTR [RBX+16],RDI
.L_69bd:

            lea EAX,DWORD PTR [R14+1]
            add R15,1
            mov DWORD PTR [RBX+24],EAX
            movzx EAX,BYTE PTR [RBP]
            mov BYTE PTR [RDI+R14*1],AL
            cmp DWORD PTR [R12+8],EDX
            jle .L_6dc0

            mov R9,QWORD PTR [R12]
            mov ECX,DWORD PTR [RBX+28]
            movsxd R14,DWORD PTR [RBX+24]
            mov RDI,QWORD PTR [RBX+16]
.L_69ea:

            lea RBP,QWORD PTR [R9+R15*1]
            mov RAX,QWORD PTR [R13]
            movsx RSI,BYTE PTR [RBP]
            test BYTE PTR [RAX+RSI*2+1],32
            jne .L_6ab8

            add EDX,1
            cmp ECX,R14D
            jg .L_69bd

            add ECX,100
            mov DWORD PTR [RSP+16],EDX
            mov DWORD PTR [RBX+28],ECX
            movsxd RSI,ECX
            test RDI,RDI
            jne .L_69a8

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov EDX,DWORD PTR [RSP+16]
            mov RDI,RAX
            jmp .L_69b9
          .byte 0x66
          .byte 0x90
.L_6a30:

            test BYTE PTR [R13+82],32
            je .L_6f39
.L_6a3b:

            mov RAX,QWORD PTR [RBX]
            cmp BYTE PTR [RAX],94
            jne .L_6955

            mov ESI,EBP
            or DWORD PTR [RBX+72],1
            mov RDI,R15
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov RBP,QWORD PTR [R15+16]
            test RBP,RBP
            je .L_6f8a

            mov RDX,R15
            jmp .L_6a73
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_6a70:

            mov RBP,R12
.L_6a73:

            mov EAX,DWORD PTR [RBP+24]
            cmp EAX,3
            jg .L_6a84

            cmp DWORD PTR [RBP+8],EAX
            jg .L_6f7a
.L_6a84:

            mov ESI,4
            mov RDI,RBP
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov R12,QWORD PTR [RBP+16]
            mov RDX,RBP
            test R12,R12
            jne .L_6a70
.L_6a9e:

            mov RDX,R13
            xor ESI,ESI
            mov RDI,R15
            call .L_51f0

            mov QWORD PTR [RBX+48],RAX
            jmp .L_6b8f
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_6ab8:

            mov EBP,EDX
            mov R13,QWORD PTR [RSP+40]
            mov RDX,R15
            mov R15,R12
            mov R12,QWORD PTR [RSP+56]
.L_6aca:

            cmp R14D,ECX
            jl .L_6afc

            add ECX,100
            mov QWORD PTR [RSP+16],RDX
            mov DWORD PTR [RBX+28],ECX
            movsxd RSI,ECX
            test RDI,RDI
            je .L_6ea0

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd R14,DWORD PTR [RBX+24]
            mov RDX,QWORD PTR [RSP+16]
            mov RDI,RAX
.L_6af8:

            mov QWORD PTR [RBX+16],RDI
.L_6afc:

            lea EAX,DWORD PTR [R14+1]
            mov DWORD PTR [RBX+24],EAX
            mov BYTE PTR [RDI+R14*1],0
            mov RCX,QWORD PTR [R15]
            sub DWORD PTR [RBX+24],1
            mov R14D,DWORD PTR [R15+8]
            cmp EBP,R14D
            jge .L_6d1b

            mov QWORD PTR [RSP+16],RCX
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RCX,QWORD PTR [RSP+16]
            movsxd RDI,EBP
            mov RSI,QWORD PTR [RAX]
            add RDI,RCX
            jmp .L_6b50
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_6b40:

            add EBP,1
            add RDI,1
            cmp R14D,EBP
            je .L_6cc8
.L_6b50:

            movsx RDX,BYTE PTR [RDI]
            test BYTE PTR [RSI+RDX*2+1],32
            jne .L_6b40

            cmp DL,61
            je .L_6d28
.L_6b64:

            cmp EBP,R14D
            jge .L_6cd3
.L_6b6d:

            mov RAX,QWORD PTR [R15]
            movsxd RDX,EBP
            movsx EDX,BYTE PTR [RAX+RDX*1]
            cmp DL,39
            je .L_6bd5

            cmp DL,40
            je .L_6bd0

            cmp DL,34
            je .L_6bd5
.L_6b86:

            mov RDI,R15
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]
.L_6b8f:

            test R12,R12
            je .L_6ef0

            mov RDI,R12
.L_6b9b:

            mov EAX,DWORD PTR [RDI+24]
            cmp DWORD PTR [RDI+8],EAX
            jle .L_6bb8

            mov QWORD PTR [RSP+72],RDI
            mov R12,RDI
            mov EBX,1
            jmp .L_6398
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_6bb8:

            mov RBX,QWORD PTR [RDI+16]
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]

            test RBX,RBX
            je .L_6ef0

            mov RDI,RBX
            jmp .L_6b9b
.L_6bd0:

            mov EDX,41
.L_6bd5:

            add EBP,1
            cmp EBP,DWORD PTR [R15+8]
            jge .L_6c6c

            movsxd RBP,EBP
            mov DWORD PTR [RSP+16],EDX
            mov RCX,RAX
            mov RSI,RBP
            mov QWORD PTR [RSP+40],R12
            mov RBP,R15
            mov R15,RSI
            jmp .L_6c2f
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_6c00:

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd R14,DWORD PTR [RBX+40]
            mov RDI,RAX
.L_6c0d:

            mov QWORD PTR [RBX+32],RDI
.L_6c11:

            lea EAX,DWORD PTR [R14+1]
            add R15,1
            mov DWORD PTR [RBX+40],EAX
            movzx EAX,BYTE PTR [R12]
            mov BYTE PTR [RDI+R14*1],AL
            cmp DWORD PTR [RBP+8],R15D
            jle .L_6c60

            mov RCX,QWORD PTR [RBP]
.L_6c2f:

            movsxd R14,DWORD PTR [RBX+40]
            mov EDX,DWORD PTR [RBX+44]
            lea R12,QWORD PTR [RCX+R15*1]
            mov RDI,QWORD PTR [RBX+32]
            cmp R14D,EDX
            jl .L_6c11

            add EDX,100
            mov DWORD PTR [RBX+44],EDX
            movsxd RSI,EDX
            test RDI,RDI
            jne .L_6c00

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_6c0d
          .byte 0x90
.L_6c60:

            mov EDX,DWORD PTR [RSP+16]
            mov R12,QWORD PTR [RSP+40]
            mov R15,RBP
.L_6c6c:

            movsxd RBP,DWORD PTR [RBX+40]
            mov RDI,QWORD PTR [RBX+32]
            test EBP,EBP
            je .L_6f4c

            lea ECX,DWORD PTR [RBP-1]
            movsxd RSI,EBP
            movsxd RAX,ECX
            sub RSI,2
            mov ECX,ECX
            sub RSI,RCX
            jmp .L_6c99
.L_6c90:

            sub RAX,1
            cmp RSI,RAX
            je .L_6ca7
.L_6c99:

            movsx ECX,BYTE PTR [RDI+RAX*1]
            movsxd RBP,EAX
            mov DWORD PTR [RBX+40],EAX
            cmp ECX,EDX
            jne .L_6c90
.L_6ca7:

            mov EAX,DWORD PTR [RBX+44]
            cmp EAX,EBP
            jle .L_6e20
.L_6cb2:

            lea EAX,DWORD PTR [RBP+1]
            mov DWORD PTR [RBX+40],EAX
            mov BYTE PTR [RDI+RBP*1],0
            sub DWORD PTR [RBX+40],1
            jmp .L_6b86
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_6cc8:

            movsxd RDI,EBP
            add RDI,RCX
            cmp BYTE PTR [RDI],61
            je .L_6d28
.L_6cd3:

            test R12,R12
            je .L_6b6d

            movsxd RAX,DWORD PTR [R12+24]
            test EAX,EAX
            je .L_6b6d

            mov RDX,QWORD PTR [R12]
            movzx EAX,BYTE PTR [RDX+RAX*1]
            cmp AL,34
            je .L_6d00

            sub EAX,39
            cmp AL,1
            ja .L_6b6d
.L_6d00:

            mov RDI,R15
            mov R15,R12
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]

            mov EBP,DWORD PTR [R12+24]
            mov R12,QWORD PTR [R12+16]
            jmp .L_6b6d
.L_6d1b:

            lea RDI,QWORD PTR [RCX+RDX*1]
            cmp BYTE PTR [RDI],61
            jne .L_6cd3

            nop
            nop
            nop
            nop
.L_6d28:

            lea RSI,QWORD PTR [RIP+.L_e037]
            lea RCX,QWORD PTR [RBX+56]
            xor EAX,EAX
            lea RDX,QWORD PTR [RBX+60]
            call QWORD PTR [RIP+__isoc99_sscanf@GOTPCREL]

            mov RSI,QWORD PTR [R15]
            mov R14D,DWORD PTR [R15+8]
            mov QWORD PTR [RSP+16],RSI
            cmp R14D,EBP
            jle .L_6f0f

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RSI,QWORD PTR [RSP+16]
            mov RDX,QWORD PTR [RAX]
            movsxd RAX,EBP
            add RAX,RSI
            jmp .L_6d80
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_6d70:

            add EBP,1
            add RAX,1
            cmp R14D,EBP
            je .L_6f18
.L_6d80:

            movsx RCX,BYTE PTR [RAX]
            test BYTE PTR [RDX+RCX*2+1],32
            je .L_6d70

            cmp R14D,EBP
            jle .L_6f0f

            lea EAX,DWORD PTR [RBP+1]
            cdqe 
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_6da0:

            mov EBP,EAX
            cmp R14D,EAX
            jle .L_6b64

            movsx RCX,BYTE PTR [RSI+RAX*1]
            add RAX,1
            test BYTE PTR [RDX+RCX*2+1],32
            jne .L_6da0

            jmp .L_6b64
.L_6dc0:

            movsxd RDX,EDX
            mov R15,R12
            mov R13,QWORD PTR [RSP+40]
            mov R12,QWORD PTR [RSP+56]
            movsxd R14,DWORD PTR [RBX+24]
            mov ECX,DWORD PTR [RBX+28]
            mov RBP,RDX
            mov RDI,QWORD PTR [RBX+16]
            jmp .L_6aca
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_6de8:

            add EDX,100
            movsxd RSI,EDX
            test RDI,RDI
            je .L_6e4a

            mov DWORD PTR [RBX+12],EDX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov RDI,RAX
.L_6dff:

            mov QWORD PTR [RBX],RDI
            movsxd RAX,DWORD PTR [RBX+8]
            jmp .L_68e5
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_6e10:

            mov QWORD PTR [RSP+88],RAX
            mov QWORD PTR [RSP+80],RAX
            jmp .L_65ef
          .byte 0x90
.L_6e20:

            add EAX,100
            movsxd RSI,EAX
.L_6e26:

            mov DWORD PTR [RBX+44],EAX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBP,DWORD PTR [RBX+40]
            mov RDI,RAX
.L_6e36:

            mov QWORD PTR [RBX+32],RDI
            jmp .L_6cb2
          .byte 0x90
.L_6e40:

            mov ESI,100
            mov EDX,100
.L_6e4a:

            mov RDI,RSI
            mov DWORD PTR [RBX+12],EDX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_6dff
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_6e60:

            mov QWORD PTR [RSP+40],RCX
            mov RDI,RSI
            mov DWORD PTR [RDX+20],EAX
            mov QWORD PTR [RSP+16],RDX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RCX,QWORD PTR [RSP+40]
            mov RDX,QWORD PTR [RSP+16]
            mov RDI,RAX
            jmp .L_67c9
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_6e90:

            mov QWORD PTR [RSP+88],RAX
            mov QWORD PTR [RSP+80],RAX
            jmp .L_656e
          .byte 0x90
.L_6ea0:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDX,QWORD PTR [RSP+16]
            mov RDI,RAX
            jmp .L_6af8
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_6ec0:

            mov EDX,DWORD PTR [RDI+60]
            mov EAX,1
            test EDX,EDX
            jne .L_6293

            jmp .L_64e9
.L_6ed5:

            mov QWORD PTR [RSP+88],RAX
            mov QWORD PTR [RSP+80],RAX
            jmp .L_66ef
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_6ee8:

            xor EAX,EAX
            jmp .L_64e9
          .byte 0x90
.L_6ef0:

            cmp QWORD PTR [RSP+8],0
            mov R12,QWORD PTR [RSP+80]
            mov QWORD PTR [RSP+72],0
            jne .L_6454

            jmp .L_6740
.L_6f0f:

            mov R14D,EBP
            nop
            nop
            nop
            nop
            nop
            nop
.L_6f18:

            mov EBP,R14D
            jmp .L_6cd3
.L_6f20:

            mov QWORD PTR [RSP+88],RAX
            mov QWORD PTR [RSP+80],RAX
            jmp .L_6491
.L_6f2f:

            mov R15,QWORD PTR [RSP+72]
            jmp .L_6690
.L_6f39:

            mov ECX,DWORD PTR [RBX+28]
            movsxd R14,DWORD PTR [RBX+24]
            mov RDI,QWORD PTR [RBX+16]
.L_6f44:

            movsxd RDX,EBP
            jmp .L_6aca
.L_6f4c:

            mov EAX,DWORD PTR [RBX+44]
            test EAX,EAX
            jg .L_6cb2

            add EAX,100
            movsxd RSI,EAX
            test RDI,RDI
            jne .L_6e26

            mov RDI,RSI
            mov DWORD PTR [RBX+44],EAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_6e36
.L_6f7a:

            mov QWORD PTR [RDX+16],0
            mov R12,RBP
            jmp .L_6a9e
.L_6f8a:

            mov RDX,R13
            xor ESI,ESI
            mov RDI,R15
            call .L_51f0

            mov QWORD PTR [RSP+72],0
            mov QWORD PTR [RBX+48],RAX
            jmp .L_6443
.L_6fa9:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
.L_6fb0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R13
.cfi_def_cfa_offset 16
.cfi_offset 13, -16
            mov R13D,EDI
            push R12
.cfi_def_cfa_offset 24
.cfi_offset 12, -24
            mov R12D,ESI
            push RBP
.cfi_def_cfa_offset 32
.cfi_offset 6, -32
            push RBX
.cfi_def_cfa_offset 40
.cfi_offset 3, -40
            mov RBX,RDX
            sub RSP,8
.cfi_def_cfa_offset 48
            movsxd RBP,DWORD PTR [RDX+8]
            mov EAX,DWORD PTR [RDX+12]
            mov RDI,QWORD PTR [RDX]
            cmp EBP,EAX
            jl .L_6ff3

            add EAX,100
            movsxd RSI,EAX
            mov DWORD PTR [RDX+12],EAX
            shl RSI,3
            test RDI,RDI
            je .L_7010

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBP,DWORD PTR [RBX+8]
            mov RDI,RAX
.L_6ff0:

            mov QWORD PTR [RBX],RDI
.L_6ff3:

            lea EAX,DWORD PTR [RBP+1]
            mov DWORD PTR [RBX+8],EAX
            lea RAX,QWORD PTR [RDI+RBP*8]
            mov DWORD PTR [RAX],R13D
            mov BYTE PTR [RAX+4],R12B
            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 40
            pop RBX
.cfi_def_cfa_offset 32
            pop RBP
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
          .byte 0x90
.L_7010:

.cfi_restore_state 
            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_6ff0
.cfi_endproc 

            nop
            nop
.L_7020:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_72bc

            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            lea R14,QWORD PTR [RIP+.L_e091]
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            mov R12,RDI
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RDX
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov RBX,RSI
            sub RSP,24
.L_7047:

.cfi_def_cfa_offset 80
            cmp QWORD PTR [R12],0
            mov EDX,DWORD PTR [RBX+8]
            je .L_7290

            test EDX,EDX
            je .L_7102

            movsxd RCX,EDX
.L_7060:

            mov RAX,QWORD PTR [RBX]
            movzx R13D,BYTE PTR [RAX+RCX*8-4]
            lea ECX,DWORD PTR [R13-43]
            and ECX,253
            je .L_7248

            xor R15D,R15D
            test EDX,EDX
            jg .L_70a5

            jmp .L_70e8
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_7088:

            mov RCX,RBP
            mov EDX,2
            mov ESI,1
            lea RDI,QWORD PTR [RIP+.L_e090]
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            mov RAX,QWORD PTR [RBX]
.L_70a5:

            lea R13,QWORD PTR [R15*8]
            lea RSI,QWORD PTR [RIP+.L_e093]
            mov RDI,RBP
            add RAX,R13
            mov ECX,DWORD PTR [RAX]
            movsx R8D,BYTE PTR [RAX+4]
            xor EAX,EAX
            lea EDX,DWORD PTR [RCX+2]
            mov RCX,R14
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            add R13,QWORD PTR [RBX]
            cmp BYTE PTR [R13+4],96
            jne .L_70de

            mov BYTE PTR [R13+4],32
.L_70de:

            add R15,1
            cmp DWORD PTR [RBX+8],R15D
            jg .L_7088
.L_70e8:

            mov RCX,RBP
            mov EDX,2
            mov ESI,1
            lea RDI,QWORD PTR [RIP+.L_e03f]
            call QWORD PTR [RIP+fwrite@GOTPCREL]
.L_7102:

            mov EAX,DWORD PTR [R12+40]
            cmp EAX,11
            je .L_7128

            cmp EAX,14
            ja .L_72eb

            lea RSI,QWORD PTR [RIP+.L_e0c4]
            movsxd RAX,DWORD PTR [RSI+RAX*4]
            add RAX,RSI
            jmp RAX
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_7128:

            mov EDX,DWORD PTR [R12+48]
            lea RSI,QWORD PTR [RIP+.L_e099]
            mov RDI,RBP
            xor EAX,EAX
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            mov R15D,DWORD PTR [RSP+12]
            add R15D,EAX
.L_7147:

            mov RDX,QWORD PTR [R12+24]
            test RDX,RDX
            je .L_7166

            lea RSI,QWORD PTR [RIP+.L_e0a2]
            mov RDI,RBP
            xor EAX,EAX
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            add R15D,EAX
.L_7166:

            cmp DWORD PTR [R12+44],1
            jbe .L_718a

            lea RDX,QWORD PTR [RIP+.L_e0a6]
            lea RSI,QWORD PTR [RIP+.L_e0ad]
            mov RDI,RBP
            xor EAX,EAX
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            add R15D,EAX
.L_718a:

            mov RAX,QWORD PTR [R12+16]
            test RAX,RAX
            je .L_71d6

            xor EDX,EDX
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_71a0:

            mov RAX,QWORD PTR [RAX+16]
            add EDX,1
            test RAX,RAX
            jne .L_71a0

            cmp EDX,1
            lea RAX,QWORD PTR [RIP+.L_e0a4]
            mov RDI,RBP
            lea RCX,QWORD PTR [RIP+.L_e218]
            lea RSI,QWORD PTR [RIP+.L_e0b4]
            cmovne RCX,RAX
            xor EAX,EAX
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            add R15D,EAX
.L_71d6:

            mov RDI,RBP
            lea RSI,QWORD PTR [RIP+.L_e0c0]
            xor EAX,EAX
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            lea EDI,DWORD PTR [R15+RAX*1]
            mov RAX,QWORD PTR [R12+8]
            mov DWORD PTR [RSP+12],EDI
            test RAX,RAX
            je .L_7280

            cmp QWORD PTR [RAX],1
            mov RDX,RBX
            sbb ESI,ESI
            and ESI,2
            add ESI,43
            call .L_6fb0

            mov RDI,QWORD PTR [R12+8]
            mov RDX,RBP
            mov RSI,RBX
            call .L_7020

            sub DWORD PTR [RBX+8],1
.L_7226:

            mov R12,QWORD PTR [R12]
            test R12,R12
            jne .L_7047

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_7248:

.cfi_restore_state 
            movsx EDX,R13B
            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e08b]
            mov RDI,RBP
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            mov EDX,124
            cmp R13B,45
            mov EAX,32
            cmovne EAX,EDX
            movsxd RCX,DWORD PTR [RBX+8]
            mov RDX,QWORD PTR [RBX]
            mov BYTE PTR [RDX+RCX*8-4],AL
            jmp .L_70e8
          .byte 0x90
.L_7280:

            mov RSI,RBP
            mov EDI,10
            call QWORD PTR [RIP+fputc@GOTPCREL]

            jmp .L_7226
.L_7290:

            test EDX,EDX
            je .L_7102

            mov RAX,QWORD PTR [RBX]
            movsxd RCX,EDX
            lea RSI,QWORD PTR [RAX+RCX*8-8]
            movzx EAX,BYTE PTR [RSI+4]
            cmp AL,43
            je .L_72b3

            cmp AL,124
            jne .L_7060
.L_72b3:

            mov BYTE PTR [RSI+4],96
            jmp .L_7060
.L_72bc:

.cfi_def_cfa_offset 8
.cfi_restore 3
.cfi_restore 6
.cfi_restore 12
.cfi_restore 13
.cfi_restore 14
.cfi_restore 15
            ret 
.L_72bd:

.cfi_def_cfa_offset 80
.cfi_offset 3, -56
.cfi_offset 6, -48
.cfi_offset 12, -40
.cfi_offset 13, -32
.cfi_offset 14, -24
.cfi_offset 15, -16
            lea RDX,QWORD PTR [RIP+.L_e048]
            nop
            nop
            nop
            nop
.L_72c8:

            lea RSI,QWORD PTR [RIP+.L_e09e]
            mov RDI,RBP
            xor EAX,EAX
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            mov R15D,EAX
            jmp .L_7147
.L_72e2:

            lea RDX,QWORD PTR [RIP+.L_e21f]
            jmp .L_72c8
.L_72eb:

            lea RDX,QWORD PTR [RIP+.L_e06d]
            jmp .L_72c8
.L_72f4:

            lea RDX,QWORD PTR [RIP+.L_e23b]
            jmp .L_72c8
.L_72fd:

            lea RDX,QWORD PTR [RIP+.L_e053]
            jmp .L_72c8
.L_7306:

            lea RDX,QWORD PTR [RIP+.L_e05a]
            jmp .L_72c8
.L_730f:

            lea RDX,QWORD PTR [RIP+.L_e042]
            jmp .L_72c8
.L_7318:

            lea RDX,QWORD PTR [RIP+.L_e05f]
            jmp .L_72c8
.L_7321:

            lea RDX,QWORD PTR [RIP+.L_e062]
            jmp .L_72c8
.L_732a:

            lea RDX,QWORD PTR [RIP+.L_e065]
            jmp .L_72c8
.L_7333:

            lea RDX,QWORD PTR [RIP+.L_e068]
            jmp .L_72c8
.L_733c:

            lea RDX,QWORD PTR [RIP+.L_e07b]
            jmp .L_72c8
.L_7345:

            lea RDX,QWORD PTR [RIP+.L_e07e]
            jmp .L_72c8
.L_7351:

            lea RDX,QWORD PTR [RIP+.L_e084]
            jmp .L_72c8
.cfi_endproc 

            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_dump
.type mkd_dump, @function
#-----------------------------------
mkd_dump:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R13
.cfi_def_cfa_offset 16
.cfi_offset 13, -16
            mov R13,RCX
            push R12
.cfi_def_cfa_offset 24
.cfi_offset 12, -24
            mov R12,RSI
            mov ESI,EDX
            push RBP
.cfi_def_cfa_offset 32
.cfi_offset 6, -32
            push RBX
.cfi_def_cfa_offset 40
.cfi_offset 3, -40
            mov RBX,RDI
            sub RSP,40
.cfi_def_cfa_offset 80
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+24],RAX
            xor EAX,EAX
            call QWORD PTR [RIP+mkd_compile@GOTPCREL]

            test EAX,EAX
            je .L_7420

            mov RAX,QWORD PTR [RBX+48]
            mov RDX,R13
            lea RSI,QWORD PTR [RIP+.L_e0a3]
            mov RDI,R12
            mov R13,RSP
            mov QWORD PTR [RSP],0
            mov QWORD PTR [RSP+8],0
            cmp QWORD PTR [RAX],1
            sbb EBP,EBP
            xor EAX,EAX
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            and EBP,2
            mov RDX,R13
            add EBP,43
            mov EDI,EAX
            mov ESI,EBP
            call .L_6fb0

            mov RDI,QWORD PTR [RBX+48]
            mov RDX,R12
            mov RSI,R13
            call .L_7020

            mov EAX,DWORD PTR [RSP+12]
            test EAX,EAX
            jne .L_7410
.L_73ef:

            mov RCX,QWORD PTR [RSP+24]
            sub RCX,QWORD PTR FS:[40]
            jne .L_7427

            add RSP,40
.cfi_remember_state 
.cfi_def_cfa_offset 40
            pop RBX
.cfi_def_cfa_offset 32
            pop RBP
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_7410:

.cfi_restore_state 
            mov RDI,QWORD PTR [RSP]
            call QWORD PTR [RIP+free@GOTPCREL]

            xor EAX,EAX
            jmp .L_73ef
          .byte 0x66
          .byte 0x90
.L_7420:

            mov EAX,4294967295
            jmp .L_73ef
.L_7427:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
.L_7430:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov R11D,EDI
            mov RCX,RDX
            mov EBP,1
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            mov R10D,DWORD PTR [RDX+24]
            mov EBX,ESI
            xor R8D,R8D
            mov EAX,DWORD PTR [RDX+52]
.L_7449:

            cmp R10D,EAX
            jle .L_747c
.L_744e:

            mov RDI,QWORD PTR [RCX+16]
            lea EDX,DWORD PTR [RAX+1]
            movsxd RSI,EAX
            lea R9D,DWORD PTR [R8+1]
            mov DWORD PTR [RCX+52],EDX
            movzx ESI,BYTE PTR [RDI+RSI*1]
            cmp ESI,92
            je .L_7490

            cmp ESI,R11D
            je .L_74b8
.L_746d:

            cmp ESI,EBX
            je .L_74c8
.L_7471:

            mov EAX,DWORD PTR [RCX+52]
            mov R8D,R9D
            cmp R10D,EAX
            jg .L_744e
.L_747c:

            mov R8D,4294967295
            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            mov EAX,R8D
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_7490:

.cfi_restore_state 
            cmp R10D,EDX
            jle .L_746d

            test EDX,EDX
            js .L_746d

            movsxd RDX,EDX
            movzx EDX,BYTE PTR [RDI+RDX*1]
            cmp EBX,EDX
            je .L_74a9

            cmp R11D,EDX
            jne .L_746d
.L_74a9:

            add EAX,2
            lea R9D,DWORD PTR [R8+2]
            mov DWORD PTR [RCX+52],EAX
            jmp .L_7471
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_74b8:

            add EBP,1
            mov EAX,EDX
            mov R8D,R9D
            jmp .L_7449
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_74c8:

            sub EBP,1
            jne .L_7471

            mov EAX,R8D
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_74e0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R13
.cfi_def_cfa_offset 16
.cfi_offset 13, -16
            push R12
.cfi_def_cfa_offset 24
.cfi_offset 12, -24
            movsxd R12,ESI
            push RBP
.cfi_def_cfa_offset 32
.cfi_offset 6, -32
            add R12,RDI
            push RBX
.cfi_def_cfa_offset 40
.cfi_offset 3, -40
            sub RSP,8
.cfi_def_cfa_offset 48
            test ESI,ESI
            jle .L_7558

            mov R13,RDI
            mov RBP,RDX
            jmp .L_7524
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_7500:

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBX,DWORD PTR [RBP+24]
            mov RDI,RAX
.L_750d:

            mov QWORD PTR [RBP+16],RDI
.L_7511:

            lea EAX,DWORD PTR [RBX+1]
            mov DWORD PTR [RBP+24],EAX
            movzx EAX,BYTE PTR [R13-1]
            mov BYTE PTR [RDI+RBX*1],AL
            cmp R12,R13
            je .L_7558
.L_7524:

            movsxd RBX,DWORD PTR [RBP+24]
            mov EAX,DWORD PTR [RBP+28]
            add R13,1
            mov RDI,QWORD PTR [RBP+16]
            cmp EBX,EAX
            jl .L_7511

            add EAX,100
            mov DWORD PTR [RBP+28],EAX
            movsxd RSI,EAX
            test RDI,RDI
            jne .L_7500

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_750d
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_7558:

            add RSP,8
.cfi_def_cfa_offset 40
            pop RBX
.cfi_def_cfa_offset 32
            pop RBP
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_7570:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            mov R12D,EDI
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            movsxd RBP,DWORD PTR [RSI+24]
            mov RBX,RSI
            mov EAX,DWORD PTR [RSI+28]
            mov RDI,QWORD PTR [RSI+16]
            cmp EBP,EAX
            jl .L_75a8

            add EAX,100
            mov DWORD PTR [RBX+28],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_75c0

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBP,DWORD PTR [RBX+24]
            mov RDI,RAX
.L_75a4:

            mov QWORD PTR [RBX+16],RDI
.L_75a8:

            lea EAX,DWORD PTR [RBP+1]
            mov DWORD PTR [RBX+24],EAX
            mov BYTE PTR [RDI+RBP*1],R12B
            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_75c0:

.cfi_restore_state 
            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_75a4
.cfi_endproc 

            nop
            nop
.L_75d0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R14
.cfi_def_cfa_offset 16
.cfi_offset 14, -16
            push R13
.cfi_def_cfa_offset 24
.cfi_offset 13, -24
            push R12
.cfi_def_cfa_offset 32
.cfi_offset 12, -32
            push RBP
.cfi_def_cfa_offset 40
.cfi_offset 6, -40
            push RBX
.cfi_def_cfa_offset 48
.cfi_offset 3, -48
            mov EBX,DWORD PTR [RDI+52]
            test EBX,EBX
            js .L_7630

            mov R14D,DWORD PTR [RDI+24]
            mov R12,RDI
            movsxd RBP,EBX
            jmp .L_75f5
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_75f0:

            mov DWORD PTR [R12+52],EBX
.L_75f5:

            cmp R14D,EBX
            jle .L_7630

            mov RAX,QWORD PTR [R12+16]
            add EBX,1
            movzx R13D,BYTE PTR [RAX+RBP*1]
            add RBP,1
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RAX,QWORD PTR [RAX]
            movzx EDX,R13B
            test BYTE PTR [RAX+RDX*2+1],32
            jne .L_75f0

            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 40
            movzx EAX,R13B
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_7630:

.cfi_restore_state 
            pop RBX
.cfi_def_cfa_offset 40
            mov EAX,4294967295
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
.L_7640:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            movsx R15D,SIL
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,RDX
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RDI
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,24
.cfi_def_cfa_offset 80
            mov RAX,QWORD PTR [RDI+16]
            mov R14D,DWORD PTR [RDI+52]
            mov QWORD PTR [RSP+8],RAX
            mov EBX,R14D
.L_7668:

            mov ECX,DWORD PTR [RBP+24]
            movsxd RAX,EBX
            jmp .L_7688
.L_7670:

            mov R12,QWORD PTR [RBP+16]
            add EBX,1
            mov DWORD PTR [RBP+52],EBX
            movzx EDX,BYTE PTR [R12+RAX*1]
            add RAX,1
            cmp R15D,EDX
            je .L_76a8
.L_7688:

            cmp ECX,EBX
            jg .L_7670

            mov DWORD PTR [RBP+52],R14D
            xor EAX,EAX
            mov BYTE PTR [RBP+48],0
.L_7696:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_76a8:

.cfi_restore_state 
            mov RDI,RBP
            call .L_75d0

            cmp EAX,41
            je .L_76ba

            mov EBX,DWORD PTR [RBP+52]
            jmp .L_7668
.L_76ba:

            movsxd R8,R14D
            add R8,QWORD PTR [RSP+8]
            movsxd RBX,EBX
            lea RAX,QWORD PTR [R8+1]
            mov QWORD PTR [R13+32],RAX
            lea RAX,QWORD PTR [R12+RBX*1]
            sub RAX,R8
            sub EAX,2
            mov DWORD PTR [R13+40],EAX
            mov EAX,1
            jmp .L_7696
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_76f0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R14
.cfi_def_cfa_offset 16
.cfi_offset 14, -16
            push R13
.cfi_def_cfa_offset 24
.cfi_offset 13, -24
            push R12
.cfi_def_cfa_offset 32
.cfi_offset 12, -32
            push RBP
.cfi_def_cfa_offset 40
.cfi_offset 6, -40
            mov RBP,RSI
            push RBX
.cfi_def_cfa_offset 48
.cfi_offset 3, -48
            mov R13D,DWORD PTR [RDI+52]
            mov RBX,RDI
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RCX,QWORD PTR [RAX]
            mov EAX,R13D
            sub EAX,1
            js .L_78d4

            cmp EAX,DWORD PTR [RBX+24]
            jge .L_78d4

            mov RDX,QWORD PTR [RBX+16]
            cdqe 
            movzx EAX,BYTE PTR [RDX+RAX*1]
            add RAX,RAX
.L_772d:

            movzx EAX,WORD PTR [RCX+RAX*1]
            test AH,32
            je .L_77c8

            mov ESI,DWORD PTR [RBX+24]
            cmp R13D,ESI
            jge .L_77e0

            lea EAX,DWORD PTR [R13+1]
            mov DWORD PTR [RBX+52],EAX
            cmp ESI,EAX
            jle .L_77e0

            mov RDX,QWORD PTR [RBX+16]
            lea EDI,DWORD PTR [R13+2]
            cdqe 
            mov DWORD PTR [RBX+52],EDI
            movzx EDX,BYTE PTR [RDX+RAX*1]
            test BYTE PTR [RCX+RDX*2+1],8
            je .L_78cc
.L_7771:

            xor R12D,R12D
            jmp .L_779d
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_7780:

            mov RDX,QWORD PTR [RBX+16]
            lea EDI,DWORD PTR [RAX+1]
            mov DWORD PTR [RBX+52],EDI
            movzx EDX,BYTE PTR [RDX+RAX*1]
            mov RAX,RDX
            add RAX,RAX
            movzx EAX,WORD PTR [RCX+RAX*1]
            test AH,8
            je .L_77c3
.L_779d:

            lea EAX,DWORD PTR [R12+R12*4]
            lea R12D,DWORD PTR [RDX+RAX*2-48]
            movsxd RAX,DWORD PTR [RBX+52]
            cmp ESI,EAX
            jg .L_7780

            mov RAX,-2
            mov EDX,4294967295
            movzx EAX,WORD PTR [RCX+RAX*1]
            test AH,8
            jne .L_779d
.L_77c3:

            cmp EDX,120
            je .L_77f0
.L_77c8:

            mov DWORD PTR [RBX+52],R13D
            xor EAX,EAX
            mov BYTE PTR [RBX+48],0
.L_77d2:

            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 40
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_77e0:

.cfi_restore_state 
            mov EDX,4294967295
            test BYTE PTR [RCX-1],8
            jne .L_7771

            jmp .L_77c8
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_77f0:

            movsxd RAX,DWORD PTR [RBX+52]
            cmp ESI,EAX
            jle .L_78a8

            mov RDX,QWORD PTR [RBX+16]
            lea EDI,DWORD PTR [RAX+1]
            mov DWORD PTR [RBX+52],EDI
            movzx EDX,BYTE PTR [RDX+RAX*1]
            mov RAX,RDX
            add RAX,RAX
.L_7810:

            movzx EAX,WORD PTR [RCX+RAX*1]
            xor R14D,R14D
            test AH,8
            jne .L_783d

            jmp .L_7863
          .byte 0x66
          .byte 0x90
.L_7820:

            mov RDX,QWORD PTR [RBX+16]
            lea EDI,DWORD PTR [RAX+1]
            mov DWORD PTR [RBX+52],EDI
            movzx EDX,BYTE PTR [RDX+RAX*1]
            mov RAX,RDX
            add RAX,RAX
            movzx EAX,WORD PTR [RCX+RAX*1]
            test AH,8
            je .L_7863
.L_783d:

            lea EAX,DWORD PTR [R14+R14*4]
            lea R14D,DWORD PTR [RDX+RAX*2-48]
            movsxd RAX,DWORD PTR [RBX+52]
            cmp ESI,EAX
            jg .L_7820

            mov RAX,-2
            mov EDX,4294967295
            movzx EAX,WORD PTR [RCX+RAX*1]
            test AH,8
            jne .L_783d
.L_7863:

            test AH,32
            jne .L_78c0
.L_7868:

            cmp EDX,41
            je .L_7891

            cmp EDX,39
            je .L_787b

            cmp EDX,34
            jne .L_77c8
.L_787b:

            movsx ESI,DL
            mov RDI,RBX
            mov RDX,RBP
            call .L_7640

            test EAX,EAX
            je .L_77c8
.L_7891:

            mov DWORD PTR [RBP+56],R14D
            mov EAX,1
            mov DWORD PTR [RBP+60],R12D
            jmp .L_77d2
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_78a8:

            mov EDX,4294967295
            mov RAX,-2
            jmp .L_7810
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_78c0:

            mov RDI,RBX
            call .L_75d0

            mov EDX,EAX
            jmp .L_7868
.L_78cc:

            xor R12D,R12D
            jmp .L_77c3
.L_78d4:

            mov RAX,-2
            jmp .L_772d
.cfi_endproc 
.L_78e0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R14
.cfi_def_cfa_offset 16
.cfi_offset 14, -16
            push R13
.cfi_def_cfa_offset 24
.cfi_offset 13, -24
            mov R13D,EDI
            push R12
.cfi_def_cfa_offset 32
.cfi_offset 12, -32
            push RBP
.cfi_def_cfa_offset 40
.cfi_offset 6, -40
            push RBX
.cfi_def_cfa_offset 48
.cfi_offset 3, -48
            movsxd RBP,DWORD PTR [RSI+40]
            mov RDI,QWORD PTR [RSI+32]
            test EBP,EBP
            jne .L_7960

            mov EAX,DWORD PTR [RSI+44]
            mov R12,RSI
            xor R14D,R14D
            test EAX,EAX
            jle .L_79a0
.L_7908:

            lea RBX,QWORD PTR [R14+R14*2]
            pxor XMM0,XMM0
            lea EAX,DWORD PTR [R14+1]
            mov ESI,100
            shl RBX,4
            mov DWORD PTR [R12+40],EAX
            mov EAX,100
            add RBX,RDI
            movups XMMWORD PTR [RBX],XMM0
            movups XMMWORD PTR [RBX+16],XMM0
            movups XMMWORD PTR [RBX+32],XMM0
.L_7935:

            mov RDI,RSI
            mov DWORD PTR [RBX+28],EAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
.L_7944:

            mov QWORD PTR [RBX+16],RDI
.L_7948:

            lea EAX,DWORD PTR [RBP+1]
            mov DWORD PTR [RBX+24],EAX
            mov BYTE PTR [RDI+RBP*1],R13B
            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 40
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_7960:

.cfi_restore_state 
            lea RAX,QWORD PTR [RBP+RBP*2]
            shl RAX,4
            lea RBX,QWORD PTR [RDI+RAX*1-48]
            movsxd RBP,DWORD PTR [RBX+24]
            mov EAX,DWORD PTR [RBX+28]
            mov RDI,QWORD PTR [RBX+16]
            cmp EBP,EAX
            jl .L_7948

            add EAX,100
            movsxd RSI,EAX
            test RDI,RDI
            je .L_7935

            mov DWORD PTR [RBX+28],EAX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBP,DWORD PTR [RBX+24]
            mov RDI,RAX
            jmp .L_7944
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_79a0:

            add EAX,100
            movsxd RDX,EAX
            mov DWORD PTR [R12+44],EAX
            lea RSI,QWORD PTR [RDX+RDX*2]
            shl RSI,4
            test RDI,RDI
            je .L_79d0

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd R14,DWORD PTR [R12+40]
            mov RDI,RAX
.L_79c6:

            mov QWORD PTR [R12+32],RDI
            jmp .L_7908
.L_79d0:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_79c6
.cfi_endproc 

            nop
            nop
.L_79e0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            mov R9D,ECX
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,RDI
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov EBX,ESI
            mov ESI,EDX
            sub RSP,40
.cfi_def_cfa_offset 96
            mov R14,QWORD PTR [RDI+16]
            mov EBP,DWORD PTR [RDI+24]
            mov QWORD PTR [RSP+8],R8
            movsxd R8,DWORD PTR [RDI+52]
            xor EDI,EDI
            lea R15D,DWORD PTR [R8-1]
            mov DWORD PTR [RSP+16],R8D
            add R8,R14
            mov EAX,R15D
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_7a20:

            mov ECX,EAX
            cmp EBP,EAX
            mov R12D,EDI
            mov EDX,EAX
            not ECX
            setg R10B
            shr ECX,31
            and CL,R10B
            je .L_7ab0

            movzx R10D,BYTE PTR [R8+RDI*1-1]
.L_7a3d:

            add RDI,1
            add EAX,1
            cmp EBX,R10D
            je .L_7a20

            test R9D,R9D
            jne .L_7a8f

            mov DWORD PTR [RSP+28],ESI
            mov DWORD PTR [RSP+24],EDX
            mov BYTE PTR [RSP+23],CL
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movzx ECX,BYTE PTR [RSP+23]
            mov EDX,DWORD PTR [RSP+24]
            mov RDI,QWORD PTR [RAX]
            mov ESI,DWORD PTR [RSP+28]
            test CL,CL
            je .L_7bb9

            movsxd RAX,EDX
            movzx EAX,BYTE PTR [R14+RAX*1]
            add RAX,RAX
.L_7a83:

            movzx EAX,WORD PTR [RDI+RAX*1]
            xor R9D,R9D
            test AH,32
            jne .L_7a97
.L_7a8f:

            xor R9D,R9D
            cmp ESI,R12D
            jle .L_7ac0
.L_7a97:

            add RSP,40
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov EAX,R9D
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_7ab0:

.cfi_restore_state 
            mov R10D,4294967295
            jmp .L_7a3d
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_7ac0:

            test CL,CL
            je .L_7a97

            movsxd R11,DWORD PTR [RSP+16]
            mov ECX,R12D
            xor R8D,R8D
            xor R10D,R10D
            jmp .L_7aec
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_7ad8:

            add R8D,1
            lea ECX,DWORD PTR [R12+R8*1]
            lea EDX,DWORD PTR [RCX+R15*1]
            cmp EBP,EDX
            jle .L_7b60
.L_7ae8:

            test EDX,EDX
            js .L_7b60
.L_7aec:

            movsxd RDX,EDX
            movzx EAX,BYTE PTR [R14+RDX*1]
            cmp EBX,EAX
            jne .L_7ad8

            movsxd RSI,ECX
            lea EAX,DWORD PTR [RCX+R15*1]
            xor EDX,EDX
            add RSI,R11
            add RSI,R14
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_7b10:

            mov EDI,EDX
            cmp EBP,EAX
            jle .L_7b2a

            test EAX,EAX
            js .L_7b2a

            movzx ECX,BYTE PTR [RSI+RDX*1-1]
            add EAX,1
            add RDX,1
            cmp EBX,ECX
            je .L_7b10
.L_7b2a:

            test EDI,EDI
            je .L_7ad8

            cmp R12D,EDI
            je .L_7bc5

            cmp R10D,EDI
            jge .L_7b4c

            cmp R12D,EDI
            setg AL
            test AL,AL
            cmovne R10D,EDI
            cmovne R9D,R8D
.L_7b4c:

            add R8D,EDI
            add R8D,1
            lea ECX,DWORD PTR [R12+R8*1]
            lea EDX,DWORD PTR [RCX+R15*1]
            cmp EBP,EDX
            jg .L_7ae8

            nop
.L_7b60:

            test R9D,R9D
            je .L_7a97

            cmp R12D,R10D
            jle .L_7b77

            sub R12D,R10D
            add R9D,R12D
            mov R12D,R10D
.L_7b77:

            mov EAX,DWORD PTR [RSP+16]
            add EAX,R12D
            js .L_7b84

            mov DWORD PTR [R13+52],EAX
.L_7b84:

            mov DWORD PTR [RSP+16],R9D
            mov RAX,QWORD PTR [RSP+8]
            mov ESI,R9D
            mov RDI,R13
            call RAX

            mov R9D,DWORD PTR [RSP+16]
            lea EAX,DWORD PTR [R12+R9*1-1]
            mov R9D,1
            add EAX,DWORD PTR [R13+52]
            js .L_7a97

            mov DWORD PTR [R13+52],EAX
            jmp .L_7a97
.L_7bb9:

            mov RAX,-2
            jmp .L_7a83
.L_7bc5:

            test R8D,R8D
            jne .L_7bd2

            xor R9D,R9D
            jmp .L_7a97
.L_7bd2:

            mov R9D,R8D
            jmp .L_7b77
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_7be0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov R10,RSI
            mov RBP,RDI
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            sub RSP,312
.cfi_def_cfa_offset 336
            mov QWORD PTR [RSP+144],RDX
            mov QWORD PTR [RSP+152],RCX
            mov QWORD PTR [RSP+160],R8
            mov QWORD PTR [RSP+168],R9
            test AL,AL
            je .L_7c53

            movaps XMMWORD PTR [RSP+176],XMM0
            movaps XMMWORD PTR [RSP+192],XMM1
            movaps XMMWORD PTR [RSP+208],XMM2
            movaps XMMWORD PTR [RSP+224],XMM3
            movaps XMMWORD PTR [RSP+240],XMM4
            movaps XMMWORD PTR [RSP+256],XMM5
            movaps XMMWORD PTR [RSP+272],XMM6
            movaps XMMWORD PTR [RSP+288],XMM7
.L_7c53:

            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+120],RAX
            xor EAX,EAX
            lea RBX,QWORD PTR [RSP+32]
            lea RCX,QWORD PTR [RSP+8]
            mov RDX,R10
            lea RAX,QWORD PTR [RSP+336]
            mov RDI,RBX
            mov DWORD PTR [RSP+8],16
            mov ESI,80
            mov QWORD PTR [RSP+16],RAX
            lea RAX,QWORD PTR [RSP+128]
            mov DWORD PTR [RSP+12],48
            mov QWORD PTR [RSP+24],RAX
            call QWORD PTR [RIP+vsnprintf@GOTPCREL]

            movsx EDI,BYTE PTR [RSP+32]
            test DIL,DIL
            je .L_7ccc

            nop
            nop
            nop
            nop
            nop
            nop
.L_7cb8:

            add RBX,1
            mov RSI,RBP
            call .L_78e0

            movsx EDI,BYTE PTR [RBX]
            test DIL,DIL
            jne .L_7cb8
.L_7ccc:

            mov RAX,QWORD PTR [RSP+120]
            sub RAX,QWORD PTR FS:[40]
            jne .L_7ce6

            add RSP,312
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
.L_7ce6:

.cfi_restore_state 
            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
.L_7cf0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            xor R15D,R15D
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14,RDI
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RDX
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,24
.cfi_def_cfa_offset 80
            cmp SIL,115
            mov RDX,QWORD PTR [RDX+16]
            mov R13D,DWORD PTR [RBP+24]
            setne R15B
            movsxd RBX,DWORD PTR [RBP+52]
            add R15D,1
            mov R12D,R15D
            and R12D,DWORD PTR [RDI]
            je .L_7d88

            test EBX,EBX
            js .L_7d62

            cmp R13D,EBX
            jle .L_7d62

            movzx EBX,BYTE PTR [RDX+RBX*1]
            mov DWORD PTR [RSP+8],ESI
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov ESI,DWORD PTR [RSP+8]
            mov RAX,QWORD PTR [RAX]
            movzx EDX,BL
            test BL,BL
            movzx EAX,WORD PTR [RAX+RDX*2]
            js .L_7e00

            test AH,32
            jne .L_7d62

            cmp BL,31
            ja .L_7e00
.L_7d62:

            movsx EDX,SIL
            mov RDI,RBP
            lea RSI,QWORD PTR [RIP+.L_e100]
            xor EAX,EAX
            call .L_7be0

            not R15D
            mov R12D,1
            and DWORD PTR [R14],R15D
            jmp .L_7ded
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_7d88:

            mov EAX,EBX
            sub EAX,2
            js .L_7dc6

            cmp EAX,R13D
            jge .L_7dc6

            cdqe 
            mov DWORD PTR [RSP+12],ESI
            movzx EDX,BYTE PTR [RDX+RAX*1]
            mov BYTE PTR [RSP+8],DL
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movzx ECX,BYTE PTR [RSP+8]
            mov ESI,DWORD PTR [RSP+12]
            mov RAX,QWORD PTR [RAX]
            test CL,CL
            movzx EAX,WORD PTR [RAX+RCX*2]
            js .L_7e10

            test AH,32
            jne .L_7dc6

            cmp CL,31
            ja .L_7e10
.L_7dc6:

            cmp R13D,EBX
            jle .L_7ded

            test EBX,EBX
            js .L_7ded

            movsx EDX,SIL
            mov RDI,RBP
            lea RSI,QWORD PTR [RIP+.L_e109]
            xor EAX,EAX
            call .L_7be0

            or DWORD PTR [R14],R15D
            mov R12D,1
.L_7ded:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov EAX,R12D
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x90
.L_7e00:

.cfi_restore_state 
            xor R12D,R12D
            test AL,4
            je .L_7ded

            jmp .L_7d62
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_7e10:

            test AL,4
            jne .L_7dc6

            jmp .L_7ded
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_7e20:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test ESI,ESI
            jle .L_7eb6

            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            movsxd RSI,ESI
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            lea R14,QWORD PTR [RIP+.L_e115]
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            lea R13,QWORD PTR [RIP+.L_e11c]
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            lea R12,QWORD PTR [RDI+RSI*1]
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RDI
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov RBX,RDX
            sub RSP,8
.cfi_def_cfa_offset 64
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_7e58:

            mov EDI,38
            lea R15,QWORD PTR [RIP+.L_e112]
            nop
            nop
            nop
            nop
.L_7e68:

            add R15,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [R15]
            test DIL,DIL
            jne .L_7e68

            movzx R15D,BYTE PTR [RBP]
            add RBP,1
            call QWORD PTR [RIP+random@GOTPCREL]

            mov RSI,R13
            mov RDI,RBX
            test AL,1
            mov EDX,R15D
            cmovne RSI,R14
            xor EAX,EAX
            call .L_7be0

            cmp R12,RBP
            jne .L_7e58

            add RSP,8
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
.L_7eb6:

.cfi_restore 3
.cfi_restore 6
.cfi_restore 12
.cfi_restore 13
.cfi_restore 14
.cfi_restore 15
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_7ec0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,24
.cfi_def_cfa_offset 80
            mov DWORD PTR [RSP+12],ECX
            test ESI,ESI
            jle .L_8008

            mov R13,RDI
            mov R15D,ESI
            lea EBP,DWORD PTR [RSI-1]
            mov RBX,RDX
            jmp .L_7f6d
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_7ef0:

            cmp R14B,38
            je .L_7fd0
.L_7efa:

            cmp R14B,60
            je .L_8038

            cmp R14B,34
            je .L_8078

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R15D,EBP
            mov R13,R12
            mov RDX,QWORD PTR [RAX]
            movzx EAX,R14B
            add RAX,RAX
.L_7f24:

            movzx EAX,WORD PTR [RDX+RAX*1]
            test AL,12
            jne .L_8020

            mov EDX,DWORD PTR [RSP+12]
            test EDX,EDX
            je .L_7f41

            test AH,32
            jne .L_8020
.L_7f41:

            cmp R14B,13
            je .L_80b0

            movzx EDX,R14B
            lea RSI,QWORD PTR [RIP+.L_e131]
            mov RDI,RBX
            xor EAX,EAX
            call .L_7be0
.L_7f60:

            lea EBP,DWORD PTR [R15-1]
            test R15D,R15D
            jle .L_8008
.L_7f6d:

            movzx R14D,BYTE PTR [R13]
            lea R12,QWORD PTR [R13+1]
            cmp R14B,92
            jne .L_7ef0

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            sub R15D,2
            mov RDX,QWORD PTR [RAX]
            test EBP,EBP
            jle .L_8068

            movzx EAX,BYTE PTR [R13+1]
            lea R12,QWORD PTR [R13+2]
            mov EBP,R15D
            mov R14,RAX
            test WORD PTR [RDX+RAX*2],8196
            jne .L_7ef0

            mov RSI,RBX
            mov EDI,92
            call .L_78e0

            cmp R14B,38
            jne .L_7efa

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_7fd0:

            mov EDI,38
            lea R14,QWORD PTR [RIP+.L_e122]
            nop
            nop
            nop
            nop
.L_7fe0:

            add R14,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [R14]
            test DIL,DIL
            jne .L_7fe0

            mov R15D,EBP
            mov R13,R12
.L_7ffb:

            lea EBP,DWORD PTR [R15-1]
            test R15D,R15D
            jg .L_7f6d
.L_8008:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_8020:

.cfi_restore_state 
            movzx EDI,R14B
            mov RSI,RBX
            call .L_78e0

            jmp .L_7f60
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_8038:

            mov EDI,38
            lea R14,QWORD PTR [RIP+.L_e128]
            nop
            nop
            nop
            nop
.L_8048:

            add R14,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [R14]
            test DIL,DIL
            jne .L_8048

            mov R15D,EBP
            mov R13,R12
            jmp .L_7ffb
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_8068:

            mov R13,R12
            mov EAX,184
            jmp .L_7f24
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_8078:

            mov EDI,37
            lea R14,QWORD PTR [RIP+.L_e12d]
            nop
            nop
            nop
            nop
.L_8088:

            add R14,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [R14]
            test DIL,DIL
            jne .L_8088

            mov R15D,EBP
            mov R13,R12
            jmp .L_7ffb
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_80b0:

            mov EDI,32
            lea RBP,QWORD PTR [RIP+.L_e090]
            nop
            nop
            nop
            nop
.L_80c0:

            add RBP,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [RBP]
            test DIL,DIL
            jne .L_80c0

            jmp .L_7f60
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
.L_80e0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSI
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            sub RSP,8
.cfi_def_cfa_offset 32
            cmp EDI,60
            je .L_8160

            cmp EDI,62
            je .L_8108

            cmp EDI,38
            je .L_8138

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            jmp .L_78e0
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_8108:

.cfi_restore_state 
            mov EDI,38
            lea RBX,QWORD PTR [RIP+.L_e138]
            nop
            nop
            nop
            nop
.L_8118:

            add RBX,1
            mov RSI,RBP
            call .L_78e0

            movsx EDI,BYTE PTR [RBX]
            test DIL,DIL
            jne .L_8118

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_8138:

.cfi_restore_state 
            lea RBX,QWORD PTR [RIP+.L_e122]
            nop
.L_8140:

            movsx EDI,DIL
            add RBX,1
            mov RSI,RBP
            call .L_78e0

            movzx EDI,BYTE PTR [RBX]
            test DIL,DIL
            jne .L_8140

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
          .byte 0x90
.L_8160:

.cfi_restore_state 
            mov EDI,38
            lea RBX,QWORD PTR [RIP+.L_e128]
            nop
            nop
            nop
            nop
.L_8170:

            add RBX,1
            mov RSI,RBP
            call .L_78e0

            movsx EDI,BYTE PTR [RBX]
            test DIL,DIL
            jne .L_8170

            add RSP,8
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
.L_8190:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,24
.cfi_def_cfa_offset 80
            mov DWORD PTR [RSP+8],EDX
            test EDX,EDX
            jle .L_8223

            mov EAX,EDX
            mov RBX,RDI
            mov R12,RSI
            xor R13D,R13D
            sub EAX,1
            mov DWORD PTR [RSP+12],EAX
            jmp .L_81e4
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_81c0:

            lea R15D,DWORD PTR [R13+1]
            cmp EBP,92
            jne .L_81d0

            cmp DWORD PTR [RSP+12],R13D
            jg .L_8238
.L_81d0:

            mov RSI,RBX
            mov EDI,EBP
            call .L_80e0
.L_81da:

            mov R13D,R15D
            cmp DWORD PTR [RSP+8],R15D
            jle .L_8223
.L_81e4:

            movsxd RDX,R13D
            movsx EBP,BYTE PTR [R12+RDX*1]
            cmp EBP,13
            jne .L_81c0

            mov EDI,32
            lea R14,QWORD PTR [RIP+.L_e090]
            nop
            nop
            nop
.L_8200:

            add R14,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [R14]
            test DIL,DIL
            jne .L_8200

            lea R15D,DWORD PTR [R13+1]
            mov R13D,R15D
            cmp DWORD PTR [RSP+8],R15D
            jg .L_81e4
.L_8223:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_8238:

.cfi_restore_state 
            mov R14,QWORD PTR [RBX+56]
            movsxd RAX,R15D
            movsx R13D,BYTE PTR [R12+RAX*1]
            test R14,R14
            jne .L_825d

            jmp .L_81d0
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_8250:

            mov R14,QWORD PTR [R14+8]
            test R14,R14
            je .L_81d0
.L_825d:

            mov RDI,QWORD PTR [R14]
            mov ESI,R13D
            call QWORD PTR [RIP+strchr@GOTPCREL]

            test RAX,RAX
            je .L_8250

            mov RSI,RBX
            mov EDI,R13D
            add R15D,1
            call .L_80e0

            jmp .L_81da
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8290:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R13
.cfi_def_cfa_offset 16
.cfi_offset 13, -16
            push R12
.cfi_def_cfa_offset 24
.cfi_offset 12, -24
            mov R12D,ESI
            push RBP
.cfi_def_cfa_offset 32
.cfi_offset 6, -32
            push RBX
.cfi_def_cfa_offset 40
.cfi_offset 3, -40
            mov RBX,RDI
            sub RSP,8
.cfi_def_cfa_offset 48
            mov EAX,DWORD PTR [RDI+52]
            sub EAX,1
            cmp ESI,1
            jle .L_82c8

            lea ECX,DWORD PTR [RSI-1]
            mov EDX,ECX
            add EDX,EAX
            js .L_82c8

            cmp EDX,DWORD PTR [RDI+24]
            jge .L_82c8

            mov RSI,QWORD PTR [RDI+16]
            movsxd RDX,EDX
            cmp BYTE PTR [RSI+RDX*1],32
            cmove R12D,ECX
.L_82c8:

            test EAX,EAX
            js .L_8360

            cmp DWORD PTR [RBX+24],EAX
            jle .L_8360

            mov RDX,QWORD PTR [RBX+16]
            cdqe 
            xor R13D,R13D
            cmp BYTE PTR [RDX+RAX*1],32
            jne .L_82f2

            sub R12D,1
            mov R13D,1
.L_82f2:

            mov EDI,60
            lea RBP,QWORD PTR [RIP+.L_e13d]
            nop
            nop
.L_8300:

            add RBP,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [RBP]
            test DIL,DIL
            jne .L_8300

            movsxd RAX,DWORD PTR [RBX+52]
            mov RDI,RBX
            mov EDX,R12D
            lea RBP,QWORD PTR [RIP+.L_e144]
            lea RSI,QWORD PTR [R13+RAX*1-1]
            add RSI,QWORD PTR [RBX+16]
            call .L_8190

            mov EDI,60
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8340:

            add RBP,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [RBP]
            test DIL,DIL
            jne .L_8340

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 40
            pop RBX
.cfi_def_cfa_offset 32
            pop RBP
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
.L_8360:

.cfi_restore_state 
            xor R13D,R13D
            jmp .L_82f2
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8370:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            mov R12D,DWORD PTR [RDI+52]
            test R12D,R12D
            js .L_8460

            mov R9D,1
            mov R11D,EDX
            mov R8D,DWORD PTR [RDI+24]
            mov RBX,RDI
            mov R10D,ESI
            lea EDX,DWORD PTR [R12+1]
            movsxd RCX,R12D
            mov EAX,R12D
            sub R9D,R12D
            jmp .L_83b2
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_83a8:

            add EAX,1
            add EDX,1
            add RCX,1
.L_83b2:

            lea EBP,DWORD PTR [R9+RAX*1]
            cmp R8D,EAX
            jle .L_8460

            mov RSI,QWORD PTR [RBX+16]
            movzx EDI,BYTE PTR [RSI+RCX*1]
            cmp R10D,EDI
            jne .L_83a8

            test EDX,EDX
            js .L_83a8

            cmp EDX,R8D
            jge .L_83a8

            movsxd RDI,EDX
            movzx EDI,BYTE PTR [RSI+RDI*1]
            cmp R11D,EDI
            jne .L_83a8

            sub R12D,2
            cmp R12D,R8D
            jge .L_8467

            test R12D,R12D
            js .L_8467

            movsxd R12,R12D
            movzx EDI,BYTE PTR [RSI+R12*1]
.L_83f7:

            mov RSI,RBX
            call .L_80e0

            mov EAX,DWORD PTR [RBX+52]
            sub EAX,1
            js .L_846e

            cmp EAX,DWORD PTR [RBX+24]
            jge .L_846e

            mov RDX,QWORD PTR [RBX+16]
            cdqe 
            movzx EDI,BYTE PTR [RDX+RAX*1]
.L_8416:

            mov RSI,RBX
            sub EBP,1
            call .L_80e0

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8428:

            movsxd RAX,DWORD PTR [RBX+52]
            mov EDI,4294967295
            cmp EAX,DWORD PTR [RBX+24]
            jge .L_8444

            mov RDX,QWORD PTR [RBX+16]
            lea ECX,DWORD PTR [RAX+1]
            mov DWORD PTR [RBX+52],ECX
            movzx EDI,BYTE PTR [RDX+RAX*1]
.L_8444:

            mov RSI,RBX
            sub EBP,1
            call .L_80e0

            cmp EBP,-2
            jne .L_8428

            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 24
            mov EAX,1
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0x90
.L_8460:

.cfi_restore_state 
            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 24
            xor EAX,EAX
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
.L_8467:

.cfi_restore_state 
            mov EDI,4294967295
            jmp .L_83f7
.L_846e:

            mov EDI,4294967295
            jmp .L_8416
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_reparse
.type ___mkd_reparse, @function
#-----------------------------------
___mkd_reparse:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            mov R15D,ESI
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14,RDI
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            mov R12,R8
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RCX
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov EBX,EDX
            sub RSP,136
.cfi_def_cfa_offset 192
            mov RSI,QWORD PTR [RCX+72]
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+120],RAX
            xor EAX,EAX
            lea R13,QWORD PTR [RSP+16]
            mov RDI,R13
            call QWORD PTR [RIP+___mkd_initmmiot@GOTPCREL]

            mov RAX,QWORD PTR [RBP+88]
            or EBX,DWORD PTR [RBP+80]
            mov DWORD PTR [RSP+96],EBX
            mov QWORD PTR [RSP+104],RAX
            mov RAX,QWORD PTR [RBP+64]
            mov QWORD PTR [RSP+80],RAX
            test R12,R12
            je .L_8590

            mov RAX,RSP
            mov QWORD PTR [RSP],R12
            mov QWORD PTR [RSP+72],RAX
            mov RAX,QWORD PTR [RBP+56]
            mov QWORD PTR [RSP+8],RAX
.L_84f8:

            mov RDX,R13
            mov ESI,R15D
            mov RDI,R14
            call .L_74e0

            mov RSI,R13
            xor EDI,EDI
            call .L_7570

            mov RDI,R13
            sub DWORD PTR [RSP+40],1
            call .L_9790

            mov RDI,R13
            call QWORD PTR [RIP+___mkd_emblock@GOTPCREL]

            movsxd R12,DWORD PTR [RSP+24]
            mov RBX,QWORD PTR [RSP+16]
            test R12D,R12D
            jle .L_8554

            add R12,RBX
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8540:

            movsx EDI,BYTE PTR [RBX]
            add RBX,1
            mov RSI,RBP
            call .L_78e0

            cmp RBX,R12
            jne .L_8540
.L_8554:

            movzx EAX,BYTE PTR [RSP+64]
            mov RSI,QWORD PTR [RBP+72]
            mov RDI,R13
            mov BYTE PTR [RBP+48],AL
            call QWORD PTR [RIP+___mkd_freemmiot@GOTPCREL]

            mov RAX,QWORD PTR [RSP+120]
            sub RAX,QWORD PTR FS:[40]
            jne .L_859e

            add RSP,136
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_8590:

.cfi_restore_state 
            mov RAX,QWORD PTR [RBP+56]
            mov QWORD PTR [RSP+72],RAX
            jmp .L_84f8
.L_859e:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_85b0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            mov R12D,ESI
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            mov RBP,RDI
            mov EDI,60
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            lea RBX,QWORD PTR [RIP+.L_e14c]
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_85d0:

            add RBX,1
            mov RSI,RBP
            call .L_78e0

            movsx EDI,BYTE PTR [RBX]
            test DIL,DIL
            jne .L_85d0

            mov RDX,QWORD PTR [RBP+16]
            movsxd RAX,DWORD PTR [RBP+52]
            xor R8D,R8D
            mov RCX,RBP
            mov ESI,R12D
            lea RBX,QWORD PTR [RIP+.L_e152]
            lea RDI,QWORD PTR [RDX+RAX*1-1]
            xor EDX,EDX
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov EDI,60
            nop
            nop
.L_8610:

            add RBX,1
            mov RSI,RBP
            call .L_78e0

            movsx EDI,BYTE PTR [RBX]
            test DIL,DIL
            jne .L_8610

            pop RBX
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8630:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R14
.cfi_def_cfa_offset 16
.cfi_offset 14, -16
            push R13
.cfi_def_cfa_offset 24
.cfi_offset 13, -24
            mov R13,RSI
            push R12
.cfi_def_cfa_offset 32
.cfi_offset 12, -32
            mov R12D,ECX
            push RBP
.cfi_def_cfa_offset 40
.cfi_offset 6, -40
            mov RBP,RDX
            push RBX
.cfi_def_cfa_offset 48
.cfi_offset 3, -48
            mov R14,QWORD PTR [RSI+16]
            mov RBX,RDI
            movsx EDI,BYTE PTR [R14]
            test DIL,DIL
            je .L_866d

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8658:

            add R14,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [R14]
            test DIL,DIL
            jne .L_8658
.L_866d:

            test BYTE PTR [R13+60],1
            je .L_87d8

            mov RAX,QWORD PTR [RBX+88]
            test RAX,RAX
            je .L_8778

            mov RCX,QWORD PTR [RAX+8]
            test RCX,RCX
            je .L_8778

            mov RDX,QWORD PTR [RAX]
            mov ESI,R12D
            mov RDI,RBP
            call RCX

            mov R14,RAX
            test RAX,RAX
            je .L_8778

            mov RDI,RAX
            call QWORD PTR [RIP+strlen@GOTPCREL]

            mov RDX,RBX
            xor ECX,ECX
            mov RDI,R14
            mov RSI,RAX
            call .L_7ec0

            mov RAX,QWORD PTR [RBX+88]
            mov RDX,QWORD PTR [RAX+32]
            test RDX,RDX
            je .L_87f9

            mov RDI,R14
            mov RSI,QWORD PTR [RAX]
            call RDX

            mov R13,QWORD PTR [R13+24]
            movsx EDI,BYTE PTR [R13]
            test DIL,DIL
            jne .L_87a8

            nop
            nop
            nop
.L_86f0:

            mov RAX,QWORD PTR [RBX+88]
            test RAX,RAX
            je .L_87cb

            nop
            nop
            nop
.L_8700:

            mov RCX,QWORD PTR [RAX+16]
            test RCX,RCX
            je .L_87cb

            mov ESI,R12D
            mov RDX,QWORD PTR [RAX]
            mov RDI,RBP
            call RCX

            mov R12,RAX
            test RAX,RAX
            je .L_87cb

            mov EDI,32
            mov RSI,RBX
            mov RBP,R12
            call .L_78e0

            movsx EDI,BYTE PTR [R12]
            test DIL,DIL
            je .L_8755

            nop
            nop
.L_8740:

            add RBP,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [RBP]
            test DIL,DIL
            jne .L_8740
.L_8755:

            mov RDX,QWORD PTR [RBX+88]
            mov RAX,QWORD PTR [RDX+32]
            test RAX,RAX
            je .L_87cb

            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 40
            mov RDI,R12
            pop RBP
.cfi_def_cfa_offset 32
            mov RSI,QWORD PTR [RDX]
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            jmp RAX
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_8778:

.cfi_restore_state 
            movsxd RAX,DWORD PTR [R13+8]
            mov ESI,R12D
            xor ECX,ECX
            mov RDX,RBX
            sub ESI,EAX
            lea RDI,QWORD PTR [RBP+RAX*1]
            call .L_7ec0
.L_8790:

            mov R13,QWORD PTR [R13+24]
            movsx EDI,BYTE PTR [R13]
            test DIL,DIL
            je .L_86f0

            nop
            nop
            nop
            nop
            nop
            nop
.L_87a8:

            add R13,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [R13]
            test DIL,DIL
            jne .L_87a8

            mov RAX,QWORD PTR [RBX+88]
            test RAX,RAX
            jne .L_8700
.L_87cb:

            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 40
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_87d8:

.cfi_restore_state 
            movsxd RAX,DWORD PTR [R13+8]
            mov ESI,R12D
            xor R8D,R8D
            mov RCX,RBX
            mov EDX,32
            sub ESI,EAX
            lea RDI,QWORD PTR [RBP+RAX*1]
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            jmp .L_8790
.L_87f9:

            mov R13,QWORD PTR [R13+24]
            movsx EDI,BYTE PTR [R13]
            test DIL,DIL
            jne .L_87a8

            jmp .L_8700
.cfi_endproc 

            nop
            nop
            nop
            nop
.L_8810:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,24
.cfi_def_cfa_offset 80
            mov EAX,DWORD PTR [RDI+80]
            mov DWORD PTR [RSP+8],ESI
            mov ECX,EAX
            mov DWORD PTR [RSP+12],EAX
            xor EAX,EAX
            and ECX,1
            jne .L_88de

            movsxd R12,DWORD PTR [RDI+52]
            mov RBX,RDI
            mov RDI,QWORD PTR [RDI+16]
            add RDI,R12
            mov QWORD PTR [RSP],RDI
            cmp ESI,7
            jg .L_88f0

            mov EAX,DWORD PTR [RSP+8]
            test EAX,EAX
            je .L_88a6
.L_8859:

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R14D,DWORD PTR [RSP+8]
            mov R13,QWORD PTR [RSP]
            lea R15,QWORD PTR [RIP+.L_e173]
            mov R12,QWORD PTR [RAX]
            nop
            nop
            nop
            nop
            nop
            nop
.L_8878:

            movsx RAX,BYTE PTR [R13]
            mov RBP,RAX
            test BYTE PTR [R12+RAX*2],8
            jne .L_889c

            movsx ESI,AL
            mov RDI,R15
            call QWORD PTR [RIP+strchr@GOTPCREL]

            test RAX,RAX
            je .L_89d0
.L_889c:

            add R13,1
            sub R14D,1
            jne .L_8878
.L_88a6:

            lea R14,QWORD PTR [RIP+.L_121e0]
            lea R15,QWORD PTR [RIP+markdown_version]
.L_88b4:

            movsxd RDX,DWORD PTR [R14+8]
            cmp DWORD PTR [RSP+8],EDX
            jl .L_88d3

            mov RSI,QWORD PTR [R14]
            mov RDI,QWORD PTR [RSP]
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            test EAX,EAX
            je .L_8a60
.L_88d3:

            add R14,16
            cmp R14,R15
            jne .L_88b4

            xor EAX,EAX
.L_88de:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_88f0:

.cfi_restore_state 
            mov EDX,7
            lea RSI,QWORD PTR [RIP+.L_e163]
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            test EAX,EAX
            jne .L_8859

            mov EAX,DWORD PTR [RSP+8]
            mov EBP,7
            lea R15D,DWORD PTR [RAX-7]
            mov RAX,QWORD PTR [RSP]
            lea R12,QWORD PTR [RAX+7]
.L_891f:

            mov EDI,60
            lea R13,QWORD PTR [RIP+.L_e159]
            nop
            nop
            nop
            nop
            nop
.L_8930:

            add R13,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [R13]
            test DIL,DIL
            jne .L_8930

            test EBP,EBP
            je .L_8aca
.L_894e:

            mov RDI,QWORD PTR [RSP]
            mov ESI,DWORD PTR [RSP+8]
            mov RDX,RBX
            lea RBP,QWORD PTR [RIP+.L_e16b]
            call .L_7e20

            mov EDI,34
            nop
            nop
            nop
            nop
            nop
            nop
.L_8970:

            add RBP,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [RBP]
            test DIL,DIL
            jne .L_8970

            mov RDI,R12
            mov RDX,RBX
            lea RBP,QWORD PTR [RIP+.L_e16e]
            mov ESI,R15D
            call .L_7e20

            mov EDI,60
            nop
.L_89a0:

            add RBP,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [RBP]
            test DIL,DIL
            jne .L_89a0
.L_89b5:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov EAX,1
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_89d0:

.cfi_restore_state 
            cmp BPL,64
            jne .L_88a6

            sub R14D,1
            mov EBP,R14D
            je .L_88a6

            movzx R15D,BYTE PTR [R13+1]
            cmp R15B,46
            je .L_88a6

            add R13,1
            xor R14D,R14D
            jmp .L_8a13
          .byte 0x90
.L_8a00:

            cmp EBP,1
            jle .L_8a42

            sub EBP,1
            mov R14D,1
.L_8a0e:

            movzx R15D,BYTE PTR [R13]
.L_8a13:

            movsx RAX,R15B
            test BYTE PTR [R12+RAX*2],8
            jne .L_8a38

            movsx ESI,R15B
            lea RDI,QWORD PTR [RIP+.L_e179]
            call QWORD PTR [RIP+strchr@GOTPCREL]

            test RAX,RAX
            je .L_88a6
.L_8a38:

            add R13,1
            cmp R15B,46
            je .L_8a00
.L_8a42:

            sub EBP,1
            jne .L_8a0e

            mov R12,QWORD PTR [RSP]
            mov R15D,DWORD PTR [RSP+8]
            test R14D,R14D
            jne .L_891f

            jmp .L_88a6
          .byte 0x66
          .byte 0x90
.L_8a60:

            test DWORD PTR [RSP+12],536870912
            jne .L_8a81

            mov ECX,DWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSI,QWORD PTR [RIP+.L_12160]
            mov RDI,RBX
            call .L_8630
.L_8a81:

            mov RSI,RBX
            mov EDI,62
            lea RBP,QWORD PTR [RIP+.L_e16e]
            call .L_78e0

            mov RDI,QWORD PTR [RSP]
            mov ESI,DWORD PTR [RSP+8]
            mov RDX,RBX
            mov ECX,1
            call .L_7ec0

            mov EDI,60
            nop
.L_8ab0:

            add RBP,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [RBP]
            test DIL,DIL
            jne .L_8ab0

            jmp .L_89b5
.L_8aca:

            mov R14D,109
            lea R13,QWORD PTR [RIP+.L_e163]
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8ae0:

            mov EDI,38
            lea RBP,QWORD PTR [RIP+.L_e112]
            nop
            nop
            nop
            nop
.L_8af0:

            add RBP,1
            mov RSI,RBX
            call .L_78e0

            movsx EDI,BYTE PTR [RBP]
            test DIL,DIL
            jne .L_8af0

            call QWORD PTR [RIP+random@GOTPCREL]

            add R13,1
            mov EDX,R14D
            mov RDI,RBX
            test AL,1
            lea RSI,QWORD PTR [RIP+.L_e11c]
            lea RAX,QWORD PTR [RIP+.L_e115]
            cmovne RSI,RAX
            xor EAX,EAX
            call .L_7be0

            lea RAX,QWORD PTR [RIP+.L_e16a]
            cmp R13,RAX
            je .L_894e

            movzx R14D,BYTE PTR [R13]
            jmp .L_8ae0
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8b50:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            mov R15,RDX
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14,RSI
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,RDI
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,R9
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,40
.cfi_def_cfa_offset 96
            mov EBX,DWORD PTR [RDI+24]
            mov DWORD PTR [RSP+20],ECX
            mov DWORD PTR [RSP+24],R8D
            call QWORD PTR [RIP+___mkd_tidy@GOTPCREL]

            movsxd RDX,DWORD PTR [R13+8]
            mov RCX,QWORD PTR [R13]
            cmp BYTE PTR [RCX+RDX*1-1],124
            jne .L_8b95

            mov RAX,RDX
            sub EAX,1
            mov DWORD PTR [R13+8],EAX
.L_8b95:

            mov EAX,60
            lea R12,QWORD PTR [RIP+.L_e17e]
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8ba8:

            movsx EDI,AL
            add R12,1
            mov RSI,RBP
            call .L_78e0

            movzx EAX,BYTE PTR [R12]
            test AL,AL
            jne .L_8ba8

            mov R8D,DWORD PTR [R13+8]
            xor R12D,R12D
            cmp EBX,R8D
            jge .L_8ceb

            mov EAX,DWORD PTR [RSP+20]
            mov R10D,R12D
            mov R12D,EBX
            mov EBX,R8D
            sub EAX,1
            mov DWORD PTR [RSP+28],EAX
            jmp .L_8c89
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_8bf0:

            lea R9D,DWORD PTR [RBX+1]
            cmp DWORD PTR [RSP+28],R10D
            jg .L_8c95
.L_8bff:

            lea RCX,QWORD PTR [RIP+.L_e218]
            cmp DWORD PTR [RSP+20],R10D
            jle .L_8c1b

            movsxd RAX,DWORD PTR [R15]
            lea RCX,QWORD PTR [RIP+.L_11720]
            mov RCX,QWORD PTR [RCX+RAX*8]
.L_8c1b:

            xor EAX,EAX
            mov RDX,R14
            lea RSI,QWORD PTR [RIP+.L_e18b]
            mov RDI,RBP
            sub EBX,R12D
            mov DWORD PTR [RSP+16],R10D
            add R15,4
            mov DWORD PTR [RSP+12],R9D
            call .L_7be0

            mov ESI,EBX
            lea R8,QWORD PTR [RIP+.L_e3e2]
            xor EDX,EDX
            mov RCX,RBP
            movsxd RDI,R12D
            add RDI,QWORD PTR [R13]
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov RDX,R14
            mov RDI,RBP
            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e196]
            call .L_7be0

            mov R10D,DWORD PTR [RSP+16]
            mov EBX,DWORD PTR [R13+8]
            mov R9D,DWORD PTR [RSP+12]
            add R10D,1
            cmp EBX,R9D
            jle .L_8ce8

            mov R12D,R9D
.L_8c89:

            mov EDX,DWORD PTR [RSP+24]
            test EDX,EDX
            jne .L_8bf0
.L_8c95:

            mov RSI,QWORD PTR [R13]
            mov EAX,R12D
            jmp .L_8ca6
          .byte 0x66
          .byte 0x90
.L_8ca0:

            mov EAX,ECX
            cmp EBX,EAX
            jle .L_8ccb
.L_8ca6:

            movsxd RDX,EAX
            lea ECX,DWORD PTR [RAX+1]
            movzx EDX,BYTE PTR [RSI+RDX*1]
            cmp DL,124
            je .L_8cd8

            lea R9D,DWORD PTR [RAX+2]
            cmp DL,92
            jne .L_8ca0

            lea EDX,DWORD PTR [RAX+3]
            mov EAX,R9D
            mov R9D,EDX
            cmp EBX,EAX
            jg .L_8ca6
.L_8ccb:

            mov EBX,EAX
            jmp .L_8bff
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_8cd8:

            mov R9D,ECX
            mov EBX,EAX
            jmp .L_8bff
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_8ce8:

            mov R12D,R10D
.L_8ceb:

            mov EAX,DWORD PTR [RSP+24]
            test EAX,EAX
            je .L_8d24

            mov R13D,DWORD PTR [RSP+20]
            cmp R13D,R12D
            jle .L_8d24

            lea RBX,QWORD PTR [RIP+.L_e192]
            nop
            nop
            nop
            nop
.L_8d08:

            mov RCX,R14
            mov RDX,R14
            mov RSI,RBX
            mov RDI,RBP
            xor EAX,EAX
            add R12D,1
            call .L_7be0

            cmp R12D,R13D
            jne .L_8d08
.L_8d24:

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e184]
.L_8d30:

            movsx EDI,AL
            add RBX,1
            mov RSI,RBP
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_8d30

            add RSP,40
.cfi_def_cfa_offset 56
            mov EAX,R12D
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8d60:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14,R8
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            mov R12,RDI
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,24
.cfi_def_cfa_offset 80
            mov EBP,DWORD PTR [RDI+80]
            mov QWORD PTR [RSP],RSI
            mov DWORD PTR [RSP+12],EDX
            test ECX,ECX
            jne .L_8e70

            lea RBX,QWORD PTR [RIP+.L_12020]
            mov R13,QWORD PTR [R8+16]
            mov R15D,DWORD PTR [R8+24]
            lea R8,QWORD PTR [RBX+320]
.L_8d9d:

            movsxd RDX,DWORD PTR [RBX+8]
            cmp R15D,EDX
            jle .L_8dc1

            mov RSI,QWORD PTR [RBX]
            mov RDI,R13
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            lea R8,QWORD PTR [RIP+.L_12160]
            test EAX,EAX
            je .L_8f50
.L_8dc1:

            add RBX,64
            cmp RBX,R8
            jne .L_8d9d

            test EBP,32768
            je .L_8e48

            test R13,R13
            je .L_8e48

            movsxd RDX,R15D
            mov ESI,58
            mov RDI,R13
            call QWORD PTR [RIP+memchr@GOTPCREL]

            mov RBX,RAX
            test RAX,RAX
            je .L_8e48

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R8,QWORD PTR [RAX]
            movsx RAX,BYTE PTR [R13]
            test BYTE PTR [R8+RAX*2+1],4
            je .L_8e48

            mov RAX,R13
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8e10:

            add RAX,1
            cmp RBX,RAX
            jbe .L_9070

            movsx RSI,BYTE PTR [RAX]
            mov RDX,RSI
            test BYTE PTR [R8+RSI*2],8
            jne .L_8e10

            lea ESI,DWORD PTR [RSI-45]
            cmp SIL,1
            seta SIL
            cmp DL,43
            setne DL
            test SIL,DL
            je .L_8e10

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_8e48:

            lea RBX,QWORD PTR [RIP+.L_12160]
            xor EAX,EAX
            mov EDX,DWORD PTR [RBX+56]
            test EDX,EBP
            je .L_8e80
.L_8e58:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_8e70:

.cfi_restore_state 
            lea RBX,QWORD PTR [RIP+.L_121a0]
.L_8e77:

            mov EDX,DWORD PTR [RBX+56]
            xor EAX,EAX
            test EDX,EBP
            jne .L_8e58
.L_8e80:

            and EBP,536870912
            jne .L_8f70

            cmp QWORD PTR [RBX+16],0
            mov RDX,QWORD PTR [R14+16]
            mov ECX,DWORD PTR [R14+24]
            je .L_8ff0

            mov RSI,RBX
            mov RDI,R12
            call .L_8630

            mov EDX,DWORD PTR [RBX+32]
            test EDX,EDX
            je .L_8ec9

            mov EDX,DWORD PTR [R14+56]
            test EDX,EDX
            jne .L_9050
.L_8ebd:

            mov EDX,DWORD PTR [R14+60]
            test EDX,EDX
            jne .L_9030
.L_8ec9:

            mov EAX,DWORD PTR [R14+40]
            test EAX,EAX
            jne .L_8fa0
.L_8ed5:

            mov R14,QWORD PTR [RBX+40]
            movsx EDI,BYTE PTR [R14]
            test DIL,DIL
            je .L_8efd

            nop
            nop
            nop
            nop
            nop
            nop
.L_8ee8:

            add R14,1
            mov RSI,R12
            call .L_78e0

            movsx EDI,BYTE PTR [R14]
            test DIL,DIL
            jne .L_8ee8
.L_8efd:

            mov EDX,DWORD PTR [RBX+56]
            mov RDI,QWORD PTR [RSP]
            xor R8D,R8D
            mov RCX,R12
            mov ESI,DWORD PTR [RSP+12]
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov RBX,QWORD PTR [RBX+48]
            movsx EDI,BYTE PTR [RBX]
            test DIL,DIL
            je .L_8f34
.L_8f20:

            add RBX,1
            mov RSI,R12
            call .L_78e0

            movsx EDI,BYTE PTR [RBX]
            test DIL,DIL
            jne .L_8f20
.L_8f34:

            mov EAX,1
.L_8f39:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_8f50:

.cfi_restore_state 
            test EBP,32832
            je .L_8e77

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_8f70:

.cfi_restore_state 
            mov ESI,DWORD PTR [RSP+12]
            mov RDI,QWORD PTR [RSP]
            mov RCX,R12
            xor R8D,R8D
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov EAX,1
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_8fa0:

.cfi_restore_state 
            mov EDI,32
            lea R15,QWORD PTR [RIP+.L_e43b]
            nop
            nop
            nop
            nop
.L_8fb0:

            add R15,1
            mov RSI,R12
            call .L_78e0

            movsx EDI,BYTE PTR [R15]
            test DIL,DIL
            jne .L_8fb0

            mov ESI,DWORD PTR [R14+40]
            mov RDI,QWORD PTR [R14+32]
            xor R8D,R8D
            mov RCX,R12
            mov EDX,32
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov RSI,R12
            mov EDI,34
            call .L_78e0

            jmp .L_8ed5
.L_8ff0:

            movsxd RBX,DWORD PTR [RBX+8]
            sub ECX,EBX
            add RBX,RDX
            test ECX,ECX
            jle .L_8f34

            movsxd RCX,ECX
            lea RBP,QWORD PTR [RBX+RCX*1]
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_9010:

            movsx EDI,BYTE PTR [RBX]
            add RBX,1
            mov RSI,R12
            call .L_78e0

            cmp RBX,RBP
            jne .L_9010

            mov EAX,1
            jmp .L_8f39
          .byte 0x66
          .byte 0x90
.L_9030:

            lea RSI,QWORD PTR [RIP+.L_e1aa]
            mov RDI,R12
            xor EAX,EAX
            call .L_7be0

            jmp .L_8ec9
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_9050:

            lea RSI,QWORD PTR [RIP+.L_e19d]
            mov RDI,R12
            xor EAX,EAX
            call .L_7be0

            jmp .L_8ebd
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_9070:

            lea RBX,QWORD PTR [RIP+.L_121e0]
            lea R8,QWORD PTR [RBX+64]
.L_907b:

            movsxd RDX,DWORD PTR [RBX+8]
            cmp R15D,EDX
            jl .L_909f

            mov RSI,QWORD PTR [RBX]
            mov RDI,R13
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            lea R8,QWORD PTR [RIP+markdown_version]
            test EAX,EAX
            je .L_8e48
.L_909f:

            add RBX,16
            cmp RBX,R8
            jne .L_907b

            xor EAX,EAX
            jmp .L_8e58
.cfi_endproc 

            nop
.L_90b0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            pxor XMM0,XMM0
            mov RDX,RSI
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,RSI
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,136
.cfi_def_cfa_offset 192
            mov RBP,QWORD PTR [RSI+16]
            mov DWORD PTR [RSP+16],EDI
            mov EDI,91
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+120],RAX
            mov EAX,DWORD PTR [RSI+52]
            mov ESI,93
            movups XMMWORD PTR [RSP+32],XMM0
            movups XMMWORD PTR [RSP+48],XMM0
            movups XMMWORD PTR [RSP+64],XMM0
            movups XMMWORD PTR [RSP+80],XMM0
            movups XMMWORD PTR [RSP+96],XMM0
            mov DWORD PTR [RSP+12],EAX
            call .L_7430

            mov DWORD PTR [RSP+20],EAX
            cmp EAX,-1
            je .L_9278

            movsxd RAX,DWORD PTR [RSP+12]
            mov EBX,DWORD PTR [R13+52]
            add RAX,RBP
            mov QWORD PTR [RSP+24],RAX
            test EBX,EBX
            js .L_959c

            mov R14D,DWORD PTR [R13+24]
            cmp EBX,R14D
            jge .L_96e0

            mov RCX,QWORD PTR [R13+16]
            movsxd RBP,EBX
            cmp BYTE PTR [RCX+RBP*1],40
            je .L_9390

            mov QWORD PTR [RSP],RCX
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RCX,QWORD PTR [RSP]
            mov RSI,QWORD PTR [RAX]
            movzx EAX,BYTE PTR [RCX+RBP*1]
            add RAX,RAX
.L_916c:

            movzx EAX,WORD PTR [RSI+RAX*1]
            test AH,32
            jne .L_92f8

            mov EAX,EBX
.L_917b:

            cmp R14D,EAX
            jle .L_9192

            mov R14,QWORD PTR [R13+16]
            movsxd RDX,EAX
            cmp BYTE PTR [R14+RDX*1],91
            je .L_9250
.L_9192:

            mov EAX,DWORD PTR [R13+80]
            mov DWORD PTR [R13+52],EBX
            mov BYTE PTR [R13+48],0
            mov EDX,EAX
            and EDX,8192
            test EAX,2097152
            je .L_91b9

            test BYTE PTR [RSP+16],1
            je .L_92c8
.L_91b9:

            mov DWORD PTR [RSP+8],0
.L_91c1:

            test EDX,EDX
            jne .L_9278

            mov EDX,DWORD PTR [RSP+40]
.L_91cd:

            test EDX,EDX
            jne .L_91ef

            mov EDI,DWORD PTR [RSP+44]
            test EDI,EDI
            jne .L_95d0
.L_91dd:

            mov RAX,QWORD PTR [RSP+24]
            mov QWORD PTR [RSP+32],RAX
            mov EAX,DWORD PTR [RSP+20]
            mov DWORD PTR [RSP+40],EAX
.L_91ef:

            mov RAX,QWORD PTR [R13+72]
            xor R12D,R12D
            lea RBP,QWORD PTR [RSP+32]
            movsxd RBX,DWORD PTR [RAX+16]
            mov RAX,QWORD PTR [RAX+8]
            mov QWORD PTR [RSP],RAX
            test RBX,RBX
            jne .L_921f

            jmp .L_9278
          .byte 0x66
          .byte 0x90
.L_9210:

            je .L_9310

            lea R12,QWORD PTR [R14+1]
.L_921a:

            cmp RBX,R12
            jbe .L_927d
.L_921f:

            lea RDX,QWORD PTR [R12+RBX*1]
            mov RAX,QWORD PTR [RSP]
            mov RDI,RBP
            shr RDX,1
            lea R8,QWORD PTR [RDX+RDX*4]
            mov R14,RDX
            shl R8,4
            lea R15,QWORD PTR [RAX+R8*1]
            mov RSI,R15
            call QWORD PTR [RIP+__mkd_footsort@GOTPCREL]

            test EAX,EAX
            jns .L_9210

            mov RBX,R14
            jmp .L_921a
          .byte 0x66
          .byte 0x90
.L_9250:

            lea EBX,DWORD PTR [RAX+1]
            mov RDX,R13
            mov ESI,93
            mov EDI,91
            mov DWORD PTR [R13+52],EBX
            call .L_7430

            mov EDX,EAX
            cmp EAX,-1
            jne .L_9580

            nop
            nop
            nop
            nop
.L_9278:

            lea RBP,QWORD PTR [RSP+32]
.L_927d:

            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_freefootnote@GOTPCREL]
.L_9286:

            mov EAX,DWORD PTR [RSP+12]
            mov BYTE PTR [R13+48],0
            mov DWORD PTR [RSP+8],0
            mov DWORD PTR [R13+52],EAX
.L_929b:

            mov RAX,QWORD PTR [RSP+120]
            sub RAX,QWORD PTR FS:[40]
            jne .L_977d

            mov EAX,DWORD PTR [RSP+8]
            add RSP,136
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_92c8:

.cfi_restore_state 
            mov R8D,DWORD PTR [RSP+20]
            mov DWORD PTR [RSP+8],0
            test R8D,R8D
            je .L_91c1

            mov RAX,QWORD PTR [RSP+24]
            cmp BYTE PTR [RAX],94
            sete AL
            movzx EAX,AL
            mov DWORD PTR [RSP+8],EAX
            jmp .L_91c1
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_92f8:

            cmp EBX,R14D
            jge .L_9192

            lea EAX,DWORD PTR [RBX+1]
            mov DWORD PTR [R13+52],EAX
            jmp .L_917b
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_9310:

            test R15,R15
            je .L_927d

            mov ESI,DWORD PTR [RSP+8]
            test ESI,ESI
            je .L_9548

            mov EAX,DWORD PTR [R15+72]
            test AL,2
            jne .L_927d

            test BYTE PTR [R13+83],32
            jne .L_9668

            or EAX,2
            mov RDX,QWORD PTR [R13+64]
            lea RSI,QWORD PTR [RIP+.L_e490]
            mov RDI,R13
            mov DWORD PTR [R15+72],EAX
            mov RAX,QWORD PTR [R13+72]
            mov ECX,DWORD PTR [RAX]
            add ECX,1
            test RDX,RDX
            mov DWORD PTR [RAX],ECX
            lea RAX,QWORD PTR [RIP+.L_e1b6]
            mov R9D,ECX
            cmove RDX,RAX
            sub RSP,8
.cfi_def_cfa_offset 200
            mov DWORD PTR [R15+68],ECX
            xor EAX,EAX
            push RCX
.cfi_def_cfa_offset 208
            mov R8,RDX
            call .L_7be0

            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_freefootnote@GOTPCREL]

            pop RDX
.cfi_def_cfa_offset 200
            pop RCX
.cfi_def_cfa_offset 192
            jmp .L_929b
.L_9390:

            add EBX,1
            mov RDI,R13
            mov DWORD PTR [R13+52],EBX
            call .L_75d0

            cmp EAX,-1
            je .L_9278

            mov R14D,DWORD PTR [R13+52]
            mov RBP,QWORD PTR [R13+16]
            cmp EAX,60
            je .L_95f0

            mov DWORD PTR [RSP+8],0
            movsxd RAX,R14D
            add RAX,RBP
.L_93c7:

            mov QWORD PTR [RSP+48],RAX
            mov DWORD PTR [RSP+56],0
            test R14D,R14D
            js .L_9278

            lea RBP,QWORD PTR [RSP+32]
            jmp .L_942d
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_93e8:

            test BYTE PTR [RSP+16],1
            je .L_9463

            mov RSI,RBP
            mov RDI,R13
            call .L_76f0

            test EAX,EAX
            jne .L_94e8

            mov EAX,DWORD PTR [RSP+56]
            mov R14D,DWORD PTR [R13+52]
            mov EBX,DWORD PTR [R13+24]
            lea R15D,DWORD PTR [RAX+1]
.L_9412:

            cmp EBX,R14D
            jle .L_941f

            add R14D,1
            mov DWORD PTR [R13+52],R14D
.L_941f:

            mov DWORD PTR [RSP+56],R15D
            test R14D,R14D
            js .L_927d
.L_942d:

            cmp R14D,DWORD PTR [R13+24]
            jge .L_927d

            mov RAX,QWORD PTR [R13+16]
            movsxd R14,R14D
            movsx ESI,BYTE PTR [RAX+R14*1]
            movzx R12D,SIL
            cmp SIL,41
            je .L_94e8

            cmp R12D,34
            je .L_94d0

            cmp R12D,39
            je .L_94d0
.L_945d:

            cmp R12D,61
            je .L_93e8
.L_9463:

            mov EDI,DWORD PTR [RSP+56]
            mov R14D,DWORD PTR [R13+52]
            mov EBX,DWORD PTR [R13+24]
            mov DWORD PTR [RSP],EDI
            lea R15D,DWORD PTR [RDI+1]
            cmp R12D,92
            jne .L_9412

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov ECX,R14D
            mov EDI,DWORD PTR [RSP]
            add ECX,1
            mov R8,QWORD PTR [RAX]
            js .L_96d4

            cmp ECX,EBX
            jge .L_96d4

            mov R9,QWORD PTR [R13+16]
            movsxd RAX,ECX
            movzx EAX,BYTE PTR [R9+RAX*1]
            add RAX,RAX
.L_94ab:

            test BYTE PTR [R8+RAX*1],4
            je .L_9412

            lea R15D,DWORD PTR [RDI+2]
            cmp R14D,EBX
            jge .L_941f

            mov DWORD PTR [R13+52],ECX
            mov R14D,ECX
            jmp .L_9412
          .byte 0x90
.L_94d0:

            mov RDX,RBP
            mov RDI,R13
            call .L_7640

            test EAX,EAX
            je .L_945d

            nop
            nop
            nop
            nop
            nop
.L_94e8:

            mov EAX,DWORD PTR [R13+52]
            test EAX,EAX
            js .L_950a

            cmp EAX,DWORD PTR [R13+24]
            jge .L_950a

            mov RCX,QWORD PTR [R13+16]
            movsxd RDX,EAX
            cmp BYTE PTR [RCX+RDX*1],41
            jne .L_950a

            add EAX,1
            mov DWORD PTR [R13+52],EAX
.L_950a:

            lea RDI,QWORD PTR [RSP+48]
            call QWORD PTR [RIP+___mkd_tidy@GOTPCREL]

            mov R9D,DWORD PTR [RSP+8]
            test R9D,R9D
            je .L_9540

            movsxd RDX,DWORD PTR [RSP+56]
            mov RCX,QWORD PTR [RSP+48]
            cmp BYTE PTR [RCX+RDX*1-1],62
            mov RAX,RDX
            jne .L_9540

            sub EAX,1
            mov DWORD PTR [RSP+56],EAX
            nop
            nop
            nop
            nop
            nop
            nop
.L_9540:

            mov R8,RBP
            jmp .L_954b
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_9548:

            mov R8,R15
.L_954b:

            mov ECX,DWORD PTR [RSP+16]
            mov EDX,DWORD PTR [RSP+20]
            mov RDI,R13
            mov RSI,QWORD PTR [RSP+24]
            call .L_8d60

            mov RDI,RBP
            mov DWORD PTR [RSP+8],EAX
            call QWORD PTR [RIP+___mkd_freefootnote@GOTPCREL]

            mov EAX,DWORD PTR [RSP+8]
            test EAX,EAX
            je .L_9286

            jmp .L_929b

            nop
            nop
.L_9580:

            movsxd RAX,EBX
            mov DWORD PTR [RSP+40],EDX
            add RAX,R14
            mov DWORD PTR [RSP+8],0
            mov QWORD PTR [RSP+32],RAX
            jmp .L_91cd
.L_959c:

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RAX,QWORD PTR [RAX]
            test BYTE PTR [RAX-1],32
            je .L_9192

            mov R14D,DWORD PTR [R13+24]
            cmp R14D,EBX
            jle .L_9192

            lea EAX,DWORD PTR [RBX+1]
            mov DWORD PTR [R13+52],EAX
            test EAX,EAX
            je .L_917b

            jmp .L_9192
.L_95d0:

            mov RDI,QWORD PTR [RSP+32]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov DWORD PTR [RSP+44],0
            jmp .L_91dd
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_95f0:

            mov R12D,DWORD PTR [R13+24]
            cmp R14D,R12D
            jge .L_9601

            add R14D,1
            mov DWORD PTR [R13+52],R14D
.L_9601:

            movsxd RAX,R14D
            mov DWORD PTR [RSP+8],1
            add RAX,RBP
            test BYTE PTR [R13+81],32
            jne .L_93c7

            mov QWORD PTR [RSP+48],RAX
            mov DWORD PTR [RSP+56],0
            jmp .L_965e
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_9630:

            lea EBX,DWORD PTR [R14+1]
            movsxd RAX,R14D
            mov DWORD PTR [R13+52],EBX
            movzx EAX,BYTE PTR [RBP+RAX*1]
            cmp EAX,62
            je .L_96f5

            mov EDI,DWORD PTR [RSP+56]
            lea R15D,DWORD PTR [RDI+1]
            cmp EAX,92
            je .L_9691
.L_9656:

            mov DWORD PTR [RSP+56],R15D
            mov R14D,EBX
.L_965e:

            cmp R12D,R14D
            jg .L_9630

            jmp .L_9278
.L_9668:

            mov RDI,QWORD PTR [RSP+24]
            mov ESI,DWORD PTR [RSP+20]
            xor R8D,R8D
            mov RCX,R13
            mov EDX,DWORD PTR [RIP+.L_12198]
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov RDI,RBP
            call QWORD PTR [RIP+___mkd_freefootnote@GOTPCREL]

            jmp .L_929b
.L_9691:

            mov DWORD PTR [RSP],EDI
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            add R14D,2
            mov EDI,DWORD PTR [RSP]
            mov R8,QWORD PTR [RAX]
            js .L_9783

            cmp R12D,R14D
            jle .L_9783

            movsxd RAX,R14D
            movzx EAX,BYTE PTR [RBP+RAX*1]
.L_96bb:

            test BYTE PTR [R8+RAX*2],4
            je .L_9656

            lea R15D,DWORD PTR [RDI+2]
            cmp R12D,EBX
            jle .L_9656

            mov DWORD PTR [R13+52],R14D
            mov EBX,R14D
            jmp .L_9656
.L_96d4:

            mov RAX,-2
            jmp .L_94ab
.L_96e0:

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RSI,QWORD PTR [RAX]
            mov RAX,-2
            jmp .L_916c
.L_96f5:

            mov RDI,R13
            lea RBP,QWORD PTR [RSP+32]
            call .L_75d0

            mov EBX,EAX
            cmp EAX,39
            je .L_9769

            cmp EAX,34
            je .L_9769
.L_970e:

            cmp EBX,61
            jne .L_975f

            test BYTE PTR [RSP+16],1
            je .L_975f

            mov RSI,RBP
            mov RDI,R13
            call .L_76f0

            test EAX,EAX
            je .L_927d
.L_972d:

            mov EAX,DWORD PTR [R13+52]
            test EAX,EAX
            js .L_974f

            cmp EAX,DWORD PTR [R13+24]
            jge .L_974f

            mov RCX,QWORD PTR [R13+16]
            movsxd RDX,EAX
            cmp BYTE PTR [RCX+RDX*1],41
            jne .L_974f

            add EAX,1
            mov DWORD PTR [R13+52],EAX
.L_974f:

            lea RDI,QWORD PTR [RSP+48]
            call QWORD PTR [RIP+___mkd_tidy@GOTPCREL]

            jmp .L_9540
.L_975f:

            cmp EBX,41
            je .L_972d

            jmp .L_927d
.L_9769:

            movsx ESI,BL
            mov RDX,RBP
            mov RDI,R13
            call .L_7640

            test EAX,EAX
            je .L_970e

            jmp .L_972d
.L_977d:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.L_9783:

            or RAX,-1
            jmp .L_96bb
.cfi_endproc 

            nop
            nop
            nop
            nop
.L_9790:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14,RDI
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,104
.cfi_def_cfa_offset 160
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+88],RAX
            xor EAX,EAX
            mov EAX,DWORD PTR [RDI+80]
            mov DWORD PTR [RSP+84],0
            mov DWORD PTR [RSP+12],EAX
            mov EAX,DWORD PTR [RDI+24]
            mov R12D,EAX
            mov DWORD PTR [RSP+68],EAX
            mov EAX,DWORD PTR [RDI+52]
            mov R13D,R12D
            mov DWORD PTR [RSP+8],EAX
            nop
            nop
            nop
            nop
.L_97d8:

            test DWORD PTR [RSP+12],16384
            je .L_9828

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RBX,QWORD PTR [RAX]
            mov EAX,DWORD PTR [RSP+8]
            test EAX,EAX
            js .L_9828

            cmp R13D,EAX
            jle .L_9bdb

            mov RDI,QWORD PTR [R14+16]
            movsxd RSI,EAX
            mov QWORD PTR [RSP+56],RSI
            mov RAX,RDI
            mov QWORD PTR [RSP+16],RDI
            add RAX,RSI
            movzx EDX,BYTE PTR [RAX]
            test BYTE PTR [RBX+RDX*2+1],4
            je .L_9854
.L_981d:

            test BYTE PTR [RSP+12],32
            je .L_9c56
.L_9828:

            mov EAX,DWORD PTR [RSP+8]
            cmp EAX,R13D
            jge .L_9be8

            mov RDI,QWORD PTR [R14+16]
            mov EBX,DWORD PTR [R14+80]
            movsxd RSI,EAX
            mov QWORD PTR [RSP+56],RSI
            mov QWORD PTR [RSP+16],RDI
            mov RAX,RDI
            mov DWORD PTR [RSP+12],EBX
            add RAX,RSI
.L_9854:

            mov EDI,DWORD PTR [RSP+8]
            lea EBX,DWORD PTR [RDI+1]
            mov DWORD PTR [R14+52],EBX
            movzx EBP,BYTE PTR [RAX]
            mov DWORD PTR [RSP+24],EBX
            mov BYTE PTR [RSP+28],BPL
            mov DWORD PTR [RSP+76],EBP
            test DWORD PTR [RSP+12],536870948
            jne .L_9ac9

            lea ESI,DWORD PTR [RDI-1]
            neg EDI
            mov RBX,QWORD PTR [RSP+16]
            add RBX,QWORD PTR [RSP+56]
            mov EDX,EDI
            movsxd RDI,ESI
            lea R15,QWORD PTR [RIP+.L_11748]
            mov DWORD PTR [RSP+64],ESI
            mov QWORD PTR [RSP+48],RDI
            shr EDX,31
            mov RDI,RBX
            mov EAX,39
            mov EBX,R13D
            mov DWORD PTR [RSP+44],EDX
            mov R13,R15
            xor R12D,R12D
            mov QWORD PTR [RSP+32],R14
            mov R15,RDI
            jmp .L_98df
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_98c8:

            add R12D,1
            add R13,32
            cmp R12D,20
            je .L_9aa8
.L_98da:

            movsx EAX,BYTE PTR [R13-8]
.L_98df:

            cmp EBP,EAX
            jne .L_98c8

            mov R14,QWORD PTR [R13]
            cmp BYTE PTR [R14],124
            je .L_99e0
.L_98f1:

            mov RDI,R14
            call QWORD PTR [RIP+strlen@GOTPCREL]

            mov R10D,EAX
            test EAX,EAX
            je .L_98c8

            movsxd RDX,EAX
            cmp BYTE PTR [R14+RDX*1-1],124
            je .L_9a30
.L_9910:

            cmp R10D,1
            mov DWORD PTR [RSP+40],R10D
            jle .L_996c

            call QWORD PTR [RIP+__ctype_tolower_loc@GOTPCREL]

            mov EDX,DWORD PTR [RSP+24]
            movsxd R10,DWORD PTR [RSP+40]
            mov R11,QWORD PTR [RAX]
            mov EAX,1
            nop
            nop
            nop
            nop
            nop
            nop
.L_9938:

            test EDX,EDX
            js .L_9b80

            cmp EBX,EDX
            jle .L_9b80

            movzx ECX,BYTE PTR [R15+RAX*1]
            shl RCX,2
.L_9951:

            movsx ESI,BYTE PTR [R14+RAX*1]
            cmp DWORD PTR [R11+RCX*1],ESI
            jne .L_98c8

            add RAX,1
            add EDX,1
            cmp R10,RAX
            jne .L_9938
.L_996c:

            movsxd R12,R12D
            lea RBP,QWORD PTR [RIP+.L_11740]
            mov R14,QWORD PTR [RSP+32]
            mov RAX,R12
            shl RAX,5
            mov RDX,QWORD PTR [RBP+RAX*1+16]
            test RDX,RDX
            je .L_99b0

            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e1f2]
            mov RDI,R14
            call .L_7be0

            mov EAX,DWORD PTR [R14+52]
            mov DWORD PTR [RSP+24],EAX
            mov EAX,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+68],EAX
            nop
            nop
            nop
.L_99b0:

            shl R12,5
            mov EAX,DWORD PTR [RSP+24]
            add EAX,DWORD PTR [RBP+R12*1+24]
            mov DWORD PTR [RSP+8],EAX
            js .L_9d07

            mov DWORD PTR [R14+52],EAX
            mov EAX,DWORD PTR [R14+80]
            mov R13D,DWORD PTR [RSP+68]
            mov DWORD PTR [RSP+12],EAX
            jmp .L_97d8
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_99e0:

            cmp BYTE PTR [RSP+44],0
            je .L_9a27

            cmp EBX,DWORD PTR [RSP+64]
            jle .L_9a27

            mov RAX,QWORD PTR [RSP+16]
            mov RDI,QWORD PTR [RSP+48]
            movzx EDX,BYTE PTR [RAX+RDI*1]
            mov BYTE PTR [RSP+40],DL
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movzx ECX,BYTE PTR [RSP+40]
            mov RAX,QWORD PTR [RAX]
            movzx EAX,WORD PTR [RAX+RCX*2]
            test CL,CL
            js .L_9b00

            test AH,32
            jne .L_9a27

            cmp CL,31
            ja .L_9b00
.L_9a27:

            add R14,1
            jmp .L_98f1
.L_9a30:

            lea R10D,DWORD PTR [RAX-1]
            mov EAX,DWORD PTR [RSP+8]
            add EAX,R10D
            js .L_9910

            cmp EBX,EAX
            jle .L_9910

            mov RDI,QWORD PTR [RSP+16]
            cdqe 
            mov DWORD PTR [RSP+72],R10D
            movzx EDX,BYTE PTR [RDI+RAX*1]
            mov BYTE PTR [RSP+40],DL
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movzx ECX,BYTE PTR [RSP+40]
            mov R10D,DWORD PTR [RSP+72]
            mov RAX,QWORD PTR [RAX]
            test CL,CL
            js .L_9a88

            test BYTE PTR [RAX+RCX*2+1],32
            jne .L_9910

            cmp CL,31
            jbe .L_9910
.L_9a88:

            test BYTE PTR [RAX+RCX*2],4
            jne .L_9910

            add R12D,1
            add R13,32
            cmp R12D,20
            jne .L_98da

            nop
            nop
            nop
            nop
.L_9aa8:

            movzx EAX,BYTE PTR [RSP+28]
            mov R14,QWORD PTR [RSP+32]
            mov R13D,EBX
            cmp AL,39
            je .L_9b0d

            cmp AL,96
            je .L_9c20

            cmp AL,34
            je .L_9b90
.L_9ac9:

            movzx EAX,BYTE PTR [RSP+28]
            cmp AL,62
            jg .L_9b60

            test AL,AL
            js .L_9b28

            movzx EAX,BYTE PTR [RSP+28]
            cmp AL,62
            ja .L_9b28

            lea RCX,QWORD PTR [RIP+.L_e548]
            movzx EDX,AL
            movsxd RAX,DWORD PTR [RCX+RDX*4]
            add RAX,RCX
            jmp RAX
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_9b00:

            test AL,4
            jne .L_9a27

            jmp .L_98c8
.L_9b0d:

            lea RDI,QWORD PTR [RSP+84]
            mov RDX,R14
            mov ESI,115
            call .L_7cf0

            test EAX,EAX
            jne .L_9b3d

            nop
            nop
            nop
            nop
            nop
.L_9b28:

            movzx EAX,BYTE PTR [RSP+28]
            mov BYTE PTR [R14+48],AL
.L_9b31:

            mov EDI,DWORD PTR [RSP+76]
            mov RSI,R14
.L_9b38:

            call .L_78e0
.L_9b3d:

            mov EAX,DWORD PTR [R14+80]
            mov DWORD PTR [RSP+12],EAX
            mov EAX,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+68],EAX
            mov R13D,EAX
            mov EAX,DWORD PTR [R14+52]
            mov DWORD PTR [RSP+8],EAX
            jmp .L_97d8

            nop
            nop
            nop
.L_9b60:

            movzx EAX,BYTE PTR [RSP+28]
            sub EAX,91
            cmp AL,35
            ja .L_9b28

            lea RDX,QWORD PTR [RIP+.L_e644]
            movzx EAX,AL
            movsxd RAX,DWORD PTR [RDX+RAX*4]
            add RAX,RDX
            jmp RAX
          .byte 0x90
.L_9b80:

            mov RCX,-4
            jmp .L_9951
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_9b90:

            lea RDI,QWORD PTR [RSP+84]
            mov RDX,R14
            mov ESI,100
            call .L_7cf0

            test EAX,EAX
            jne .L_9b3d
.L_9ba6:

            test BYTE PTR [R14+80],32
            je .L_a59a

            mov EAX,38
            lea RBX,QWORD PTR [RIP+.L_e1c9]
            nop
            nop
            nop
.L_9bc0:

            movsx EDI,AL
            add RBX,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_9bc0

            jmp .L_9b3d
.L_9bdb:

            test BYTE PTR [RBX-1],4
            jne .L_981d

            nop
            nop
            nop
.L_9be8:

            mov DWORD PTR [R14+52],0
            mov DWORD PTR [R14+24],0
            mov RAX,QWORD PTR [RSP+88]
            sub RAX,QWORD PTR FS:[40]
            jne .L_ab5d

            add RSP,104
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_9c20:

.cfi_restore_state 
            mov EAX,DWORD PTR [RSP+24]
            test EAX,EAX
            js .L_9c3e

            cmp EAX,EBX
            jge .L_9c3e

            movsxd RBX,EAX
            mov RAX,QWORD PTR [RSP+16]
            cmp BYTE PTR [RAX+RBX*1],96
            je .L_a3ac
.L_9c3e:

            test BYTE PTR [RSP+12],32
            je .L_a5a7
.L_9c49:

            mov RSI,R14
            mov EDI,96
            jmp .L_9b38
.L_9c56:

            mov EAX,DWORD PTR [RSP+8]
            mov EDX,1
            xor EBP,EBP
            lea R12D,DWORD PTR [RAX-1]
            jmp .L_9ca0
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_9c70:

            test AL,AL
            js .L_9c96

            test BYTE PTR [RBX+RAX*2+1],32
            jne .L_9cd4

            mov ESI,R15D
            lea RDI,QWORD PTR [RIP+.L_e1e6]
            call QWORD PTR [RIP+strchr@GOTPCREL]

            test RAX,RAX
            jne .L_9cd4

            cmp R15D,13
            je .L_9cd4
.L_9c96:

            lea EDX,DWORD PTR [RBP+2]
            lea EAX,DWORD PTR [RDX+R12*1]
.L_9c9d:

            add EBP,1
.L_9ca0:

            cmp EAX,R13D
            jge .L_9cd4

            mov RCX,QWORD PTR [R14+16]
            cdqe 
            movzx EAX,BYTE PTR [RCX+RAX*1]
            movzx R15D,AL
            cmp AL,92
            jne .L_9c70

            lea ECX,DWORD PTR [RBP+2]
            lea EAX,DWORD PTR [RCX+R12*1]
            cmp EAX,R13D
            jge .L_a676

            lea ECX,DWORD PTR [RDX+2]
            mov EBP,EDX
            lea EAX,DWORD PTR [RCX+R12*1]
            mov EDX,ECX
            jmp .L_9c9d
.L_9cd4:

            cmp EBP,1
            jle .L_9828

            mov ESI,EBP
            mov RDI,R14
            call .L_8810

            test EAX,EAX
            jne .L_a84c

            mov EAX,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+68],EAX
            mov R13D,EAX
            mov EAX,DWORD PTR [R14+52]
            mov DWORD PTR [RSP+8],EAX
            jmp .L_9828
.L_9d07:

            mov EAX,DWORD PTR [R14+80]
            mov R13D,DWORD PTR [RSP+68]
            mov DWORD PTR [RSP+12],EAX
            mov EAX,DWORD PTR [RSP+24]
            mov DWORD PTR [RSP+8],EAX
            jmp .L_97d8
.L_9d21:

            test DWORD PTR [RSP+12],2096
            je .L_a64e
.L_9d2f:

            mov RSI,R14
            mov EDI,126
            jmp .L_9b38
.L_9d3c:

            mov EAX,DWORD PTR [RSP+8]
            mov R12D,DWORD PTR [R14+24]
            mov EBX,DWORD PTR [RSP+24]
            sub EAX,1
            cmp R12D,EAX
            setg DL
            cmp EBX,1
            setg CL
            and EDX,ECX
            test DWORD PTR [RSP+12],528
            je .L_a5e9
.L_9d67:

            test DL,DL
            je .L_9d99

            mov RDI,QWORD PTR [RSP+16]
            cdqe 
            movzx R13D,BYTE PTR [RDI+RAX*1]
.L_9d77:

            test R13B,R13B
            js .L_9dee

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R8,RAX
            movzx EAX,R13B
            mov RDX,QWORD PTR [R8]
            test BYTE PTR [RDX+RAX*2+1],32
            jne .L_9d99

            cmp R13B,31
            ja .L_9dee
.L_9d99:

            mov EAX,DWORD PTR [RSP+24]
            cmp EAX,R12D
            not EAX
            setl R15B
            shr EAX,31
            and R15D,EAX
.L_9dac:

            test R15B,R15B
            je .L_9b31

            movsxd RAX,DWORD PTR [RSP+24]
            mov RDI,QWORD PTR [RSP+16]
            movzx R15D,BYTE PTR [RDI+RAX*1]
            test R15B,R15B
            js .L_9dee

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R8,RAX
            movsx RAX,R15B
            mov RDX,QWORD PTR [R8]
            test BYTE PTR [RDX+RAX*2+1],32
            jne .L_9b31

            cmp R15B,31
            jbe .L_9b31
.L_9dee:

            test BYTE PTR [RSP+12],32
            jne .L_9b31

            mov EAX,DWORD PTR [RSP+24]
            mov R15D,1
            test EAX,EAX
            js .L_a4b0

            cmp EAX,R12D
            jge .L_a4b0

            mov ECX,DWORD PTR [RSP+76]
            mov RSI,QWORD PTR [RSP+16]
            mov EAX,DWORD PTR [RSP+24]
            jmp .L_9e3b
.L_9e23:

            add EAX,1
            mov DWORD PTR [R14+52],EAX
.L_9e2a:

            test EAX,EAX
            js .L_a4b0

            cmp EAX,R12D
            jge .L_a4b0
.L_9e3b:

            movsxd RDX,EAX
            movzx EDX,BYTE PTR [RSI+RDX*1]
            cmp ECX,EDX
            jne .L_a4b0

            add R15D,1
            cmp EAX,R12D
            jl .L_9e23

            mov EAX,DWORD PTR [R14+52]
            jmp .L_9e2a
.L_9e59:

            test DWORD PTR [RSP+12],304
            jne .L_a0a9

            movzx EBP,BYTE PTR [R14+48]
            test BPL,BPL
            je .L_a0a9

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RCX,QWORD PTR [RAX]
            movsx RAX,BPL
            test WORD PTR [RCX+RAX*2],8196
            je .L_9e94

            cmp BPL,41
            jne .L_a0a9
.L_9e94:

            mov EAX,DWORD PTR [RSP+24]
            mov ESI,DWORD PTR [R14+24]
            test EAX,EAX
            js .L_a0a9

            cmp EAX,ESI
            jge .L_a0a9

            movsxd RBP,EAX
            add RBP,QWORD PTR [RSP+16]
            movzx EAX,BYTE PTR [RBP]
            test AL,AL
            js .L_a8e8

            movsx RDX,AL
            test BYTE PTR [RCX+RDX*2+1],32
            jne .L_a0a9

            cmp AL,31
            jbe .L_a0a9

            cmp AL,40
            jne .L_a8e8

            mov EAX,DWORD PTR [RSP+8]
            mov RDX,R14
            mov ESI,41
            add RBP,1
            mov EDI,40
            add EAX,2
            mov DWORD PTR [R14+52],EAX
            call .L_7430

            mov R12D,EAX
            test EAX,EAX
            jle .L_a09c
.L_9f0b:

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e1d3]
.L_9f17:

            movsx EDI,AL
            add RBX,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_9f17

            lea R8,QWORD PTR [RIP+.L_e1f7]
            mov RCX,R14
            xor EDX,EDX
            mov ESI,R12D
            mov RDI,RBP
            lea RBX,QWORD PTR [RIP+.L_e1d9]
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov EAX,60
.L_9f51:

            movsx EDI,AL
            add RBX,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_9f51

            jmp .L_9b3d
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_9f70:

            test BYTE PTR [RSP+12],32
            je .L_a5d2
.L_9f7b:

            mov RSI,R14
            mov EDI,91
            jmp .L_9b38
.L_9f88:

            mov EAX,DWORD PTR [RSP+24]
            cmp EAX,R13D
            jge .L_a821

            mov R15D,DWORD PTR [RSP+8]
            movsxd RBX,EAX
            mov RAX,QWORD PTR [RSP+16]
            add R15D,2
            mov DWORD PTR [R14+52],R15D
            movzx EBP,BYTE PTR [RAX+RBX*1]
            mov R12D,EBP
            cmp EBP,60
            je .L_a930

            jle .L_a020

            cmp EBP,94
            je .L_a89e

            cmp EBP,124
            jne .L_a474
.L_9fce:

            test DWORD PTR [RSP+12],1024
            je .L_a066
.L_9fdc:

            mov RSI,R14
            mov EDI,92
            call .L_78e0

            mov EAX,DWORD PTR [R14+52]
            mov DWORD PTR [RSP+8],EAX
            sub EAX,1
            js .L_aab5

            mov EDI,DWORD PTR [R14+80]
            mov DWORD PTR [R14+52],EAX
            mov DWORD PTR [RSP+8],EAX
            mov DWORD PTR [RSP+12],EDI
            mov EDI,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+68],EDI
            mov R13D,EDI
            jmp .L_97d8
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_a020:

            cmp EBP,40
            je .L_ab45

            jle .L_a070

            cmp EBP,58
            je .L_9fce
.L_a030:

            mov RBX,QWORD PTR [R14+56]
            movsx R12D,R12B
            test RBX,RBX
            jne .L_a055

            jmp .L_a881
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_a048:

            mov RBX,QWORD PTR [RBX+8]
            test RBX,RBX
            je .L_a881
.L_a055:

            mov RDI,QWORD PTR [RBX]
            mov ESI,R12D
            call QWORD PTR [RIP+strchr@GOTPCREL]

            test RAX,RAX
            je .L_a048
.L_a066:

            mov RSI,R14
            mov EDI,EBP
            jmp .L_9b38
.L_a070:

            mov EAX,38
            lea RBX,QWORD PTR [RIP+.L_e122]
            cmp EBP,38
            jne .L_a030
.L_a081:

            movsx EDI,AL
            add RBX,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_a081

            jmp .L_9b3d
.L_a09c:

            mov EAX,DWORD PTR [RSP+24]
            mov BYTE PTR [R14+48],0
            mov DWORD PTR [R14+52],EAX
.L_a0a9:

            mov RSI,R14
            mov EDI,94
            jmp .L_9b38
.L_a0b6:

            mov EAX,DWORD PTR [RSP+24]
            test EAX,EAX
            js .L_a0d5

            cmp EAX,R13D
            jge .L_a0d5

            movsxd RBX,EAX
            mov RAX,QWORD PTR [RSP+16]
            cmp BYTE PTR [RAX+RBX*1],91
            je .L_a7d7
.L_a0d5:

            mov RSI,R14
            mov EDI,33
            jmp .L_9b38
.L_a0e2:

            test BYTE PTR [RSP+12],32
            jne .L_a44d

            mov ESI,DWORD PTR [RSP+24]
            test ESI,ESI
            js .L_a44d

            mov EDI,DWORD PTR [RSP+12]
            mov R12D,1
            mov EAX,DWORD PTR [RSP+24]
            xor EBP,EBP
            mov QWORD PTR [RSP+32],R14
            xor EBX,EBX
            mov R14D,R13D
            mov R13D,R12D
            and EDI,134217728
            mov DWORD PTR [RSP+28],1
            mov DWORD PTR [RSP+40],EDI
            jmp .L_a185
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_a130:

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RAX,QWORD PTR [RAX]
            movzx EDX,WORD PTR [RAX+R15*2]
            test DH,32
            jne .L_a67d

            mov EAX,DWORD PTR [RSP+8]
            lea R13D,DWORD PTR [RBP+2]
            add EAX,R13D
            cmp R12D,47
            je .L_a17a

            mov ECX,DWORD PTR [RSP+40]
            test ECX,ECX
            je .L_a16c

            cmp R12D,45
            je .L_a17a

            cmp R12D,95
            je .L_a17a
.L_a16c:

            mov EDI,DWORD PTR [RSP+28]
            and EDX,8
            cmove EDI,EBX
            mov DWORD PTR [RSP+28],EDI
.L_a17a:

            add EBP,1
            test EAX,EAX
            js .L_a448
.L_a185:

            cmp R14D,EAX
            jle .L_a448

            mov RDI,QWORD PTR [RSP+16]
            cdqe 
            movzx R15D,BYTE PTR [RDI+RAX*1]
            movzx R12D,R15B
            cmp R15B,62
            je .L_a67d

            cmp R12D,92
            jne .L_a130

            mov EDI,DWORD PTR [RSP+8]
            lea EDX,DWORD PTR [RBP+2]
            mov EAX,EDI
            add EAX,EDX
            js .L_a448

            cmp R14D,EAX
            jle .L_aacd

            lea EDX,DWORD PTR [R13+2]
            mov EBP,R13D
            mov DWORD PTR [RSP+28],0
            lea EAX,DWORD PTR [RDX+RDI*1]
            mov R13D,EDX
            jmp .L_a17a
.L_a1df:

            mov EAX,DWORD PTR [RSP+8]
            mov R12D,DWORD PTR [R14+24]
            sub EAX,1
            cmp EAX,R12D
            setl DL
            cmp DWORD PTR [RSP+24],1
            setg CL
            and EDX,ECX
            jmp .L_9d67
.L_a1ff:

            movsxd RAX,DWORD PTR [RSP+24]
            test EAX,EAX
            js .L_a26c

            cmp EAX,R13D
            jge .L_a21c

            mov RDI,QWORD PTR [RSP+16]
            cmp BYTE PTR [RDI+RAX*1],35
            je .L_a7b8
.L_a21c:

            mov EBP,DWORD PTR [R14+24]
            mov EAX,1
.L_a225:

            mov EDI,DWORD PTR [RSP+8]
            mov R13,QWORD PTR [RSP+16]
            movsxd RBX,DWORD PTR [RSP+24]
            lea R12D,DWORD PTR [RAX+RDI*1+1]
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_a240:

            cmp EBP,EBX
            jle .L_a26c

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movzx EDX,BYTE PTR [R13+RBX*1]
            mov RAX,QWORD PTR [RAX]
            test BYTE PTR [RAX+RDX*2],8
            jne .L_a398

            mov RAX,QWORD PTR [RSP+16]
            cmp BYTE PTR [RAX+RBX*1],59
            je .L_a874
.L_a26c:

            movsx EDI,BYTE PTR [RSP+28]
            lea RBX,QWORD PTR [RIP+.L_e122]
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_a280:

            add RBX,1
            mov RSI,R14
            call .L_78e0

            movsx EDI,BYTE PTR [RBX]
            test DIL,DIL
            jne .L_a280

            jmp .L_9b3d
.L_a299:

            test DWORD PTR [RSP+12],1073741824
            je .L_9b28

            mov EAX,DWORD PTR [RSP+24]
            test EAX,EAX
            js .L_9b28

            cmp EAX,R13D
            jge .L_9b28

            movsxd RBX,EAX
            mov RAX,QWORD PTR [RSP+16]
            cmp BYTE PTR [RAX+RBX*1],36
            jne .L_9b28

            mov EAX,DWORD PTR [RSP+8]
            mov EDX,36
            mov ESI,36
            mov RDI,R14
            add EAX,2
            mov DWORD PTR [R14+52],EAX
            call .L_8370

            test EAX,EAX
            jne .L_a82e

            mov RSI,R14
            mov EDI,36
            call .L_78e0

            jmp .L_9b28
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_a308:

            test BYTE PTR [RSP+12],32
            je .L_a58d

            mov EAX,38
            lea RBX,QWORD PTR [RIP+.L_e138]
            nop
.L_a320:

            movsx EDI,AL
            add RBX,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_a320

            jmp .L_9b3d
.L_a33b:

            mov EDX,DWORD PTR [RSP+12]
            lea RBX,QWORD PTR [RIP+.L_e090]
            and EDX,32
            cmp EDX,1
            sbb EAX,EAX
            and EAX,28
            add EAX,32
            test EDX,EDX
            lea RDX,QWORD PTR [RIP+.L_e1e0]
            cmove RBX,RDX
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_a368:

            movsx EDI,AL
            add RBX,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_a368

            jmp .L_9b3d
.L_a383:

            mov EAX,DWORD PTR [RSP+24]
            mov R13D,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+8],EAX
            jmp .L_97d8
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_a398:

            movsxd RBX,R12D
            add R12D,1
            test EBX,EBX
            jns .L_a240

            jmp .L_a26c
.L_a3ac:

            mov EAX,DWORD PTR [RSP+8]
            add EAX,2
            cmp R13D,EAX
            jle .L_9c3e

            test EAX,EAX
            js .L_9c3e

            mov RDI,QWORD PTR [RSP+16]
            mov EBP,2
            cdqe 
.L_a3d0:

            movzx ECX,BYTE PTR [RDI+RAX*1]
            cmp ECX,92
            je .L_a414
.L_a3d9:

            cmp ECX,96
            je .L_9c3e

            mov EAX,DWORD PTR [RSP+8]
            lea EBX,DWORD PTR [RBP+1]
            add EAX,EBX
            mov ESI,EAX
            cmp R13D,EAX
            not ESI
            setg DL
            shr ESI,31
            and EDX,ESI
            cmp ECX,39
            je .L_a430

            mov EBP,EBX
.L_a401:

            test DL,DL
            je .L_9c3e

            cdqe 
            movzx ECX,BYTE PTR [RDI+RAX*1]
            cmp ECX,92
            jne .L_a3d9
.L_a414:

            mov EAX,DWORD PTR [RSP+8]
            add EBP,2
            add EAX,EBP
            mov EDX,EAX
            not EDX
            mov ECX,EDX
            shr ECX,31
            cmp EAX,R13D
            setl DL
            and EDX,ECX
            jmp .L_a401
.L_a430:

            test DL,DL
            je .L_9c3e

            cdqe 
            cmp BYTE PTR [RDI+RAX*1],39
            je .L_aa20

            mov EBP,EBX
            jmp .L_a3d0
.L_a448:

            mov R14,QWORD PTR [RSP+32]
.L_a44d:

            mov EAX,38
            lea RBX,QWORD PTR [RIP+.L_e128]
.L_a459:

            movsx EDI,AL
            add RBX,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_a459

            jmp .L_9b3d
.L_a474:

            cmp EBP,91
            jne .L_a030

            test DWORD PTR [RSP+12],1073741824
            je .L_a030

            mov EDX,93
.L_a490:

            mov ESI,92
            mov RDI,R14
            call .L_8370

            test EAX,EAX
            je .L_a030

            jmp .L_9b3d
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_a4b0:

            movsxd RBX,DWORD PTR [R14+40]
            mov EAX,DWORD PTR [R14+44]
            mov RDI,QWORD PTR [R14+32]
            cmp EBX,EAX
            jl .L_a4ec

            add EAX,100
            movsxd RDX,EAX
            mov DWORD PTR [R14+44],EAX
            lea RSI,QWORD PTR [RDX+RDX*2]
            shl RSI,4
            test RDI,RDI
            je .L_a98a

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBX,DWORD PTR [R14+40]
            mov RDI,RAX
.L_a4e8:

            mov QWORD PTR [R14+32],RDI
.L_a4ec:

            lea EAX,DWORD PTR [RBX+1]
            pxor XMM0,XMM0
            mov DWORD PTR [R14+40],EAX
            lea RAX,QWORD PTR [RBX+RBX*2]
            movzx EBX,BYTE PTR [RSP+28]
            shl RAX,4
            add RDI,RAX
            xor EAX,EAX
            cmp BL,42
            setne AL
            movups XMMWORD PTR [RDI+8],XMM0
            add EAX,1
            mov QWORD PTR [RDI+40],0
            mov BYTE PTR [RDI+8],BL
            movups XMMWORD PTR [RDI+24],XMM0
            movsxd RBX,DWORD PTR [R14+40]
            mov DWORD PTR [RDI],EAX
            mov EAX,DWORD PTR [R14+44]
            mov DWORD PTR [RDI+4],R15D
            mov RDI,QWORD PTR [R14+32]
            cmp EBX,EAX
            jl .L_a567

            add EAX,100
            movsxd RDX,EAX
            mov DWORD PTR [R14+44],EAX
            lea RSI,QWORD PTR [RDX+RDX*2]
            shl RSI,4
            test RDI,RDI
            je .L_a99b

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBX,DWORD PTR [R14+40]
            mov RDI,RAX
.L_a563:

            mov QWORD PTR [R14+32],RDI
.L_a567:

            lea EAX,DWORD PTR [RBX+1]
            pxor XMM0,XMM0
            mov DWORD PTR [R14+40],EAX
            lea RAX,QWORD PTR [RBX+RBX*2]
            shl RAX,4
            add RDI,RAX
            movups XMMWORD PTR [RDI],XMM0
            movups XMMWORD PTR [RDI+16],XMM0
            movups XMMWORD PTR [RDI+32],XMM0
            jmp .L_9b3d
.L_a58d:

            mov RSI,R14
            mov EDI,62
            jmp .L_9b38
.L_a59a:

            mov RSI,R14
            mov EDI,34
            jmp .L_9b38
.L_a5a7:

            lea R8,QWORD PTR [RIP+.L_8290]
            mov ECX,1
            mov EDX,1
            mov RDI,R14
            mov ESI,96
            call .L_79e0

            test EAX,EAX
            je .L_9c49

            jmp .L_9b3d
.L_a5d2:

            xor EDI,EDI
            mov RSI,R14
            call .L_90b0

            test EAX,EAX
            je .L_9f7b

            jmp .L_9b3d
.L_a5e9:

            mov ECX,EBX
            cmp EBX,R12D
            not ECX
            setl R15B
            shr ECX,31
            and R15D,ECX
            test DL,DL
            je .L_9dac

            mov RBP,QWORD PTR [RSP+16]
            cdqe 
            movzx R13D,BYTE PTR [RBP+RAX*1]
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RAX,QWORD PTR [RAX]
            movzx EDX,R13B
            test BYTE PTR [RAX+RDX*2],8
            je .L_9d77

            test R15B,R15B
            je .L_9d77

            movsxd RDX,EBX
            movzx EDX,BYTE PTR [RBP+RDX*1]
            test BYTE PTR [RAX+RDX*2],8
            je .L_9d77

            mov RSI,R14
            mov EDI,95
            jmp .L_9b38
.L_a64e:

            xor ECX,ECX
            mov EDX,2
            mov ESI,126
            mov RDI,R14
            lea R8,QWORD PTR [RIP+.L_85b0]
            call .L_79e0

            test EAX,EAX
            je .L_9d2f

            jmp .L_9b3d
.L_a676:

            mov EDX,ECX
            jmp .L_9c9d
.L_a67d:

            mov ECX,R12D
            mov R12D,R13D
            mov R13D,R14D
            mov R14,QWORD PTR [RSP+32]
            test EBP,EBP
            je .L_a44d

            mov EAX,DWORD PTR [RSP+28]
            test EAX,EAX
            jne .L_a6cb

            cmp EBP,2
            jle .L_a9bb

            movsxd RAX,DWORD PTR [RSP+24]
            add RAX,QWORD PTR [RSP+16]
            cmp BYTE PTR [RAX],33
            jne .L_a9bb

            cmp BYTE PTR [RAX+1],45
            jne .L_a9bb

            cmp BYTE PTR [RAX+2],45
            jne .L_a9bb
.L_a6cb:

            mov EAX,DWORD PTR [RSP+8]
            mov RDI,QWORD PTR [RSP+56]
            movsxd RSI,EBP
            lea RDX,QWORD PTR [RSI+RDI*1+1]
            lea EAX,DWORD PTR [RBP+RAX*1+1]
            add RDX,QWORD PTR [RSP+16]
.L_a6e5:

            test EAX,EAX
            js .L_a44d

            cmp R13D,EAX
            jle .L_a44d

            movzx ECX,BYTE PTR [RDX]
            add EAX,1
            add RDX,1
            cmp CL,62
            jne .L_a6e5

            movsxd RAX,DWORD PTR [RSP+24]
            cmp EAX,R13D
            jge .L_ac17

            mov RDI,QWORD PTR [RSP+16]
            movzx EBP,BYTE PTR [RDI+RAX*1]
.L_a71c:

            test BYTE PTR [RSP+12],8
            jne .L_a44d

            call QWORD PTR [RIP+__ctype_toupper_loc@GOTPCREL]

            mov RAX,QWORD PTR [RAX]
            mov EAX,DWORD PTR [RAX+RBP*4]
            cmp EAX,65
            je .L_abd2

            cmp EAX,73
            je .L_ab68
.L_a745:

            mov RSI,R14
            mov EDI,60
            call .L_78e0

            mov EAX,DWORD PTR [R14+52]
            mov DWORD PTR [RSP+8],EAX
            test EAX,EAX
            jns .L_a788

            jmp .L_aab5
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_a768:

            add EAX,1
            mov RSI,R14
            mov DWORD PTR [R14+52],EAX
            movzx EDI,BYTE PTR [RDX]
            call .L_78e0

            mov EDX,DWORD PTR [R14+52]
            mov EAX,EDX
            test EDX,EDX
            js .L_aab1
.L_a788:

            mov ECX,DWORD PTR [R14+24]
            mov R13D,ECX
            cmp ECX,EAX
            jle .L_a79f

            movsxd RDX,EAX
            add RDX,QWORD PTR [R14+16]
            cmp BYTE PTR [RDX],62
            jne .L_a768
.L_a79f:

            mov EDI,DWORD PTR [R14+80]
            mov DWORD PTR [RSP+68],ECX
            mov DWORD PTR [RSP+8],EAX
            mov DWORD PTR [RSP+12],EDI
            jmp .L_97d8
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_a7b8:

            mov EAX,DWORD PTR [RSP+8]
            mov EBP,DWORD PTR [R14+24]
            add EAX,2
            mov DWORD PTR [RSP+24],EAX
            js .L_a26c

            mov EAX,2
            jmp .L_a225
.L_a7d7:

            mov EAX,DWORD PTR [RSP+8]
            add EAX,2
            mov DWORD PTR [R14+52],EAX
            test BYTE PTR [RSP+12],32
            jne .L_a7fa

            mov RSI,R14
            mov EDI,1
            call .L_90b0

            test EAX,EAX
            jne .L_a82e
.L_a7fa:

            movzx EAX,BYTE PTR [RSP+28]
            lea RBX,QWORD PTR [RIP+.L_e1d0]
.L_a806:

            movsx EDI,AL
            add RBX,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_a806

            jmp .L_9b3d
.L_a821:

            mov RSI,R14
            mov EDI,92
            call .L_78e0
.L_a82e:

            mov EAX,DWORD PTR [R14+80]
            mov R13D,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+12],EAX
            mov EAX,DWORD PTR [R14+52]
            mov DWORD PTR [RSP+68],R13D
            mov DWORD PTR [RSP+8],EAX
            jmp .L_97d8
.L_a84c:

            mov EDI,DWORD PTR [R14+24]
            mov EAX,DWORD PTR [R14+52]
            mov DWORD PTR [RSP+68],EDI
            mov R13D,EDI
            mov DWORD PTR [RSP+8],EAX
            add EBP,EAX
            js .L_9828

            mov DWORD PTR [R14+52],EBP
            mov DWORD PTR [RSP+8],EBP
            jmp .L_9828
.L_a874:

            mov RSI,R14
            mov EDI,38
            jmp .L_9b38
.L_a881:

            mov ESI,EBP
            lea RDI,QWORD PTR [RIP+.L_e1fd]
            call QWORD PTR [RIP+strchr@GOTPCREL]

            test RAX,RAX
            jne .L_a066

            jmp .L_9fdc
.L_a89e:

            mov RSI,R14
            test DWORD PTR [RSP+12],272
            je .L_a9ac

            mov EDI,92
            call .L_78e0

            mov EAX,DWORD PTR [R14+52]
            mov DWORD PTR [RSP+8],EAX
            sub EAX,1
            js .L_ab1e

            mov DWORD PTR [R14+52],EAX
.L_a8ce:

            mov EDI,DWORD PTR [R14+80]
            mov R13D,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+8],EAX
            mov DWORD PTR [RSP+12],EDI
            mov DWORD PTR [RSP+68],R13D
            jmp .L_97d8
.L_a8e8:

            mov EDX,DWORD PTR [RSP+24]
            mov RDI,QWORD PTR [RSP+16]
            xor EAX,EAX
            add RDI,QWORD PTR [RSP+56]
.L_a8f8:

            mov R12D,EAX
            cmp ESI,EDX
            jle .L_a917

            test EDX,EDX
            js .L_a917

            movzx R8D,BYTE PTR [RDI+RAX*1+1]
            add EDX,1
            add RAX,1
            test BYTE PTR [RCX+R8*2],8
            jne .L_a8f8
.L_a917:

            test R12D,R12D
            je .L_a0a9

            mov EBX,DWORD PTR [RSP+24]
            add EBX,R12D
            mov DWORD PTR [R14+52],EBX
            jmp .L_9f0b
.L_a930:

            test R15D,R15D
            js .L_ab34

            cmp R15D,R13D
            jge .L_ab34

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RDI,QWORD PTR [RSP+16]
            movsxd R13,R15D
            lea RBX,QWORD PTR [RIP+.L_e128]
            mov RDX,QWORD PTR [RAX]
            mov EAX,38
            movzx ECX,BYTE PTR [RDI+R13*1]
            test BYTE PTR [RDX+RCX*2+1],32
            je .L_aae0
.L_a96f:

            movsx EDI,AL
            add RBX,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_a96f

            jmp .L_9b3d
.L_a98a:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_a4e8
.L_a99b:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_a563
.L_a9ac:

            mov EDI,94
            call .L_78e0

            jmp .L_a82e
.L_a9bb:

            mov DWORD PTR [RSP+8],ECX
            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movsxd RCX,DWORD PTR [RSP+8]
            mov RAX,QWORD PTR [RAX]
            test BYTE PTR [RAX+RCX*2+1],32
            jne .L_a44d

            mov ESI,EBP
            mov RDI,R14
            call .L_8810

            test EAX,EAX
            je .L_a44d

            mov EDI,DWORD PTR [R14+80]
            mov EAX,DWORD PTR [R14+52]
            mov EDX,R12D
            mov DWORD PTR [RSP+12],EDI
            mov EDI,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+8],EAX
            mov DWORD PTR [RSP+68],EDI
            mov R13D,EDI
            add EDX,EAX
            js .L_97d8

            mov DWORD PTR [R14+52],EDX
            mov DWORD PTR [RSP+8],EDX
            jmp .L_97d8
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_aa20:

            mov EAX,38
            lea R12,QWORD PTR [RIP+.L_e1b9]
.L_aa2c:

            movsx EDI,AL
            add R12,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [R12]
            test AL,AL
            jne .L_aa2c

            movsxd RAX,DWORD PTR [R14+52]
            mov RDX,QWORD PTR [R14+16]
            lea ESI,DWORD PTR [RBP-2]
            xor R8D,R8D
            mov RCX,R14
            lea RBP,QWORD PTR [RIP+.L_e1c1]
            lea RDI,QWORD PTR [RDX+RAX*1+1]
            xor EDX,EDX
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov EAX,38
.L_aa6e:

            movsx EDI,AL
            add RBP,1
            mov RSI,R14
            call .L_78e0

            movzx EAX,BYTE PTR [RBP]
            test AL,AL
            jne .L_aa6e

            mov EAX,DWORD PTR [R14+52]
            mov DWORD PTR [RSP+8],EAX
            add EBX,EAX
            js .L_aab5

            mov EAX,DWORD PTR [R14+80]
            mov DWORD PTR [R14+52],EBX
            mov DWORD PTR [RSP+8],EBX
            mov DWORD PTR [RSP+12],EAX
            mov EAX,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+68],EAX
            mov R13D,EAX
            jmp .L_97d8
.L_aab1:

            mov DWORD PTR [RSP+8],EDX
.L_aab5:

            mov EAX,DWORD PTR [R14+80]
            mov DWORD PTR [RSP+12],EAX
            mov EAX,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+68],EAX
            mov R13D,EAX
            jmp .L_97d8
.L_aacd:

            mov DWORD PTR [RSP+28],0
            add EBP,1
            mov R13D,EDX
            jmp .L_a185
.L_aae0:

            mov EDI,92
            mov RSI,R14
            call .L_78e0

            mov EAX,DWORD PTR [R14+52]
            mov EDI,EAX
            sub EDI,1
            mov DWORD PTR [RSP+8],EDI
            js .L_a8ce

            mov EAX,DWORD PTR [RSP+8]
            mov R13D,DWORD PTR [R14+24]
            mov DWORD PTR [R14+52],EAX
            mov EAX,DWORD PTR [R14+80]
            mov DWORD PTR [RSP+68],R13D
            mov DWORD PTR [RSP+12],EAX
            jmp .L_97d8
.L_ab1e:

            mov EAX,DWORD PTR [R14+80]
            mov R13D,DWORD PTR [R14+24]
            mov DWORD PTR [RSP+12],EAX
            mov DWORD PTR [RSP+68],R13D
            jmp .L_97d8
.L_ab34:

            mov EAX,38
            lea RBX,QWORD PTR [RIP+.L_e128]
            jmp .L_a96f
.L_ab45:

            test DWORD PTR [RSP+12],1073741824
            je .L_a030

            mov EDX,41
            jmp .L_a490
.L_ab5d:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]

            nop
            nop
            nop
            nop
            nop
.L_ab68:

            test BYTE PTR [RSP+12],2
            je .L_a745

            movsxd RBX,DWORD PTR [RSP+24]
            mov R15,QWORD PTR [RSP+16]
            mov EDX,2
            lea RSI,QWORD PTR [RIP+.L_e1fa]
            lea RDI,QWORD PTR [R15+RBX*1+1]
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            test EAX,EAX
            jne .L_a745

            mov EBX,DWORD PTR [RSP+8]
            add EBX,4
            js .L_a44d

            cmp R13D,EBX
            jle .L_a44d

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movsxd RBX,EBX
            movzx EDX,BYTE PTR [R15+RBX*1]
            mov RAX,QWORD PTR [RAX]
            test BYTE PTR [RAX+RDX*2],8
            je .L_a44d

            jmp .L_a745
.L_abd2:

            test BYTE PTR [RSP+12],1
            je .L_a745

            mov EBX,DWORD PTR [RSP+8]
            add EBX,2
            js .L_a44d

            cmp R13D,EBX
            jle .L_a44d

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov RDI,QWORD PTR [RSP+16]
            movsxd RBX,EBX
            mov RAX,QWORD PTR [RAX]
            movzx EDX,BYTE PTR [RDI+RBX*1]
            test BYTE PTR [RAX+RDX*2],8
            jne .L_a745

            jmp .L_a44d
.L_ac17:

            or RBP,-1
            jmp .L_a71c
.cfi_endproc 
.L_ac20:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            mov R15,RCX
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14,RDI
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            mov R12,RDX
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov RBX,RSI
            sub RSP,88
.cfi_def_cfa_offset 144
            mov QWORD PTR [RSP+8],RDI
            mov RDI,RCX
            mov QWORD PTR [RSP+48],RSI
            call QWORD PTR [RIP+___mkd_emblock@GOTPCREL]

            test RBX,RBX
            je .L_b6c4

            test R12,R12
            lea RAX,QWORD PTR [RIP+.L_e0af]
            mov RDI,R15
            mov RDX,RBX
            lea RSI,QWORD PTR [RIP+.L_e20e]
            mov RCX,R12
            cmove RSI,RAX
            xor EAX,EAX
            call .L_7be0

            mov RDI,R15
            call QWORD PTR [RIP+___mkd_emblock@GOTPCREL]

            test R14,R14
            je .L_b614
.L_ac8d:

            mov RAX,QWORD PTR [RSP+8]
            mov EBX,DWORD PTR [RAX+40]
            cmp EBX,14
            ja .L_ada7

            nop
            nop
.L_aca0:

            lea RCX,QWORD PTR [RIP+.L_e6d4]
            mov EDX,EBX
            movsxd RAX,DWORD PTR [RCX+RDX*4]
            add RAX,RCX
            jmp RAX
.L_acb2:

            mov RAX,QWORD PTR [RSP+8]
            mov RBP,QWORD PTR [RAX+8]
            test RBP,RBP
            je .L_ad50

            cmp EBX,7
            je .L_b7ad

            mov EDX,111
            mov RDI,R15
            xor EAX,EAX
            mov R12D,111
            lea RSI,QWORD PTR [RIP+.L_e2b1]
            call .L_7be0

            cmp EBX,9
            je .L_b87d
.L_acf2:

            lea RSI,QWORD PTR [RIP+.L_e2f4]
            mov RDI,R15
            xor EAX,EAX
            call .L_7be0

            lea RBX,QWORD PTR [RIP+.L_e2c0]
            nop
            nop
            nop
            nop
            nop
            nop
.L_ad10:

            mov RDX,QWORD PTR [RBP+24]
            mov RDI,QWORD PTR [RBP+8]
            mov RCX,R15
            mov RSI,RBX
            call .L_ac20

            mov RSI,R15
            mov EDI,10
            call .L_78e0

            mov RBP,QWORD PTR [RBP]
            test RBP,RBP
            jne .L_ad10

            mov EDX,R12D
            lea RSI,QWORD PTR [RIP+.L_e2c3]
            mov RDI,R15
            xor EAX,EAX
            call .L_7be0

            nop
            nop
            nop
.L_ad50:

            mov RAX,QWORD PTR [RSP+8]
            mov RAX,QWORD PTR [RAX]
            mov QWORD PTR [RSP+8],RAX
            test RAX,RAX
            je .L_b60c

            mov RDI,R15
            lea RBX,QWORD PTR [RIP+.L_e216]
            call QWORD PTR [RIP+___mkd_emblock@GOTPCREL]

            mov EAX,10
            nop
            nop
            nop
            nop
            nop
.L_ad80:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_ad80

            mov RAX,QWORD PTR [RSP+8]
            mov EBX,DWORD PTR [RAX+40]
            cmp EBX,14
            jbe .L_aca0
.L_ada7:

            mov RAX,QWORD PTR [RSP+8]
            mov RBX,QWORD PTR [RAX+16]
            movsxd RBP,DWORD PTR [RAX+44]
            nop
            nop
            nop
            nop
.L_adb8:

            test RBX,RBX
            je .L_ae1b
.L_adbd:

            mov EAX,DWORD PTR [RBX+8]
            mov RDX,QWORD PTR [RBX+16]
            test EAX,EAX
            je .L_b5e0

            test RDX,RDX
            je .L_ade7

            cmp EAX,2
            jle .L_ade7

            mov RDI,QWORD PTR [RBX]
            movsxd RDX,EAX
            cmp BYTE PTR [RDI+RDX*1-2],32
            je .L_b68b
.L_ade7:

            mov RDI,RBX
            call QWORD PTR [RIP+___mkd_tidy@GOTPCREL]

            mov ESI,DWORD PTR [RBX+8]
            mov RDI,QWORD PTR [RBX]
            mov RDX,R15
            call .L_74e0

            cmp QWORD PTR [RBX+16],0
            je .L_ae1b

            mov RSI,R15
            mov EDI,10
            call .L_7570

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            jne .L_adbd
.L_ae1b:

            lea RAX,QWORD PTR [RIP+.L_11700]
            mov RBX,QWORD PTR [RAX+RBP*8]
            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            je .L_ae46

            nop
            nop
            nop
.L_ae30:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_ae30
.L_ae46:

            mov RDI,R15
            call .L_9790

            lea RAX,QWORD PTR [RIP+.L_116e0]
            mov RBX,QWORD PTR [RAX+RBP*8]
            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            je .L_ad50

            nop
            nop
            nop
            nop
.L_ae68:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_ae68

            jmp .L_ad50
.L_ae83:

            mov RAX,QWORD PTR [RSP+8]
            xor EBX,EBX
            mov RBP,QWORD PTR [RAX+16]
            test RBP,RBP
            jne .L_aeb0

            jmp .L_ad50
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_aea0:

            add EBX,1
.L_aea3:

            mov RBP,QWORD PTR [RBP+16]
            test RBP,RBP
            je .L_ad50
.L_aeb0:

            mov EAX,DWORD PTR [RBP+8]
            test EAX,EAX
            je .L_aea0

            test EBX,EBX
            je .L_aed5

            nop
            nop
            nop
            nop
            nop
.L_aec0:

            mov RSI,R15
            mov EDI,10
            call .L_78e0

            sub EBX,1
            jne .L_aec0

            mov EAX,DWORD PTR [RBP+8]
.L_aed5:

            mov RBX,QWORD PTR [RBP]
            movsxd R12,EAX
            add R12,RBX
            test EAX,EAX
            jle .L_aefc

            nop
            nop
            nop
            nop
            nop
.L_aee8:

            movsx EDI,BYTE PTR [RBX]
            add RBX,1
            mov RSI,R15
            call .L_78e0

            cmp RBX,R12
            jne .L_aee8
.L_aefc:

            mov RSI,R15
            mov EDI,10
            call .L_78e0

            mov RBP,QWORD PTR [RBP+16]
            test RBP,RBP
            je .L_ad50

            mov EAX,DWORD PTR [RBP+8]
            test EAX,EAX
            jne .L_aed5

            mov EBX,1
            jmp .L_aea3
.L_af27:

            mov RCX,QWORD PTR [RSP+8]
            lea RAX,QWORD PTR [RIP+.L_e236]
            lea RSI,QWORD PTR [RIP+.L_e232]
            mov RDX,QWORD PTR [RCX+24]
            mov RDI,QWORD PTR [RCX+8]
            mov RCX,R15
            test RDX,RDX
            cmove RSI,RAX
            call .L_ac20

            jmp .L_ad50
.L_af56:

            mov RAX,QWORD PTR [RSP+8]
            mov RBP,QWORD PTR [RAX+8]
            test RBP,RBP
            je .L_ad50

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e241]
            nop
            nop
            nop
            nop
.L_af78:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_af78

            nop
            nop
.L_af90:

            mov RBX,QWORD PTR [RBP+16]
            test RBX,RBX
            je .L_b00f

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_afa0:

            mov EDI,60
            lea R12,QWORD PTR [RIP+.L_e247]
            nop
            nop
            nop
            nop
.L_afb0:

            add R12,1
            mov RSI,R15
            call .L_78e0

            movsx EDI,BYTE PTR [R12]
            test DIL,DIL
            jne .L_afb0

            mov RDI,QWORD PTR [RBX]
            mov ESI,DWORD PTR [RBX+8]
            xor R8D,R8D
            mov RCX,R15
            xor EDX,EDX
            lea R12,QWORD PTR [RIP+.L_e24c]
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov EDI,60
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_aff0:

            add R12,1
            mov RSI,R15
            call .L_78e0

            movsx EDI,BYTE PTR [R12]
            test DIL,DIL
            jne .L_aff0

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            jne .L_afa0
.L_b00f:

            mov RDX,QWORD PTR [RBP+24]
            mov RDI,QWORD PTR [RBP+8]
            mov RCX,R15
            lea RSI,QWORD PTR [RIP+.L_e2cb]
            call .L_ac20

            mov RSI,R15
            mov EDI,10
            call .L_78e0

            mov RBP,QWORD PTR [RBP]
            test RBP,RBP
            jne .L_af90

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e253]
            nop
            nop
            nop
            nop
.L_b050:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b050

            jmp .L_ad50
.L_b06b:

            mov R13,QWORD PTR [R15+88]
            mov RAX,QWORD PTR [RSP+8]
            cmp QWORD PTR [R13+40],0
            mov RBX,QWORD PTR [RAX+32]
            mov RBP,QWORD PTR [RAX+16]
            je .L_b126

            test RBP,RBP
            je .L_b893

            mov RAX,RBP
            xor EDX,EDX
            nop
            nop
            nop
.L_b098:

            mov ECX,DWORD PTR [RAX+8]
            mov RAX,QWORD PTR [RAX+16]
            lea EDX,DWORD PTR [RDX+RCX*1+1]
            test RAX,RAX
            jne .L_b098

            add EDX,1
            xor R14D,R14D
            movsxd RDI,EDX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov R12,RAX
            nop
            nop
            nop
            nop
            nop
            nop
.L_b0c0:

            movsxd RDI,R14D
            movsxd RDX,DWORD PTR [RBP+8]
            mov RSI,QWORD PTR [RBP]
            add RDI,R12
            call QWORD PTR [RIP+memcpy@GOTPCREL]

            mov EAX,DWORD PTR [RBP+8]
            add EAX,R14D
            lea R14D,DWORD PTR [RAX+1]
            cdqe 
            mov BYTE PTR [R12+RAX*1],10
            mov RBP,QWORD PTR [RBP+16]
            test RBP,RBP
            jne .L_b0c0

            movsxd RAX,R14D
            add RAX,R12
.L_b0f4:

            mov BYTE PTR [RAX],0
            xor EDX,EDX
            mov RAX,QWORD PTR [R13+40]
            test RBX,RBX
            je .L_b109

            cmp BYTE PTR [RBX],0
            cmovne RDX,RBX
.L_b109:

            mov RDI,R12
            mov ESI,R14D
            call RAX

            mov RDI,R12
            mov RBP,RAX
            call QWORD PTR [RIP+free@GOTPCREL]

            test RBP,RBP
            jne .L_b641
.L_b126:

            mov EAX,60
            lea R12,QWORD PTR [RIP+.L_e219]
            nop
            nop
            nop
            nop
            nop
            nop
.L_b138:

            movsx EDI,AL
            add R12,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [R12]
            test AL,AL
            jne .L_b138

            test RBX,RBX
            je .L_b15e

            cmp BYTE PTR [RBX],0
            jne .L_b7ce
.L_b15e:

            mov RSI,R15
            mov EDI,62
            xor EBX,EBX
            call .L_78e0

            test RBP,RBP
            jne .L_b4c0
.L_b176:

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e224]
            nop
            nop
            nop
            nop
            nop
            nop
.L_b188:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b188

            jmp .L_ad50
.L_b1a3:

            mov RAX,QWORD PTR [RSP+8]
            mov RCX,R15
            xor EDX,EDX
            xor ESI,ESI
            mov RDI,QWORD PTR [RAX+8]
            call .L_ac20

            jmp .L_ad50
.L_b1bd:

            mov EAX,DWORD PTR [R15+80]
            test EAX,67108864
            jne .L_b706

            test AH,16
            jne .L_b73c
.L_b1d5:

            mov RAX,QWORD PTR [RSP+8]
            lea RSI,QWORD PTR [RIP+.L_e2d3]
            mov RDI,R15
            mov EDX,DWORD PTR [RAX+48]
            xor EAX,EAX
            call .L_7be0
.L_b1ee:

            mov RBX,QWORD PTR [RSP+8]
            mov RDX,R15
            mov RAX,QWORD PTR [RBX+16]
            mov ESI,DWORD PTR [RAX+8]
            mov RDI,QWORD PTR [RAX]
            call .L_74e0

            mov RDI,R15
            call .L_9790

            mov EDX,DWORD PTR [RBX+48]
            mov RDI,R15
            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e2d9]
            call .L_7be0

            jmp .L_ad50
.L_b226:

            mov RAX,QWORD PTR [RSP+8]
            mov RAX,QWORD PTR [RAX+16]
            mov RCX,QWORD PTR [RAX+16]
            mov QWORD PTR [RSP+64],RAX
            mov QWORD PTR [RSP+40],RCX
            mov RCX,QWORD PTR [RCX+16]
            mov QWORD PTR [RSP+56],RCX
            mov RCX,RAX
            movsxd RAX,DWORD PTR [RAX+24]
            mov RCX,QWORD PTR [RCX]
            mov RDX,RAX
            cmp BYTE PTR [RCX+RAX*1],124
            je .L_b6e0
.L_b25d:

            mov RAX,QWORD PTR [RSP+40]
            mov DWORD PTR [RSP+36],0
            xor R12D,R12D
            mov QWORD PTR [RSP+16],0
            mov R14D,DWORD PTR [RAX+24]
            mov EBX,DWORD PTR [RAX+8]
            mov QWORD PTR [RSP+24],0
            mov RBP,QWORD PTR [RAX]
            cmp R14D,EBX
            jge .L_b384

            mov QWORD PTR [RSP+72],R15
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_b2a0:

            mov BYTE PTR [RSP+35],0
            mov EAX,R14D
            xor R12D,R12D
            jmp .L_b2da
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_b2b0:

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            movsx RDX,R13B
            mov RSI,QWORD PTR [RAX]
            mov EAX,R14D
            test BYTE PTR [RSI+RDX*2+1],32
            jne .L_b2d6

            cmp BYTE PTR [RSP+35],0
            mov R12D,R13D
            jne .L_b2d6

            mov BYTE PTR [RSP+35],R13B
.L_b2d6:

            cmp EAX,EBX
            jge .L_b304
.L_b2da:

            movsxd RDX,EAX
            lea R14D,DWORD PTR [RAX+1]
            movzx R13D,BYTE PTR [RBP+RDX*1]
            cmp R13B,124
            je .L_b307

            lea R15D,DWORD PTR [RAX+2]
            cmp R13B,92
            jne .L_b2b0

            lea EDX,DWORD PTR [RAX+3]
            mov EAX,R15D
            mov R15D,EDX
            cmp EAX,EBX
            jl .L_b2da
.L_b304:

            mov R14D,R15D
.L_b307:

            cmp BYTE PTR [RSP+35],58
            je .L_b5e8

            xor EBX,EBX
            cmp R12B,58
            sete BL
            lea EBX,DWORD PTR [RBX+RBX*2]
.L_b31e:

            mov EAX,DWORD PTR [RSP+36]
            mov ECX,DWORD PTR [RSP+16]
            cmp EAX,ECX
            jg .L_b354

            add EAX,100
            mov RDI,QWORD PTR [RSP+24]
            mov DWORD PTR [RSP+36],EAX
            cdqe 
            lea RSI,QWORD PTR [RAX*4]
            test RDI,RDI
            je .L_b5f9

            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov QWORD PTR [RSP+24],RAX
.L_b354:

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+24]
            mov DWORD PTR [RCX+RAX*4],EBX
            mov RCX,QWORD PTR [RSP+40]
            lea R12D,DWORD PTR [RAX+1]
            add RAX,1
            mov QWORD PTR [RSP+16],RAX
            mov EBX,DWORD PTR [RCX+8]
            cmp R14D,EBX
            jl .L_b2a0

            mov R15,QWORD PTR [RSP+72]
.L_b384:

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e272]
.L_b390:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b390

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e27b]
            nop
            nop
            nop
            nop
            nop
            nop
.L_b3b8:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b3b8

            mov RDX,QWORD PTR [RSP+24]
            mov RDI,QWORD PTR [RSP+64]
            mov R9,R15
            xor R8D,R8D
            mov ECX,R12D
            lea RSI,QWORD PTR [RIP+.L_e2e0]
            lea RBP,QWORD PTR [RIP+.L_e284]
            call .L_8b50

            mov EBX,EAX
            mov EAX,60
            nop
            nop
            nop
            nop
            nop
.L_b400:

            movsx EDI,AL
            add RBP,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBP]
            test AL,AL
            jne .L_b400

            cmp EBX,R12D
            jl .L_b50c

            jle .L_b8a9

            movsxd RBP,R12D
            mov RDI,QWORD PTR [RSP+24]
            mov R13D,DWORD PTR [RSP+36]
            shl RBP,2
            jmp .L_b45a
.L_b439:

            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov RDI,RAX
.L_b442:

            add R12D,1
            mov DWORD PTR [RDI+RBP*1],0
            add RBP,4
            cmp EBX,R12D
            je .L_b502
.L_b45a:

            cmp R12D,R13D
            jl .L_b442

            add R13D,100
            movsxd RAX,R13D
            lea RSI,QWORD PTR [RAX*4]
            test RDI,RDI
            jne .L_b439

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_b442
.L_b481:

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e259]
            nop
            nop
            nop
.L_b490:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b490

            jmp .L_ad50
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_b4b0:

            add EBX,1
.L_b4b3:

            mov RBP,QWORD PTR [RBP+16]
            test RBP,RBP
            je .L_b176
.L_b4c0:

            mov EDX,DWORD PTR [RBP+8]
            cmp EDX,DWORD PTR [RBP+24]
            jle .L_b4b0

            test EBX,EBX
            je .L_b4e5

            nop
            nop
            nop
            nop
.L_b4d0:

            mov RSI,R15
            mov EDI,10
            call .L_78e0

            sub EBX,1
            jne .L_b4d0

            mov EDX,DWORD PTR [RBP+8]
.L_b4e5:

            mov RSI,QWORD PTR [RBP]
            mov RDI,R15
            xor EBX,EBX
            call .L_8190

            mov RSI,R15
            mov EDI,10
            call .L_78e0

            jmp .L_b4b3
.L_b502:

            mov QWORD PTR [RSP+24],RDI
            mov DWORD PTR [RSP+36],R13D
.L_b50c:

            mov EAX,60
            lea RBP,QWORD PTR [RIP+.L_e28e]
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_b520:

            movsx EDI,AL
            add RBP,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBP]
            test AL,AL
            jne .L_b520

            cmp QWORD PTR [RSP+56],0
            lea RBP,QWORD PTR [RIP+.L_e2e3]
            je .L_b573

            mov R13,QWORD PTR [RSP+24]
            mov R12,QWORD PTR [RSP+56]
.L_b550:

            mov RDI,R12
            mov R9,R15
            mov R8D,1
            mov ECX,EBX
            mov RDX,R13
            mov RSI,RBP
            call .L_8b50

            mov R12,QWORD PTR [R12+16]
            test R12,R12
            jne .L_b550
.L_b573:

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e297]
            nop
.L_b580:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b580

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e2a1]
            nop
            nop
            nop
            nop
            nop
            nop
.L_b5a8:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b5a8

            mov EAX,DWORD PTR [RSP+36]
            test EAX,EAX
            je .L_ad50

            mov RDI,QWORD PTR [RSP+24]
            call QWORD PTR [RIP+free@GOTPCREL]

            jmp .L_ad50
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_b5e0:

            mov RBX,RDX
            jmp .L_adb8
.L_b5e8:

            xor EBX,EBX
            cmp R12B,58
            setne BL
            add EBX,1
            jmp .L_b31e
.L_b5f9:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov QWORD PTR [RSP+24],RAX
            jmp .L_b354
.L_b60c:

            cmp QWORD PTR [RSP+48],0
            je .L_b62a
.L_b614:

            mov RDX,QWORD PTR [RSP+48]
            lea RSI,QWORD PTR [RIP+.L_e2ab]
            mov RDI,R15
            xor EAX,EAX
            call .L_7be0
.L_b62a:

            add RSP,88
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov RDI,R15
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+___mkd_emblock@GOTPCREL]
.L_b641:

.cfi_restore_state 
            mov RDI,RBP
            call QWORD PTR [RIP+strlen@GOTPCREL]

            test EAX,EAX
            jle .L_b66d

            sub EAX,1
            mov RBX,RBP
            lea R12,QWORD PTR [RBP+RAX*1+1]
.L_b659:

            movsx EDI,BYTE PTR [RBX]
            add RBX,1
            mov RSI,R15
            call .L_78e0

            cmp RBX,R12
            jne .L_b659
.L_b66d:

            mov RDX,QWORD PTR [R15+88]
            mov RAX,QWORD PTR [RDX+32]
            test RAX,RAX
            je .L_ad50

            mov RSI,QWORD PTR [RDX]
            mov RDI,RBP
            call RAX

            jmp .L_ad50
.L_b68b:

            cmp BYTE PTR [RDI+RDX*1-1],32
            jne .L_ade7

            lea ESI,DWORD PTR [RAX-2]
            mov RDX,R15
            call .L_74e0

            mov RSI,R15
            mov EDI,13
            call .L_7570

            mov RSI,R15
            mov EDI,10
            call .L_7570

            mov RBX,QWORD PTR [RBX+16]
            jmp .L_adb8
.L_b6c4:

            mov RDI,R15
            call QWORD PTR [RIP+___mkd_emblock@GOTPCREL]

            cmp QWORD PTR [RSP+8],0
            jne .L_ac8d

            jmp .L_b62a
          .byte 0x66
          .byte 0x90
.L_b6e0:

            mov RAX,QWORD PTR [RSP+40]
            mov RCX,QWORD PTR [RSP+64]
            jmp .L_b6f6
.L_b6ec:

            mov EDX,DWORD PTR [RAX+24]
            mov RCX,RAX
            mov RAX,QWORD PTR [RAX+16]
.L_b6f6:

            add EDX,1
            mov DWORD PTR [RCX+24],EDX
            test RAX,RAX
            jne .L_b6ec

            jmp .L_b25d
.L_b706:

            mov RAX,QWORD PTR [RSP+8]
            lea RSI,QWORD PTR [RIP+.L_e2ce]
            mov RDI,R15
            mov EDX,DWORD PTR [RAX+48]
            xor EAX,EAX
            call .L_7be0

            test BYTE PTR [R15+81],16
            jne .L_b821
.L_b72a:

            mov RSI,R15
            mov EDI,62
            call .L_78e0

            jmp .L_b1ee
.L_b73c:

            mov EAX,60
            lea RBX,QWORD PTR [RIP+.L_e260]
.L_b748:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b748

            mov RAX,QWORD PTR [RSP+8]
            mov R9,R15
            mov R8D,1
            mov RCX,R15
            lea RDX,QWORD PTR [RIP+.L_78e0]
            lea RBX,QWORD PTR [RIP+.L_e26a]
            mov RAX,QWORD PTR [RAX+16]
            mov ESI,DWORD PTR [RAX+8]
            mov RDI,QWORD PTR [RAX]
            call QWORD PTR [RIP+mkd_string_to_anchor@GOTPCREL]

            mov EAX,34
.L_b792:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b792

            jmp .L_b1d5
.L_b7ad:

            mov EDX,117
            mov RDI,R15
            xor EAX,EAX
            mov R12D,117
            lea RSI,QWORD PTR [RIP+.L_e2b1]
            call .L_7be0

            jmp .L_acf2
.L_b7ce:

            mov EAX,32
            lea R12,QWORD PTR [RIP+.L_e451]
.L_b7da:

            movsx EDI,AL
            add R12,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [R12]
            test AL,AL
            jne .L_b7da

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            je .L_b80f
.L_b7f9:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b7f9
.L_b80f:

            mov RSI,R15
            mov EDI,34
            call .L_78e0

            jmp .L_b15e
.L_b821:

            mov EAX,32
            lea RBX,QWORD PTR [RIP+.L_e40a]
.L_b82d:

            movsx EDI,AL
            add RBX,1
            mov RSI,R15
            call .L_78e0

            movzx EAX,BYTE PTR [RBX]
            test AL,AL
            jne .L_b82d

            mov RAX,QWORD PTR [RSP+8]
            mov R9,R15
            mov R8D,1
            mov RCX,R15
            lea RDX,QWORD PTR [RIP+.L_78e0]
            mov RAX,QWORD PTR [RAX+16]
            mov ESI,DWORD PTR [RAX+8]
            mov RDI,QWORD PTR [RAX]
            call QWORD PTR [RIP+mkd_string_to_anchor@GOTPCREL]

            mov RSI,R15
            mov EDI,34
            call .L_78e0

            jmp .L_b72a
.L_b87d:

            lea RSI,QWORD PTR [RIP+.L_e2b6]
            mov RDI,R15
            xor EAX,EAX
            call .L_7be0

            jmp .L_acf2
.L_b893:

            mov EDI,1
            xor R14D,R14D
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov R12,RAX
            jmp .L_b0f4
.L_b8a9:

            mov EBX,R12D
            jmp .L_b50c
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_document
.type mkd_document, @function
#-----------------------------------
mkd_document:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,8
.cfi_def_cfa_offset 64
            test RDI,RDI
            je .L_bae0

            mov R8D,DWORD PTR [RDI+56]
            mov R14,RDI
            test R8D,R8D
            je .L_bae0

            mov RCX,QWORD PTR [RDI+80]
            mov EDI,DWORD PTR [RDI+64]
            mov R12,RSI
            test EDI,EDI
            je .L_b918

            mov RDI,QWORD PTR [RCX]
            mov R13D,DWORD PTR [RCX+8]
.L_b8fc:

            mov QWORD PTR [R12],RDI
.L_b900:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov EAX,R13D
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_b918:

.cfi_restore_state 
            mov RDI,QWORD PTR [R14+48]
            xor EDX,EDX
            xor ESI,ESI
            call .L_ac20

            mov R15,QWORD PTR [R14+80]
            test BYTE PTR [R15+82],32
            jne .L_b9b0
.L_b934:

            movsxd R13,DWORD PTR [R15+8]
            mov RDI,QWORD PTR [R15]
            mov DWORD PTR [R14+64],1
            test R13D,R13D
            jne .L_b9a0
.L_b948:

            mov EAX,DWORD PTR [R15+12]
            cmp EAX,R13D
            jg .L_b978

            add EAX,100
            mov DWORD PTR [R15+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_baeb

            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov R15,QWORD PTR [R14+80]
            mov RDI,RAX
            movsxd R13,DWORD PTR [R15+8]
.L_b975:

            mov QWORD PTR [R15],RDI
.L_b978:

            lea EAX,DWORD PTR [R13+1]
            mov DWORD PTR [R15+8],EAX
            mov BYTE PTR [RDI+R13*1],0
            mov RAX,QWORD PTR [R14+80]
            mov ECX,DWORD PTR [RAX+8]
            mov RDI,QWORD PTR [RAX]
            lea R13D,DWORD PTR [RCX-1]
            mov DWORD PTR [RAX+8],R13D
            jmp .L_b8fc
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_b9a0:

            movsxd RAX,R13D
            cmp BYTE PTR [RDI+RAX*1-1],0
            je .L_b8fc

            jmp .L_b948
.L_b9b0:

            mov RAX,QWORD PTR [R15+72]
            mov ESI,DWORD PTR [RAX]
            test ESI,ESI
            je .L_b934

            lea RSI,QWORD PTR [RIP+.L_e4d0]
            mov RDI,R15
            xor EAX,EAX
            mov R13D,1
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov RAX,QWORD PTR [R15+72]
            mov ECX,DWORD PTR [RAX]
            test ECX,ECX
            jle .L_babd

            nop
            nop
            nop
            nop
.L_b9e8:

            mov EDX,DWORD PTR [RAX+16]
            xor EBP,EBP
            test EDX,EDX
            jg .L_ba0d

            jmp .L_bab0
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_ba00:

            add RBP,1
            cmp DWORD PTR [RAX+16],EBP
            jle .L_bab0
.L_ba0d:

            lea RBX,QWORD PTR [RBP+RBP*4]
            shl RBX,4
            add RBX,QWORD PTR [RAX+8]
            cmp DWORD PTR [RBX+68],R13D
            jne .L_ba00

            test BYTE PTR [RBX+72],2
            je .L_ba00

            mov RDX,QWORD PTR [R15+64]
            mov ECX,R13D
            mov RDI,R15
            lea RAX,QWORD PTR [RIP+.L_e1b6]
            lea RSI,QWORD PTR [RIP+.L_e2e6]
            test RDX,RDX
            cmove RDX,RAX
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov RDI,QWORD PTR [RBX+48]
            xor EDX,EDX
            xor ESI,ESI
            mov RCX,R15
            call .L_ac20

            mov RDX,QWORD PTR [R15+64]
            mov ECX,DWORD PTR [RBX+68]
            mov RDI,R15
            lea RAX,QWORD PTR [RIP+.L_e1b6]
            lea RSI,QWORD PTR [RIP+.L_e4f8]
            test RDX,RDX
            cmove RDX,RAX
            xor EAX,EAX
            add RBP,1
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            lea RSI,QWORD PTR [RIP+.L_e2f7]
            mov RDI,R15
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov RAX,QWORD PTR [R15+72]
            cmp DWORD PTR [RAX+16],EBP
            jg .L_ba0d

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_bab0:

            add R13D,1
            cmp R13D,DWORD PTR [RAX]
            jle .L_b9e8
.L_babd:

            mov RDI,R15
            lea RSI,QWORD PTR [RIP+.L_e2fe]
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov R15,QWORD PTR [R14+80]
            jmp .L_b934
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_bae0:

            mov R13D,4294967295
            jmp .L_b900
.L_baeb:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_b975
.cfi_endproc 

            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_freeLine
.type ___mkd_freeLine, @function
#-----------------------------------
___mkd_freeLine:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov EAX,DWORD PTR [RDI+12]
            mov RBP,RDI
            test EAX,EAX
            jne .L_bb18

            mov RDI,RBP
            pop RBP
.cfi_remember_state 
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+free@GOTPCREL]
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_bb18:

.cfi_restore_state 
            mov RDI,QWORD PTR [RDI]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov RDI,RBP
            pop RBP
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+free@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_freeLines
.type ___mkd_freeLines, @function
#-----------------------------------
___mkd_freeLines:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R14
.cfi_def_cfa_offset 16
.cfi_offset 14, -16
            push R13
.cfi_def_cfa_offset 24
.cfi_offset 13, -24
            push R12
.cfi_def_cfa_offset 32
.cfi_offset 12, -32
            push RBP
.cfi_def_cfa_offset 40
.cfi_offset 6, -40
            mov RBP,RDI
            sub RSP,8
.cfi_def_cfa_offset 48
            mov R12,QWORD PTR [RDI+16]
            test R12,R12
            je .L_bb83

            mov R13,QWORD PTR [R12+16]
            test R13,R13
            je .L_bb7a

            mov R14,QWORD PTR [R13+16]
            test R14,R14
            je .L_bb71

            mov RDI,QWORD PTR [R14+16]
            test RDI,RDI
            je .L_bb68

            call ___mkd_freeLines
.L_bb68:

            mov RDI,R14
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]
.L_bb71:

            mov RDI,R13
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]
.L_bb7a:

            mov RDI,R12
            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]
.L_bb83:

            add RSP,8
.cfi_def_cfa_offset 40
            mov RDI,RBP
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_freeParagraph
.type ___mkd_freeParagraph, @function
#-----------------------------------
___mkd_freeParagraph:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RDI
            mov RDI,QWORD PTR [RDI]
            test RDI,RDI
            je .L_bbb1

            call ___mkd_freeParagraph
.L_bbb1:

            mov RDI,QWORD PTR [RBP+8]
            test RDI,RDI
            je .L_bbbf

            call ___mkd_freeParagraph
.L_bbbf:

            mov RDI,QWORD PTR [RBP+16]
            test RDI,RDI
            je .L_bbce

            call QWORD PTR [RIP+___mkd_freeLines@GOTPCREL]
.L_bbce:

            mov RDI,QWORD PTR [RBP+24]
            test RDI,RDI
            je .L_bbdd

            call QWORD PTR [RIP+free@GOTPCREL]
.L_bbdd:

            mov RDI,QWORD PTR [RBP+32]
            test RDI,RDI
            je .L_bbec

            call QWORD PTR [RIP+free@GOTPCREL]
.L_bbec:

            mov RDI,RBP
            pop RBP
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+free@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_freefootnote
.type ___mkd_freefootnote, @function
#-----------------------------------
___mkd_freefootnote:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBX
.cfi_def_cfa_offset 16
.cfi_offset 3, -16
            mov ECX,DWORD PTR [RDI+12]
            mov RBX,RDI
            test ECX,ECX
            jne .L_bc78

            mov DWORD PTR [RDI+8],0
.L_bc12:

            mov EDX,DWORD PTR [RBX+28]
            test EDX,EDX
            jne .L_bc60

            mov DWORD PTR [RBX+24],0
.L_bc20:

            mov EAX,DWORD PTR [RBX+44]
            test EAX,EAX
            jne .L_bc48

            mov DWORD PTR [RBX+40],0
.L_bc2e:

            mov RDI,QWORD PTR [RBX+48]
            test RDI,RDI
            je .L_bc40

            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+___mkd_freeParagraph@GOTPCREL]
          .byte 0x66
          .byte 0x90
.L_bc40:

.cfi_restore_state 
            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_bc48:

.cfi_restore_state 
            mov RDI,QWORD PTR [RBX+32]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov QWORD PTR [RBX+40],0
            jmp .L_bc2e
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_bc60:

            mov RDI,QWORD PTR [RBX+16]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov QWORD PTR [RBX+24],0
            jmp .L_bc20

            nop
            nop
            nop
            nop
.L_bc78:

            mov RDI,QWORD PTR [RDI]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov QWORD PTR [RBX+8],0
            jmp .L_bc12
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_freefootnotes
.type ___mkd_freefootnotes, @function
#-----------------------------------
___mkd_freefootnotes:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov RAX,QWORD PTR [RDI+72]
            test RAX,RAX
            je .L_bd18

            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RDI
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            sub RSP,8
.cfi_def_cfa_offset 32
            mov ECX,DWORD PTR [RAX+16]
            test ECX,ECX
            jle .L_bccf

            xor EBX,EBX
            nop
            nop
            nop
            nop
            nop
.L_bcb0:

            lea RDI,QWORD PTR [RBX+RBX*4]
            add RBX,1
            shl RDI,4
            add RDI,QWORD PTR [RAX+8]
            call QWORD PTR [RIP+___mkd_freefootnote@GOTPCREL]

            mov RAX,QWORD PTR [RBP+72]
            cmp DWORD PTR [RAX+16],EBX
            jg .L_bcb0
.L_bccf:

            mov EDX,DWORD PTR [RAX+20]
            test EDX,EDX
            jne .L_bcf0

            mov DWORD PTR [RAX+16],0
            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 24
            mov RDI,RAX
            pop RBX
.cfi_restore 3
.cfi_def_cfa_offset 16
            pop RBP
.cfi_restore 6
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+free@GOTPCREL]
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_bcf0:

.cfi_restore_state 
            mov RDI,QWORD PTR [RAX+8]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov RDI,QWORD PTR [RBP+72]
            mov QWORD PTR [RDI+16],0
            add RSP,8
.cfi_def_cfa_offset 24
            pop RBX
.cfi_restore 3
.cfi_def_cfa_offset 16
            pop RBP
.cfi_restore 6
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+free@GOTPCREL]
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_bd18:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_initmmiot
.type ___mkd_initmmiot, @function
#-----------------------------------
___mkd_initmmiot:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_bd60

            push RBX
.cfi_def_cfa_offset 16
.cfi_offset 3, -16
            mov RBX,RDI
            lea RDI,QWORD PTR [RDI+8]
            xor EAX,EAX
            mov QWORD PTR [RDI-8],0
            mov RCX,RBX
            mov QWORD PTR [RDI+80],0
            and RDI,-8
            sub RCX,RDI
            add ECX,96
            shr ECX,3

            rep stosq QWORD PTR [RDI]

            test RSI,RSI
            je .L_bd68

            mov QWORD PTR [RBX+72],RSI
            pop RBX
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_bd60:

.cfi_restore 3
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_bd68:

.cfi_def_cfa_offset 16
.cfi_offset 3, -16
            mov EDI,24
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov QWORD PTR [RBX+72],RAX
            mov QWORD PTR [RAX+8],0
            mov QWORD PTR [RAX+16],0
            pop RBX
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_freemmiot
.type ___mkd_freemmiot, @function
#-----------------------------------
___mkd_freemmiot:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_be60

            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSI
            push RBX
.cfi_def_cfa_offset 24
.cfi_offset 3, -24
            mov RBX,RDI
            sub RSP,8
.cfi_def_cfa_offset 32
            mov ECX,DWORD PTR [RDI+28]
            test ECX,ECX
            jne .L_be10

            mov EDX,DWORD PTR [RBX+12]
            mov DWORD PTR [RDI+24],0
            test EDX,EDX
            jne .L_be29
.L_bdba:

            mov EAX,DWORD PTR [RBX+44]
            mov DWORD PTR [RBX+8],0
            test EAX,EAX
            jne .L_be41
.L_bdc8:

            mov DWORD PTR [RBX+40],0
.L_bdcf:

            cmp QWORD PTR [RBX+72],RBP
            je .L_bdde

            mov RDI,RBX
            call QWORD PTR [RIP+___mkd_freefootnotes@GOTPCREL]
.L_bdde:

            lea RDI,QWORD PTR [RBX+8]
            mov QWORD PTR [RBX],0
            xor EAX,EAX
            and RDI,-8
            mov QWORD PTR [RBX+88],0
            sub RBX,RDI
            lea ECX,DWORD PTR [RBX+96]
            shr ECX,3

            rep stosq QWORD PTR [RDI]

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBX
.cfi_def_cfa_offset 16
            pop RBP
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_be10:

.cfi_restore_state 
            mov RDI,QWORD PTR [RDI+16]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov EDX,DWORD PTR [RBX+12]
            mov QWORD PTR [RBX+24],0
            test EDX,EDX
            je .L_bdba
.L_be29:

            mov RDI,QWORD PTR [RBX]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov EAX,DWORD PTR [RBX+44]
            mov QWORD PTR [RBX+8],0
            test EAX,EAX
            je .L_bdc8
.L_be41:

            mov RDI,QWORD PTR [RBX+32]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov QWORD PTR [RBX+40],0
            jmp .L_bdcf
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_be60:

.cfi_def_cfa_offset 8
.cfi_restore 3
.cfi_restore 6
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_freeLineRange
.type ___mkd_freeLineRange, @function
#-----------------------------------
___mkd_freeLineRange:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBX
.cfi_def_cfa_offset 16
.cfi_offset 3, -16
            mov RBX,RDI
            mov RDI,QWORD PTR [RDI+16]
            mov RAX,RDI
            cmp RDI,RSI
            jne .L_be94

            jmp .L_be9f
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_be88:

            mov RDX,QWORD PTR [RAX+16]
            cmp RDX,RSI
            je .L_beb0

            mov RAX,RDX
.L_be94:

            test RAX,RAX
            jne .L_be88
.L_be99:

            call QWORD PTR [RIP+___mkd_freeLines@GOTPCREL]
.L_be9f:

            mov QWORD PTR [RBX+16],0
            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_beb0:

.cfi_restore_state 
            mov QWORD PTR [RAX+16],0
            mov RDI,QWORD PTR [RBX+16]
            jmp .L_be99
.cfi_endproc 

            nop
            nop
#-----------------------------------
.align 16
.globl mkd_cleanup
.type mkd_cleanup, @function
#-----------------------------------
mkd_cleanup:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_bf50

            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            cmp DWORD PTR [RDI],425723697
            mov RBP,RDI
            je .L_bee0

            pop RBP
.cfi_remember_state 
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_bee0:

.cfi_restore_state 
            mov RDI,QWORD PTR [RDI+80]
            test RDI,RDI
            je .L_befb

            xor ESI,ESI
            call QWORD PTR [RIP+___mkd_freemmiot@GOTPCREL]

            mov RDI,QWORD PTR [RBP+80]
            call QWORD PTR [RIP+free@GOTPCREL]
.L_befb:

            mov RDI,QWORD PTR [RBP+48]
            test RDI,RDI
            je .L_bf0a

            call QWORD PTR [RIP+___mkd_freeParagraph@GOTPCREL]
.L_bf0a:

            mov RDI,QWORD PTR [RBP+8]
            test RDI,RDI
            je .L_bf19

            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]
.L_bf19:

            mov RDI,QWORD PTR [RBP+16]
            test RDI,RDI
            je .L_bf28

            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]
.L_bf28:

            mov RDI,QWORD PTR [RBP+24]
            test RDI,RDI
            je .L_bf37

            call QWORD PTR [RIP+___mkd_freeLine@GOTPCREL]
.L_bf37:

            mov RDI,QWORD PTR [RBP+32]
            test RDI,RDI
            je .L_bf46

            call QWORD PTR [RIP+___mkd_freeLines@GOTPCREL]
.L_bf46:

            mov RDI,RBP
            pop RBP
.cfi_restore 6
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+free@GOTPCREL]
.L_bf50:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_doc_title
.type mkd_doc_title, @function
#-----------------------------------
mkd_doc_title:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_bf90

            mov RAX,QWORD PTR [RDI+8]
            test RAX,RAX
            je .L_bf92

            movsxd RDX,DWORD PTR [RAX+24]
            cmp DWORD PTR [RAX+8],EDX
            jle .L_bf90

            test EDX,EDX
            js .L_bf90

            add RDX,QWORD PTR [RAX]
            mov EAX,0
            cmp BYTE PTR [RDX],0
            cmovne RAX,RDX
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_bf90:

            xor EAX,EAX
.L_bf92:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_doc_author
.type mkd_doc_author, @function
#-----------------------------------
mkd_doc_author:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_bfd0

            mov RAX,QWORD PTR [RDI+16]
            test RAX,RAX
            je .L_bfd2

            movsxd RDX,DWORD PTR [RAX+24]
            cmp DWORD PTR [RAX+8],EDX
            jle .L_bfd0

            test EDX,EDX
            js .L_bfd0

            add RDX,QWORD PTR [RAX]
            mov EAX,0
            cmp BYTE PTR [RDX],0
            cmovne RAX,RDX
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_bfd0:

            xor EAX,EAX
.L_bfd2:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_doc_date
.type mkd_doc_date, @function
#-----------------------------------
mkd_doc_date:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_c010

            mov RAX,QWORD PTR [RDI+24]
            test RAX,RAX
            je .L_c012

            movsxd RDX,DWORD PTR [RAX+24]
            cmp DWORD PTR [RAX+8],EDX
            jle .L_c010

            test EDX,EDX
            js .L_c010

            add RDX,QWORD PTR [RAX]
            mov EAX,0
            cmp BYTE PTR [RDX],0
            cmovne RAX,RDX
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_c010:

            xor EAX,EAX
.L_c012:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_toc
.type mkd_toc, @function
#-----------------------------------
mkd_toc:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,56
.cfi_def_cfa_offset 112
            mov QWORD PTR [RSP+8],RSI
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+40],RAX
            xor EAX,EAX
            test RSI,RSI
            je .L_c35b

            mov R13,RDI
            test RDI,RDI
            je .L_c35b

            mov RAX,QWORD PTR [RDI+80]
            test RAX,RAX
            je .L_c35b

            mov QWORD PTR [RSI],0
            xor R15D,R15D
            test BYTE PTR [RAX+81],16
            jne .L_c0a0
.L_c075:

            mov RAX,QWORD PTR [RSP+40]
            sub RAX,QWORD PTR FS:[40]
            jne .L_c366

            add RSP,56
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov EAX,R15D
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_c0a0:

.cfi_restore_state 
            movabs RAX,858993459200
            mov EDI,200
            mov QWORD PTR [RSP+24],RAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov R12,QWORD PTR [R13+48]
            mov QWORD PTR [RSP+16],RAX
            test R12,R12
            je .L_c2ff

            mov DWORD PTR [RSP+4],1
            xor EBX,EBX
            lea R14,QWORD PTR [RIP+.L_e218]
            jmp .L_c0ed
          .byte 0x90
.L_c0e0:

            mov R12,QWORD PTR [R12]
            test R12,R12
            je .L_c260
.L_c0ed:

            cmp DWORD PTR [R12+40],14
            jne .L_c0e0

            mov RBP,QWORD PTR [R12+8]
            test RBP,RBP
            jne .L_c111

            jmp .L_c0e0
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_c108:

            mov RBP,QWORD PTR [RBP]
            test RBP,RBP
            je .L_c0e0
.L_c111:

            cmp DWORD PTR [RBP+40],11
            jne .L_c108

            cmp QWORD PTR [RBP+16],0
            je .L_c108

            mov EDX,DWORD PTR [RBP+48]
            lea R15,QWORD PTR [RSP+16]
            cmp EBX,EDX
            jle .L_c172

            nop
            nop
            nop
            nop
            nop
            nop
.L_c130:

            mov EAX,EBX
            sub EAX,EDX
            cmp EAX,1
            je .L_c14b

            lea RSI,QWORD PTR [RIP+.L_e217]
            mov RDI,R15
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]
.L_c14b:

            sub EBX,1
            mov R9,R14
            mov RCX,R14
            mov RDI,R15
            mov EDX,EBX
            mov R8D,EBX
            lea RSI,QWORD PTR [RIP+.L_e710]
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov EDX,DWORD PTR [RBP+48]
            cmp EDX,EBX
            jl .L_c130
.L_c172:

            cmp EDX,EBX
            je .L_c310

            mov EAX,DWORD PTR [RSP+4]
            test EAX,EAX
            jne .L_c190

            cmp EDX,EBX
            jg .L_c330

            nop
            nop
            nop
            nop
            nop
            nop
.L_c190:

            mov RCX,R14
            cmp EDX,EBX
            jle .L_c1db
.L_c197:

            mov EDX,EBX
            lea RSI,QWORD PTR [RIP+.L_e723]
            mov RDI,R15
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov EDX,DWORD PTR [RBP+48]
            mov EAX,EDX
            sub EAX,EBX
            add EBX,1
            cmp EAX,1
            jle .L_c190

            mov RCX,R14
            mov EDX,EBX
            lea RSI,QWORD PTR [RIP+.L_e72c]
            mov RDI,R15
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov EDX,DWORD PTR [RBP+48]
            mov RCX,R14
            cmp EDX,EBX
            jg .L_c197
.L_c1db:

            lea RSI,QWORD PTR [RIP+.L_e735]
            mov RDI,R15
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov RAX,QWORD PTR [RBP+16]
            mov R9,QWORD PTR [R13+80]
            mov RCX,R15
            mov R8D,1
            mov RDX,QWORD PTR [RIP+Csputc@GOTPCREL]
            mov ESI,DWORD PTR [RAX+8]
            mov RDI,QWORD PTR [RAX]
            call QWORD PTR [RIP+mkd_string_to_anchor@GOTPCREL]

            lea RSI,QWORD PTR [RIP+.L_e16b]
            mov RDI,R15
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov RAX,QWORD PTR [RBP+16]
            mov ECX,536870912
            mov RDI,R15
            mov EDX,DWORD PTR [RAX+8]
            mov RSI,QWORD PTR [RAX]
            call QWORD PTR [RIP+Csreparse@GOTPCREL]

            lea RSI,QWORD PTR [RIP+.L_e16e]
            mov RDI,R15
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov DWORD PTR [RSP+4],0
            jmp .L_c108
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_c260:

            lea R15,QWORD PTR [RSP+16]
            lea RBP,QWORD PTR [RIP+.L_e218]
            test EBX,EBX
            jle .L_c294
.L_c270:

            sub EBX,1
            xor EAX,EAX
            mov R9,RBP
            mov RCX,RBP
            mov R8D,EBX
            mov EDX,EBX
            lea RSI,QWORD PTR [RIP+.L_e710]
            mov RDI,R15
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            test EBX,EBX
            jne .L_c270
.L_c294:

            mov R15D,DWORD PTR [RSP+24]
            mov EAX,DWORD PTR [RSP+28]
            test R15D,R15D
            jle .L_c2f7

            mov RDI,QWORD PTR [RSP+16]
            movsxd RBX,R15D
            cmp R15D,EAX
            jl .L_c2d5

            add EAX,100
            mov DWORD PTR [RSP+28],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_c34a

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBX,DWORD PTR [RSP+24]
            mov RDI,RAX
.L_c2d0:

            mov QWORD PTR [RSP+16],RDI
.L_c2d5:

            lea EAX,DWORD PTR [RBX+1]
            mov DWORD PTR [RSP+24],EAX
            mov BYTE PTR [RDI+RBX*1],0
            mov RDI,QWORD PTR [RSP+16]
            call QWORD PTR [RIP+strdup@GOTPCREL]

            mov RCX,QWORD PTR [RSP+8]
            mov QWORD PTR [RCX],RAX
            mov EAX,DWORD PTR [RSP+28]
.L_c2f7:

            test EAX,EAX
            je .L_c075
.L_c2ff:

            mov RDI,QWORD PTR [RSP+16]
            call QWORD PTR [RIP+free@GOTPCREL]

            jmp .L_c075
          .byte 0x90
.L_c310:

            lea RSI,QWORD PTR [RIP+.L_e2f7]
            mov RDI,R15
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov EDX,DWORD PTR [RBP+48]
            jmp .L_c190
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_c330:

            lea RSI,QWORD PTR [RIP+.L_e217]
            mov RDI,R15
            xor EAX,EAX
            call QWORD PTR [RIP+Csprintf@GOTPCREL]

            mov EDX,DWORD PTR [RBP+48]
            jmp .L_c190
.L_c34a:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_c2d0
.L_c35b:

            mov R15D,4294967295
            jmp .L_c075
.L_c366:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_generatetoc
.type mkd_generatetoc, @function
#-----------------------------------
mkd_generatetoc:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R13
.cfi_def_cfa_offset 16
.cfi_offset 13, -16
            mov R13,RSI
            push R12
.cfi_def_cfa_offset 24
.cfi_offset 12, -24
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            mov EBX,4294967295
            sub RSP,16
.cfi_def_cfa_offset 48
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+8],RAX
            xor EAX,EAX
            mov RSI,RSP
            mov QWORD PTR [RSP],0
            call QWORD PTR [RIP+mkd_toc@GOTPCREL]

            mov R12D,EAX
            test EAX,EAX
            jle .L_c3c0

            mov RDI,QWORD PTR [RSP]
            movsxd RDX,EAX
            mov RCX,R13
            mov ESI,1
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            mov EBX,EAX
.L_c3c0:

            mov RDI,QWORD PTR [RSP]
            test RDI,RDI
            je .L_c3cf

            call QWORD PTR [RIP+free@GOTPCREL]
.L_c3cf:

            cmp EBX,R12D
            mov EAX,4294967295
            cmovne R12D,EAX
            mov RAX,QWORD PTR [RSP+8]
            sub RAX,QWORD PTR FS:[40]
            jne .L_c3f8

            add RSP,16
.cfi_remember_state 
.cfi_def_cfa_offset 32
            mov EAX,R12D
            pop RBX
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
.L_c3f8:

.cfi_restore_state 
            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
.L_c400:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_c470

            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            mov R12,RSI
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            mov RBP,RDI
            push RBX
.L_c40f:

.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            cmp DWORD PTR [RBP+40],5
            je .L_c438
.L_c415:

            mov RDI,QWORD PTR [RBP+8]
            test RDI,RDI
            je .L_c426

            mov RSI,R12
            call .L_c400
.L_c426:

            mov RBP,QWORD PTR [RBP]
            test RBP,RBP
            jne .L_c40f

            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_c438:

.cfi_restore_state 
            mov RBX,QWORD PTR [RBP+16]
            test RBX,RBX
            je .L_c415

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_c448:

            mov EDX,DWORD PTR [RBX+8]
            mov RSI,QWORD PTR [RBX]
            mov RDI,R12
            call QWORD PTR [RIP+Cswrite@GOTPCREL]

            mov RSI,R12
            mov EDI,10
            call QWORD PTR [RIP+Csputc@GOTPCREL]

            mov RBX,QWORD PTR [RBX+16]
            test RBX,RBX
            jne .L_c448

            jmp .L_c415
.L_c470:

.cfi_def_cfa_offset 8
.cfi_restore 3
.cfi_restore 6
.cfi_restore 12
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_css
.type mkd_css, @function
#-----------------------------------
mkd_css:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            sub RSP,32
.cfi_def_cfa_offset 64
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+24],RAX
            xor EAX,EAX
            test RSI,RSI
            je .L_c590

            mov RBX,RDI
            test RDI,RDI
            je .L_c590

            mov EDX,DWORD PTR [RDI+56]
            test EDX,EDX
            je .L_c590

            mov QWORD PTR [RSI],0
            mov RBP,RSI
            mov EDI,200
            movabs RAX,858993459200
            mov QWORD PTR [RSP+8],RAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,QWORD PTR [RBX+48]
            mov RSI,RSP
            mov QWORD PTR [RSP],RAX
            call .L_c400

            mov R12D,DWORD PTR [RSP+8]
            test R12D,R12D
            jle .L_c51f

            mov EAX,DWORD PTR [RSP+12]
            mov RDI,QWORD PTR [RSP]
            movsxd RBX,R12D
            cmp EAX,R12D
            jle .L_c548
.L_c506:

            lea EAX,DWORD PTR [RBX+1]
            mov DWORD PTR [RSP+8],EAX
            mov BYTE PTR [RDI+RBX*1],0
            mov RDI,QWORD PTR [RSP]
            call QWORD PTR [RIP+strdup@GOTPCREL]

            mov QWORD PTR [RBP],RAX
.L_c51f:

            mov EAX,DWORD PTR [RSP+12]
            test EAX,EAX
            jne .L_c570
.L_c527:

            mov RAX,QWORD PTR [RSP+24]
            sub RAX,QWORD PTR FS:[40]
            jne .L_c598

            add RSP,32
.cfi_remember_state 
.cfi_def_cfa_offset 32
            mov EAX,R12D
            pop RBX
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_c548:

.cfi_restore_state 
            add EAX,100
            mov DWORD PTR [RSP+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_c580

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBX,DWORD PTR [RSP+8]
            mov RDI,RAX
.L_c565:

            mov QWORD PTR [RSP],RDI
            jmp .L_c506
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_c570:

            mov RDI,QWORD PTR [RSP]
            call QWORD PTR [RIP+free@GOTPCREL]

            jmp .L_c527
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_c580:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_c565
          .byte 0x66
          .byte 0x90
.L_c590:

            mov R12D,4294967295
            jmp .L_c527
.L_c598:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
#-----------------------------------
.align 16
.globl mkd_generatecss
.type mkd_generatecss, @function
#-----------------------------------
mkd_generatecss:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R13
.cfi_def_cfa_offset 16
.cfi_offset 13, -16
            mov R13,RSI
            push R12
.cfi_def_cfa_offset 24
.cfi_offset 12, -24
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            xor EBX,EBX
            sub RSP,16
.cfi_def_cfa_offset 48
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+8],RAX
            xor EAX,EAX
            mov RSI,RSP
            call QWORD PTR [RIP+mkd_css@GOTPCREL]

            mov R12D,EAX
            test EAX,EAX
            jle .L_c5e5

            mov RDI,QWORD PTR [RSP]
            movsxd RDX,EAX
            mov RCX,R13
            mov ESI,1
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            mov EBX,EAX
.L_c5e5:

            mov RDI,QWORD PTR [RSP]
            test RDI,RDI
            je .L_c5f4

            call QWORD PTR [RIP+free@GOTPCREL]
.L_c5f4:

            cmp EBX,R12D
            mov EAX,4294967295
            cmovne R12D,EAX
            mov RAX,QWORD PTR [RSP+8]
            sub RAX,QWORD PTR FS:[40]
            jne .L_c61d

            add RSP,16
.cfi_remember_state 
.cfi_def_cfa_offset 32
            mov EAX,R12D
            pop RBX
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
.L_c61d:

.cfi_restore_state 
            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_generatexml
.type mkd_generatexml, @function
#-----------------------------------
mkd_generatexml:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test ESI,ESI
            jle .L_c754

            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            movsxd RSI,ESI
            mov R15,RDI
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            lea R14,QWORD PTR [RIP+.L_e218]
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            lea R13,QWORD PTR [RIP+.L_e138]
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            mov R12,RDX
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            lea RBP,QWORD PTR [RIP+.L_e750]
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            lea RBX,QWORD PTR [RDI+RSI*1]
            sub RSP,8
.cfi_def_cfa_offset 64
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_c670:

            movzx EDX,BYTE PTR [R15]
            add R15,1
            lea EAX,DWORD PTR [RDX-34]
            cmp AL,28
            ja .L_c690

            movzx EAX,AL
            movsxd RAX,DWORD PTR [RBP+RAX*4]
            add RAX,RBP
            jmp RAX
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_c690:

            movzx EDI,DL
            test DL,128
            je .L_c728

            test DL,DL
            js .L_c728

            mov RDI,R14
.L_c6a7:

            mov RSI,R12
            call QWORD PTR [RIP+fputs@GOTPCREL]

            cmp EAX,-1
            je .L_c6e1
.L_c6b5:

            cmp RBX,R15
            jne .L_c670

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 56
            xor EAX,EAX
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_c6d0:

.cfi_restore_state 
            mov RDI,R13
            mov RSI,R12
            call QWORD PTR [RIP+fputs@GOTPCREL]

            cmp EAX,-1
            jne .L_c6b5
.L_c6e1:

            mov EAX,4294967295
.L_c6e6:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_c6f8:

.cfi_restore_state 
            lea RDI,QWORD PTR [RIP+.L_e747]
            jmp .L_c6a7
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_c708:

            lea RDI,QWORD PTR [RIP+.L_e1c9]
            jmp .L_c6a7
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_c718:

            lea RDI,QWORD PTR [RIP+.L_e128]
            jmp .L_c6a7
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_c728:

            mov RSI,R12
            call QWORD PTR [RIP+fputc@GOTPCREL]

            cmp EAX,-1
            jne .L_c6b5

            mov EAX,4294967295
            jmp .L_c6e6

            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_c748:

            lea RDI,QWORD PTR [RIP+.L_e122]
            jmp .L_c6a7
.L_c754:

.cfi_def_cfa_offset 8
.cfi_restore 3
.cfi_restore 6
.cfi_restore 12
.cfi_restore 13
.cfi_restore 14
.cfi_restore 15
            xor EAX,EAX
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_xml
.type mkd_xml, @function
#-----------------------------------
mkd_xml:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            mov R15,RDI
            mov EDI,200
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,RDX
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            movsxd RBX,ESI
            sub RSP,40
.cfi_def_cfa_offset 96
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+24],RAX
            movabs RAX,858993459200
            mov QWORD PTR [RSP+8],RAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov QWORD PTR [RSP],RAX
            test EBX,EBX
            jle .L_c930

            add RBX,R15
            mov R12,RSP
            lea RBP,QWORD PTR [RIP+.L_e7c4]
            lea R14,QWORD PTR [RIP+.L_e138]
            nop
.L_c7c0:

            movzx EDX,BYTE PTR [R15]
            add R15,1
            lea EAX,DWORD PTR [RDX-34]
            cmp AL,28
            ja .L_c7e0

            movzx EAX,AL
            movsxd RAX,DWORD PTR [RBP+RAX*4]
            add RAX,RBP
            jmp RAX
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_c7e0:

            movzx EDI,DL
            test DL,128
            je .L_c8d0

            test DL,DL
            js .L_c8d0

            xor EDX,EDX
            lea RSI,QWORD PTR [RIP+.L_e218]
            nop
            nop
            nop
.L_c800:

            mov RDI,R12
            call QWORD PTR [RIP+Cswrite@GOTPCREL]
.L_c809:

            cmp R15,RBX
            jne .L_c7c0

            movsxd RBX,DWORD PTR [RSP+8]
            mov EAX,DWORD PTR [RSP+12]
            mov RDI,QWORD PTR [RSP]
            cmp EBX,EAX
            jl .L_c8e0

            add EAX,100
            mov DWORD PTR [RSP+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_c920

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBX,DWORD PTR [RSP+8]
.L_c841:

            mov QWORD PTR [RSP],RAX
            lea EDX,DWORD PTR [RBX+1]
            add RAX,RBX
.L_c84b:

            mov DWORD PTR [RSP+8],EDX
            mov BYTE PTR [RAX],0
            mov RDI,QWORD PTR [RSP]
            call QWORD PTR [RIP+strdup@GOTPCREL]

            mov QWORD PTR [R13],RAX
            mov EAX,DWORD PTR [RSP+8]
            sub EAX,1
            mov RCX,QWORD PTR [RSP+24]
            sub RCX,QWORD PTR FS:[40]
            jne .L_c93a

            add RSP,40
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_c890:

.cfi_restore_state 
            mov EDX,4
            mov RSI,R14
            jmp .L_c800
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_c8a0:

            mov EDX,6
            lea RSI,QWORD PTR [RIP+.L_e747]
            jmp .L_c800
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_c8b8:

            mov EDX,6
            lea RSI,QWORD PTR [RIP+.L_e1c9]
            jmp .L_c800
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_c8d0:

            mov RSI,R12
            call QWORD PTR [RIP+Csputc@GOTPCREL]

            jmp .L_c809
          .byte 0x66
          .byte 0x90
.L_c8e0:

            movsxd RAX,EBX
            lea EDX,DWORD PTR [RBX+1]
            add RAX,RDI
            jmp .L_c84b
          .byte 0x66
          .byte 0x90
.L_c8f0:

            mov EDX,4
            lea RSI,QWORD PTR [RIP+.L_e128]
            jmp .L_c800
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_c908:

            mov EDX,5
            lea RSI,QWORD PTR [RIP+.L_e122]
            jmp .L_c800
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_c920:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            jmp .L_c841
          .byte 0x66
          .byte 0x90
.L_c930:

            mov EDX,1
            jmp .L_c84b
.L_c93a:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 
#-----------------------------------
.align 16
.globl Csputc
.type Csputc, @function
#-----------------------------------
Csputc:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            mov R12D,EDI
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            movsxd RBP,DWORD PTR [RSI+8]
            mov RBX,RSI
            mov EAX,DWORD PTR [RSI+12]
            mov RDI,QWORD PTR [RSI]
            cmp EBP,EAX
            jl .L_c976

            add EAX,100
            mov DWORD PTR [RBX+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_c988

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBP,DWORD PTR [RBX+8]
            mov RDI,RAX
.L_c973:

            mov QWORD PTR [RBX],RDI
.L_c976:

            lea EAX,DWORD PTR [RBP+1]
            mov DWORD PTR [RBX+8],EAX
            mov BYTE PTR [RDI+RBP*1],R12B
            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_c988:

.cfi_restore_state 
            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_c973
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl Csprintf
.type Csprintf, @function
#-----------------------------------
Csprintf:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            mov R12,RDI
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            mov RBX,RSI
            sub RSP,208
.cfi_def_cfa_offset 240
            mov QWORD PTR [RSP+48],RDX
            mov QWORD PTR [RSP+56],RCX
            mov QWORD PTR [RSP+64],R8
            mov QWORD PTR [RSP+72],R9
            test AL,AL
            je .L_ca00

            movaps XMMWORD PTR [RSP+80],XMM0
            movaps XMMWORD PTR [RSP+96],XMM1
            movaps XMMWORD PTR [RSP+112],XMM2
            movaps XMMWORD PTR [RSP+128],XMM3
            movaps XMMWORD PTR [RSP+144],XMM4
            movaps XMMWORD PTR [RSP+160],XMM5
            movaps XMMWORD PTR [RSP+176],XMM6
            movaps XMMWORD PTR [RSP+192],XMM7
.L_ca00:

            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+24],RAX
            xor EAX,EAX
            mov ESI,DWORD PTR [R12+12]
            movsxd RDI,DWORD PTR [R12+8]
            mov RBP,RSP
            mov EAX,100
            jmp .L_ca8e
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_ca28:

            mov RDI,R8
            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov ESI,DWORD PTR [R12+12]
            movsxd RDI,DWORD PTR [R12+8]
            mov R8,RAX
.L_ca3e:

            lea RAX,QWORD PTR [RSP+240]
            sub ESI,EDI
            mov RDX,RBX
            add RDI,R8
            mov QWORD PTR [RSP+8],RAX
            lea RAX,QWORD PTR [RSP+32]
            movsxd RSI,ESI
            mov RCX,RBP
            mov QWORD PTR [R12],R8
            mov DWORD PTR [RSP],16
            mov DWORD PTR [RSP+4],48
            mov QWORD PTR [RSP+16],RAX
            call QWORD PTR [RIP+vsnprintf@GOTPCREL]

            mov ESI,DWORD PTR [R12+12]
            movsxd RDI,DWORD PTR [R12+8]
            mov EDX,ESI
            sub EDX,EDI
            cmp EDX,EAX
            jge .L_cad0
.L_ca8e:

            lea EDX,DWORD PTR [RAX+RDI*1]
            mov R8,QWORD PTR [R12]
            cmp EDX,ESI
            jl .L_ca3e

            lea EAX,DWORD PTR [RAX+RDI*1+100]
            mov DWORD PTR [R12+12],EAX
            movsxd RSI,EAX
            test R8,R8
            jne .L_ca28

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov ESI,DWORD PTR [R12+12]
            movsxd RDI,DWORD PTR [R12+8]
            mov R8,RAX
            jmp .L_ca3e
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_cad0:

            add EDI,EAX
            mov DWORD PTR [R12+8],EDI
            mov RCX,QWORD PTR [RSP+24]
            sub RCX,QWORD PTR FS:[40]
            jne .L_caf3

            add RSP,208
.cfi_remember_state 
.cfi_def_cfa_offset 32
            pop RBX
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
.L_caf3:

.cfi_restore_state 
            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl Cswrite
.type Cswrite, @function
#-----------------------------------
Cswrite:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R13
.cfi_def_cfa_offset 16
.cfi_offset 13, -16
            mov R13,RSI
            push R12
.cfi_def_cfa_offset 24
.cfi_offset 12, -24
            mov R12D,EDX
            push RBP
.cfi_def_cfa_offset 32
.cfi_offset 6, -32
            push RBX
.cfi_def_cfa_offset 40
.cfi_offset 3, -40
            mov RBX,RDI
            sub RSP,8
.cfi_def_cfa_offset 48
            movsxd RBP,DWORD PTR [RDI+8]
            mov RDI,QWORD PTR [RDI]
            lea EAX,DWORD PTR [RBP+RDX*1]
            cmp DWORD PTR [RBX+12],EAX
            jg .L_cb3f

            lea EAX,DWORD PTR [RBP+RDX*1+100]
            mov DWORD PTR [RBX+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_cb68

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBP,DWORD PTR [RBX+8]
            mov RDI,RAX
.L_cb3f:

            mov QWORD PTR [RBX],RDI
            movsxd RDX,R12D
            add RDI,RBP
            mov RSI,R13
            call QWORD PTR [RIP+memcpy@GOTPCREL]

            add DWORD PTR [RBX+8],R12D
            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 40
            mov EAX,R12D
            pop RBX
.cfi_def_cfa_offset 32
            pop RBP
.cfi_def_cfa_offset 24
            pop R12
.cfi_def_cfa_offset 16
            pop R13
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_cb68:

.cfi_restore_state 
            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_cb3f
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl Csreparse
.type Csreparse, @function
#-----------------------------------
Csreparse:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            mov R15D,ECX
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14D,EDX
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,RSI
            xor ESI,ESI
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RDI
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,120
.cfi_def_cfa_offset 176
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+104],RAX
            xor EAX,EAX
            mov R12,RSP
            mov RDI,R12
            call QWORD PTR [RIP+___mkd_initmmiot@GOTPCREL]

            mov ESI,R14D
            mov RDI,R13
            xor R8D,R8D
            mov RCX,R12
            mov EDX,R15D
            call QWORD PTR [RIP+___mkd_reparse@GOTPCREL]

            mov RDI,R12
            call QWORD PTR [RIP+___mkd_emblock@GOTPCREL]

            movsxd R14,DWORD PTR [RSP+8]
            mov RDI,QWORD PTR [RBP]
            mov R13,QWORD PTR [RSP]
            mov RBX,R14
            test RDI,RDI
            je .L_cc48

            add EBX,DWORD PTR [RBP+12]
            mov DWORD PTR [RBP+12],EBX
            movsxd RSI,EBX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov EBX,DWORD PTR [RSP+8]
.L_cbfe:

            movsxd RDI,DWORD PTR [RBP+8]
            mov RSI,R13
            mov QWORD PTR [RBP],RAX
            mov RDX,R14
            add EBX,EDI
            add RDI,RAX
            mov DWORD PTR [RBP+8],EBX
            call QWORD PTR [RIP+memcpy@GOTPCREL]

            xor ESI,ESI
            mov RDI,R12
            call QWORD PTR [RIP+___mkd_freemmiot@GOTPCREL]

            mov RAX,QWORD PTR [RSP+104]
            sub RAX,QWORD PTR FS:[40]
            jne .L_cc5c

            add RSP,120
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_cc48:

.cfi_restore_state 
            mov EDI,DWORD PTR [RBP+12]
            add EDI,R14D
            mov DWORD PTR [RBP+12],EDI
            movsxd RDI,EDI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            jmp .L_cbfe
.L_cc5c:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_xhtmlpage
.type mkd_xhtmlpage, @function
#-----------------------------------
mkd_xhtmlpage:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            mov R12,RDI
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            mov RBP,RDX
            sub RSP,8
.cfi_def_cfa_offset 32
            call QWORD PTR [RIP+mkd_compile@GOTPCREL]

            test EAX,EAX
            je .L_cd80

            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e838]
            mov RDI,RBP
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            cmp EAX,-1
            je .L_cd80

            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e913]
            mov RDI,RBP
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            cmp EAX,-1
            je .L_cd80

            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e91b]
            mov RDI,RBP
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            cmp EAX,-1
            je .L_cd80

            mov RDI,R12
            call QWORD PTR [RIP+mkd_doc_title@GOTPCREL]

            mov RDX,RAX
            test RAX,RAX
            je .L_cd04

            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e0a3]
            mov RDI,RBP
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            cmp EAX,-1
            je .L_cd80
.L_cd04:

            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e923]
            mov RDI,RBP
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            cmp EAX,-1
            je .L_cd80

            mov RSI,RBP
            mov RDI,R12
            call QWORD PTR [RIP+mkd_generatecss@GOTPCREL]

            cmp EAX,-1
            je .L_cd80

            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e92d]
            mov RDI,RBP
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            cmp EAX,-1
            je .L_cd80

            mov RSI,RBP
            mov RDI,R12
            call QWORD PTR [RIP+mkd_generatehtml@GOTPCREL]

            cmp EAX,-1
            je .L_cd80

            mov RDI,RBP
            xor EAX,EAX
            lea RSI,QWORD PTR [RIP+.L_e93d]
            call QWORD PTR [RIP+fprintf@GOTPCREL]

            cmp EAX,-1
            sete AL
            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 24
            movzx EAX,AL
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            neg EAX
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_cd80:

.cfi_restore_state 
            add RSP,8
.cfi_def_cfa_offset 24
            mov EAX,4294967295
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
.L_cd90:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDI,RDI
            je .L_cda0

            jmp QWORD PTR [RIP+free@GOTPCREL]
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_cda0:

            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_cdb0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RDX,RDX
            je .L_ce40

            push R14
.cfi_def_cfa_offset 16
.cfi_offset 14, -16
            xor R8D,R8D
            push R13
.cfi_def_cfa_offset 24
.cfi_offset 13, -24
            push R12
.cfi_def_cfa_offset 32
.cfi_offset 12, -32
            push RBP
.cfi_def_cfa_offset 40
.cfi_offset 6, -40
            mov RBP,RDI
            sub RSP,8
.cfi_def_cfa_offset 48
            test RDI,RDI
            je .L_cdd4

            cmp BYTE PTR [RDI],47
            je .L_cde8
.L_cdd4:

            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 40
            mov RAX,R8
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_cde8:

.cfi_restore_state 
            movsxd R13,ESI
            mov RDI,RDX
            mov R12,RDX
            call QWORD PTR [RIP+strlen@GOTPCREL]

            mov R14,RAX
            lea RDI,QWORD PTR [R13+RAX*1+2]
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov R8,RAX
            test RAX,RAX
            je .L_cdd4

            lea RDX,QWORD PTR [R14+1]
            mov RSI,R12
            mov RDI,RAX
            call QWORD PTR [RIP+memcpy@GOTPCREL]

            mov RDX,R13
            mov RSI,RBP
            mov RDI,RAX
            call QWORD PTR [RIP+strncat@GOTPCREL]

            add RSP,8
.cfi_def_cfa_offset 40
            mov R8,RAX
            pop RBP
.cfi_def_cfa_offset 32
            pop R12
.cfi_def_cfa_offset 24
            mov RAX,R8
            pop R13
.cfi_def_cfa_offset 16
            pop R14
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0x90
.L_ce40:

.cfi_restore 6
.cfi_restore 12
.cfi_restore 13
.cfi_restore 14
            xor R8D,R8D
            mov RAX,R8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_basename
.type mkd_basename, @function
#-----------------------------------
mkd_basename:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            mov R12,RSI
            lea RSI,QWORD PTR [RIP+.L_cdb0]
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            mov RBP,RDI
            sub RSP,8
.cfi_def_cfa_offset 32
            call QWORD PTR [RIP+mkd_e_url@GOTPCREL]

            mov RSI,R12
            mov RDI,RBP
            call QWORD PTR [RIP+mkd_e_data@GOTPCREL]

            add RSP,8
.cfi_def_cfa_offset 24
            mov RDI,RBP
            lea RSI,QWORD PTR [RIP+.L_cd90]
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+mkd_e_free@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
.L_ce90:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            xor R12D,R12D
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            mov EAX,DWORD PTR [RDI+4]
            mov RBX,RDI
            test EAX,EAX
            jg .L_ced0

            jmp .L_cf00
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_cea8:

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RBP,DWORD PTR [RBX+24]
            mov RDI,RAX
.L_ceb5:

            mov QWORD PTR [RBX+16],RDI
.L_ceb9:

            lea EAX,DWORD PTR [RBP+1]
            add R12D,1
            mov DWORD PTR [RBX+24],EAX
            movzx EAX,BYTE PTR [RBX+8]
            mov BYTE PTR [RDI+RBP*1],AL
            cmp R12D,DWORD PTR [RBX+4]
            jge .L_cf00
.L_ced0:

            movsxd RBP,DWORD PTR [RBX+24]
            mov EAX,DWORD PTR [RBX+28]
            mov RDI,QWORD PTR [RBX+16]
            cmp EBP,EAX
            jl .L_ceb9

            add EAX,100
            mov DWORD PTR [RBX+28],EAX
            movsxd RSI,EAX
            test RDI,RDI
            jne .L_cea8

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            jmp .L_ceb5
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_cf00:

            mov DWORD PTR [RBX+4],0
            pop RBX
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
.L_cf10:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            movsxd RAX,ESI
            mov R10,RDI
            lea R8D,DWORD PTR [RSI+1]
            mov R9D,ECX
            lea RAX,QWORD PTR [RAX+RAX*2]
            mov EDI,EDX
            shl RAX,4
            lea RCX,QWORD PTR [R10+RAX*1]
            cmp R8D,EDX
            jg .L_cf70

            lea RAX,QWORD PTR [R10+RAX*1+52]
            jmp .L_cf4d
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_cf40:

            add R8D,1
            add RAX,48
            cmp R8D,EDI
            jg .L_cf70
.L_cf4d:

            mov EDX,DWORD PTR [RAX-4]
            test EDX,EDX
            je .L_cf5a

            mov ESI,DWORD PTR [RAX]
            test ESI,ESI
            jle .L_cf40
.L_cf5a:

            cmp EDX,DWORD PTR [RCX]
            jne .L_cf40

            mov EDX,DWORD PTR [RAX]
            cmp EDX,2
            jg .L_cf6a

            cmp EDX,R9D
            jne .L_cf40
.L_cf6a:

            mov EAX,R8D
            ret 
          .byte 0x66
          .byte 0x90
.L_cf70:

            xor R8D,R8D
            mov EAX,R8D
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_cf80:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,56
.cfi_def_cfa_offset 112
            mov QWORD PTR [RSP],RDI
            mov DWORD PTR [RSP+44],ESI
            mov DWORD PTR [RSP+40],EDX
            cmp ESI,EDX
            jg .L_d1a0

            movsxd RAX,DWORD PTR [RSP+44]
            lea R14,QWORD PTR [RIP+.L_12240]
            lea R12,QWORD PTR [RAX+RAX*2]
            mov RBP,RAX
            mov RAX,QWORD PTR [RSP]
            shl R12,4
            mov R15,QWORD PTR [RAX+32]
            mov R13,R15
            jmp .L_cfe1
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_cfd0:

            add EBP,1
            add R12,48
            cmp DWORD PTR [RSP+40],EBP
            jl .L_d1a0
.L_cfe1:

            mov EDX,DWORD PTR [R13+R12*1]
            test EDX,EDX
            je .L_cfd0

            mov QWORD PTR [RSP+8],R12
            mov R12,R13
.L_cff2:

            mov RAX,QWORD PTR [RSP+8]
            lea R15,QWORD PTR [R12+RAX*1]
            mov R11D,DWORD PTR [R15+4]
            cmp R11D,1
            je .L_d160

            cmp R11D,2
            je .L_d290

            test R11D,R11D
            je .L_d280

            mov R11D,DWORD PTR [RSP+40]
            mov ECX,1
            mov ESI,EBP
            mov RDI,R12
            mov EDX,R11D
            call .L_cf10

            mov EDX,R11D
            mov ECX,2
            mov ESI,EBP
            mov RDI,R12
            mov EBX,EAX
            mov R11D,2
            call .L_cf10

            mov EDX,EAX
            cmp EBX,EAX
            jle .L_d05b

            mov EDX,EBX
            mov R11D,1
.L_d05b:

            test EDX,EDX
            je .L_d183
.L_d063:

            lea R9D,DWORD PTR [R11-1]
            movsxd RBX,R9D
            lea RAX,QWORD PTR [RBX+RBX*2]
            shl RAX,3
            mov QWORD PTR [RSP+16],RAX
            add RAX,10
            mov QWORD PTR [RSP+24],RAX
.L_d080:

            movsxd RAX,EDX
            mov RDI,QWORD PTR [RSP]
            mov ESI,EBP
            lea RCX,QWORD PTR [RAX+RAX*2]
            shl RCX,4
            lea R13,QWORD PTR [R12+RCX*1]
            sub DWORD PTR [R13+4],R11D
            sub DWORD PTR [R15+4],R11D
            call .L_cf80

            lea RAX,QWORD PTR [RBX+RBX*2]
            mov ESI,DWORD PTR [R15+24]
            lea R10,QWORD PTR [R14+RAX*8]
            mov R12D,DWORD PTR [R10+20]
            lea EAX,DWORD PTR [RSI+R12*1-1]
            cmp DWORD PTR [R15+28],EAX
            jle .L_d210

            mov RDI,QWORD PTR [R15+16]
.L_d0c5:

            mov QWORD PTR [R15+16],RDI
            test ESI,ESI
            jne .L_d253
.L_d0d1:

            mov RSI,QWORD PTR [RSP+16]
            lea EDX,DWORD PTR [R12-1]
            movsxd RDX,EDX
            add RSI,R14
            call QWORD PTR [RIP+memcpy@GOTPCREL]

            lea RAX,QWORD PTR [RBX+RBX*2]
            mov RDI,QWORD PTR [R13+32]
            lea RDX,QWORD PTR [R14+RAX*8]
            movsxd R12,DWORD PTR [RDX+20]
            mov QWORD PTR [RSP+16],RDX
            lea EAX,DWORD PTR [R12-1]
            add DWORD PTR [R15+24],EAX
            mov R15,QWORD PTR [RSP+24]
            mov RBX,R12
            add R15,R14
            test RDI,RDI
            je .L_d2d0

            add EBX,DWORD PTR [R13+44]
            mov DWORD PTR [R13+44],EBX
            movsxd RSI,EBX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov RDX,QWORD PTR [RSP+16]
            mov EBX,DWORD PTR [RDX+20]
.L_d132:

            movsxd RDI,DWORD PTR [R13+40]
            mov RDX,R12
            mov QWORD PTR [R13+32],RAX
            mov RSI,R15
            add EBX,EDI
            add RDI,RAX
            mov DWORD PTR [R13+40],EBX
            call QWORD PTR [RIP+memcpy@GOTPCREL]

            mov RAX,QWORD PTR [RSP]
            mov R12,QWORD PTR [RAX+32]
            jmp .L_cff2
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_d160:

            mov EDX,DWORD PTR [RSP+40]
            mov ECX,1
            mov ESI,EBP
            mov RDI,R12
            mov R11D,1
            call .L_cf10

            mov EDX,EAX
            test EDX,EDX
            jne .L_d063
.L_d183:

            mov R13,R12
            mov R12,QWORD PTR [RSP+8]
            add EBP,1
            add R12,48
            cmp DWORD PTR [RSP+40],EBP
            jge .L_cfe1

            nop
            nop
            nop
            nop
.L_d1a0:

            mov EAX,DWORD PTR [RSP+44]
            mov ECX,DWORD PTR [RSP+40]
            add EAX,1
            lea EDX,DWORD PTR [RCX-1]
            cmp EAX,EDX
            jge .L_d1fc

            mov R13D,DWORD PTR [RSP+40]
            movsxd RSI,DWORD PTR [RSP+44]
            cdqe 
            lea RBX,QWORD PTR [RAX+RAX*2]
            mov R12,QWORD PTR [RSP]
            sub R13D,3
            shl RBX,4
            sub R13D,ESI
            add RSI,R13
            lea RBP,QWORD PTR [RSI+RSI*2]
            shl RBP,4
            add RBP,96
.L_d1e0:

            mov RDI,QWORD PTR [R12+32]
            add RDI,RBX
            mov EAX,DWORD PTR [RDI]
            test EAX,EAX
            je .L_d1f3

            call .L_ce90
.L_d1f3:

            add RBX,48
            cmp RBX,RBP
            jne .L_d1e0
.L_d1fc:

            add RSP,56
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_d210:

.cfi_restore_state 
            mov RDI,QWORD PTR [R15+16]
            lea EAX,DWORD PTR [RSI+R12*1+99]
            mov QWORD PTR [RSP+32],R10
            movsxd R11,EAX
            test RDI,RDI
            je .L_d2f0

            mov RSI,R11
            mov DWORD PTR [R15+28],EAX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov R10,QWORD PTR [RSP+32]
            mov ESI,DWORD PTR [R15+24]
            mov RDI,RAX
            mov R12D,DWORD PTR [R10+20]
            mov QWORD PTR [R15+16],RDI
            test ESI,ESI
            je .L_d0d1
.L_d253:

            movsxd RDX,R12D
            movsxd R10,ESI
            mov RSI,RDI
            lea R11,QWORD PTR [RDI+RDX*1-1]
            mov RDX,R10
            mov RDI,R11
            call QWORD PTR [RIP+memmove@GOTPCREL]

            lea RAX,QWORD PTR [RBX+RBX*2]
            mov RDI,QWORD PTR [R15+16]
            mov R12D,DWORD PTR [R14+RAX*8+20]
            jmp .L_d0d1

            nop
.L_d280:

            mov R13,R12
            mov R12,RAX
            jmp .L_cfd0
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_d290:

            mov EDX,DWORD PTR [RSP+40]
            mov ECX,2
            mov ESI,EBP
            mov RDI,R12
            call .L_cf10

            mov EDX,EAX
            test EAX,EAX
            je .L_d160

            mov QWORD PTR [RSP+24],34
            mov EBX,1
            mov QWORD PTR [RSP+16],24
            jmp .L_d080
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_d2d0:

            mov EDI,DWORD PTR [R13+44]
            add EDI,R12D
            mov DWORD PTR [R13+44],EDI
            movsxd RDI,EDI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            jmp .L_d132
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_d2f0:

            mov DWORD PTR [RSP+32],ESI
            mov RDI,R11
            mov DWORD PTR [R15+28],EAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov ESI,DWORD PTR [RSP+32]
            mov RDI,RAX
            jmp .L_d0c5
.cfi_endproc 

            nop
            nop
            nop
#-----------------------------------
.align 16
.globl ___mkd_emblock
.type ___mkd_emblock, @function
#-----------------------------------
___mkd_emblock:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            xor ESI,ESI
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            mov R13,RDI
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            xor EBP,EBP
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,8
.cfi_def_cfa_offset 64
            mov EAX,DWORD PTR [RDI+40]
            lea EDX,DWORD PTR [RAX-1]
            call .L_cf80

            mov ESI,DWORD PTR [R13+40]
            test ESI,ESI
            jg .L_d3c2

            jmp .L_d480
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_d348:

            mov DWORD PTR [RBX+40],0
.L_d34f:

            mov R12D,DWORD PTR [RBX+24]
            test R12D,R12D
            je .L_d3b4

            mov EAX,DWORD PTR [R13+12]
            mov RDI,QWORD PTR [R13]
            movsxd R15,R12D
            mov R14,QWORD PTR [RBX+16]
            add EAX,R12D
            mov DWORD PTR [R13+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_d4a0

            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov R12D,DWORD PTR [RBX+24]
.L_d384:

            movsxd RDI,DWORD PTR [R13+8]
            mov QWORD PTR [R13],RAX
            mov RDX,R15
            mov RSI,R14
            add R12D,EDI
            add RDI,RAX
            mov DWORD PTR [R13+8],R12D
            call QWORD PTR [RIP+memcpy@GOTPCREL]

            mov EAX,DWORD PTR [RBX+28]
            test EAX,EAX
            jne .L_d460

            mov DWORD PTR [RBX+24],0
.L_d3b4:

            add RBP,1
            cmp DWORD PTR [R13+40],EBP
            jle .L_d480
.L_d3c2:

            lea RBX,QWORD PTR [RBP+RBP*2]
            shl RBX,4
            add RBX,QWORD PTR [R13+32]
            mov ECX,DWORD PTR [RBX]
            test ECX,ECX
            je .L_d3dd

            mov RDI,RBX
            call .L_ce90
.L_d3dd:

            mov R12D,DWORD PTR [RBX+40]
            test R12D,R12D
            je .L_d34f

            mov EAX,DWORD PTR [R13+12]
            mov RDI,QWORD PTR [R13]
            movsxd R15,R12D
            mov R14,QWORD PTR [RBX+32]
            add EAX,R12D
            mov DWORD PTR [R13+12],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_d4b0

            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov R12D,DWORD PTR [RBX+40]
.L_d416:

            movsxd RDI,DWORD PTR [R13+8]
            mov RDX,R15
            mov QWORD PTR [R13],RAX
            mov RSI,R14
            add R12D,EDI
            add RDI,RAX
            mov DWORD PTR [R13+8],R12D
            call QWORD PTR [RIP+memcpy@GOTPCREL]

            mov EDX,DWORD PTR [RBX+44]
            test EDX,EDX
            je .L_d348

            mov RDI,QWORD PTR [RBX+32]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov QWORD PTR [RBX+40],0
            jmp .L_d34f
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_d460:

            mov RDI,QWORD PTR [RBX+16]
            add RBP,1
            call QWORD PTR [RIP+free@GOTPCREL]

            mov QWORD PTR [RBX+24],0
            cmp DWORD PTR [R13+40],EBP
            jg .L_d3c2
.L_d480:

            mov DWORD PTR [R13+40],0
            add RSP,8
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_d4a0:

.cfi_restore_state 
            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            jmp .L_d384
          .byte 0x66
          .byte 0x90
.L_d4b0:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            jmp .L_d416
.cfi_endproc 

            nop
            nop
#-----------------------------------
.align 16
.globl gfm_populate
.type gfm_populate, @function
#-----------------------------------
gfm_populate:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RSI
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            mov RBX,RDI
            sub RSP,56
.cfi_def_cfa_offset 112
            mov DWORD PTR [RSP+8],EDX
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+40],RAX
            xor EAX,EAX
            call QWORD PTR [RIP+__mkd_new_Document@GOTPCREL]

            mov R12,RAX
            test RAX,RAX
            je .L_d5ce

            mov DWORD PTR [RAX+68],4
            xor R13D,R13D
            lea R14,QWORD PTR [RSP+16]
            mov QWORD PTR [RSP+16],0
            mov QWORD PTR [RSP+24],0
            nop
            nop
            nop
            nop
            nop
.L_d520:

            mov RDI,RBP
            call RBX

            mov R15D,EAX
            cmp EAX,-1
            je .L_d5a4
.L_d52d:

            cmp R15D,10
            je .L_d5f8

            call QWORD PTR [RIP+__ctype_b_loc@GOTPCREL]

            mov R8,RAX
            movsxd RAX,R15D
            mov RCX,QWORD PTR [R8]
            test WORD PTR [RCX+RAX*2],24576
            jne .L_d554

            test R15B,128
            je .L_d520
.L_d554:

            movsxd RCX,DWORD PTR [RSP+24]
            mov EAX,DWORD PTR [RSP+28]
            mov RDI,QWORD PTR [RSP+16]
            cmp ECX,EAX
            jl .L_d58c

            add EAX,100
            movsxd RSI,EAX
            test RDI,RDI
            je .L_d720

            mov DWORD PTR [RSP+28],EAX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RCX,DWORD PTR [RSP+24]
            mov RDI,RAX
.L_d587:

            mov QWORD PTR [RSP+16],RDI
.L_d58c:

            lea EAX,DWORD PTR [RCX+1]
            mov DWORD PTR [RSP+24],EAX
            mov BYTE PTR [RDI+RCX*1],R15B
            mov RDI,RBP
            call RBX

            mov R15D,EAX
            cmp EAX,-1
            jne .L_d52d
.L_d5a4:

            mov EDX,DWORD PTR [RSP+24]
            test EDX,EDX
            jne .L_d7d0
.L_d5b0:

            mov EAX,DWORD PTR [RSP+28]
            test EAX,EAX
            jne .L_d7b0

            mov DWORD PTR [RSP+24],0
.L_d5c4:

            cmp R13D,3
            je .L_d740
.L_d5ce:

            mov RAX,QWORD PTR [RSP+40]
            sub RAX,QWORD PTR FS:[40]
            jne .L_d7f6

            add RSP,56
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov RAX,R12
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.L_d5f8:

.cfi_restore_state 
            cmp R13D,-1
            je .L_d6b0

            cmp R13D,2
            jg .L_d6a0

            movsxd RDX,DWORD PTR [RSP+24]
            mov RDI,QWORD PTR [RSP+16]
            test EDX,EDX
            je .L_d6ba

            add R13D,1
            cmp BYTE PTR [RDI],37
            je .L_d681

            mov EAX,DWORD PTR [RSP+28]
            cmp EDX,EAX
            jl .L_d64c

            add EAX,100
            movsxd RSI,EAX
.L_d635:

            mov DWORD PTR [RSP+28],EAX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd RDX,DWORD PTR [RSP+24]
            mov RDI,RAX
.L_d647:

            mov QWORD PTR [RSP+16],RDI
.L_d64c:

            lea EAX,DWORD PTR [RDX+1]
            mov DWORD PTR [RSP+24],EAX
            mov BYTE PTR [RDI+RDX*1],32
            movsxd R13,DWORD PTR [RSP+24]
            mov EAX,DWORD PTR [RSP+28]
            cmp R13D,EAX
            jge .L_d6f0

            mov RAX,QWORD PTR [RSP+16]
.L_d66e:

            lea EDX,DWORD PTR [R13+1]
            mov DWORD PTR [RSP+24],EDX
            mov BYTE PTR [RAX+R13*1],32
            mov R13D,4294967295
.L_d681:

            mov RSI,R14
            mov RDI,R12
            call QWORD PTR [RIP+__mkd_enqueue@GOTPCREL]

            mov DWORD PTR [RSP+24],0
            jmp .L_d520
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_d6a0:

            cmp R13D,-1
            jne .L_d681

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_d6b0:

            movsxd RDX,DWORD PTR [RSP+24]
            mov RDI,QWORD PTR [RSP+16]
.L_d6ba:

            mov EAX,DWORD PTR [RSP+28]
            cmp EAX,EDX
            jg .L_d64c

            add EAX,100
            movsxd RSI,EAX
            test RDI,RDI
            jne .L_d635

            mov DWORD PTR [RSP+12],EDX
            mov RDI,RSI
            mov DWORD PTR [RSP+28],EAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            movsxd RDX,DWORD PTR [RSP+12]
            mov RDI,RAX
            jmp .L_d647
          .byte 0x90
.L_d6f0:

            mov RDI,QWORD PTR [RSP+16]
            add EAX,100
            mov DWORD PTR [RSP+28],EAX
            movsxd RSI,EAX
            test RDI,RDI
            je .L_d7e8

            call QWORD PTR [RIP+realloc@GOTPCREL]

            movsxd R13,DWORD PTR [RSP+24]
.L_d713:

            mov QWORD PTR [RSP+16],RAX
            jmp .L_d66e
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_d720:

            mov DWORD PTR [RSP+12],ECX
            mov RDI,RSI
            mov DWORD PTR [RSP+28],EAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            movsxd RCX,DWORD PTR [RSP+12]
            mov RDI,RAX
            jmp .L_d587
          .byte 0x66
          .byte 0x90
.L_d740:

            test DWORD PTR [RSP+8],65552
            jne .L_d5ce

            mov RBX,QWORD PTR [R12+32]
            mov ESI,1
            mov RDI,RBX
            mov QWORD PTR [R12+8],RBX
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov RDI,QWORD PTR [RBX+16]
            mov ESI,1
            mov QWORD PTR [R12+16],RDI
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov RAX,QWORD PTR [RBX+16]
            mov ESI,1
            mov RDI,QWORD PTR [RAX+16]
            mov QWORD PTR [R12+24],RDI
            call QWORD PTR [RIP+__mkd_trim_line@GOTPCREL]

            mov RAX,QWORD PTR [RBX+16]
            mov RAX,QWORD PTR [RAX+16]
            mov RAX,QWORD PTR [RAX+16]
            mov QWORD PTR [R12+32],RAX
            jmp .L_d5ce
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_d7b0:

            mov RDI,QWORD PTR [RSP+16]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov QWORD PTR [RSP+24],0
            jmp .L_d5c4
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_d7d0:

            lea RSI,QWORD PTR [RSP+16]
            mov RDI,R12
            call QWORD PTR [RIP+__mkd_enqueue@GOTPCREL]

            jmp .L_d5b0
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_d7e8:

            mov RDI,RSI
            call QWORD PTR [RIP+malloc@GOTPCREL]

            jmp .L_d713
.L_d7f6:

            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl gfm_string
.type gfm_string, @function
#-----------------------------------
gfm_string:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            sub RSP,40
.cfi_def_cfa_offset 48
            and EDX,196608
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RSP+24],RAX
            xor EAX,EAX
            mov QWORD PTR [RSP],RDI
            mov RDI,QWORD PTR [RIP+__mkd_io_strget@GOTPCREL]
            mov DWORD PTR [RSP+8],ESI
            mov RSI,RSP
            call QWORD PTR [RIP+gfm_populate@GOTPCREL]

            mov RCX,QWORD PTR [RSP+24]
            sub RCX,QWORD PTR FS:[40]
            jne .L_d847

            add RSP,40
.cfi_remember_state 
.cfi_def_cfa_offset 8
            ret 
.L_d847:

.cfi_restore_state 
            call QWORD PTR [RIP+__stack_chk_fail@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
#-----------------------------------
.align 16
.globl gfm_in
.type gfm_in, @function
#-----------------------------------
gfm_in:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov EDX,ESI
            mov RSI,RDI
            mov RDI,QWORD PTR [RIP+fgetc@GOTPCREL]
            and EDX,196608
            jmp QWORD PTR [RIP+gfm_populate@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_initialize
.type mkd_initialize, @function
#-----------------------------------
mkd_initialize:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov EAX,DWORD PTR [RIP+.L_12270]
            test EAX,EAX
            jne .L_d880

            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_d880:

            sub RSP,8
.cfi_def_cfa_offset 16
            xor EDI,EDI
            mov DWORD PTR [RIP+.L_12270],0
            call QWORD PTR [RIP+time@GOTPCREL]

            add RSP,8
.cfi_def_cfa_offset 8
            mov RDI,RAX
            jmp QWORD PTR [RIP+srandom@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_d8b0:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov EAX,DWORD PTR [RDI+8]
            mov EDX,DWORD PTR [RSI+8]
            cmp EAX,EDX
            jne .L_d8d0

            mov RSI,QWORD PTR [RSI]
            mov RDI,QWORD PTR [RDI]
            movsxd RDX,EAX
            jmp QWORD PTR [RIP+strncasecmp@GOTPCREL]
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_d8d0:

            sub EAX,EDX
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_sort_tags
.type mkd_sort_tags, @function
#-----------------------------------
mkd_sort_tags:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov RAX,QWORD PTR [RIP+extratags@GOTPCREL]
            lea RCX,QWORD PTR [RIP+.L_d8b0]
            mov EDX,16
            movsxd RSI,DWORD PTR [RAX+8]
            mov RDI,QWORD PTR [RAX]
            jmp QWORD PTR [RIP+qsort@GOTPCREL]
.cfi_endproc 
#-----------------------------------
.align 16
.globl mkd_search_tags
.type mkd_search_tags, @function
#-----------------------------------
mkd_search_tags:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            movsxd RAX,ESI
            xor R15D,R15D
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14D,30
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            lea R13,QWORD PTR [RIP+.L_12280]
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            mov RBP,RAX
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,24
.cfi_def_cfa_offset 80
            mov QWORD PTR [RSP],RDI
            mov QWORD PTR [RSP+8],RAX
            nop
            nop
            nop
.L_d930:

            cmp R14,R15
            jbe .L_d96a
.L_d935:

            lea RBX,QWORD PTR [R14+R15*1]
            shr RBX,1
            mov R12,RBX
            shl R12,4
            add R12,R13
            mov EDX,DWORD PTR [R12+8]
            cmp EBP,EDX
            je .L_d9e0

            mov EAX,EBP
            sub EAX,EDX
            test EAX,EAX
            js .L_d9fb
.L_d95f:

            jle .L_d9c5

            lea R15,QWORD PTR [RBX+1]
            cmp R14,R15
            ja .L_d935
.L_d96a:

            mov RAX,QWORD PTR [RIP+extratags@GOTPCREL]
            movsxd R14,DWORD PTR [RAX+8]
            test R14D,R14D
            je .L_d9c2

            mov R13,QWORD PTR [RAX]
            movsxd RAX,EBP
            xor R15D,R15D
            mov QWORD PTR [RSP+8],RAX
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
.L_d990:

            cmp R15,R14
            jae .L_d9c2
.L_d995:

            lea RBX,QWORD PTR [R15+R14*1]
            shr RBX,1
            mov R12,RBX
            shl R12,4
            add R12,R13
            mov EDX,DWORD PTR [R12+8]
            cmp EBP,EDX
            je .L_da10

            mov EAX,EBP
            sub EAX,EDX
.L_d9b3:

            test EAX,EAX
            js .L_da08

            jle .L_d9c5

            lea R15,QWORD PTR [RBX+1]
            cmp R15,R14
            jb .L_d995
.L_d9c2:

            xor R12D,R12D
.L_d9c5:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov RAX,R12
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_d9e0:

.cfi_restore_state 
            mov RDX,QWORD PTR [RSP+8]
            mov RSI,QWORD PTR [R12]
            mov RDI,QWORD PTR [RSP]
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            test EAX,EAX
            jns .L_d95f
.L_d9fb:

            mov R14,RBX
            jmp .L_d930
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_da08:

            mov R14,RBX
            jmp .L_d990
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_da10:

            mov RDX,QWORD PTR [RSP+8]
            mov RSI,QWORD PTR [R12]
            mov RDI,QWORD PTR [RSP]
            call QWORD PTR [RIP+strncasecmp@GOTPCREL]

            jmp .L_d9b3
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_define_tag
.type mkd_define_tag, @function
#-----------------------------------
mkd_define_tag:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R12
.cfi_def_cfa_offset 16
.cfi_offset 12, -16
            push RBP
.cfi_def_cfa_offset 24
.cfi_offset 6, -24
            mov RBP,RDI
            push RBX
.cfi_def_cfa_offset 32
.cfi_offset 3, -32
            mov EBX,ESI
            call QWORD PTR [RIP+strlen@GOTPCREL]

            mov RDI,RBP
            mov RSI,RAX
            call QWORD PTR [RIP+mkd_search_tags@GOTPCREL]

            test RAX,RAX
            je .L_da58

            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_da58:

.cfi_restore_state 
            mov R12,QWORD PTR [RIP+extratags@GOTPCREL]
            movsxd RAX,DWORD PTR [R12+8]
            test EAX,EAX
            jne .L_da90

            mov ESI,1600
            mov EAX,100
.L_da72:

            mov RDI,RSI
            mov DWORD PTR [R12+12],EAX
            call QWORD PTR [RIP+malloc@GOTPCREL]

            mov RDI,RAX
            movsxd RAX,DWORD PTR [R12+8]
            mov QWORD PTR [R12],RDI
            jmp .L_da9d
          .byte 0x66
          .byte 0x90
.L_da90:

            mov EDX,DWORD PTR [R12+12]
            mov RDI,QWORD PTR [R12]
            cmp EAX,EDX
            jge .L_dac9
.L_da9d:

            lea EDX,DWORD PTR [RAX+1]
            shl RAX,4
            mov DWORD PTR [R12+8],EDX
            lea R12,QWORD PTR [RDI+RAX*1]
            mov RDI,RBP
            mov QWORD PTR [R12],RBP
            call QWORD PTR [RIP+strlen@GOTPCREL]

            mov DWORD PTR [R12+12],EBX
            mov DWORD PTR [R12+8],EAX
            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 24
            pop RBP
.cfi_def_cfa_offset 16
            pop R12
.cfi_def_cfa_offset 8
            ret 
.L_dac9:

.cfi_restore_state 
            lea EAX,DWORD PTR [RDX+100]
            movsxd RSI,EAX
            shl RSI,4
            test RDI,RDI
            je .L_da72

            mov DWORD PTR [R12+12],EAX
            call QWORD PTR [RIP+realloc@GOTPCREL]

            mov RDI,RAX
            movsxd RAX,DWORD PTR [R12+8]
            mov QWORD PTR [R12],RDI
            jmp .L_da9d
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_deallocate_tags
.type mkd_deallocate_tags, @function
#-----------------------------------
mkd_deallocate_tags:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBX
.cfi_def_cfa_offset 16
.cfi_offset 3, -16
            mov RBX,QWORD PTR [RIP+extratags@GOTPCREL]
            mov EDX,DWORD PTR [RBX+8]
            test EDX,EDX
            jle .L_db1d

            mov EAX,DWORD PTR [RBX+12]
            test EAX,EAX
            jne .L_db20

            mov DWORD PTR [RBX+8],0
.L_db1d:

            pop RBX
.cfi_remember_state 
.cfi_def_cfa_offset 8
            ret 
          .byte 0x90
.L_db20:

.cfi_restore_state 
            mov RDI,QWORD PTR [RBX]
            call QWORD PTR [RIP+free@GOTPCREL]

            mov QWORD PTR [RBX+8],0
            pop RBX
.cfi_def_cfa_offset 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_with_html5_tags
.type mkd_with_html5_tags, @function
#-----------------------------------
mkd_with_html5_tags:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            mov EAX,DWORD PTR [RIP+.L_12480]
            test EAX,EAX
            je .L_db50

            ret 
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.L_db50:

            sub RSP,8
.cfi_def_cfa_offset 16
            xor ESI,ESI
            lea RDI,QWORD PTR [RIP+.L_e9d2]
            mov DWORD PTR [RIP+.L_12480],1
            call QWORD PTR [RIP+mkd_define_tag@GOTPCREL]

            xor ESI,ESI
            lea RDI,QWORD PTR [RIP+.L_e9d8]
            call QWORD PTR [RIP+mkd_define_tag@GOTPCREL]

            xor ESI,ESI
            lea RDI,QWORD PTR [RIP+.L_ea93]
            call QWORD PTR [RIP+mkd_define_tag@GOTPCREL]

            xor ESI,ESI
            lea RDI,QWORD PTR [RIP+.L_e9df]
            call QWORD PTR [RIP+mkd_define_tag@GOTPCREL]

            xor ESI,ESI
            lea RDI,QWORD PTR [RIP+.L_e9e3]
            call QWORD PTR [RIP+mkd_define_tag@GOTPCREL]

            xor ESI,ESI
            lea RDI,QWORD PTR [RIP+.L_e9eb]
            call QWORD PTR [RIP+mkd_define_tag@GOTPCREL]

            xor EAX,EAX
            add RSP,8
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+mkd_sort_tags@GOTPCREL]
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_flags_are
.type mkd_flags_are, @function
#-----------------------------------
mkd_flags_are:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push R15
.cfi_def_cfa_offset 16
.cfi_offset 15, -16
            push R14
.cfi_def_cfa_offset 24
.cfi_offset 14, -24
            mov R14,RDI
            push R13
.cfi_def_cfa_offset 32
.cfi_offset 13, -32
            push R12
.cfi_def_cfa_offset 40
.cfi_offset 12, -40
            push RBP
.cfi_def_cfa_offset 48
.cfi_offset 6, -48
            push RBX
.cfi_def_cfa_offset 56
.cfi_offset 3, -56
            sub RSP,24
.cfi_def_cfa_offset 80
            mov DWORD PTR [RSP+12],ESI
            mov DWORD PTR [RSP+8],EDX
            test EDX,EDX
            jne .L_ddf0
.L_dbf1:

            mov EAX,33
            lea RBX,QWORD PTR [RIP+.L_119d0]
            mov EBP,1
            mov R8D,1
            lea R13,QWORD PTR [RIP+.L_e9f3]
            lea R15,QWORD PTR [RIP+.L_ea00]
            jmp .L_dc9c

            nop
            nop
            nop
            nop
            nop
.L_dc20:

            test EBP,EBP
            jne .L_dd00
.L_dc28:

            mov RCX,R14
            mov EDX,4
            mov ESI,1
            mov RDI,R15
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            test R12D,R12D
            je .L_dd40

            mov RSI,R14
            mov RDI,R13
            call QWORD PTR [RIP+fputs@GOTPCREL]
.L_dc53:

            mov RCX,R14
            mov EDX,5
            mov ESI,1
            lea RDI,QWORD PTR [RIP+.L_ea05]
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            test EBP,EBP
            je .L_dd20
.L_dc75:

            mov EDX,EBP
            lea RAX,QWORD PTR [RIP+_DYNAMIC]
            xor EDX,1
            cmp RBX,RAX
            je .L_dda0

            mov R13,QWORD PTR [RBX+8]
            mov R8D,DWORD PTR [RBX]
            mov EBP,EDX
            add RBX,16
            movzx EAX,BYTE PTR [R13]
.L_dc9c:

            and R8D,DWORD PTR [RSP+12]
            mov R12D,R8D
            cmp AL,33
            jne .L_dcb7

            add R13,1
            test R8D,R8D
            sete R12B
            movzx R12D,R12B
.L_dcb7:

            mov EDX,DWORD PTR [RSP+8]
            test EDX,EDX
            jne .L_dc20

            mov RSI,R14
            mov EDI,32
            call QWORD PTR [RIP+fputc@GOTPCREL]

            test R12D,R12D
            jne .L_dd88

            mov RSI,R14
            mov EDI,33
            call QWORD PTR [RIP+fputc@GOTPCREL]

            mov RSI,R14
            mov RDI,R13
            call QWORD PTR [RIP+fputs@GOTPCREL]

            jmp .L_dc75
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_dd00:

            mov RCX,R14
            mov EDX,5
            mov ESI,1
            lea RDI,QWORD PTR [RIP+.L_e9fa]
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            jmp .L_dc28
          .byte 0x90
.L_dd20:

            mov RCX,R14
            mov EDX,6
            mov ESI,1
            lea RDI,QWORD PTR [RIP+.L_e184]
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            jmp .L_dc75
          .byte 0x90
.L_dd40:

            mov RCX,R14
            mov EDX,3
            mov ESI,1
            lea RDI,QWORD PTR [RIP+.L_ea0b]
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            mov RSI,R14
            mov RDI,R13
            call QWORD PTR [RIP+fputs@GOTPCREL]

            mov RCX,R14
            mov EDX,4
            mov ESI,1
            lea RDI,QWORD PTR [RIP+.L_ea0f]
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            jmp .L_dc53
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_dd88:

            mov RSI,R14
            mov RDI,R13
            call QWORD PTR [RIP+fputs@GOTPCREL]

            jmp .L_dc75
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_dda0:

            mov EAX,DWORD PTR [RSP+8]
            test EAX,EAX
            je .L_ddd8

            cmp EBP,1
            jne .L_de0f
.L_ddad:

            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            mov RCX,R14
            mov EDX,9
            mov ESI,1
            pop RBX
.cfi_def_cfa_offset 48
            lea RDI,QWORD PTR [RIP+.L_e2a1]
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            jmp QWORD PTR [RIP+fwrite@GOTPCREL]
          .byte 0xf
          .byte 0x1f
          .byte 0x0
.L_ddd8:

.cfi_restore_state 
            add RSP,24
.cfi_remember_state 
.cfi_def_cfa_offset 56
            pop RBX
.cfi_def_cfa_offset 48
            pop RBP
.cfi_def_cfa_offset 40
            pop R12
.cfi_def_cfa_offset 32
            pop R13
.cfi_def_cfa_offset 24
            pop R14
.cfi_def_cfa_offset 16
            pop R15
.cfi_def_cfa_offset 8
            ret 
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_ddf0:

.cfi_restore_state 
            mov RCX,RDI
            mov EDX,30
            mov ESI,1
            lea RDI,QWORD PTR [RIP+.L_eb28]
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            jmp .L_dbf1
.L_de0f:

            mov RCX,R14
            mov EDX,6
            mov ESI,1
            lea RDI,QWORD PTR [RIP+.L_e184]
            call QWORD PTR [RIP+fwrite@GOTPCREL]

            jmp .L_ddad
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
#-----------------------------------
.align 16
.globl mkd_mmiot_flags
.type mkd_mmiot_flags, @function
#-----------------------------------
mkd_mmiot_flags:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            test RSI,RSI
            je .L_de40

            mov ESI,DWORD PTR [RSI+80]
            jmp QWORD PTR [RIP+mkd_flags_are@GOTPCREL]
          .byte 0x66
          .byte 0x90
.L_de40:

            ret 
.cfi_endproc 
#===================================
# end section .text
#===================================

#===================================
.section .rodata ,"a",@progbits
.align 16
#===================================

.L_e000:
          .string "0123456789abcdef"
.L_e011:
          .string "id:"
.L_e015:
          .string "class:"
.L_e01c:
          .string "*-+"
.L_e020:
          .string "-->"
.L_e024:
          .string "id"
.L_e027:
          .string "class"
.L_e02d:
          .string "%s=\"%.*s\""
.L_e037:
          .string "=%dx%d"
.L_e03e:
          .byte 0x21
.L_e03f:
          .string "--"
.L_e042:
          .string "style"
.L_e048:
          .string "whitespace"
.L_e053:
          .string "markup"
.L_e05a:
          .string "html"
.L_e05f:
          .string "dl"
.L_e062:
          .string "ul"
.L_e065:
          .string "ol"
.L_e068:
          .string "item"
.L_e06d:
          .string "mystery node!"
.L_e07b:
          .string "hr"
.L_e07e:
          .string "table"
.L_e084:
          .string "source"
.L_e08b:
          .string "--%c"
.L_e090:
          .byte 0x20
.L_e091:
          .byte 0x20
          .byte 0x0
.L_e093:
          .string "%*s%c"
.L_e099:
          .string "[h%d"
.L_e09e:
          .string "[%s"
.L_e0a2:
          .byte 0x20
.L_e0a3:
          .byte 0x25
.L_e0a4:
          .string "s"
.L_e0a6:
          .string "center"
.L_e0ad:
          .byte 0x2c
          .byte 0x20
.L_e0af:
          .string "<%s>"
.L_e0b4:
          .string ", %d line%s"
.L_e0c0:
          .string "]"
          .zero 2
.L_e0c4:
          .long .L_72bd-.L_e0c4
          .long .L_72e2-.L_e0c4
          .long .L_72f4-.L_e0c4
          .long .L_72fd-.L_e0c4
          .long .L_7306-.L_e0c4
          .long .L_730f-.L_e0c4
          .long .L_7318-.L_e0c4
          .long .L_7321-.L_e0c4
          .long .L_732a-.L_e0c4
          .long .L_72eb-.L_e0c4
          .long .L_7333-.L_e0c4
          .long .L_72eb-.L_e0c4
          .long .L_733c-.L_e0c4
          .long .L_7345-.L_e0c4
          .long .L_7351-.L_e0c4
.L_e100:
          .string "&r%cquo;"
.L_e109:
          .string "&l%cquo;"
.L_e112:
          .byte 0x26
          .byte 0x23
          .byte 0x0
.L_e115:
          .string "x%02x;"
.L_e11c:
          .string "%02d;"
.L_e122:
          .byte 0x26
          .byte 0x61
          .byte 0x6d
          .byte 0x70
          .byte 0x3b
          .byte 0x0
.L_e128:
          .byte 0x26
          .byte 0x6c
          .byte 0x74
          .byte 0x3b
          .byte 0x0
.L_e12d:
          .byte 0x25
          .byte 0x32
          .byte 0x32
          .byte 0x0
.L_e131:
          .string "%%%02X"
.L_e138:
          .byte 0x26
          .byte 0x67
          .byte 0x74
          .byte 0x3b
          .byte 0x0
.L_e13d:
          .byte 0x3c
          .byte 0x63
          .byte 0x6f
          .byte 0x64
          .byte 0x65
          .byte 0x3e
          .byte 0x0
.L_e144:
          .byte 0x3c
          .byte 0x2f
          .byte 0x63
          .byte 0x6f
          .byte 0x64
          .byte 0x65
          .byte 0x3e
          .byte 0x0
.L_e14c:
          .byte 0x3c
          .byte 0x64
          .byte 0x65
          .byte 0x6c
          .byte 0x3e
          .byte 0x0
.L_e152:
          .byte 0x3c
          .byte 0x2f
          .byte 0x64
          .byte 0x65
          .byte 0x6c
          .byte 0x3e
          .byte 0x0
.L_e159:
          .byte 0x3c
          .byte 0x61
          .byte 0x20
          .byte 0x68
          .byte 0x72
          .byte 0x65
          .byte 0x66
          .byte 0x3d
          .byte 0x22
          .byte 0x0
.L_e163:
          .byte 0x6d
          .byte 0x61
          .byte 0x69
          .byte 0x6c
          .byte 0x74
          .byte 0x6f
          .byte 0x3a
.L_e16a:
          .zero 1
.L_e16b:
          .byte 0x22
.L_e16c:
          .byte 0x3e
          .byte 0x0
.L_e16e:
          .byte 0x3c
          .byte 0x2f
          .byte 0x61
          .byte 0x3e
          .byte 0x0
.L_e173:
          .string "._-+*"
.L_e179:
          .string "._-+"
.L_e17e:
          .byte 0x3c
          .byte 0x74
          .byte 0x72
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e184:
          .byte 0x3c
          .byte 0x2f
          .byte 0x74
          .byte 0x72
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e18b:
          .string "<%s%s>"
.L_e192:
          .byte 0x3c
          .byte 0x25
          .byte 0x73
          .byte 0x3e
.L_e196:
          .string "</%s>\n"
.L_e19d:
          .string " height=\"%d\""
.L_e1aa:
          .string " width=\"%d\""
.L_e1b6:
          .string "fn"
.L_e1b9:
          .byte 0x26
          .byte 0x6c
          .byte 0x64
          .byte 0x71
          .byte 0x75
          .byte 0x6f
          .byte 0x3b
          .byte 0x0
.L_e1c1:
          .byte 0x26
          .byte 0x72
          .byte 0x64
          .byte 0x71
          .byte 0x75
          .byte 0x6f
          .byte 0x3b
          .byte 0x0
.L_e1c9:
          .byte 0x26
          .byte 0x71
          .byte 0x75
          .byte 0x6f
          .byte 0x74
          .byte 0x3b
          .byte 0x0
.L_e1d0:
          .byte 0x21
          .byte 0x5b
          .byte 0x0
.L_e1d3:
          .byte 0x3c
          .byte 0x73
          .byte 0x75
          .byte 0x70
          .byte 0x3e
          .byte 0x0
.L_e1d9:
          .byte 0x3c
          .byte 0x2f
          .byte 0x73
          .byte 0x75
          .byte 0x70
          .byte 0x3e
          .byte 0x0
.L_e1e0:
          .byte 0x3c
          .byte 0x62
          .byte 0x72
          .byte 0x2f
          .byte 0x3e
          .byte 0x0
.L_e1e6:
          .string "\'\"()[]{}<>`"
.L_e1f2:
          .string "&%s;"
.L_e1f7:
          .string "()"
.L_e1fa:
          .string "MG"
.L_e1fd:
          .string ">#.-+{}]![*_\\()`"
.L_e20e:
          .string "<%s %s>"
.L_e216:
          .byte 0xa
.L_e217:
          .byte 0xa
.L_e218:
          .zero 1
.L_e219:
          .byte 0x3c
          .byte 0x70
          .byte 0x72
          .byte 0x65
          .byte 0x3e
          .byte 0x3c
.L_e21f:
          .string "code"
.L_e224:
          .byte 0x3c
          .byte 0x2f
          .byte 0x63
          .byte 0x6f
          .byte 0x64
          .byte 0x65
          .byte 0x3e
          .byte 0x3c
          .byte 0x2f
          .byte 0x70
          .byte 0x72
          .byte 0x65
          .byte 0x3e
          .byte 0x0
.L_e232:
          .string "div"
.L_e236:
          .byte 0x62
          .byte 0x6c
          .byte 0x6f
          .byte 0x63
          .byte 0x6b
.L_e23b:
          .string "quote"
.L_e241:
          .byte 0x3c
          .byte 0x64
          .byte 0x6c
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e247:
          .byte 0x3c
          .byte 0x64
          .byte 0x74
          .byte 0x3e
          .byte 0x0
.L_e24c:
          .byte 0x3c
          .byte 0x2f
          .byte 0x64
          .byte 0x74
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e253:
          .byte 0x3c
          .byte 0x2f
          .byte 0x64
          .byte 0x6c
          .byte 0x3e
          .byte 0x0
.L_e259:
          .byte 0x3c
          .byte 0x68
          .byte 0x72
          .byte 0x20
          .byte 0x2f
          .byte 0x3e
          .byte 0x0
.L_e260:
          .byte 0x3c
          .byte 0x61
          .byte 0x20
          .byte 0x6e
          .byte 0x61
          .byte 0x6d
          .byte 0x65
          .byte 0x3d
          .byte 0x22
          .byte 0x0
.L_e26a:
          .byte 0x22
          .byte 0x3e
          .byte 0x3c
          .byte 0x2f
          .byte 0x61
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e272:
          .byte 0x3c
          .byte 0x74
          .byte 0x61
          .byte 0x62
          .byte 0x6c
          .byte 0x65
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e27b:
          .byte 0x3c
          .byte 0x74
          .byte 0x68
          .byte 0x65
          .byte 0x61
          .byte 0x64
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e284:
          .byte 0x3c
          .byte 0x2f
          .byte 0x74
          .byte 0x68
          .byte 0x65
          .byte 0x61
          .byte 0x64
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e28e:
          .byte 0x3c
          .byte 0x74
          .byte 0x62
          .byte 0x6f
          .byte 0x64
          .byte 0x79
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e297:
          .byte 0x3c
          .byte 0x2f
          .byte 0x74
          .byte 0x62
          .byte 0x6f
          .byte 0x64
          .byte 0x79
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e2a1:
          .byte 0x3c
          .byte 0x2f
          .byte 0x74
          .byte 0x61
          .byte 0x62
          .byte 0x6c
          .byte 0x65
          .byte 0x3e
          .byte 0xa
          .byte 0x0
.L_e2ab:
          .string "</%s>"
.L_e2b1:
          .string "<%cl"
.L_e2b6:
          .string " type=\"a\""
.L_e2c0:
          .string "li"
.L_e2c3:
          .string "</%cl>\n"
.L_e2cb:
          .string "dd"
.L_e2ce:
          .string "<h%d"
.L_e2d3:
          .string "<h%d>"
.L_e2d9:
          .string "</h%d>"
.L_e2e0:
          .string "th"
.L_e2e3:
          .string "td"
.L_e2e6:
          .byte 0x3c
          .byte 0x6c
          .byte 0x69
          .byte 0x20
          .byte 0x69
          .byte 0x64
          .byte 0x3d
          .byte 0x22
          .byte 0x25
          .byte 0x73
          .byte 0x3a
          .byte 0x25
          .byte 0x64
          .byte 0x22
.L_e2f4:
          .string ">\n"
.L_e2f7:
          .string "</li>\n"
.L_e2fe:
          .string "</ol>\n</div>\n"
.L_e30c:
          .string "</p>"
.L_e311:
          .string "<p>"
.L_e315:
          .byte 0x20
          .byte 0x73
          .byte 0x74
          .byte 0x79
          .byte 0x6c
          .byte 0x65
          .byte 0x3d
          .byte 0x22
          .byte 0x74
          .byte 0x65
          .byte 0x78
          .byte 0x74
          .byte 0x2d
          .byte 0x61
          .byte 0x6c
          .byte 0x69
          .byte 0x67
          .byte 0x6e
          .byte 0x3a
          .byte 0x63
          .byte 0x65
          .byte 0x6e
          .byte 0x74
          .byte 0x65
          .byte 0x72
          .byte 0x3b
.L_e32f:
          .string "\""
.L_e331:
          .string " style=\"text-align:left;\""
.L_e34b:
          .string " style=\"text-align:right;\""
.L_e366:
          .string "\'s|"
.L_e36a:
          .string "rsquo"
.L_e370:
          .string "\'t|"
.L_e374:
          .string "\'re|"
.L_e379:
          .string "\'ll|"
.L_e37e:
          .string "\'ve|"
.L_e383:
          .string "\'m|"
.L_e387:
          .string "\'d|"
.L_e38b:
          .string "---"
.L_e38f:
          .string "mdash"
.L_e395:
          .string "ndash"
.L_e39b:
          .string "..."
.L_e39f:
          .string "hellip"
.L_e3a6:
          .string ". . ."
.L_e3ac:
          .string "(c)"
.L_e3b0:
          .string "copy"
.L_e3b5:
          .string "(r)"
.L_e3b9:
          .string "reg"
.L_e3bd:
          .string "(tm)"
.L_e3c2:
          .string "trade"
.L_e3c8:
          .string "|3/4|"
.L_e3ce:
          .string "frac34"
.L_e3d5:
          .string "|3/4ths|"
.L_e3de:
          .byte 0x7c
          .byte 0x31
          .byte 0x2f
          .byte 0x32
.L_e3e2:
          .string "|"
.L_e3e4:
          .string "frac12"
.L_e3eb:
          .string "|1/4|"
.L_e3f1:
          .string "frac14"
.L_e3f8:
          .string "|1/4th|"
.L_e400:
          .string "&#0;"
.L_e405:
          .byte 0x3c
          .byte 0x73
          .byte 0x70
          .byte 0x61
          .byte 0x6e
.L_e40a:
          .byte 0x20
          .byte 0x69
          .byte 0x64
          .byte 0x3d
          .byte 0x22
          .byte 0x0
.L_e410:
          .string "</span>"
.L_e418:
          .string "raw:"
.L_e41d:
          .string "lang:"
.L_e423:
          .string "<span lang=\""
.L_e430:
          .string "abbr:"
.L_e436:
          .byte 0x3c
          .byte 0x61
          .byte 0x62
          .byte 0x62
          .byte 0x72
.L_e43b:
          .byte 0x20
          .byte 0x74
          .byte 0x69
          .byte 0x74
          .byte 0x6c
          .byte 0x65
          .byte 0x3d
          .byte 0x22
          .byte 0x0
.L_e444:
          .string "</abbr>"
.L_e44c:
          .byte 0x3c
          .byte 0x73
          .byte 0x70
          .byte 0x61
          .byte 0x6e
.L_e451:
          .byte 0x20
          .byte 0x63
          .byte 0x6c
          .byte 0x61
          .byte 0x73
          .byte 0x73
          .byte 0x3d
          .byte 0x22
          .byte 0x0
.L_e45a:
          .string "<img src=\""
.L_e465:
          .string " alt=\""
.L_e46c:
          .string "\" />"
.L_e471:
          .string "https:"
.L_e478:
          .string "http:"
.L_e47e:
          .string "news:"
.L_e484:
          .string "ftp:"
          .zero 7
.L_e490:
          .string "<sup id=\"%sref:%d\"><a href=\"#%s:%d\" rel=\"footnote\">%d</a></sup>"
.L_e4d0:
          .string "\n<div class=\"footnotes\">\n<hr/>\n<ol>\n"
          .zero 3
.L_e4f8:
          .string "<a href=\"#%sref:%d\" rev=\"footnote\">&#8617;</a>"
          .zero 1
.L_e528:
          .string "<p style=\"text-align:center;\">"
          .zero 1
.L_e548:
          .long .L_a383-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_a33b-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_a0b6-.L_e548
          .long .L_9ba6-.L_e548
          .long .L_9b28-.L_e548
          .long .L_a299-.L_e548
          .long .L_9b28-.L_e548
          .long .L_a1ff-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_a1df-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_9b28-.L_e548
          .long .L_a0e2-.L_e548
          .long .L_9b28-.L_e548
          .long .L_a308-.L_e548
.L_e644:
          .long .L_9f70-.L_e644
          .long .L_9f88-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9e59-.L_e644
          .long .L_9d3c-.L_e644
          .long .L_9c3e-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9b28-.L_e644
          .long .L_9d21-.L_e644
.L_e6d4:
          .long .L_ad50-.L_e6d4
          .long .L_b06b-.L_e6d4
          .long .L_af27-.L_e6d4
          .long .L_ada7-.L_e6d4
          .long .L_ae83-.L_e6d4
          .long .L_ad50-.L_e6d4
          .long .L_af56-.L_e6d4
          .long .L_acb2-.L_e6d4
          .long .L_acb2-.L_e6d4
          .long .L_acb2-.L_e6d4
          .long .L_ada7-.L_e6d4
          .long .L_b1bd-.L_e6d4
          .long .L_b481-.L_e6d4
          .long .L_b226-.L_e6d4
          .long .L_b1a3-.L_e6d4
.L_e710:
          .string "</li>\n%*s</ul>\n%*s"
.L_e723:
          .string "%*s<ul>\n"
.L_e72c:
          .string "%*s<li>\n"
.L_e735:
          .string "%*s<li><a href=\"#"
.L_e747:
          .string "&apos;"
          .zero 2
.L_e750:
          .long .L_c708-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c748-.L_e750
          .long .L_c6f8-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c690-.L_e750
          .long .L_c718-.L_e750
          .long .L_c690-.L_e750
          .long .L_c6d0-.L_e750
.L_e7c4:
          .long .L_c8b8-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c908-.L_e7c4
          .long .L_c8a0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c8f0-.L_e7c4
          .long .L_c7e0-.L_e7c4
          .long .L_c890-.L_e7c4
.L_e838:
          .string "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE html  PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n"
.L_e913:
          .string "<head>\n"
.L_e91b:
          .string "<title>"
.L_e923:
          .string "</title>\n"
.L_e92d:
          .string "</head>\n<body>\n"
.L_e93d:
          .string "</body>\n</html>\n"
.L_e94e:
          .string "DL"
.L_e951:
          .string "H1"
.L_e954:
          .string "H2"
.L_e957:
          .string "H3"
.L_e95a:
          .string "H4"
.L_e95d:
          .string "H5"
.L_e960:
          .string "H6"
.L_e963:
          .string "HR"
.L_e966:
          .string "OL"
.L_e969:
          .string "UL"
.L_e96c:
          .string "BDO"
.L_e970:
          .string "DFN"
.L_e974:
          .string "DIV"
.L_e978:
          .byte 0x4d
          .byte 0x41
.L_e97a:
          .string "P"
.L_e97c:
          .string "PRE"
.L_e980:
          .string "WBR"
.L_e984:
          .string "XMP"
.L_e988:
          .string "FORM"
.L_e98d:
          .string "NOBR"
.L_e992:
          .string "TABLE"
.L_e998:
          .string "CENTER"
.L_e99f:
          .string "IFRAME"
.L_e9a6:
          .string "OBJECT"
.L_e9ad:
          .string "ADDRESS"
.L_e9b5:
          .string "LISTING"
.L_e9bd:
          .string "PLAINTEXT"
.L_e9c7:
          .string "BLOCKQUOTE"
.L_e9d2:
          .string "ASIDE"
.L_e9d8:
          .string "FOOTER"
.L_e9df:
          .string "NAV"
.L_e9e3:
          .string "SECTION"
.L_e9eb:
          .string "ARTICLE"
.L_e9f3:
          .string "!LINKS"
.L_e9fa:
          .string " <tr>"
.L_ea00:
          .string "<td>"
.L_ea05:
          .string "</td>"
.L_ea0b:
          .string "<s>"
.L_ea0f:
          .string "</s>"
.L_ea14:
          .string "!IMAGE"
.L_ea1b:
          .string "!PANTS"
.L_ea22:
          .string "!HTML"
.L_ea28:
          .string "STRICT"
.L_ea2f:
          .string "TAGTEXT"
.L_ea37:
          .string "!EXT"
.L_ea3c:
          .string "CDATA"
.L_ea42:
          .byte 0x21
          .byte 0x53
          .byte 0x55
          .byte 0x50
          .byte 0x45
          .byte 0x52
.L_ea48:
          .string "SCRIPT"
.L_ea4f:
          .string "!RELAXED"
.L_ea58:
          .string "!TABLES"
.L_ea60:
          .string "!STRIKETHROUGH"
.L_ea6f:
          .string "TOC"
.L_ea73:
          .string "MKD_1_COMPAT"
.L_ea80:
          .string "AUTOLINK"
.L_ea89:
          .string "SAFELINK"
.L_ea92:
          .byte 0x21
.L_ea93:
          .string "HEADER"
.L_ea9a:
          .string "TABSTOP"
.L_eaa2:
          .string "!DIVQUOTE"
.L_eaac:
          .string "!ALPHALIST"
.L_eab7:
          .string "!DLIST"
.L_eabe:
          .string "FOOTNOTE"
.L_eac7:
          .byte 0x21
.L_eac8:
          .string "STYLE"
.L_eace:
          .string "!DLDISCOUNT"
.L_eada:
          .string "DLEXTRA"
.L_eae2:
          .string "FENCEDCODE"
.L_eaed:
          .string "IDANCHOR"
.L_eaf6:
          .string "GITHUBTAGS"
.L_eb01:
          .string "URLENCODEDANCHOR"
.L_eb12:
          .string "LATEX"
.L_eb18:
          .string "EXPLICITLIST"
          .zero 3
.L_eb28:
          .string "<table class=\"mkd_flags_are\">\n"
#===================================
# end section .rodata
#===================================

#===================================
.section .init_array ,"wa"
.align 8
#===================================

          .quad .L_30f0
#===================================
# end section .init_array
#===================================

#===================================
.section .fini_array ,"wa"
.align 8
#===================================

#===================================
# end section .fini_array
#===================================

#===================================
.section .data.rel.ro ,"wa",@progbits
.align 32
#===================================

.L_116e0:
          .quad .L_e218
          .quad .L_e30c
          .quad .L_e30c
          .zero 8
.L_11700:
          .quad .L_e218
          .quad .L_e311
          .quad .L_e528
          .zero 8
.L_11720:
          .quad .L_e218
          .quad .L_e315
          .quad .L_e331
          .quad .L_e34b
.L_11740:
          .string "\'"
          .zero 6
.L_11748:
          .quad .L_e366
          .quad .L_e36a
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x27
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e370
          .quad .L_e36a
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x27
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e374
          .quad .L_e36a
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x27
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e379
          .quad .L_e36a
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x27
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e37e
          .quad .L_e36a
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x27
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e383
          .quad .L_e36a
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x27
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e387
          .quad .L_e36a
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x2d
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e38b
          .quad .L_e38f
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x2d
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e03f
          .quad .L_e395
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x2e
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e39b
          .quad .L_e39f
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x2e
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e3a6
          .quad .L_e39f
          .byte 0x4
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x28
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e3ac
          .quad .L_e3b0
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x28
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e3b5
          .quad .L_e3b9
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x28
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e3bd
          .quad .L_e3c2
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x33
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e3c8
          .quad .L_e3ce
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x33
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e3d5
          .quad .L_e3ce
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x31
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e3de
          .quad .L_e3e4
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x31
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e3eb
          .quad .L_e3f1
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x31
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e3f8
          .quad .L_e3f1
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x26
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e400
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e9f3
.L_119d0:
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea14
          .byte 0x4
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea1b
          .byte 0x8
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea22
          .byte 0x10
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea28
          .byte 0x20
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea2f
          .byte 0x40
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea37
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea3c
          .byte 0x0
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea42
          .byte 0x0
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea4f
          .byte 0x0
          .byte 0x4
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea58
          .byte 0x0
          .byte 0x8
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea60
          .byte 0x0
          .byte 0x10
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea6f
          .byte 0x0
          .byte 0x20
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea73
          .byte 0x0
          .byte 0x40
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea80
          .byte 0x0
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea89
          .byte 0x0
          .byte 0x0
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea92
          .byte 0x0
          .byte 0x0
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea9a
          .byte 0x0
          .byte 0x0
          .byte 0x4
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eaa2
          .byte 0x0
          .byte 0x0
          .byte 0x8
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eaac
          .byte 0x0
          .byte 0x0
          .byte 0x10
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eab7
          .byte 0x0
          .byte 0x0
          .byte 0x20
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eabe
          .byte 0x0
          .byte 0x0
          .byte 0x40
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eac7
          .byte 0x0
          .byte 0x0
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eace
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eada
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eae2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x4
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eaed
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x8
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eaf6
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x10
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eb01
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x40
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eb12
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eb18
_DYNAMIC:
#===================================
# end section .data.rel.ro
#===================================

#===================================
.data
.align 32
#===================================

.L_12000:
          .quad .L_12000
          .zero 8
.L_12010:
          .quad .L_e03e
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_12020:
          .quad .L_e011
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e405
          .quad .L_e32f
          .zero 8
          .quad .L_e16c
          .quad .L_e410
          .zero 8
          .quad .L_e418
          .byte 0x4
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x8
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e41d
          .byte 0x5
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e423
          .quad .L_e32f
          .zero 8
          .quad .L_e16c
          .quad .L_e410
          .zero 8
          .quad .L_e430
          .byte 0x5
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e436
          .quad .L_e32f
          .zero 8
          .quad .L_e16c
          .quad .L_e444
          .zero 8
          .quad .L_e015
          .byte 0x6
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e44c
          .quad .L_e32f
          .zero 8
          .quad .L_e16c
          .quad .L_e410
          .zero 8
.L_12160:
          .zero 16
          .quad .L_e159
          .quad .L_e32f
          .zero 8
          .quad .L_e16c
          .quad .L_e16e
.L_12198:
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_121a0:
          .zero 16
          .quad .L_e45a
          .quad .L_e32f
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e465
          .quad .L_e46c
          .byte 0x22
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_121e0:
          .quad .L_e471
          .byte 0x6
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e478
          .byte 0x5
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e47e
          .byte 0x5
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e484
          .byte 0x4
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
#-----------------------------------
.align 16
.globl markdown_version
.type markdown_version, @object
#-----------------------------------
markdown_version:
          .string "2.2.7"
          .zero 26
.L_12240:
          .string "<em>"
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x3c
          .byte 0x2f
          .byte 0x65
          .byte 0x6d
          .byte 0x3e
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x5
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x3c
          .byte 0x73
          .byte 0x74
          .byte 0x72
          .byte 0x6f
          .byte 0x6e
          .byte 0x67
          .byte 0x3e
          .byte 0x0
          .byte 0x0
          .byte 0x3c
          .byte 0x2f
          .byte 0x73
          .byte 0x74
          .byte 0x72
          .byte 0x6f
          .byte 0x6e
          .byte 0x67
          .byte 0x3e
          .byte 0x0
          .byte 0x9
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_12270:
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.L_12280:
          .quad .L_e97a
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e94e
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e951
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e954
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e957
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e95a
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e95d
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e960
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e963
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x1
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e966
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e969
          .byte 0x2
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e96c
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e970
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e974
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e978
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e97c
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e980
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e984
          .byte 0x3
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e988
          .byte 0x4
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e98d
          .byte 0x4
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_eac8
          .byte 0x5
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e992
          .byte 0x5
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e998
          .byte 0x6
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e99f
          .byte 0x6
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e9a6
          .byte 0x6
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_ea48
          .byte 0x6
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e9ad
          .byte 0x7
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e9b5
          .byte 0x7
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e9bd
          .byte 0x9
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .quad .L_e9c7
          .byte 0xa
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
#===================================
# end section .data
#===================================

#===================================
.bss
.align 16
#===================================

.L_12460:
          .zero 16
#-----------------------------------
.align 16
.globl extratags
.type extratags, @object
#-----------------------------------
extratags:
          .zero 16
.L_12480:
          .zero 8
#===================================
# end section .bss
#===================================
