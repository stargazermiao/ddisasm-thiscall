#===================================
.intel_syntax noprefix
#===================================

nop
nop
nop
nop
nop
nop
nop
nop

#===================================
.section .interp ,"a",@progbits
.align 1
#===================================

          .byte 0x2f
          .byte 0x6c
          .byte 0x69
          .byte 0x62
          .byte 0x36
          .byte 0x34
          .byte 0x2f
          .byte 0x6c
          .byte 0x64
          .byte 0x2d
          .byte 0x6c
          .byte 0x69
          .byte 0x6e
          .byte 0x75
          .byte 0x78
          .byte 0x2d
          .byte 0x78
          .byte 0x38
          .byte 0x36
          .byte 0x2d
          .byte 0x36
          .byte 0x34
          .byte 0x2e
          .byte 0x73
          .byte 0x6f
          .byte 0x2e
          .byte 0x32
          .byte 0x0
#===================================
# end section .interp
#===================================

#===================================
.text
.align 16
#===================================

          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x66
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
#-----------------------------------
.globl main
.type main, @function
#-----------------------------------
main:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSP
.cfi_def_cfa_register 6
            sub RSP,64
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RBP-8],RAX
            xor EAX,EAX
            lea RAX,QWORD PTR [RBP-48]
            mov EDX,7
            mov ESI,10
            mov RDI,RAX
            call _ZN9RectangleC1Eii

            lea RAX,QWORD PTR [RBP-32]
            mov EDX,5
            mov ESI,10
            mov RDI,RAX
            call _ZN8TriangleC1Eii

            lea RAX,QWORD PTR [RBP-48]
            mov QWORD PTR [RBP-56],RAX
            mov RAX,QWORD PTR [RBP-56]
            mov RAX,QWORD PTR [RAX]
            mov RDX,QWORD PTR [RAX]
            mov RAX,QWORD PTR [RBP-56]
            mov RDI,RAX
            call RDX

            lea RAX,QWORD PTR [RBP-32]
            mov QWORD PTR [RBP-56],RAX
            mov RAX,QWORD PTR [RBP-56]
            mov RAX,QWORD PTR [RAX]
            mov RDX,QWORD PTR [RAX]
            mov RAX,QWORD PTR [RBP-56]
            mov RDI,RAX
            call RDX

            mov EAX,0
            mov RCX,QWORD PTR [RBP-8]
            sub RCX,QWORD PTR FS:[40]
            je .L_120b

            call __stack_chk_fail@PLT
.L_120b:

            leave 
.cfi_def_cfa 7, 8
            ret 
.cfi_endproc 
_Z41__static_initialization_and_destruction_0ii:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSP
.cfi_def_cfa_register 6
            sub RSP,16
            mov DWORD PTR [RBP-4],EDI
            mov DWORD PTR [RBP-8],ESI
            cmp DWORD PTR [RBP-4],1
            jne .L_1253

            cmp DWORD PTR [RBP-8],65535
            jne .L_1253

            lea RDI,QWORD PTR [RIP+_ZStL8__ioinit]
            call _ZNSt8ios_base4InitC1Ev@PLT

            lea RDX,QWORD PTR [RIP+16456]
            lea RSI,QWORD PTR [RIP+_ZStL8__ioinit]
            mov RAX,QWORD PTR [RIP+_ZNSt8ios_base4InitD1Ev@GOTPCREL]
            mov RDI,RAX
            call __cxa_atexit@PLT
.L_1253:

            nop
            leave 
.cfi_def_cfa 7, 8
            ret 
.cfi_endproc 
_GLOBAL__sub_I_main:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSP
.cfi_def_cfa_register 6
            mov ESI,65535
            mov EDI,1
            call _Z41__static_initialization_and_destruction_0ii

            pop RBP
.cfi_def_cfa 7, 8
            ret 
.cfi_endproc 

            nop
#-----------------------------------
.align 4
.weak _ZN5ShapeC2Eii
.type _ZN5ShapeC2Eii, @function
#-----------------------------------
_ZN5ShapeC2Eii:
#-----------------------------------
.align 4
.weak _ZN5ShapeC1Eii
.type _ZN5ShapeC1Eii, @function
#-----------------------------------
_ZN5ShapeC1Eii:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSP
.cfi_def_cfa_register 6
            mov QWORD PTR [RBP-8],RDI
            mov DWORD PTR [RBP-12],ESI
            mov DWORD PTR [RBP-16],EDX
            lea RDX,QWORD PTR [RIP+_ZTV5Shape+16]
            mov RAX,QWORD PTR [RBP-8]
            mov QWORD PTR [RAX],RDX
            mov RAX,QWORD PTR [RBP-8]
            mov EDX,DWORD PTR [RBP-12]
            mov DWORD PTR [RAX+8],EDX
            mov RAX,QWORD PTR [RBP-8]
            mov EDX,DWORD PTR [RBP-16]
            mov DWORD PTR [RAX+12],EDX
            nop
            pop RBP
.cfi_def_cfa 7, 8
            ret 
.cfi_endproc 

            nop
#-----------------------------------
.align 16
.weak _ZN5Shape4areaEv
.type _ZN5Shape4areaEv, @function
#-----------------------------------
_ZN5Shape4areaEv:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSP
.cfi_def_cfa_register 6
            mov QWORD PTR [RBP-8],RDI
            mov EAX,0
            pop RBP
.cfi_def_cfa 7, 8
            ret 
.cfi_endproc 

            nop
#-----------------------------------
.align 16
.weak _ZN9RectangleC2Eii
.type _ZN9RectangleC2Eii, @function
#-----------------------------------
_ZN9RectangleC2Eii:
#-----------------------------------
.align 16
.weak _ZN9RectangleC1Eii
.type _ZN9RectangleC1Eii, @function
#-----------------------------------
_ZN9RectangleC1Eii:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSP
.cfi_def_cfa_register 6
            sub RSP,16
            mov QWORD PTR [RBP-8],RDI
            mov DWORD PTR [RBP-12],ESI
            mov DWORD PTR [RBP-16],EDX
            mov RAX,QWORD PTR [RBP-8]
            mov EDX,DWORD PTR [RBP-16]
            mov ECX,DWORD PTR [RBP-12]
            mov ESI,ECX
            mov RDI,RAX
            call _ZN5ShapeC1Eii

            lea RDX,QWORD PTR [RIP+_ZTV9Rectangle+16]
            mov RAX,QWORD PTR [RBP-8]
            mov QWORD PTR [RAX],RDX
            nop
            leave 
.cfi_def_cfa 7, 8
            ret 
.cfi_endproc 

            nop
#-----------------------------------
.align 8
.weak _ZN9Rectangle4areaEv
.type _ZN9Rectangle4areaEv, @function
#-----------------------------------
_ZN9Rectangle4areaEv:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSP
.cfi_def_cfa_register 6
            sub RSP,16
            mov QWORD PTR [RBP-8],RDI
            lea RSI,QWORD PTR [RIP+.L_2005]
            lea RDI,QWORD PTR [RIP+_ZSt4cout]
            call _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT

            mov RDX,RAX
            mov RAX,QWORD PTR [RIP+_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL]
            mov RSI,RAX
            mov RDI,RDX
            call _ZNSolsEPFRSoS_E@PLT

            mov RAX,QWORD PTR [RBP-8]
            mov EDX,DWORD PTR [RAX+8]
            mov RAX,QWORD PTR [RBP-8]
            mov EAX,DWORD PTR [RAX+12]
            imul EAX,EDX
            leave 
.cfi_def_cfa 7, 8
            ret 
.cfi_endproc 

            nop
#-----------------------------------
.align 16
.weak _ZN8TriangleC2Eii
.type _ZN8TriangleC2Eii, @function
#-----------------------------------
_ZN8TriangleC2Eii:
#-----------------------------------
.align 16
.weak _ZN8TriangleC1Eii
.type _ZN8TriangleC1Eii, @function
#-----------------------------------
_ZN8TriangleC1Eii:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSP
.cfi_def_cfa_register 6
            sub RSP,16
            mov QWORD PTR [RBP-8],RDI
            mov DWORD PTR [RBP-12],ESI
            mov DWORD PTR [RBP-16],EDX
            mov RAX,QWORD PTR [RBP-8]
            mov EDX,DWORD PTR [RBP-16]
            mov ECX,DWORD PTR [RBP-12]
            mov ESI,ECX
            mov RDI,RAX
            call _ZN5ShapeC1Eii

            lea RDX,QWORD PTR [RIP+_ZTV8Triangle+16]
            mov RAX,QWORD PTR [RBP-8]
            mov QWORD PTR [RAX],RDX
            nop
            leave 
.cfi_def_cfa 7, 8
            ret 
.cfi_endproc 

            nop
#-----------------------------------
.align 8
.weak _ZN8Triangle4areaEv
.type _ZN8Triangle4areaEv, @function
#-----------------------------------
_ZN8Triangle4areaEv:

.cfi_startproc 
.cfi_lsda 255
.cfi_personality 255
.cfi_def_cfa 7, 8
.cfi_offset 16, -8
            push RBP
.cfi_def_cfa_offset 16
.cfi_offset 6, -16
            mov RBP,RSP
.cfi_def_cfa_register 6
            sub RSP,16
            mov QWORD PTR [RBP-8],RDI
            lea RSI,QWORD PTR [RIP+.L_201c]
            lea RDI,QWORD PTR [RIP+_ZSt4cout]
            call _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT

            mov RDX,RAX
            mov RAX,QWORD PTR [RIP+_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL]
            mov RSI,RAX
            mov RDI,RDX
            call _ZNSolsEPFRSoS_E@PLT

            mov RAX,QWORD PTR [RBP-8]
            mov EDX,DWORD PTR [RAX+8]
            mov RAX,QWORD PTR [RBP-8]
            mov EAX,DWORD PTR [RAX+12]
            imul EAX,EDX
            mov EDX,EAX
            shr EDX,31
            add EAX,EDX
            sar EAX,1
            leave 
.cfi_def_cfa 7, 8
            ret 
.cfi_endproc 

            nop
            nop
            nop
            nop
            nop
            nop
            nop
            nop
#===================================
# end section .text
#===================================

#===================================
.section .rodata ,"a",@progbits
.align 8
#===================================

          .byte 0x1
          .byte 0x0
          .byte 0x2
          .byte 0x0
_ZStL19piecewise_construct:
          .zero 1
.L_2005:
          .string "Rectangle class area :"
.L_201c:
          .string "Triangle class area :"
          .zero 6
#-----------------------------------
.align 8
.weak _ZTS8Triangle
.type _ZTS8Triangle, @object
#-----------------------------------
_ZTS8Triangle:
          .string "8Triangle"
          .zero 6
#-----------------------------------
.align 8
.weak _ZTS9Rectangle
.type _ZTS9Rectangle, @object
#-----------------------------------
_ZTS9Rectangle:
          .string "9Rectangle"
#-----------------------------------
.weak _ZTS5Shape
.type _ZTS5Shape, @object
#-----------------------------------
_ZTS5Shape:
          .string "5Shape"
#===================================
# end section .rodata
#===================================

#===================================
.section .init_array ,"wa"
.align 8
#===================================

__frame_dummy_init_array_entry:
__init_array_start:
          .quad _GLOBAL__sub_I_main
#===================================
# end section .init_array
#===================================

#===================================
.section .fini_array ,"wa"
.align 8
#===================================

__do_global_dtors_aux_fini_array_entry:
__init_array_end:
#===================================
# end section .fini_array
#===================================

#===================================
.section .data.rel.ro ,"wa",@progbits
.align 8
#===================================

#-----------------------------------
.align 16
.weak _ZTV8Triangle
.type _ZTV8Triangle, @object
#-----------------------------------
_ZTV8Triangle:
          .zero 8
          .quad _ZTI8Triangle
          .quad _ZN8Triangle4areaEv
#-----------------------------------
.align 8
.weak _ZTV9Rectangle
.type _ZTV9Rectangle, @object
#-----------------------------------
_ZTV9Rectangle:
          .zero 8
          .quad _ZTI9Rectangle
          .quad _ZN9Rectangle4areaEv
#-----------------------------------
.align 16
.weak _ZTV5Shape
.type _ZTV5Shape, @object
#-----------------------------------
_ZTV5Shape:
          .zero 8
          .quad _ZTI5Shape
          .quad _ZN5Shape4areaEv
#-----------------------------------
.align 8
.weak _ZTI8Triangle
.type _ZTI8Triangle, @object
#-----------------------------------
_ZTI8Triangle:
          .quad _ZTVN10__cxxabiv120__si_class_type_infoE+16
          .quad _ZTS8Triangle
          .quad _ZTI5Shape
#-----------------------------------
.align 16
.weak _ZTI9Rectangle
.type _ZTI9Rectangle, @object
#-----------------------------------
_ZTI9Rectangle:
          .quad _ZTVN10__cxxabiv120__si_class_type_infoE+16
          .quad _ZTS9Rectangle
          .quad _ZTI5Shape
#-----------------------------------
.align 8
.weak _ZTI5Shape
.type _ZTI5Shape, @object
#-----------------------------------
_ZTI5Shape:
          .quad _ZTVN10__cxxabiv117__class_type_infoE+16
          .quad _ZTS5Shape
_DYNAMIC:
#===================================
# end section .data.rel.ro
#===================================

#===================================
.data
.align 8
#===================================

#-----------------------------------
.align 16
.weak data_start
.type data_start, @notype
#-----------------------------------
data_start:
          .zero 8
          .quad 16456
#===================================
# end section .data
#===================================

#===================================
.bss
.align 64
#===================================

#-----------------------------------
.align 16
.globl _ZSt4cout_copy
.type _ZSt4cout_copy, @object
#-----------------------------------
_ZSt4cout_copy:
          .zero 272
completed.0:
          .zero 1
_ZStL8__ioinit:
          .zero 7
#-----------------------------------
.align 8
.globl _end
.type _end, @notype
#-----------------------------------
_end:
#===================================
# end section .bss
#===================================
# WARNING: integral symbol __abi_tag may not have been correctly relocated
.set __abi_tag, 0x2e8
