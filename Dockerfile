FROM ubuntu:latest

ENV PYTHON_VERSION 3.9.1

RUN apt update && apt -y upgrade

RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata

RUN apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
        libreadline-dev libsqlite3-dev wget ca-certificates curl llvm libncurses5-dev \ 
        xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev \
        mecab-ipadic-utf8 git libboost-all-dev cmake flex bison libzmq3-dev \ 
        autoconf libtool pkg-config libc++-dev libc++abi-dev bundler automake \
        g++ unzip  

# set up grpc
# ENV GRPC_RELEASE_TAG v1.12.x
RUN git clone https://github.com/grpc/grpc /var/local/git/grpc --recurse-submodules && \
		cd /var/local/git/grpc && \
    git submodule update --init && \
    mkdir -p cmake/build && \
    cd cmake/build && \
    cmake ../.. && \
    make && make install

# setup python

ENV PYENV_ROOT /root/.pyenv
ENV PATH $PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH

RUN set -ex \
    && curl https://pyenv.run | bash \
    && pyenv update \
    && pyenv install $PYTHON_VERSION \
    && pyenv global $PYTHON_VERSION \
    && pyenv rehash

RUN cd && git clone https://github.com/GrammaTech/gtirb.git
RUN cd ~/gtirb && git checkout 458fdd30af0 && \
    cmake -Bbuild && cd build && make -j8 && make install

RUN apt install -y mcpp

RUN cd /home && git clone https://github.com/GrammaTech/capstone.git && \
    cd /home/capstone && make && make install

RUN cd ~ && git clone https://github.com/souffle-lang/souffle.git && \
    cd ~/souffle && git checkout 238d4a && \
    ./bootstrap && sh ./configure --enable-64bit-domain && make -j8 && make install

RUN cd ~ && git clone https://github.com/StarGazerM/gtirb-pprinter.git && \
    cd ~/gtirb-pprinter && git checkout nocs-check && \
    cmake -Bbuild && cd build && make -j8 && make install


RUN cd /home && git clone https://git.zephyr-software.com/opensrc/libehp.git && \
    cd /home/libehp && cmake . -Bbuild && cd build && cmake --build . && make install

RUN cd /home && git clone https://github.com/lief-project/LIEF.git && \
    cd /home/LIEF && cmake . -Bbuild && cd build && make -j8 && make install

RUN cd /home && wget https://github.com/lief-project/LIEF/releases/download/0.11.4/LIEF-0.11.4-Linux-x86_64.tar.gz && \
    tar -xvf LIEF-0.11.4-Linux-x86_64.tar.gz

RUN cd /home && git clone https://github.com/StarGazerM/ddisasm.git && \
    cd /home/ddisasm && cmake -Bbuild -DLIEF_ROOT=/home/LIEF-0.11.4-Linux-x86_64 \ 
    && cd build && make -j8 && make install

ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

RUN ldconfig