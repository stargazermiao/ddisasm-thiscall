D3RE: A python ddisasm datalog rules wrapper for RE
---------------------------

#How to use

1. open ghidra, load binary and start `ghidra_bridge`(a ghdira bridge to allow use py3 out of ghidra)
2. `cd tool`
2. `python ddisasm-server.py -I dl/ddisasm_include.dl`
3. `python ddisasm-cli.py -B ../example/cgc/CROMU_00038 -G 1`


Paper link: https://arxiv.org/abs/2101.04718

-----------------

First of all it is **not** a ddisasm tool wrapper, it is just a wraper for reusing rule inside ddisasm. This little program will contain:

NOTE: please compile my forked version of ddisasm, every rule is exposed in there

- make all rule in ddisasm has output

- generate a datalog "interface" for ddisasm so new rule can take everything from a datalog folder as input

- a grpc server can work with different client(using python + grpc + asyncio)

- allow user upload datalog rule and run datalog rules

- query facts and generated output

- upload facts 

- every time a new rule imported reuse result of previous rule

- edit loaded datalog file

- query generated asm


----------------------------

**Problem**

- fix no argument `run` command

- need to handle option part for #ifdef like things

- handle inline rules

- generate GTIRB file

- allow add single datalog rule line

- add relation template decsribption 

**Datalog Plan**

-  extend current def-use analysis into global variable too

-  detect possible uninitialized global varible: this can be a unsound guese, if some global variable is uninitlized but used in a function

-  some other uninitialized case ....(still thinking)

-  detect uninitialized buffer by taint input (my plan is find input by search `getc` `scanf` function ...)


**Example**

- some of my hand written C++

- CGC bench from GrammaTech

- souffle itself


